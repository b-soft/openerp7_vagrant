# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

#from openerp import addons
import logging
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import tools

import datetime

_logger = logging.getLogger(__name__)

ECHELON =  [ 
              ('a','A'),
              ('b','B'),
              ('c','C'),
              ('d','D'),
              ('e','E'),
              ('f','F'),
              ('g','G'),
              ('g1','G1'),
              ('g2','G2'),
              ('g3','G3'),
              ('g6','G6'),
              ('n','None'),

             ]

CATEGORIE =  [ 
              (2,'II'),
              (3,'III'),
              (4,'IV'),
              (5,'V'),
              (6,'VI'),
              (7,'VII'),
              (8,'VIII'),
              (9,'IX'),
              ('0','None'),
             ]

sal_A = [59659, 77569, 105784, 136985, 170634, 180231, 253301, 312474]
sal_B = [63489, 84365, 113548, 144927, 179412, 194263, 268582, 339812]
sal_C = [67379, 91125, 121277, 156745, 192172, 209726, 287422, 367153]
sal_D = [71231, 97911, 129042, 164744, 200983, 228139, 310914, 394493]
sal_E = [75326, 104693, 136772, 172684, 209739, 239245, 322742, 421852]
sal_F = [78955, 111476, 144526, 180628, 218503, 253268, 339234, 449181]
sal_G = [82584, 118259, 152280, 188572, 227267, 267291, 355726, 476510]
sal_G1 = [86213, 125042, 160034, 196516, 236031, 281314, 372218, 503839]
sal_G2 = [89842, 131825, 167788, 204460, 244795, 295337, 388710, 531168]
sal_G3 = [93471, 138608, 175542, 212404, 253559, 309360, 405202, 558497]
sal_G6 = [97100, 145391, 183296, 220348, 262323, 323383, 421694, 640484]


class hr_payslip(osv.osv):
    _inherit = 'hr.payslip'

    def payslip_create(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        employee_line_obj = self.pool.get('hr.employee')
        for obj in self.browse(cr, uid, ids):
	    employees = []
            search_line_ids = employee_line_obj.search(cr, uid, [('local', '=', True), ('pslip_true', '=', True)])
            _logger.info('----SEARCH-----{0}'.format(search_line_ids))
	    if obj.employee_line:
		valu = 0
	#	employees = []
		for employee_line_id in obj.employee_line:
		    if valu == 0:
			valu = employee_line_id
		    employees.append(employee_line_id.id)
            #employee_line_obj.unlink(cr, uid, search_line_ids, context=context)

            vals = {    
                #'name': obj.employee_id.name,
                #'local': True,
                #'cnps': obj.employee_id.cnps,
                #'njours': obj.employee_id.njours,
                #'salaire': obj.employee_id.salaire_brute,
                #'total': obj.employee_id.total,
                #'plafond': obj.employee_id.plafond,
                #'ligne': obj.employee_id.ligne,
                #'irpp': obj.employee_id.irpp,
                #'cent': obj.employee_id.cent,
                #'taxe_com': obj.employee_id.taxe_com,
                #'redevance': obj.employee_id.redevance,
                #'credit_foncier': obj.employee_id.credit_foncier,
                'pslip_line_id': obj.id

                }
            if obj.employee_id.local:
		for line_id in search_line_ids:
		    if line_id not in employees:
                        data = employee_line_obj.copy(cr, uid, line_id, context=context)
                        employee_line_obj.write(cr, uid, valu.id, vals, context=context)
        return True

    def _get_employee_status(self, cr, uid, ids, field_name, arg, context): 
        res = {}
        for obj in self.browse(cr, uid, ids):
            res[obj.id] = obj.employee_id.local
        return res

    _columns = {
	'local': fields.function(_get_employee_status, type='boolean', string=''),
	'employee_line': fields.one2many('hr.employee', 'pslip_line_id', 'Payement'),
	}
hr_payslip()


'''class hr_payslip_run(osv.osv):
    _inherit = 'hr.payslip.run'

    _columns = {
        'employee_line': fields.one2many('hr.employee', 'pslip_run_id', 'Payement'),
        }
hr_payslip_run()'''

 

class hr_employee(osv.osv):
    _inherit = 'hr.employee'

    '''def _return_date1(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        fmt = "%Y-%m-%d" 
        for obj in self.browse(cr, uid, ids):
            try:
                a = datetime.datetime.now().strftime(fmt)
                currentdate = datetime.datetime.strptime(a, fmt)
                b = obj.taxi_deadline
                taxidate = datetime.datetime.strptime(b, fmt)
                _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(taxidate))
                timedelta = taxidate - currentdate
                diff = timedelta.days
                result[obj.id] = diff
            except:
                pass

        return result'''

    def get_remb_mutuel(self, cr, uid, ids, name, args, context=None):
        res = dict.fromkeys(ids, False)
	fmt = "%Y-%m-%d"
        for mutuel_id in self.browse(cr, uid, ids):
            try:
                a = datetime.datetime.now().strftime(fmt)
                currentdate = datetime.datetime.strptime(a, fmt)
                b = mutuel_id.date_mutuel
                mutuel = datetime.datetime.strptime(b, fmt)
                #_logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(taxidate))
                timedelta = currentdate - mutuel
                diff = timedelta.days
		_logger.info("<<<<taxidate>>>---td--{0}<<<{1}>>".format(diff,type(diff)))
		#res[mutuel_id.id] = diff
		if (diff >= 30):
		    val = (mutuel_id.credit_mutuel - mutuel_id.prel_mutuel)
                    res[mutuel_id.id] = val
		    _logger.info("<<<<taxidate>>>---VAL--{0}<<<>>".format(val))
		    if val < 0:
			val = 0
                if (diff >= 60):
                    val = (mutuel_id.credit_mutuel - 2*mutuel_id.prel_mutuel)
                    res[mutuel_id.id] = val
                    _logger.info("<<<<taxidate>>>---VAL--{0}<<<>>".format(val))
                    if val < 0:
                        val = 0

			
            except:
                pass

        return res

    def get_remb_crdt_ret(self, cr, uid, ids, name, args, context=None):
        res = dict.fromkeys(ids, False)
        fmt = "%Y-%m-%d"
        for retraite_id in self.browse(cr, uid, ids):
            try:
                a = datetime.datetime.now().strftime(fmt)
                currentdate = datetime.datetime.strptime(a, fmt)
                b = retraite_id.date_ret
                retraite = datetime.datetime.strptime(b, fmt)
                #_logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(taxidate))
                timedelta = currentdate - retraite
                diff = timedelta.days
                _logger.info("<<<<taxidate>>>---td--{0}<<<{1}>>".format(diff,type(diff)))
                #res[mutuel_id.id] = diff
                if (diff >= 30):
                    val = (retraite_id.credit_ret - retraite_id.prel_ret)
                    res[retraite_id.id] = val
                    _logger.info("<<<<taxidate>>>---VAL--{0}<<<>>".format(val))
                    if val < 0:
                        val = 0
                if (diff >= 60):
                    val = (retraite_id.credit_ret - 2*retraite_id.prel_ret)
                    res[retraite_id.id] = val
                    _logger.info("<<<<taxidate>>>---VAL--{0}<<<>>".format(val))
                    if val < 0:
                        val = 0
            except:
                pass

        return res


    def get_remb_crdt_hab(self, cr, uid, ids, name, args, context=None):
        res = dict.fromkeys(ids, False)
        fmt = "%Y-%m-%d"
        for habitat_id in self.browse(cr, uid, ids):
            try:
                a = datetime.datetime.now().strftime(fmt)
                currentdate = datetime.datetime.strptime(a, fmt)
                b = habitat_id.date_hab
                habitat = datetime.datetime.strptime(b, fmt)
                #_logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(taxidate))
                timedelta = currentdate - habitat
                diff = timedelta.days
                _logger.info("<<<<taxidate>>>---td--{0}<<<{1}>>".format(diff,type(diff)))
                #res[mutuel_id.id] = diff
                if (diff >= 30):
                    val = (habitat_id.credit_hab - habitat_id.prel_hab)
                    res[habitat_id.id] = val
                    _logger.info("<<<<taxidate>>>---VAL--{0}<<<>>".format(val))
                    if val < 0:
                        val = 0
                if (diff >= 60):
                    val = (habitat_id.credit_hab - 2*habitat_id.prel_hab)
                    res[habitat_id.id] = val
                    _logger.info("<<<<taxidate>>>---VAL--{0}<<<>>".format(val))
                    if val < 0:
                        val = 0
            except:
                pass

        return res

    def _get_slip(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        for obj in self.browse(cr, uid, ids):
            if obj.slip_ids:
                for slip in obj.slip_ids:
                    if slip.employee_id.id == obj.id:
                        #if not obj.contract_id.is_foreign:
                        result[obj.id] = slip and slip.id 

        return result

    def _get_slip_true(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        for obj in self.browse(cr, uid, ids):
            if obj.slip_ids:
                for slip in obj.slip_ids:
                    if slip.employee_id.id == obj.id:
                        if not obj.contract_id.is_foreign:
		            val = True
                            result[obj.id] = val
                        if obj.contract_id.is_foreign:
                            val = False
                            result[obj.id] = val

        return result


    def _get_contract(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        for obj in self.browse(cr, uid, ids):
            if obj.contract_id:
                #for slip in obj.slip_ids:
                    #if slip.employee_id.id == obj.id:
                    #if not obj.contract_id.is_foreign:
                result[obj.id] = obj.contract_id and obj.contract_id.id

        return result


    '''def _get_slip_run(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        for obj in self.browse(cr, uid, ids):
            if obj.slip_ids:
                for slip in obj.slip_ids:
                    #if slip.employee_id.id == obj.id:
                    if not obj.contract_id.is_foreign:
                        result[obj.id] = slip.payslip_run_id and slip.payslip_run_id.id

        return result


    def _get_slip_run(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
	try:
            for obj in self.browse(cr, uid, ids):
	        try:
	            if obj.slip_ids:
	                for slip in obj.slip_ids:
		            #if slip.employee_id.id == obj.id:
			    if not obj.contract_id.is_foreign:
                                result[obj.id] = slip.payslip_run_id and slip.payslip_run_id.id

        	        return result
	        except AttributeError:
		    result[obj.id] = False 
		    return result

	except AttributeError: 
	    result[obj.id] = False
	
	return result'''

    def _get_employee_status(self, cr, uid, ids, field_name, arg, context): 
        res = {}
        for obj in self.browse(cr, uid, ids):
            if not obj.contract_id.is_foreign:
                val = True
                res[obj.id] = val
            if obj.contract_id.is_foreign:
            	val = False
                res[obj.id] = val
        return res

    def _get_salary(self, cr, uid, ids, field_name, arg, context): 
        res = {}
        for obj in self.browse(cr, uid, ids):
            val = 0
            if obj.contract_id.is_foreign:
    		res[obj.id] = val
            if not obj.contract_id.is_foreign:
                val = obj.contract_id.salaire
                res[obj.id] = val
        return res
  
    def _get_tdl(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for obj in self.browse(cr, uid, ids):
            val = 0
            if obj.contract_id.is_foreign:
                res[obj.id] = val
            if not obj.contract_id.is_foreign:
                val = obj.contract_id.salaire
		if val > 50000 and val <= 100000:
                    res[obj.id] = 84
                if val > 100000 and val <= 125000:
                    res[obj.id] = 250
                if val > 125000 and val <= 150000:
                    res[obj.id] = 334
                if val > 150000 and val <= 200000:
                    res[obj.id] = 416
                if val > 200000 and val <= 250000:
                    res[obj.id] = 500
                if val > 250000 and val <= 300000:
                    res[obj.id] = 667
                if val > 300000 and val <= 500000:
                    res[obj.id] = 750
                if val > 500000:
                    res[obj.id] = 833
		
        return res

    def _get_rav(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for obj in self.browse(cr, uid, ids):
            val = 0
            if obj.contract_id.is_foreign:
                res[obj.id] = val
            if not obj.contract_id.is_foreign:
                val = obj.contract_id.salaire
                if val > 50000 and val <= 100000:
                    res[obj.id] = 84
                if val > 100000 and val <= 125000:
                    res[obj.id] = 250
                if val > 125000 and val <= 150000:
                    res[obj.id] = 334
                if val > 150000 and val <= 200000:
                    res[obj.id] = 416
                if val > 200000 and val <= 250000:
                    res[obj.id] = 500
                if val > 250000 and val <= 300000:
                    res[obj.id] = 667
                if val > 300000 and val <= 500000:
                    res[obj.id] = 750
                if val > 500000:
                    res[obj.id] = 833

        return res
      
    def _get_scp(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for obj in self.browse(cr, uid, ids):
            val = 0
            if obj.contract_id.is_foreign:
                res[obj.id] = val
            if not obj.contract_id.is_foreign:
                val = obj.salaire_brute
		if val < 300000:
                    res[obj.id] = val
		else:
		    res[obj.id] = 300000
        return res

    def _get_irpp(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for obj in self.browse(cr, uid, ids):
            val = 0
            if obj.contract_id.is_foreign:
                #val = obj.contract_id.wage
                res[obj.id] = val
            if not obj.contract_id.is_foreign:
                val = obj.contract_id.salaire
                res[obj.id] = val
        return res

    _columns = {
        'cnps': fields.char('Immatriculation CNPS', help='Caisse Nationale de la Prevoyance Sociale'),
        'membre_mutuel': fields.boolean('Membre Mutuelle IIA'),
        'complement_com': fields.integer(''),
        'prime_astreinte': fields.integer('Prime Astreinte Exceptionnelle'),
        'enfant_scolarise': fields.integer(''),
        'cotisation_fer': fields.integer(''),
        'prel_mutuel': fields.integer(''),
        'prel_ret': fields.integer(''),
        'prel_hab': fields.integer(''),
        'credit_mutuel': fields.integer(''),	
        'remb_mutuel': fields.function(get_remb_mutuel, type='integer', string =''),
        'credit_ret': fields.integer(''),
        'remb_crdt_ret': fields.function(get_remb_crdt_ret, type='integer', string =''),
        'credit_hab': fields.integer(''),
        'remb_crdt_hab': fields.function(get_remb_crdt_hab, type='integer', string =''),
        'heure_sub_20': fields.integer(''),
        'heure_sub_30': fields.integer(''),
        'heure_sub_40': fields.integer(''),
        'heure_sub_50': fields.integer(''),
        'date_mutuel': fields.date(''),
	'date_ret': fields.date(''),
        'date_hab': fields.date(''),
        'chauffeur': fields.boolean(''),
        'pslip_id': fields.function(_get_slip, type='many2one', relation="hr.payslip", store=True ),
        'pslip_true': fields.function(_get_slip_true, type='boolean'),
        'econtract_id': fields.function(_get_contract, type='many2one', relation="hr.contract", store=True ),
	'pslip_line_id': fields.many2one("hr.payslip", "Paye", select=True),
        #'pslip_run_id': fields.function(_get_slip_run, type='many2one', relation="hr.payslip.run", store=True ),
        'njours': fields.integer('Nombre de Jours'),
        'salaire_brute': fields.function(_get_salary, type='integer', string='Salaire Brute', store=True),
        'total': fields.function(_get_salary, type='integer', string='total', store=True),
        'plafond': fields.function(_get_scp, type='integer', string='SCP', store=True),
        'ligne': fields.integer('Ligne'),
        'irpp': fields.integer('Retenue I.R.P.P.'),
        'cent': fields.integer('CAC.'),
        'taxe_com': fields.function(_get_tdl, type='integer', string='TDL', store=True),
        'redevance': fields.function(_get_rav, type='integer', string='RAV', store=True),
        'credit_foncier': fields.integer('CFC'),
        'local': fields.function(_get_employee_status, type='boolean', string='', store=True),
    }

    _defaults = {
	'membre_mutuel': False,
	'chauffeur': False,
	'njours': lambda *a: 30,
	#'salaire_brute': lambda *a: 0,
	#'total': lambda *a: 0,
        #'plafond': lambda *a: 0,
        #'irpp': lambda *a: 0,
        #'cent': lambda *a: 0,
        #'taxe_com': lambda *a: 0,
        #'redevance': lambda *a: 0,
        #'credit_foncier': lambda *a: 0,
        #'local': False,
	}

hr_employee()


class hr_contract(osv.osv):
    _inherit = 'hr.contract'

    def get_sal(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for sal_id in self.browse(cr, uid, ids):
            try: #if not sal_id.is_foreign:
                if sal_id.categ == 2 and sal_id.echel == 'a':
                    res[sal_id.id] = sal_A[0]
                if sal_id.categ == 3 and sal_id.echel == 'a':
                    res[sal_id.id] = sal_A[1]
                if sal_id.categ == 4 and sal_id.echel == 'a':
                    res[sal_id.id] = sal_A[2]
                if sal_id.categ == 5 and sal_id.echel == 'a':
                    res[sal_id.id] = sal_A[3]
                if sal_id.categ == 6 and sal_id.echel == 'a':
                    res[sal_id.id] = sal_A[4]
                if sal_id.categ == 7 and sal_id.echel == 'a':
                    res[sal_id.id] = sal_A[5]
                if sal_id.categ == 8 and sal_id.echel == 'a':
                    res[sal_id.id] = sal_A[6]
                if sal_id.categ == 9 and sal_id.echel == 'a':
                    res[sal_id.id] = sal_A[7]
                if sal_id.categ == 2 and sal_id.echel == 'b':
                    res[sal_id.id] = sal_B[0]
                if sal_id.categ == 3 and sal_id.echel == 'b':
                    res[sal_id.id] = sal_B[1]
                if sal_id.categ == 4 and sal_id.echel == 'b':
                    res[sal_id.id] = sal_B[2]
                if sal_id.categ == 5 and sal_id.echel == 'b':
                    res[sal_id.id] = sal_B[3]
                if sal_id.categ == 6 and sal_id.echel == 'b':
                    res[sal_id.id] = sal_B[4]
                if sal_id.categ == 7 and sal_id.echel == 'b':
                    res[sal_id.id] = sal_B[5]
                if sal_id.categ == 8 and sal_id.echel == 'b':
                    res[sal_id.id] = sal_B[6]
                if sal_id.categ == 9 and sal_id.echel == 'b':
                    res[sal_id.id] = sal_B[7]
                if sal_id.categ == 2 and sal_id.echel == 'c':
                    res[sal_id.id] = sal_C[0]
                if sal_id.categ == 3 and sal_id.echel == 'c':
                    res[sal_id.id] = sal_C[1]
                if sal_id.categ == 4 and sal_id.echel == 'c':
                    res[sal_id.id] = sal_C[2]
                if sal_id.categ == 5 and sal_id.echel == 'c':
                    res[sal_id.id] = sal_C[3]
                if sal_id.categ == 6 and sal_id.echel == 'c':
                    res[sal_id.id] = sal_C[4]
                if sal_id.categ == 7 and sal_id.echel == 'c':
                    res[sal_id.id] = sal_C[5]
                if sal_id.categ == 8 and sal_id.echel == 'c':
                    res[sal_id.id] = sal_C[6]
                if sal_id.categ == 9 and sal_id.echel == 'c':
                    res[sal_id.id] = sal_C[7]
                if sal_id.categ == 2 and sal_id.echel == 'd':
                    res[sal_id.id] = sal_D[0]
                if sal_id.categ == 3 and sal_id.echel == 'd':
                    res[sal_id.id] = sal_D[1]
                if sal_id.categ == 4 and sal_id.echel == 'd':
                    res[sal_id.id] = sal_D[2]
                if sal_id.categ == 5 and sal_id.echel == 'd':
                    res[sal_id.id] = sal_D[3]
                if sal_id.categ == 6 and sal_id.echel == 'd':
                    res[sal_id.id] = sal_D[4]
                if sal_id.categ == 7 and sal_id.echel == 'd':
                    res[sal_id.id] = sal_D[5]
                if sal_id.categ == 8 and sal_id.echel == 'd':
                    res[sal_id.id] = sal_D[6]
                if sal_id.categ == 9 and sal_id.echel == 'd':
                    res[sal_id.id] = sal_D[7]
                if sal_id.categ == 2 and sal_id.echel == 'e':
                    res[sal_id.id] = sal_E[0]
                if sal_id.categ == 3 and sal_id.echel == 'e':
                    res[sal_id.id] = sal_E[1]
                if sal_id.categ == 4 and sal_id.echel == 'e':
                    res[sal_id.id] = sal_E[2]
                if sal_id.categ == 5 and sal_id.echel == 'e':
                    res[sal_id.id] = sal_E[3]
                if sal_id.categ == 6 and sal_id.echel == 'e':
                    res[sal_id.id] = sal_E[4]
                if sal_id.categ == 7 and sal_id.echel == 'e':
                    res[sal_id.id] = sal_E[5]
                if sal_id.categ == 8 and sal_id.echel == 'e':
                    res[sal_id.id] = sal_E[6]
                if sal_id.categ == 9 and sal_id.echel == 'e':
                    res[sal_id.id] = sal_E[7]
                if sal_id.categ == 2 and sal_id.echel == 'f':
                    res[sal_id.id] = sal_F[0]
                if sal_id.categ == 3 and sal_id.echel == 'f':
                    res[sal_id.id] = sal_F[1]
                if sal_id.categ == 4 and sal_id.echel == 'f':
                    res[sal_id.id] = sal_F[2]
                if sal_id.categ == 5 and sal_id.echel == 'f':
                    res[sal_id.id] = sal_F[3]
                if sal_id.categ == 6 and sal_id.echel == 'f':
                    res[sal_id.id] = sal_F[4]
                if sal_id.categ == 7 and sal_id.echel == 'f':
                    res[sal_id.id] = sal_F[5]
                if sal_id.categ == 8 and sal_id.echel == 'f':
                    res[sal_id.id] = sal_F[6]
                if sal_id.categ == 9 and sal_id.echel == 'f':
                    res[sal_id.id] = sal_F[7]
                if sal_id.categ == 2 and sal_id.echel == 'g':
                    res[sal_id.id] = sal_G[0]
                if sal_id.categ == 3 and sal_id.echel == 'g':
                    res[sal_id.id] = sal_G[1]
                if sal_id.categ == 4 and sal_id.echel == 'g':
                    res[sal_id.id] = sal_G[2]
                if sal_id.categ == 5 and sal_id.echel == 'g':
                    res[sal_id.id] = sal_G[3]
                if sal_id.categ == 6 and sal_id.echel == 'g':
                    res[sal_id.id] = sal_G[4]
                if sal_id.categ == 7 and sal_id.echel == 'g':
                    res[sal_id.id] = sal_G[5]
                if sal_id.categ == 8 and sal_id.echel == 'g':
                    res[sal_id.id] = sal_G[6]
                if sal_id.categ == 9 and sal_id.echel == 'g':
                    res[sal_id.id] = sal_G[7]
                if sal_id.categ == 2 and sal_id.echel == 'g1':
                    res[sal_id.id] = sal_G1[0]
                if sal_id.categ == 3 and sal_id.echel == 'g1':
                    res[sal_id.id] = sal_G1[1]
                if sal_id.categ == 4 and sal_id.echel == 'g1':
                    res[sal_id.id] = sal_G1[2]
                if sal_id.categ == 5 and sal_id.echel == 'g1':
                    res[sal_id.id] = sal_G1[3]
                if sal_id.categ == 6 and sal_id.echel == 'g1':
                    res[sal_id.id] = sal_G1[4]
                if sal_id.categ == 7 and sal_id.echel == 'g1':
                    res[sal_id.id] = sal_G1[5]
                if sal_id.categ == 8 and sal_id.echel == 'g1':
                    res[sal_id.id] = sal_G1[6]
                if sal_id.categ == 9 and sal_id.echel == 'g1':
                    res[sal_id.id] = sal_G1[7]
                if sal_id.categ == 2 and sal_id.echel == 'g2':
                    res[sal_id.id] = sal_G2[0]
                if sal_id.categ == 3 and sal_id.echel == 'g2':
                    res[sal_id.id] = sal_G2[1]
                if sal_id.categ == 4 and sal_id.echel == 'g2':
                    res[sal_id.id] = sal_G2[2]
                if sal_id.categ == 5 and sal_id.echel == 'g2':
                    res[sal_id.id] = sal_G2[3]
                if sal_id.categ == 6 and sal_id.echel == 'g2':
                    res[sal_id.id] = sal_G2[4]
                if sal_id.categ == 7 and sal_id.echel == 'g2':
                    res[sal_id.id] = sal_G2[5]
                if sal_id.categ == 8 and sal_id.echel == 'g2':
                    res[sal_id.id] = sal_G2[6]
                if sal_id.categ == 9 and sal_id.echel == 'g2':
                    res[sal_id.id] = sal_G2[7]
                if sal_id.categ == 2 and sal_id.echel == 'g3':
                    res[sal_id.id] = sal_G3[0]
                if sal_id.categ == 3 and sal_id.echel == 'g3':
                    res[sal_id.id] = sal_G3[1]
                if sal_id.categ == 4 and sal_id.echel == 'g3':
                    res[sal_id.id] = sal_G3[2]
                if sal_id.categ == 5 and sal_id.echel == 'g3':
                    res[sal_id.id] = sal_G3[3]
                if sal_id.categ == 6 and sal_id.echel == 'g3':
                    res[sal_id.id] = sal_G3[4]
                if sal_id.categ == 7 and sal_id.echel == 'g3':
                    res[sal_id.id] = sal_G3[5]
                if sal_id.categ == 8 and sal_id.echel == 'g3':
                    res[sal_id.id] = sal_G3[6]
                if sal_id.categ == 9 and sal_id.echel == 'g3':
                    res[sal_id.id] = sal_G3[7]
                if sal_id.categ == 2 and sal_id.echel == 'g6':
                    res[sal_id.id] = sal_G6[0]
                if sal_id.categ == 3 and sal_id.echel == 'g6':
                    res[sal_id.id] = sal_G6[1]
                if sal_id.categ == 4 and sal_id.echel == 'g6':
                    res[sal_id.id] = sal_G6[2]
                if sal_id.categ == 5 and sal_id.echel == 'g6':
                    res[sal_id.id] = sal_G6[3]
                if sal_id.categ == 6 and sal_id.echel == 'g6':
                    res[sal_id.id] = sal_G6[4]
                if sal_id.categ == 7 and sal_id.echel == 'g6':
                    res[sal_id.id] = sal_G6[5]
                if sal_id.categ == 8 and sal_id.echel == 'g6':
                    res[sal_id.id] = sal_G6[6]
                if sal_id.categ == 9 and sal_id.echel == 'g6':
                    res[sal_id.id] = sal_G6[7]
                if sal_id.categ == 0 and sal_id.echel == 'n':
                    res[sal_id.id] = 0
            except:
		pass
            #logging.info(result)
        return res

    def get_ancienete(self, cr, uid, ids, name, args, context=None):
        res = dict.fromkeys(ids, False)
        fmt = "%Y-%m-%d"
        for ancienete_id in self.browse(cr, uid, ids):
            try:
                a = datetime.datetime.now().strftime(fmt)
                currentdate = datetime.datetime.strptime(a, fmt)
                b = ancienete_id.date_start
                ancienete = datetime.datetime.strptime(b, fmt)
                #_logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(taxidate))
                timedelta = currentdate - ancienete
                diff = timedelta.days
                _logger.info("<<<<taxidate>>>---td--{0}<<<{1}>>".format(diff,type(diff)))
                #res[mutuel_id.id] = diff
                if (diff >= 30*24):
		    if ancienete_id.categ == 2:
			res[ancienete_id.id] = sal_A[0]*0.04
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.04
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.04
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.04
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.04
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.04
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.04
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.04
                    if ancienete_id.is_foreign:
                        res[ancienete_id.id] =ancienete_id.wage*0.04
                    if ancienete_id.is_foreign and ancienete_id.is_director:
                        res[ancienete_id.id] =ancienete_id.wage*0.1

                if (diff >= 30*36):
		    if ancienete_id.categ == 2:  
                        res[ancienete_id.id] = sal_A[0]*0.06
			_logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.06))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.06
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.06
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.06
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.06
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.06
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.06
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.06

                if (diff >= 30*48):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.08
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.08))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.08
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.08
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.08
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.08
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.08
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.08
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.06 

                if (diff >= 30*60):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.1
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.1))
                    if ancienete_id.categ == 3:             
                        res[ancienete_id.id] = sal_A[1]*0.1
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.1
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.1
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.1
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.1
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.1
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.1

                if (diff >= 30*72):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.12
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.12))
                    if ancienete_id.categ == 3:             
                        res[ancienete_id.id] = sal_A[1]*0.12
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.12
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.12
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.12
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.12
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.12
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.12

                if (diff >= 30*84):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.14
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.14))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.14
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.14
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.14
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.14
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.14
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.14
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.14

                if (diff >= 30*96):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.16
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.16))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.16
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.16
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.16
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.16
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.16
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.16
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.16

                if (diff >= 30*108):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.18
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.18))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.18
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.18
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.18
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.18
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.18
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.18
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.18

                if (diff >= 30*120):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.2
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.20))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.2
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.2
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.2
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.2
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.2
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.2
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.2

                if (diff >= 30*132):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.22
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.22))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.22
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.22
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.22
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.22
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.22
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.22
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.22

                if (diff >= 30*144):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.24
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.24))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.24
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.24
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.24
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.24
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.24
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.24
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.24

                if (diff >= 30*156):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.26
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.26))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.26
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.26
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.26
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.26
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.26
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.26
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.26

                if (diff >= 30*168):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.28
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.28))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.28
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.28
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.28
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.28
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.28
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.28
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.28

                if (diff >= 30*180):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.3
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.3))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.3
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.3
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.3
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.3
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.3
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.3
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.3

                if (diff >= 30*192):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.32
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.32))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.32
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.32
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.32
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.32
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.32
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.32
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.32

                if (diff >= 30*204):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.34
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.34))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.34
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.34
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.34
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.34
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.34
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.34
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.34

                if (diff >= 30*216):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.36
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.36))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.36
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.36
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.36
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.36
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.36
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.36
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.36

                if (diff >= 30*228):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.38
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.38))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.38
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.38
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.38
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.38
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.38
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.38
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.38

                if (diff >= 30*240):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.4
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.4))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.4
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.4
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.4
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.4
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.4
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.4
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.4

                if (diff >= 30*252):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.42
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.42))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.42
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.42
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.42
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.42
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.42
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.42
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.42

                if (diff >= 30*264):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.44
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.44))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.44
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.44
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.44
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.44
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.44
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.44
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.44

                if (diff >= 30*276):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.46
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.4))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.46
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.46
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.46
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.46
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.46
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.46
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.46

                if (diff >= 30*288):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.48
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.48))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.48
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.48
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.48
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.48
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.48
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.48
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.48

                if (diff >= 30*300):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.5
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.5))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.5
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.5
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.5
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.5
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.5
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.5
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.5

                if (diff >= 30*312):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.52
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.52))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.52
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.52
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.52
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.52
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.52
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.52
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.52

                if (diff >= 30*324):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.54
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.5))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.54
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.54
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.54
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.54
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.54
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.54
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.54

                if (diff >= 30*336):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.56
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.5))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.56
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.56
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.56
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.56
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.56
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.56
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.56

                if (diff >= 30*348):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.58
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.5))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.58
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.58
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.58
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.58
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.58
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.58
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.58

                if (diff >= 30*360):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.6
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.6))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.6
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.6
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.6
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.6
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.6
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.6
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.6

                if (diff >= 30*372):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.62
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.6))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.62
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.62
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.62
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.62
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.62
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.62
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.62

                if (diff >= 30*384):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.64
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.6))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.64
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.64
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.64
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.64
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.64
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.64
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.64

                if (diff >= 30*396):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.66
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.6))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.66
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.66
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.66
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.66
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.66
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.66
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.66

                if (diff >= 30*408):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.68
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.6))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.68
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.68
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.68
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.68
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.68
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.68
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.68

                if (diff >= 30*420):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.7
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.7))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.7
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.7
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.7
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.7
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.7
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.7
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.7

                if (diff >= 30*432):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.72
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.7))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.72
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.72
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.72
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.72
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.72
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.72
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.72

                if (diff >= 30*444):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.74
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.7))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.74
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.74
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.74
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.74
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.74
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.74
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.74

                if (diff >= 30*456):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.76
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.7))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.76
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.76
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.76
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.76
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.76
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.76
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.76

                if (diff >= 30*468):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.78
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.7))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.78
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.78
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.78
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.78
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.78
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.78
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.78

                if (diff >= 30*480):
                    if ancienete_id.categ == 2:
                        res[ancienete_id.id] = sal_A[0]*0.8
                        _logger.info("<<<<taxidate>>>---td--{0}<<<>>".format(sal_A[0]*0.8))
                    if ancienete_id.categ == 3:
                        res[ancienete_id.id] = sal_A[1]*0.8
                    if ancienete_id.categ == 4:
                        res[ancienete_id.id] = sal_A[2]*0.8
                    if ancienete_id.categ == 5:
                        res[ancienete_id.id] = sal_A[3]*0.8
                    if ancienete_id.categ == 6:
                        res[ancienete_id.id] = sal_A[4]*0.8
                    if ancienete_id.categ == 7:
                        res[ancienete_id.id] = sal_A[5]*0.8
                    if ancienete_id.categ == 8:
                        res[ancienete_id.id] = sal_A[6]*0.8
                    if ancienete_id.categ == 9:
                        res[ancienete_id.id] = sal_A[7]*0.8

            except:
                pass

        return res

    _columns = {
        'echel': fields.selection(ECHELON, string='Echelon'),
        'is_foreign': fields.boolean(''),
        'is_director': fields.boolean(''),
        'categ': fields.selection(CATEGORIE, string='Categorie'),
        'salaire': fields.function(get_sal, type='integer', string ='Salaire de Base', help='Salaire de Base', store=True),
	'wage': fields.integer('Wage', required=True, help="Basic Salary of the employee"),
	'type_id': fields.many2one('hr.contract.type', "Contract Type"),
	'ancienete': fields.function(get_ancienete, type='integer', string =''),
	}

    _defaults = {
	'is_foreign': False,
        'categ': '0',
        'echel': 'n',
	'is_director': False,
	'salaire': lambda *a: 0, 
	}
hr_contract()
