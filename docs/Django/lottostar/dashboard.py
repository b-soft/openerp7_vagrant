from admin_tools.dashboard.modules import LinkList
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from admin_tools.dashboard import Dashboard, AppIndexDashboard
from admin_tools.dashboard import modules

class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for site.

    XXX: This should be extending the Menu class to define custom navigation menus.
    """
    columns = 3
    def __init__(self, **kwargs):
        Dashboard.__init__(self, **kwargs)

        # append an app list module for "Administration"
        self.children.append(modules.AppList(
            _('Content'),
            models=("games.*", 'pages.*', "blog.*", "faq.*"),
        ))

        self.children.append(modules.AppList(
            _('Games'),
            models=('game.*',),
        ))

        self.children.append(modules.AppList(
            _('Promotions'),
            models=('promotions.*',),
        ))

        self.children.append(modules.AppList(
            _('Translations'),
            models=('trans.*',),
        ))

        self.children.append(modules.AppList(
            _('Administration'),
            models=('django.contrib.*', 'account.*',),
        ))
        self.children.append(modules.AppList(
            _('System'),
            models=('system.*',),
        ))

        self.children.append(modules.AppList(
            _('Survey'),
            models=('surveys.*',),
        ))

        # append a recent actions module
        self.children.append(
            modules.RecentActions(_('Recent Actions'), 5)
        )


    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        pass


class CustomAppIndexDashboard(AppIndexDashboard):
    """
    Custom app index dashboard for site.
    """

    # we disable title because its redundant with the model list module
    title = ''

    def __init__(self, *args, **kwargs):
        AppIndexDashboard.__init__(self, *args, **kwargs)

        self.children += [
            modules.ModelList(self.app_title, self.models),
            modules.RecentActions(include_list=self.get_app_content_types()),
            ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        pass