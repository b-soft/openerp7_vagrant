import urllib
import urllib2
from django.contrib.humanize.templatetags.humanize import intcomma
from django.contrib.sites.models import Site
from django.http.response import HttpResponse
from django.template.context import Context
from django.template.loader import get_template
from django.views.generic.base import TemplateView
from apps.game.models import Game, Event
from django.contrib.auth.models import User

class ResultEmailTest(TemplateView):
    template_name = "result_mailer_test.html"
    def get_context_data(self, **kwargs):
        context = super(ResultEmailTest, self).get_context_data(**kwargs)

        site = Site.objects.get_current()
        site_url = 'http://127.0.0.1:8000'

        user = User.objects.get(id=2)
        event = Event.objects.get(id=585)

        amount = round(float(event.jackpot), 2)
        jackpot = "&euro; %s%s" % (intcomma(int(amount)), ("%0.2f" % amount)[-3:])

        winning_phrase = 'The latest <b>%s</b> %s jackpot has been won.' % (jackpot, event.game)

        #load the template
        html_template = get_template('emails/results_email.html')

        numbers_html = '''
            <tr>
            <td style="padding-left: 10px; font-size: 12px; line-height: 30px; border-left: 1px solid #ebebeb;">107</td>
            <td style="padding-left: 10px; font-size: 12px;"><div><span>11-</span><span>39-</span><span>43-</span><span>46-</span><span>75-</span><span>77</span></div></td>
            <td style="padding-left: 10px; font-size: 12px; border-right: 1px solid #ebebeb;">
            <div style="color: #005b8c;">No match</div></td>
         </tr>
         <tr>
            <td style="padding-left: 10px; font-size: 12px; line-height: 30px; border-left: 1px solid #ebebeb;">107</td>
            <td style="padding-left: 10px; font-size: 12px;"><div><span>35-</span><span>51-</span><span><b>55-</b></span><span>68-</span><span>74-</span><span>80</span></div></td>
            <td style="padding-left: 10px; font-size: 12px; border-right: 1px solid #ebebeb;">
            <div style="color: #005b8c;">Matched 1 number</div></td>
            </tr>
            <tr>
            <td style="padding-left: 10px; font-size: 12px; line-height: 30px; border-left: 1px solid #ebebeb;">107</td>
            <td style="padding-left: 10px; font-size: 12px;"><div><span>18-</span><span>45-</span><span><b>47-</b></span><span><b>70-</b></span><span>76-</span><span><b>85</b></span></div></td>
            <td style="padding-left: 10px; font-size: 12px; border-right: 1px solid #ebebeb;">
            <div style="color: #005b8c;">Matched 3 numbers</div></td>
            </tr>
        '''

        #get games next estimated jackpots
        superenalotto_game = Game.objects.get(slug="superenalotto")
        superenalotto = superenalotto_game.next_draw_date()
        jackpot1 = "&euro; %s%s" % (intcomma(int(superenalotto.jackpot)), ("%0.2f" % superenalotto.jackpot)[-3:])

        spanishdaily_game = Game.objects.get(slug="spanish-daily")
        spanishdaily = spanishdaily_game.next_draw_date()
        jackpot2 = "&euro; %s%s" % (intcomma(int(spanishdaily.jackpot)), ("%0.2f" % spanishdaily.jackpot)[-3:])

        euromillions_game = Game.objects.get(slug="euromillions")
        next_euro = euromillions_game.next_draw_date()
        jackpot3 = "&euro; %s%s" % (intcomma(int(next_euro.jackpot)), ("%0.2f" % next_euro.jackpot)[-3:])
        context = Context({
            'game': event.game,
            'user_name': user.get_full_name,
            'site_domain': site_url,
            'winning_phrase': winning_phrase,
            'balls1': event.balls1,
            'balls2': event.balls2,
            'balls3': event.balls3,
            'jackpot1': jackpot1,
            'jackpot2': jackpot2,
            'jackpot3': jackpot3,
            'user_entries': numbers_html
        })

        context['email'] = html_template.render(context)

        return context


def maxmind(request):

    params = urllib.urlencode(dict(i = '41.164.141.98', city='Cape Town', region = 11,
        country = 'South Africa', license_key = 'b0auVAfWoN6i'))
    url = urllib2.urlopen('https://minfraud.maxmind.com/app/ccv2r', params)
    result =  url.read()
    country_match = result.split(';')[1]
    if country_match.split('=')[1] == "Yes":
       print "Yessssssssss"

    exit()

    return HttpResponse('Test')