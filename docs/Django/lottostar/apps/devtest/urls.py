from django.conf.urls.defaults import *
from apps.devtest import views

urlpatterns = patterns('',
    url('^mailer/$', views.ResultEmailTest.as_view(), name='results_mailer'),
    url('^maxmind/$', 'devtest.views.maxmind', name='test_maxmind'),
)
