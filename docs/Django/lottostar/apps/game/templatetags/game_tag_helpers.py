from datetime import datetime
from django import template
from django.contrib.humanize.templatetags.humanize import intcomma
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from game.models import Game, Event
from apps.game.models import GameDrawResult

register = template.Library()

@register.simple_tag
def get_draw_prize(event_id, prize_id):
    try:
        amount = GameDrawResult.objects.get(event_id=event_id, game_prize_id=prize_id).amount
        amount = round(float(amount), 2)
        return "%s%s" % (intcomma(int(amount)), ("%0.2f" % amount)[-3:])
    except:
        return "-"


@register.filter(name='currency')
def currency(amount):
    try:
        amount = round(float(amount), 2)
        return "&euro; %s%s" % (intcomma(int(amount)), ("%0.2f" % amount)[-3:])
    except:
        return ""

@register.filter
def admin_link_game(value, request):
    try:
        game = Game.objects.get(id=request.session["admin_game_id"])
        if game.game != None:
            return mark_safe('<a href="%s">%s</a> &rsaquo; <a href="%s">%s</a>' % (reverse("admin:game_game_change", args=(game.game.id,)),game.game.name, reverse("admin:game_gameproxy_change", args=(game.id,)),game.name))
        else:
            return mark_safe('<a href="%s">%s</a>' % (reverse("admin:game_game_change", args=(game.id,)),game.name))
    except:
        return "No Game Selected"

@register.filter
def admin_label_game(value, request):
    try:
        return Game.objects.get(id=request.session["admin_game_id"]).name
    except:
        return "No Game Selected"

@register.filter
def get_range(value):
    return range(0, value+1)

@register.filter
def get_range_no_zero(value):
    return range(1, value+1)

@register.filter
def set_integer(value):
    return int(value)


@register.filter
def get_query_set(game):
    now = datetime.now()
    return game.event_set.filter(draw_datetime__lte=now, game_status="complete").order_by('-draw_datetime')


@register.simple_tag
def display_winning_numbers(game):

    draw = game.current_draw()

    html = """
           <div class="row-fluid">
           <div class="txt-center">
    """

    for ball in draw.balls1:
        html += '<div class="main-Ball">%s</div>' % ball
    if draw.balls2:
        for ball in draw.balls2:
            html += '<div class="bonus-Ball">%s</div>' % ball

    if draw.balls3:
        for ball in draw.balls3:
            html += '<div class="super-Ball">%s</div>' % ball

    html += "</div></div>"

    return html

@register.simple_tag
def display_game_bonus(balls2, balls3):
    html = """
           <div class="row-fluid">
           <div class="txt-center">
    """

    if balls2:
        if balls2 and not balls3:
            for ball in balls2:
                html += '<div class="super-Ball">%s</div>' % ball
        else:
            if balls2:
                if balls2 and balls3:
                    for ball in balls3:
                        html += '<div class="super-Ball">%s</div>' % ball
    html += "</div></div>"
    return html

@register.simple_tag
def display_game_winning_numbers(balls1, balls2):

    html = """
           <div class="row-fluid">
           <div class="txt-center">
    """

    if balls1:
        for ball in balls1:
            html += '<div class="main-Ball">%s</div>' % ball
    if balls2:
        for ball in balls2:
            html += '<div class="bonus-Ball">%s</div>' % ball

    html += "</div></div>"

    return html
