from django import template
from django.db.models.aggregates import Count
from play.models import Transaction


register = template.Library()

@register.filter
def get_transaction_dates(dates_list, product):
    date_events = []
    unique_date_list = []

    tickets = []
    for entry in dates_list:
        if entry.ticket.product == product and not entry.event.draw_datetime in date_events:
            date_events.append(entry.event.draw_datetime)
            unique_date_list.append(entry)

    list_events = []

    for date_event in unique_date_list:
        tickets=0
        sales=0 #counts non voucher entries
        vouchers=0
        total_value=0

        unique_customers = Transaction.objects.filter(
            ticket__entry__event=date_event.event
        ).values("user_id").annotate(Count("id"))

        total_transactions = []
        for entry in dates_list:
            if entry.ticket.product == product and entry.event == date_event.event:
                tickets += 1

                if entry.price == 0.00:
                    vouchers += 1
                else:
                    sales += 1
                    total_value += entry.price

                if entry.ticket.transaction not in total_transactions:
                    if int(entry.ticket.transaction.transaction_type) == 2:
                        total_transactions.append(entry.ticket.transaction)

        avg_ticket_per_customer='{:.2f}'.format(float(tickets)/len(unique_customers))
        desc = {
            "draw_date": date_event.event.draw_datetime,
            "rollover": date_event.event.rollover,
            "est_jackpot_draw_size": date_event.event.jackpot,
            "tickets": tickets,
            "vouchers": vouchers,
            "cash_sales": sales,
            "trans": len(total_transactions),
            "unique": len(unique_customers),
            "eplpt": avg_ticket_per_customer,
            "eur": total_value
        }
        list_events.append(desc)

    return list_events