from django import template
from django.utils.safestring import mark_safe
from game.models import Game, Product

register = template.Library()

@register.filter
def get_range(value):
    return range(1, value+1)

@register.filter
def split_balls(selection, ballset):
    balls = ("%s" % selection).split("-")
    ball_html = "<div class='btn-group pull-left'> "
    btn_class = ""
    if len(balls) < 2:
        btn_class = "btn-color"
    for ball in balls:
        ball_html += "<div class='ball_box btn %s btn-large active' id='ball_box_%s'>%s</div>" % (btn_class, ballset.id, ball)
    ball_html += "</div>"
    return mark_safe(ball_html)


@register.filter
def get_all_games(game):
    return Game.objects.filter(game=game)


#TODO: FIX THIS
@register.filter
def get_tier(prize):
    html = "Match "
    for ballset in prize.game.gameballset_set.all():
        if prize.prize_ballset.get("%s" % ballset.id, False):
            if int(prize.prize_ballset["%s" % ballset.id]["main"]):
                html += "%s " % int(prize.prize_ballset["%s" % ballset.id]["main"])
            if int(prize.prize_ballset["%s" % ballset.id]["bonus"]):
                if not int(prize.prize_ballset["%s" % ballset.id]["main"]):
                    html += "%s Bonus" % int(prize.prize_ballset["%s" % ballset.id]["bonus"])
                else:
                    html += "&amp; %s Bonus" % int(prize.prize_ballset["%s" % ballset.id]["bonus"])
            return mark_safe(html)
    return ""


@register.filter
def has_addon(game, request):

    product = False
    if request.GET.get("product_id", False):
        product = Product.objects.get(pk=request.GET["product_id"])
    else:
        if "/play/" in ("%s" % request.path):
            slug = ("%s" % request.path).replace("/play/", "").replace("/", "")
            product = Product.objects.get(slug=slug)
        if "/games/entry/delete/" in ("%s" % request.path):
            slug = ("%s" % request.path).split("/")[4]
            product = Product.objects.get(slug=slug)

    for addon in request.session.get("game_addons", []):
        if addon["game"] == game and addon["product"] == product:
            return True
    return False