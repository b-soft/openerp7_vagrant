from django.contrib.sites.models import Site
from django.dispatch.dispatcher import Signal
from django.template.base import Template
from game.models import Event
from play.models.transaction import Entry
from utils.mailer import SendMail
from django.db import transaction, IntegrityError, connection

@transaction.commit_manually
def handle_results_email(result_email_user, result_email, sender, **kwargs):
    result_email_user = result_email_user
    result_email = result_email

    user = result_email_user.user
    game = result_email.game.name

    site = Site.objects.get_current()
    site_url = str(('%s%s') % ('http://', site.domain))

    entries = Entry.objects.filter(ticket__transaction__user=user, event=result_email.event)

    sql = '''

        SELECT
            pt.game_id,
            tt.user_id,
            entry_main_balls.ticket_id,
            entry_main_balls.id AS entry_id,
            entry_main_balls.balls1,
            main_balls,
            bonus_balls
            FROM
            (   SELECT (SELECT ARRAY(select unnest(array%s)
            INTERSECT
            SELECT unnest(balls1) FROM play_entry WHERE id=pe.id)),
                pe.id,
                pe.ticket_id,
                pe.balls1,
                pe.is_processed,
                pe.event_id
            FROM play_entry pe
            )  entry_main_balls (main_balls),
            (   SELECT (SELECT ARRAY(select unnest(array%s)
            INTERSECT
            SELECT unnest(balls2) FROM play_entry WHERE id=pe.id)),
                pe.id,
                pe.ticket_id,
                pe.event_id
            FROM play_entry pe
            )  bonus_main_balls (bonus_balls)
            INNER JOIN play_ticket pt ON pt.id = bonus_main_balls.ticket_id
            INNER JOIN play_transaction tt ON tt.id = pt.transaction_id
            INNER JOIN game_game ga ON ga.id = pt.game_id
            WHERE entry_main_balls.id = bonus_main_balls.id
            AND entry_main_balls.event_id = %d
            AND entry_main_balls.is_processed = True
            AND tt.user_id = %d
            ORDER BY entry_main_balls.id
            LIMIT 2
        '''

    query = sql % (result_email.event.balls1, result_email.event.balls1, result_email.event.id, user.id)

    try :
        cursor = connection.cursor()
        cursor.execute(query)
        transaction.commit()
    except IntegrityError, e:
        transaction.rollback()
        return False



    entries_html = '''
    <table width="530" cellspacing="0" cellpadding="0">
    <thead>
    <tr>
        <td style="background-color: #00b1ff; color: #ffffff; font-size: 14px; font-weight: bold; padding-left: 10px;" width="78" height="25">Draw</td>
        <td style="background-color: #00b1ff; color: #ffffff; font-size: 14px; font-weight: bold; padding-left: 10px;" width="205" height="25">Your Entry</td>
        <td style="background-color: #00b1ff; color: #ffffff; font-size: 14px; font-weight: bold; padding-left: 10px;" width="247" height="25">Matches</td>
    </tr>
    </thead>
    <tbody>
        %s
    </tbody>
    </table>'''

    html_entries = '''<tr>
    <td style="padding-left: 10px; font-size: 12px; line-height: 30px; border-left: 1px solid #ebebeb;">%d</td>
    <td style="padding-left: 10px; font-size: 12px;"><div>%s</div></td>
    <td style="padding-left: 10px; font-size: 12px; border-right: 1px solid #ebebeb;">
    <div style="color: #005b8c;">%s</div></td>
    </tr>
    '''

    numbers_html = ''
    for row in cursor.fetchall():
        html = ''
        for number in row[4]:
            if number in row[5]:
                html += '<span><b>%d</b></span>' % number
            else:
                html += '<span>%d</span>' % number

        numbers_html += html_entries % (result_email.event.id, html, 'Message')


    entries = entries_html % numbers_html



#    if not entries:
#        result_email_user.has_failed = True
#        result_email_user.save()
#        return False

    #game_next_estimate = Event.objects.filter(game=result_email.game, jackpot__gte=1).order_by('-draw_datetime')[:1].get()

#    html_content = entries
#    base_template = "emails/results_email.html"
#    html_content = '{% extends "' + base_template + '" %}{% block content %}' + html_content + '{% endblock %}'
#    print Template(html_content)
#    exit()


#    SendMail(
#        slug="results-email",
#        to=['taffy@symfonysl.com'],
#        subject='Your %s results for %s' % (game, result_email.date),
#        user=user,
#        site=site,
#        site_url=site_url,
#        has_won="been won" if result_email.jackpot_won else "rolled over",
#        jackpot=result_email.jackpot,
#        next_estimate=result_email.next_estimate,
#        winning_numbers=result_email.balls1,
#        game=game,
#        html_entries=entries,
#    ).send("emails/results_email.html")

#
#    result_email_user.has_sent = False
#    result_email_user.save()

    print "Sent to: ", user.email

send_results_email = Signal(providing_args=["user"])
send_results_email.connect(handle_results_email)