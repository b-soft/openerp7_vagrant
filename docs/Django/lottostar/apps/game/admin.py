import calendar, datetime
from django.contrib import admin
from django.contrib.admin import SimpleListFilter
from django.core.urlresolvers import reverse
from django.utils import simplejson
from models import *
from django import forms
from django.forms.models import inlineformset_factory
from django.contrib.admin.util import flatten_fieldsets
from django.utils.functional import curry
from django.utils.translation import ugettext_lazy as _
from flutter_modeladmin import RemoveDeleteAdmin
from reversion import VersionAdmin


class ProductGameAdmin(admin.StackedInline):
    model = ProductGame
    extra = 1
    max_num = 1

class ProductAdmin(RemoveDeleteAdmin, VersionAdmin):
    inlines = [ProductGameAdmin]
    list_display = ["name", 'slug', 'status', "created_at"]
    search_fields = ["name", "description"]
    list_filter = ["status"]
    fieldsets = (
        (None, {'fields': (('name','slug',),'status', 'site')}),
        ("Description", {'fields': ('description',), "classes":('collapse',)}),
    )
    prepopulated_fields = {'slug': ('name',)}
admin.site.register(Product, ProductAdmin)

class ProductGameBoostAdmin(RemoveDeleteAdmin, VersionAdmin):
    list_display = ["name", 'price', 'prefix', 'boost', "product_game", "created_at"]
    list_filter = ["product_game"]
    list_editable = ['prefix', 'price']
admin.site.register(ProductGameBoost, ProductGameBoostAdmin)


class BallsetInlineFormset(forms.models.BaseInlineFormSet):
    def clean(self):
        # get forms that actually have valid data
        count = 0
        for form in self.forms:
            try:
                if form.cleaned_data:
                    count += 1
            except AttributeError:
                # annoyingly, if a subform is invalid Django explicity raises
                # an AttributeError for cleaned_data
                pass
        if count < 1:
            raise forms.ValidationError('You must have at least one ballset')



class GameBallsetAdmin(admin.StackedInline):
    formset = BallsetInlineFormset
    model = Ballset
    extra = 1
    max_num = 6

    def get_formset(self, request, obj=None, **kwargs):
        """Returns a BaseInlineFormSet class for use in admin add/change views."""
        if self.declared_fieldsets:
            fields = flatten_fieldsets(self.declared_fieldsets)
        else:
            fields = None
        if self.exclude is None:
            exclude = []
        else:
            exclude = list(self.exclude)
        exclude.extend(kwargs.get("exclude", []))
        exclude.extend(self.get_readonly_fields(request, obj))
        exclude = exclude or None
        if obj and hasattr(obj, 'id'):
            _extra = 0
        else:
            _extra = self.extra
        defaults = { "form": self.form, "formset": self.formset, "fk_name": self.fk_name, "fields": fields, "exclude": exclude,
                     "formfield_callback": curry(self.formfield_for_dbfield, request=request), "extra": _extra, "max_num": self.max_num, "can_delete": self.can_delete}
        defaults.update(kwargs)
        return inlineformset_factory(self.parent_model, self.model, **defaults)


def game_controls(obj):
    return '<a style="margin:5px" href="%s?e=%s" title="Rules"><img src="/static/img/icons/script.png"/></a>'\
           '<a style="margin:5px" href="%s?e=%s" title="Events"><img src="/static/img/icons/calendar.png"/></a>'\
           '<a style="margin:5px" href="%s?e=%s" title="Prizes"><img src="/static/img/icons/rosette.png"/></a>'\
           '<a style="margin:5px" href="%s?e=%s" title="Games"><img src="/static/img/icons/sport_shuttlecock.png"/></a>'\
           % (reverse("admin:game_eventrule_changelist"), obj.id,
              reverse("admin:game_event_changelist"), obj.id,
              reverse("admin:game_gameprize_changelist"), obj.id,
              reverse("admin:game_gameproxy_changelist"), obj.id)
game_controls.short_description = "Controls"
game_controls.allow_tags = True


class GameAdmin(RemoveDeleteAdmin, VersionAdmin):
    inlines = [GameBallsetAdmin]

    list_display = ['name', "game_type", "name", "active", "status", "multiplier_price", "jackpot", "rollover", "cost", "price", "points", "multiplier_price", game_controls, "created_at"]
    list_filter = ["game_type", "status"]
    prepopulated_fields = {'slug': ('name',)}

    fieldsets = (
        (None, {'fields': (('active', 'name'),'slug',('game_type','status',),)}),
        ("Properties", {'fields': (('multiplier_price'),('cost','price',), ('points','jackpot'), 'rollover',)}),
        ("Description", {'fields': ('description',), "classes":('collapse',)}),
    )

    def queryset(self, request):
        return Game.objects.filter(game=None)
admin.site.register(Game, GameAdmin)

def add_months(sourcedate,months):
    month = sourcedate.month - 1 + months
    year = sourcedate.year + month / 12
    month = month % 12 + 1
    day = min(sourcedate.day,calendar.monthrange(year,month)[1])
    return datetime.date(year,month,day)

class DrawDateListFilter(SimpleListFilter):
    title = 'month'
    parameter_name = 'draw'

    def lookups(self, request, model_admin):
        return (
            ('current', 'Current month'),
            ('next', 'Next month'),
            ('last', 'Last month'),
        )

    def queryset(self, request, queryset):
        now = datetime.datetime.now()
        if self.value() == 'current':
            return queryset.filter(draw_datetime__gte=datetime.date(now.year, now.month, 1),
                draw_datetime__lte=datetime.date(now.year, now.month, calendar.monthrange(now.year, now.month)[1]))
        if self.value() == 'last':
            return queryset.filter(draw_datetime__gte=datetime.date(now.year, now.month - 1, 1),
                draw_datetime__lte=datetime.date(now.year, now.month - 1, 28))
        if self.value() == 'next':
            add_now = add_months(now, 1)
            return queryset.filter(draw_datetime__gte=datetime.date(add_now.year, add_now.month, 1),
                draw_datetime__lte=datetime.date(add_now.year, add_now.month, calendar.monthrange(add_now.year, add_now.month)[1]))


class EventAdmin(RemoveDeleteAdmin, VersionAdmin):
    change_form_template = "admin/games/session_change_form.html"
    change_list_template = "admin/games/session_change_list.html"

    list_filter = ["draw_datetime", DrawDateListFilter]
    list_display = ["draw_datetime", "cutoff", "game_status", "progress", "rollover", "jackpot", "payout", "balls1", "entries", "created_at"]
    readonly_fields = ["game"]

    fieldsets = (
        (None, {'fields': ('game',('rollover','game_status','progress'),('draw_datetime','cutoff',), ('jackpot', 'payout'), ('balls1', 'entries'))}),
    )

    def queryset(self, request):
        return Event.objects.filter(game = request.session.get("admin_game_id", False))

    def save_model(self, request, obj, form, change):
        if not change:
            game_id = request.session.get("admin_game_id", False)
            if game_id:
                obj.game = Game.objects.get(id=game_id)
        return super(EventAdmin, self).save_model(request, obj, form, change)

    def get_changelist(self, request, **kwargs):
        request.session["admin_game_id"] = request.GET.get("e", request.session.get("admin_game_id", False))
        return super(EventAdmin, self).get_changelist(request, **kwargs)

    def get_model_perms(self, request): #hide from admin list
        return {}
admin.site.register(Event, EventAdmin)


class EventRuleAdmin(RemoveDeleteAdmin, VersionAdmin):
    change_form_template = "admin/games/rule_change_form.html"
    change_list_template = "admin/games/session_change_list.html"

    list_display = ["schedule", "cutoff", "params", "created_at"]
    readonly_fields = ['game']
    fieldsets = (
        (None, {'fields': ('game',('schedule','cutoff'),)}),
        )

    def add_view(self, request, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context["params_time"] = "20:30"
        return super(EventRuleAdmin, self).add_view(request, form_url, extra_context=extra_context)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        event = EventRule.objects.get(id=object_id)
        try:
            extra_context["params_date"] = event.params["datetime"]
        except:
            pass
        try:
            extra_context["params_dow"] = event.params["dow"]
        except:
            pass
        try:
            extra_context["params_day"] = int(event.params["day"])
        except:
            pass
        try:
            extra_context["params_time"] = event.params["time"]
        except:
            pass

        return super(EventRuleAdmin, self).change_view(request, object_id, form_url, extra_context=extra_context)


    def queryset(self, request):
        return EventRule.objects.filter(game = request.session.get("admin_game_id", False))

    def save_model(self, request, obj, form, change):
        if not change:
            game_id = request.session.get("admin_game_id", False)
            if game_id:
                obj.game = Game.objects.get(id=game_id)

        if request.method == "POST":
            params = {}
            if form.cleaned_data["schedule"] == "once":
                params["datetime"] = "%sT" % request.POST["params_date"].replace(" ", "Z")
            if form.cleaned_data["schedule"] == "weekly":
                params["dow"] = request.POST["params_dow"]
                params["time"] = request.POST["params_time"]
            if form.cleaned_data["schedule"] == "monthly":
                params["day"] = request.POST["params_day"]
                params["time"] = request.POST["params_time"]
            if form.cleaned_data["schedule"] == "yearly":
                params["datetime"] = "%sT" % request.POST["params_date"].replace(" ", "Z")
            obj.params = simplejson.dumps(params)

        return super(EventRuleAdmin, self).save_model(request, obj, form, change)

    def get_changelist(self, request, **kwargs):
        request.session["admin_game_id"] = request.GET.get("e", request.session.get("admin_game_id", False))
        return super(EventRuleAdmin, self).get_changelist(request, **kwargs)

    def get_model_perms(self, request): #hide from admin list
        return {}
admin.site.register(EventRule, EventRuleAdmin)



class GamePrizeAdmin(RemoveDeleteAdmin, VersionAdmin):
    change_form_template = "admin/games/prize_change_form.html"
    change_list_template = "admin/games/session_change_list.html"

    list_editable = ["tier"]

    list_display = ["prize_type", 'name', "tier", 'main_balls', 'bonus_balls', "value", "created_at"]
    readonly_fields = ["game"]

    fieldsets = (
        (None, {'fields': ('game', ('name', 'tier'), ('value','prize_type') ) }),
    )

    def queryset(self, request):
        return GamePrize.objects.filter(game = request.session.get("admin_game_id", False))


    def save_model(self, request, obj, form, change):

        if not change:
            game_id = request.session.get("admin_game_id", False)
            if game_id:
                obj.game = Game.objects.get(id=game_id)
            ballset_list = Ballset.objects.filter(game=Game.objects.get(id=request.session.get("admin_game_id", False)))\
            .order_by("id")
        else:
            ballset_list = Ballset.objects.filter(game=obj.game).order_by("id")

        if request.method == "POST":
            if ballset_list:
                ballset_main_set = []
                ballset_bonus_set = []

                for ballset in ballset_list:
                    ballset_main = 0; ballset_bonus = 0
                    if request.POST.get("ballset_main_%s" % ballset.id,  False):
                        ballset_main = request.POST["ballset_main_%s" % ballset.id]
                    if request.POST.get("ballset_bonus_%s" % ballset.id,  False):
                        ballset_bonus = request.POST["ballset_bonus_%s" % ballset.id]

                    obj.ballset_id = ballset.id
                    ballset_main_set.append([int(ballset.id), int(ballset_main)])
                    ballset_bonus_set.append([int(ballset.id), int(ballset_bonus)])

                obj.main_balls = ballset_main_set
                obj.bonus_balls = ballset_bonus_set

        return super(GamePrizeAdmin, self).save_model(request, obj, form, change)

    def add_view(self, request, form_url='', extra_context=None):
        extra_context = extra_context or {}
        ballset_list = Ballset.objects.filter(game=Game.objects.get(id=request.session.get("admin_game_id", False)))
        post_ballset = []
        for ballset in ballset_list:
            post_ballset.append({
                "name": ballset.name,
                "id": ballset.id,
                "main": int(ballset.main),
                "bonus": int(ballset.bonus),
                "main_sel": int(request.POST.get("ballset_main_%s" % ballset.id,  False)),
                "bonus_sel": int(request.POST.get("ballset_bonus_%s" % ballset.id,  False))
            })
        ballset_list = post_ballset
        extra_context['ballset_list'] = ballset_list
        return super(GamePrizeAdmin, self).add_view(request, form_url, extra_context=extra_context)


    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        game_prize = GamePrize.objects.get(id=object_id)
        ballset_list = Ballset.objects.filter(game=game_prize.game).order_by("id")

        obj_main_ballset = game_prize.main_balls
        obj_bonus_ballset = game_prize.bonus_balls

        post_ballset = []
        i=0
        for ballset in ballset_list:
            main_sel = int(request.POST.get("ballset_main_%s" % obj_main_ballset[i][0],  obj_main_ballset[i][1]))
            bonus_sel = int(request.POST.get("ballset_bonus_%s" % obj_bonus_ballset[i][0],  obj_bonus_ballset[i][1]))
            post_ballset.append({
                "name": ballset.name,
                "id": obj_main_ballset[i][0],
                "main": ballset.main,
                "bonus": ballset.bonus,
                "main_sel": main_sel,
                "bonus_sel": bonus_sel
            })
            i += 1

        ballset_list = post_ballset
        extra_context['ballset_list'] = ballset_list
        return super(GamePrizeAdmin, self).change_view(request, object_id, form_url, extra_context=extra_context)

    def get_changelist(self, request, **kwargs):
        request.session["admin_game_id"] = request.GET.get("e", request.session.get("admin_game_id", False))
        return super(GamePrizeAdmin, self).get_changelist(request, **kwargs)

    def get_model_perms(self, request): #hide from admin list
        return {}
admin.site.register(GamePrize, GamePrizeAdmin)


def proxy_game_controls(obj):
    return '<a style="margin:5px" href="%s?e=%s" title="Prizes"><img src="/static/img/icons/rosette.png"/></a>'\
           %  (reverse("admin:game_gameprize_changelist"), obj.id)
proxy_game_controls.short_description = "Controls"
proxy_game_controls.allow_tags = True

class GameProxyAdmin(admin.ModelAdmin):
    inlines = [GameBallsetAdmin, ]
    change_form_template = "admin/games/session_change_form.html"
    change_list_template = "admin/games/session_change_list.html"
    object_id = False

    list_display = ['name', "status", "jackpot","exclude_selection", "price", "points", proxy_game_controls, "created_at"]
    list_filter = ["game_type", "multiplier_price", "exclude_selection"]

    fieldsets = (
        (None, {'fields': (('game', 'status',), ('active', 'name'),)}),
        ("Properties", {'fields': (('jackpot', 'price',), ('points',"exclude_selection"),)}),
        ("Description", {'fields': ('description',), "classes":('collapse',)}),
    )

    def get_model_perms(self, request): #hide from admin list
        return {}

    #TODO: Error on editing item - does not save game_id and redirects to /game/ not /gameproxy/
    def save_model(self, request, obj, form, change):
        if not change:
            game_id = request.session.get("admin_game_id", False)
            if game_id:
                obj.game = Game.objects.get(id=game_id)
        else:
            save_obj = self.get_object(request, obj.id)
            obj.game = save_obj.game
        return super(GameProxyAdmin, self).save_model(request, obj, form, change)

    def get_object(self, request, object_id):
        self.object_id = object_id
        return super(GameProxyAdmin, self).get_object(request, object_id)

    def queryset(self, request):
        if self.object_id:
            return Game.objects.filter(id = self.object_id)
        else:
            return Game.objects.filter(game = request.session.get("admin_game_id", False))

    def get_changelist(self, request, **kwargs):
        request.session["admin_game_id"] = request.GET.get("e", request.session.get("admin_game_id", False))
        return super(GameProxyAdmin, self).get_changelist(request, **kwargs)


class GameProxy(Game):
    class Meta:
        proxy = True
        app_label = 'game'
        verbose_name = _('Game')
        verbose_name_plural = _('Games')
admin.site.register(GameProxy, GameProxyAdmin)
