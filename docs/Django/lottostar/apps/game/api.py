from tastypie.resources import ModelResource
from models import Game

class GameResource(ModelResource):
    class Meta:
        queryset = Game.objects.all()
        resource_name = 'game'
