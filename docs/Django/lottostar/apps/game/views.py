import datetime
from django.core.cache import cache
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.views.decorators.csrf import csrf_exempt, requires_csrf_token
from django.views.generic import ListView, DetailView
from django.views.generic.edit import FormMixin
from apps.game.models import Game, GameDrawResult, Event, Product
from play.models import Entry
from settings import CACHE_TIME
from play.forms.game import GameOverviewForm


class FormListView(FormMixin, ListView):
    def get(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        self.form = self.get_form(form_class)

        self.object_list = self.get_queryset()
        context = self.get_context_data(object_list=self.object_list, form=self.form)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)


class GameOverView(FormListView):
    form_class = GameOverviewForm
    model = Game
    template_name = 'admin/games/game_overview.html'

    def get_context_data(self, **kwargs):
        context = super(GameOverView, self).get_context_data(**kwargs)
        game_ids = [] #default game ids

        products = Product.objects.filter(status="active")
        for product in products:
            game_ids.append(product.id)

        games=context['form']['games'].value()
        if games is not None and len(games) > 0:
            game_ids = games

        products = products.filter(id__in=game_ids)

        start_date=datetime.datetime.now()
        from_date=context['form']['from_date'].value()
        if from_date and from_date is not None:
            start_date = datetime.datetime.strptime(from_date, '%d-%m-%Y').strftime('%Y-%m-%d 00:00:00')

        end_date=datetime.datetime.now() + datetime.timedelta(weeks=1)
        to_date=context['form']['to_date'].value()
        if to_date and to_date is not None:
            end_date=datetime.datetime.strptime(to_date, '%d-%m-%Y').strftime('%Y-%m-%d 23:59:59')

        event_list = Event.objects.filter(
            draw_datetime__gte = start_date,
            draw_datetime__lte = end_date,
            game_id__in = game_ids
        ).order_by("draw_datetime")

        context["dates_list"] = Entry.objects.filter(event__in = event_list).select_related("draw_datetime", "ticket", "ticket__product", "ticket__transaction", "event")
        context["product_list"] = products

        return context


class GameResultsList(ListView):
    model = Product
    template_name = "game/results_list.html"
    context_object_name = "product_list"


class GameResultsGameList(DetailView):
    model = Product
    template_name = "game/results_detail.html"

    def get_context_data(self, **kwargs):
        context = super(GameResultsGameList, self).get_context_data(**kwargs)
        game = self.get_object().get_game()
        game_list = Event.objects.filter(game=game, draw_datetime__lte=datetime.datetime.today()).order_by("-draw_datetime")
        context["game_list"] = game_list
        context["lottery_draw"] = game_list[0]
        return context


def draw_result_view(request):
    context = {}
    product = Product.objects.get(slug=request.GET['product_slug'])
    context["lottery_draw"] = Event.objects.get(id=int(request.GET['event_id']))
    context["product"] = product
    context["game"] = product.get_game()
    return render_to_response('game/draw_results.html', context, context_instance=RequestContext(request))
