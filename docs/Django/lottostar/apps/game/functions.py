import datetime
from random import sample
from django.db import transaction
from django.db.models.query import QuerySet
from djorm_expressions.base import SqlExpression
from account.models.profile import UserProfile
from game.models import Event, Ballset, GamePrize
from play.models import Transaction, Ticket, Entry
from play.models.result_email import ResultEmail
from play.models.result_email_user import ResultEmailUser
from system.models.log import Log


class ArrayExpression(object):
    def __init__(self, field):
        self.field = field

    def contains(self, value):
        return SqlExpression(self.field, "@>", value)

    def overlap(self, value):
        return SqlExpression(self.field, "&&", value)


def quick_pick(game):
    z = 0
    entry = ''
    game_ball_set_list = Ballset.objects.filter(game=game).order_by('id')
    entries = []
    for game_ball_set in game_ball_set_list:
        entries_list = sorted(sample(range(game_ball_set.ball_min, game_ball_set.ball_max), game_ball_set.main))
        entries.append(entries_list)
    return entries


def create_game_entries(game):
    try:
        event = Event.objects.filter(game_id__exact=game.id, draw_datetime__gt=datetime.datetime.now())[0]
    except Event.DoesNotExist:
        return False

    profile_list = UserProfile.objects.all()

    for profile in profile_list:
        #save transaction
        transaction = Transaction(
            user_id = profile.user_id,
            transaction_type_id = 1,
            payment_status = "completed",
            value = game.price,
        )
        transaction.save()

        #save ticket
        ticket = Ticket(
            transaction = transaction,
            game = game,
        )
        ticket.save()

        #save entry
        entries = 0
        for i in range(1,10):
            entry_list= quick_pick(game)

            if len(entry_list) == 2:
                main_balls1 = entry_list[0]
                main_balls2 = entry_list[1]
                main_balls3 = []
            else:
                main_balls1 = entry_list[0]
                main_balls2 = []
                main_balls3 = []

            entry = Entry(
                ticket = ticket,
                balls1 = main_balls1,
                balls2 = main_balls2,
                balls3 = main_balls3,
                event = event,
            )
            entry.save()
            entries += 1

        #update transaction amount
        transaction.value=game.price*entries
        transaction.save()

        #log transaction
        message = "Ticket purchase confirmation. Ticket #%d"
        log = Log(user_id=profile.user_id, system_log_type_id=1, description=message, log_vars=[ticket.id])
        log.save()


def create_result_email(event):
    query = Entry.objects.filter(ticket__event=event).all().query
    game_entries = QuerySet(query=query, model=Entry)

    resultemail, created = ResultEmail.objects.get_or_create(game_id=event.game_id,
        event_id=event.id)
    if not resultemail.date:
        resultemail.date = event.draw_datetime

    if not resultemail.balls:
        resultemail.balls = event.balls1

    if not resultemail.jackpot:
        first_tier = event.gamedrawresult_set.all().order_by('id')[0]

        resultemail.jackpot = first_tier.amount
        resultemail.jackpot_won = 1 if first_tier.winners > 0 else 0

    if not resultemail.next_estimate:
        resultemail.next_estimate = event.game.next_draw_date().jackpot

    if not resultemail.next_date:
        print resultemail.next_date
        resultemail.next_date = event.game.next_draw_date().draw_datetime

    resultemail.save()

    print "Create results email"

    for entries in game_entries:
        resultemail, created = ResultEmailUser.objects.get_or_create(user_id=entries.ticket.transaction.user_id,
            event_id=event.id, result_email_id=resultemail.id)

    print "Completed"