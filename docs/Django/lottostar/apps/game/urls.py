from django.conf.urls import *
from apps.game import views

urlpatterns = patterns('',
    url(r'^admin/game/overview/$', views.GameOverView.as_view(), name='admin_game_overview'),
    url('^results/$', views.GameResultsList.as_view(), name='game_results'),
    url('^results/(?P<slug>[a-zA-Z0-9_.-]+)/$', views.GameResultsGameList.as_view(), name='game_results_game'),
    url(r'^game/draw-result/$', views.draw_result_view, name='draw_result'),
)
