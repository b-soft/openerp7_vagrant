import datetime
from django.contrib.sites.models import Site
from django.db import models
from django.utils.translation import ugettext_lazy as _
from djorm_expressions.models import ExpressionManager
from djorm_pgarray.fields import ArrayField
from json_field import JSONField

class Product(models.Model):
    site = models.ManyToManyField(Site, blank=True, null=True)
    STATUS_CHOICES = (
        ("pending",      _("Pending")),
        ("active",       _("Active")),
        ("disabled",     _("Disabled")),
        ("deleted",     _("Deleted")),
    )

    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255)

    status = models.CharField(max_length=255, choices=STATUS_CHOICES)

    cost = models.FloatField(default=0, blank=True) # for reporting
    price = models.FloatField(default=0, blank=True) # for reporting

    description = models.TextField(blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    def __unicode__(self):
        return self.name

    def get_current_winning(self):
        return Event.objects.filter(game=self.get_game(), draw_datetime__lte=datetime.datetime.today()).order_by("-draw_datetime")[:1].get()

    def get_jackpot(self):
        return self.productgame_set.all()[:1].get().game.jackpot

    def get_game(self):
        return self.productgame_set.all()[:1].get().game

    def get_product_game(self):
        return self.productgame_set.all()[:1].get()

    def get_game_price(self):
        return self.productgame_set.all()[:1].get().game.price

    def get_remaining_time(self):
        #s = (self.productgame_set.all()[:1].get().game.next_draw_date().draw_datetime - datetime.datetime.now()).seconds
        s = self.productgame_set.all()[:1].get().game.next_draw_date().draw_datetime - datetime.datetime.now()
        days, remainder = divmod(s.total_seconds(), 86400)
        hours, remainder = divmod(remainder, 3600)
        minutes, seconds = divmod(remainder, 60)
        return '%02d:%02d:%02d' % (days, hours, minutes)

    class Meta:
        verbose_name = "Product"
        verbose_name_plural = "Products"


class Game(models.Model):
    GAME_TYPE_CHOICES = (
        ("lottery", _("Lottery")),
    )
    STATUS_CHOICES = (
        ("pending",      _("Pending")),
        ("active",       _("Active")),
        ("disabled",     _("Disabled")),
    )

    game = models.ForeignKey("self", related_name="games", blank=True, null=True)

    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, blank=True, null=True)

    active = models.BooleanField(default=True, blank=True)
    status = models.CharField(max_length=255, choices=STATUS_CHOICES)
    game_type = models.CharField(max_length=255, choices=GAME_TYPE_CHOICES, default="lottery") # exclude from admin

    exclude_selection = models.BooleanField(default=False, blank=True)

    description = models.TextField(blank=True, null=True)

    jackpot = models.FloatField(default=0)
    rollover = models.IntegerField(default=0, null=True, blank=True)

    multiplier_price = models.FloatField(default=0, null=True, blank=True)

    cost = models.FloatField(default=0)
    price = models.FloatField(default=0, null=True, blank=True)
    points = models.FloatField(default=0, null=True, blank=True) #todo: user has points

    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    def __unicode__(self):
        return self.name

    def get_events(self):
        return Event.objects.filter(game=self, draw_datetime__gte=datetime.datetime.today()).order_by("draw_datetime")

    def current_draw(self):
        try:
            return Event.objects.filter(game_id=self.id, draw_datetime__lte=datetime.datetime.now(), game_status="complete").order_by('-draw_datetime')[0]
        except :
            return None

    def next_draw_date(self):
        return Event.objects.filter(draw_datetime__gt=datetime.datetime.now(), game_id=self.id).order_by("draw_datetime")[0]

    #get last draw date

    class Meta:
        verbose_name = "Game"
        verbose_name_plural = "Games"
        ordering = ["-name"]


class Event(models.Model):
    GAME_STATUS_CHOICES = (
        ("open",        _("Open")),
        ("closed",      _("Closed")),
        ("cancelled",   _("Cancelled")),
        ("paused",      _("Paused")),
        ("complete",    _("Complete")),
    )
    PROGRESS_CHOICES = (
        ("pending",        _("Pending")),
        ("results",        _("Results")),
        ("winnings",       _("Winnings")),
        ("notification",   _("Notification")),
        ("complete",       _("Complete")),
    )
    game = models.ForeignKey(Game)

    game_status = models.CharField(max_length=255, choices=GAME_STATUS_CHOICES)
    progress = models.CharField(max_length=255, choices=PROGRESS_CHOICES)

    draw_datetime = models.DateTimeField()
    cutoff = models.DateTimeField()

    rollover = models.IntegerField(default=0, null=True, blank=True)

    has_event_result_entry = models.BooleanField(default=False, blank=True)
    has_event_result_winner = models.BooleanField(default=False, blank=True)

    processed_results = models.BooleanField(default=False, blank=True)
    processed_winnings = models.BooleanField(default=False, blank=True)

    jackpot = models.FloatField(default=0)
    payout = models.FloatField(default=0)

    balls1 = ArrayField(dbtype="int", null=True, blank=True)
    balls2 = ArrayField(dbtype="int", null=True, blank=True)
    balls3 = ArrayField(dbtype="int", null=True, blank=True)

    entries = models.IntegerField(max_length=11) # for reporting

    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    objects = ExpressionManager()

    def __unicode__(self):
        return self.draw_datetime.strftime("%d/%m/%Y")

    class Meta:
        verbose_name = "Event"
        verbose_name_plural = "Events"

class Ballset(models.Model):
    game = models.ForeignKey(Game)
    name = models.CharField(max_length=255)
    main = models.IntegerField()
    bonus = models.IntegerField()
    ball_min = models.IntegerField()
    ball_max = models.IntegerField()
    wheeling_limit = models.IntegerField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    class Meta:
        verbose_name = "Ballset"
        verbose_name_plural = "Ballsets"


class GamePrize(models.Model):
    PRIZE_TYPE_CHOICES = (
        ("jackpot",     _("Jackpot")),
        ("cash",        _("Cash")),
        ("credit",      _("Credit")),
        ("voucher",     _("Voucher")),
        ("multiplier",  _("Multiplier")),
        ("discount",    _("Discount")),
    )

    game = models.ForeignKey(Game)
    prize_type = models.CharField(max_length=255, choices=PRIZE_TYPE_CHOICES, default="cash") #exclude from admin
    tier = models.IntegerField(max_length=3)

    name = models.CharField(max_length=255)
    value = models.FloatField(default=0)

    main_balls = ArrayField(dbtype="int", null=True, blank=True)
    bonus_balls = ArrayField(dbtype="int", null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "Prize"
        verbose_name_plural = "Prizes"
        ordering = ["tier"]


class EventPrize(models.Model):
    PRIZE_TYPE_CHOICES = (
        ("jackpot",     _("Jackpot")),
        ("cash",        _("Cash")),
        ("credit",      _("Credit")),
        ("voucher",     _("Voucher")),
        ("multiplier",  _("Multiplier")),
        ("discount",    _("Discount")),
    )
    event = models.ForeignKey(Event)
    prize_type = models.CharField(max_length=255, choices=PRIZE_TYPE_CHOICES)
    tier = models.IntegerField(max_length=3)
    name = models.CharField(max_length=255)
    value = models.FloatField(default=0)

    winners = models.IntegerField(null=True, blank=True)
    matching_ballset = JSONField(blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["tier"]
        verbose_name = "Prize"
        verbose_name_plural = "Prizes"



class EventRule(models.Model):
    SCHEDULE_CHOICES = (
        ("once",    _("Once")),
        ("weekly",  _("Weekly")),
        ("monthly", _("Monthly")),
        ("yearly",  _("Yearly")),
    )

    game = models.ForeignKey(Game)
    schedule = models.CharField(max_length=255, choices=SCHEDULE_CHOICES)
    params = JSONField(blank=True, null=True)
    cutoff = models.IntegerField(max_length=3)

    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    class Meta:
        verbose_name = "Event Rule"
        verbose_name_plural = "Event Rules"

    def __unicode__(self):
        return "%s %s"  % (self.game.name, self.schedule)

#*
class ProductGame(models.Model):
    product = models.ForeignKey(Product)
    game = models.ForeignKey(Game)
    discount = models.FloatField(default=0, blank=True)
    allow_addons = models.BooleanField(default=True, blank=True)
    allow_addons_discount = models.BooleanField(default=True, blank=True)

    consecutive = models.BooleanField(default=False, blank=True)
    entries = models.IntegerField(default=0, blank=True)

    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    def __unicode__(self):
        return "%s - %s" % (self.product.name , self.product.name)


class ProductGameBoost(models.Model):
    product_game = models.ForeignKey(ProductGame)

    name = models.CharField(max_length=50)
    price = models.DecimalField(max_digits=20, decimal_places=2)
    prefix = models.IntegerField(max_length=6)
    boost = models.DecimalField(max_digits=20, decimal_places=2)

    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    def __unicode__(self):
        return self.name


class GameDrawResult(models.Model):
    event = models.ForeignKey(Event)

    #todo: requires game or product game
    game_prize = models.ForeignKey(GamePrize)

    winners = models.IntegerField()
    amount = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)

    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return "%s | %s" % (self.winners, self.amount)
