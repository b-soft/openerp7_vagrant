import re
import datetime
from django import forms
from game.models import Product, Ballset, Event
from play.functions import get_dow

class BaseGameForm(forms.Form):
    product = forms.IntegerField()
    edit_id = forms.IntegerField(required=False)

    def clean_product(self):
        product = self.cleaned_data['product']
        if not Product.objects.filter(pk=product).count():
            raise forms.ValidationError("No Product Found")
        return product

class ToggleForm(BaseGameForm):
    ballset = forms.IntegerField()
    ticket = forms.IntegerField()
    number = forms.IntegerField()
    is_ticked = forms.BooleanField(required=False,initial=False)
    limit = forms.IntegerField(required=False)

    def clean_ballset(self):
        ballset = self.cleaned_data['ballset']
        if not Ballset.objects.filter(pk=ballset).count():
            raise forms.ValidationError("No Ballset Found")
        return ballset

class BoostForm(BaseGameForm):
    boost = forms.IntegerField()

class AddonForm(BaseGameForm):
    ticket = forms.IntegerField()
    number = forms.IntegerField()

class TicketClearForm(BaseGameForm):
    ticket = forms.IntegerField()

class NumbersForm(BaseGameForm):
    ticket = forms.IntegerField()
    numbers = forms.CharField()

    def clean_numbers(self):
        numbers = self.cleaned_data['numbers']
        if not re.match("^[0-9_-]*$", numbers):
            raise forms.ValidationError("numbers are invalid")
        return numbers


class PageForm(BaseGameForm):
    page = forms.IntegerField()

class DrawForm(BaseGameForm):
    start = forms.IntegerField()
    duration = forms.IntegerField()
    days = forms.CharField()

    def clean_days(self):
        days = self.cleaned_data["days"]
        if days == "":
            days = get_dow(self.cleaned_data["product"])
        else:
            if not re.match("^[a-z-]*$", days):
                raise forms.ValidationError("please exclude numbers from days")
        return days

    def clean_start(self):
        start = self.cleaned_data['start']
        if not Event.objects.filter(pk=start, draw_datetime__gte=datetime.datetime.today()).count():
            raise forms.ValidationError("event has exceeded over time")
        return start
