from datetime import datetime
import json
from BeautifulSoup import BeautifulSoup
import urllib2
import django
from django.core.management.base import AppCommand, NoArgsCommand
from django.utils.html import strip_tags
from itertools import islice
import re
from apps.game.models import Game, GameDrawResult, Event

def take(n, iterable):
    return list(islice(iterable, n))

class Command(NoArgsCommand):

    def handle_noargs(self, **options):
        try:
            game = Game.objects.get(slug="euromillions", active=True)
        except Game.DoesNotExist:
            game = False

        if game:
            try:
                req = urllib2.Request('https://lottery.ie/prizes-and-results/?game=euromillions')
                req.add_header('User-agent', 'Mozilla 5.10')
                res = urllib2.urlopen(req)
                html = res.read()
            except urllib2.HTTPError:
                html = None

            if html is not None:
                current_draw = Event.objects.filter(game_id__exact=game.id,
                    draw_datetime__lte=datetime.now()).order_by("-draw_datetime")[0]

                next_draw = Event.objects.filter(game_id__exact=game.id,
                    draw_datetime__gte=datetime.now()).order_by("draw_datetime")[0]

                #get ballsets for main game
                game_winning_combinations = game.gameprize_set.all()
                game_ballsets = game.ballset_set.all()

                soup = BeautifulSoup(html)

                if not current_draw.has_event_result_entry:
                    date_soup = soup.find('h2', {'class': 'date-heading LotteryRegular'})
                    draw_date = django.utils.html.strip_tags(date_soup)
                    current_draw_date = current_draw.draw_datetime.strftime("%d %B %Y")

                    if draw_date.strip() == current_draw_date:
                        print "Getting lottery winning numbers"
                        #Getting the main winning numbers
                        numbers_soup = soup.findAll("ul", {"class": "euromillions-winning-numbers LotteryBold"})
                        main_numbers = []
                        for li in numbers_soup[0].findAll("li"):
                            main_numbers.append(int(django.utils.html.strip_tags(li).strip()))

                        #Getting the bonus winning numbers
                        bonus_soup = soup.findAll("ul", {"class": "euromillions-bonus LotteryBold"})
                        bonus_numbers = []
                        for li in bonus_soup[0].findAll("li"):
                            bonus_numbers.append(int(django.utils.html.strip_tags(li).strip()))

                        #Get a count of main and bonus balls
                        if len(game_ballsets) == 1:
                            main_winning_entries = game_ballsets[0].main
                            bonus_winning_entries = game_ballsets[0].bonus
                        else:
                            bonus_winning_entries = game_ballsets[0].main
                            main_winning_entries = game_ballsets[1].main

                        if len(main_numbers) != main_winning_entries:
                            print "Main ball mismatch"
                            return False

                            #For bonus numbers
                        if len(bonus_numbers) != bonus_winning_entries:
                            print "Bonus ball mismatch"
                            return False

                        if len(main_numbers) == main_winning_entries and len(bonus_numbers) == bonus_winning_entries:
                            #save the entries
                            print "Winning numbers: %s %s" % (main_numbers, bonus_numbers)
                            current_draw.balls1 = main_numbers
                            current_draw.balls2 = bonus_numbers
                            current_draw.has_event_result_entry = True
                            current_draw.save()

                if current_draw.has_event_result_entry and not current_draw.has_event_result_winner:
                    print "Getting lottery winnings"
                    main_game_prize = []
                    main_game_winners = []

                    count = 0
                    for winnings in soup.find("table", {"class": "prize-breakdown", "summary":"Prize Breakdown"}).findAll("tr")[1:]:
                        winners_count =  strip_tags(winnings.contents[3]).strip().replace(",", "")
                        cleaned_prize =  strip_tags(winnings.contents[7]).strip().replace("&euro;", "").replace(",", "")
                        main_game_winners.append(winners_count)
                        main_game_prize.append(cleaned_prize)
                        count += 1

                    #save main game winnings
                    if len(main_game_prize) == len(game_winning_combinations):
                        for key, prize in enumerate(main_game_prize):
                            if key is 0:
                                if main_game_prize[0] == '0.00' or main_game_prize[0] == '0.0' or main_game_prize[0] == 0:
                                    payout_amount = current_draw.jackpot
                                    next_draw.rollover += 1
                                else:
                                    payout_amount = main_game_prize[0]
                                    next_draw.rollover = 0
                            else:
                                payout_amount = main_game_prize[key]

                            game_draw_result = GameDrawResult(
                                event = current_draw,
                                game_prize = game_winning_combinations[key],
                                winners = int(main_game_winners[key]),
                                amount = payout_amount
                            )
                            game_draw_result.save()

                        current_draw.has_event_result_winner = True
                        current_draw.game_status = "complete"
                        current_draw.save()

                #get the next estimate jackpot
                if next_draw.jackpot < 1 and current_draw.has_event_result_entry and current_draw.has_event_result_winner:
                    try:
                        req = urllib2.Request('http://www.euromillions.be/FR/Jouer_et_Gagner/Jeux_de_tirage/euromillions/default.aspx')
                        req.add_header('User-agent', 'Mozilla 5.10')
                        res = urllib2.urlopen(req)
                        html = res.read()
                    except urllib2.HTTPError:
                        html = None

                    if html is not None:
                        soup = BeautifulSoup(html)
                        print "Getting the next estimated jackpot"
                        next_jackpot_soup = soup.find("div", {"class": "contentMinBorder"}).findAll("div")[15]

                        text_re = re.compile('([,0-9.]+)')
                        text = text_re.findall(str(next_jackpot_soup))
                        cleaned_jackpot = strip_tags(text[3]).strip().replace('.','')

                        if cleaned_jackpot > 1 and cleaned_jackpot != 'None':
                            print "Jackpot amount (%s)" % cleaned_jackpot
                            next_draw.jackpot = cleaned_jackpot
                            game.jackpot = cleaned_jackpot
                            current_draw.processed_results = True

                            current_draw.save()
                            game.save()
                            next_draw.save()

            print "Scrape completed"