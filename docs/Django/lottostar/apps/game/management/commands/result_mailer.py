from django.core.mail.message import EmailMultiAlternatives
from django.core.management.base import NoArgsCommand
from django.db import transaction
from play.models import SystemEmail



@transaction.commit_manually
class Command(NoArgsCommand):
    def handle(self, *args, **options):
        print "Sending emails"

        result_emails = SystemEmail.objects.filter(is_sent=False)

        for result_email in result_emails:
            text_content = result_email.body_text
            html_content = result_email.body_html
            msg = EmailMultiAlternatives(result_email.subject, text_content, result_email.from_address[0], result_email.to_address)
            msg.attach_alternative(html_content, "text/html")
            msg.send()

            result_email.is_sent = True
            result_email.save()
