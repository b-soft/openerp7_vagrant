import datetime
from django.core.management.base import NoArgsCommand
from django.core.management.base import AppCommand
from game.models import Event, Game

NO_LAUNCH = None
#==============================================================================

def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + datetime.timedelta(n)


class Command(NoArgsCommand):
    option_list = AppCommand.option_list

    def handle_noargs(self, **options):

        """ A command to execute """

        for game in Game.objects.filter(game=None):
            print "GAME: %s" % game.name
            for rule in game.eventrule_set.all():
                from_date = datetime.datetime.now()
                from_date_week = datetime.datetime.now() + datetime.timedelta(days=7)

                if rule.schedule == "once":
                    input_date = datetime.datetime.strptime(rule.params["datetime"], "%Y-%m-%dZ%H:%MT")
                    Event.objects.get_or_create(
                        game = game,
                        draw_datetime = input_date,
                        defaults = {
                            "game_status": "open",
                            "progress": "pending",
                            "cutoff": input_date - datetime.timedelta(hours=rule.cutoff),
                            "rollover": game.rollover,
                            "jackpot": game.jackpot,
                            "payout": 0,
                            "entries":0
                        }
                    )

                if rule.schedule == "weekly":
                    for item_date in daterange(from_date, from_date_week):
                        if item_date.strftime("%A").lower() == rule.params["dow"]: # get the date of the 'day of the week'
                            for date_range in daterange(item_date, item_date + datetime.timedelta(days=365)):
                                if date_range.strftime("%A").lower() == rule.params["dow"]: # every day of the week for a year
                                    string_date = "%s %s" % (date_range.strftime("%Y-%m-%d"), rule.params["time"])
                                    input_date = datetime.datetime.strptime(string_date, "%Y-%m-%d %H:%M")
                                    Event.objects.get_or_create(
                                        game = game,
                                        draw_datetime = input_date,
                                        defaults = {
                                            "game_status": "open",
                                            "progress": "pending",
                                            "cutoff": input_date - datetime.timedelta(hours=rule.cutoff),
                                            "rollover": game.rollover,
                                            "jackpot": game.jackpot,
                                            "payout": 0,
                                            "entries":0,
                                            'draw_datetime': input_date
                                        }
                                    )


                if rule.schedule == "monthly":
                    pass

                if rule.schedule == "yearly":
                    pass
