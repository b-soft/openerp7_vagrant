from datetime import datetime, timedelta
from django.core.mail import send_mail
from django.core.management import call_command
from django.core.management.base import NoArgsCommand
from django.db.models.query_utils import Q
from apps.game.models import Event

class Command(NoArgsCommand):

    def handle_noargs(self, **options):
        now = datetime.now()
        from_date = now - timedelta(hours=80)
        to_date = now - timedelta(hours=1)

        try:
            events = Event.objects.filter(
                Q(game__active=True, draw_datetime__gte=from_date, draw_datetime__lte=to_date) & Q(
                Q(has_event_result_entry=False) | Q(has_event_result_winner=False)))
        except Event.DoesNotExist:
            events = False

        if events:
            for event in events:
                print "Attempting to scrape %s" % event.game
                if event.game.slug == 'superenalotto':
                    call_command('superenalotto')
                elif event.game.slug == 'euromillions':
                    call_command('euromillions')
                else:
                    call_command('spanish-daily', event.game)

                updated_event = Event.objects.get(id=event.id)
                error = False
                if not updated_event.has_event_result_entry:
                    error = 'Failed to fetch draw balls'
                elif not updated_event.has_event_result_winner:
                    error = 'Failed to fetch winners or prizes'
                elif not updated_event.processed_results:
                    error = 'Failed to process entries, check next estimated'

                if error:
                    send_mail('Scrape error: %s' % event.game, error, 'support@lottostar.co.za',
                        ['taffy@symfonysl.com'], fail_silently=False)