from django.contrib.auth.models import User
from django.contrib.humanize.templatetags.humanize import intcomma
from django.contrib.sites.models import Site
from django.core.management.base import NoArgsCommand
from django.db import transaction, IntegrityError, connection
from django.template.context import Context
from django.template.loader import get_template
from django.utils.html import strip_tags
from game.models import Event, Game
from play.models.result_email_user import ResultEmailUser
from play.models.system_email import SystemEmail

@transaction.commit_manually
class Command(NoArgsCommand):
    def handle(self, *args, **options):

        try:
            result_email_users = ResultEmailUser.objects.filter(has_sent=False)
        except ResultEmailUser.DoesNotExist, e:
            print "ResultEmailUser does not exist or emails have been already processed."
            return False

        site = Site.objects.get_current()
        site_url = str(('%s%s') % ('http://', site.domain))

        for result_email_user in result_email_users:
            user = result_email_user.user
            event = result_email_user.event

            first_prize = event.gamedrawresult_set.all().order_by('id')
            number_of_jackpot_winners = first_prize[0].winners

            sql = '''
                SELECT
                    pt.game_id,
                    tt.user_id,
                    entry_main_balls.ticket_id,
                    entry_main_balls.id AS entry_id,
                    entry_main_balls.balls1,
                    main_balls,
                    bonus_balls
                    FROM
                    (   SELECT (SELECT ARRAY(select unnest(array%s)
                    INTERSECT
                    SELECT unnest(balls1) FROM play_entry WHERE id=pe.id)),
                        pe.id,
                        pe.ticket_id,
                        pe.balls1,
                        pe.is_processed,
                        pe.event_id
                    FROM play_entry pe
                    )  entry_main_balls (main_balls),
                    (   SELECT (SELECT ARRAY(select unnest(array%s)
                    INTERSECT
                    SELECT unnest(balls2) FROM play_entry WHERE id=pe.id)),
                        pe.id,
                        pe.ticket_id,
                        pe.event_id
                    FROM play_entry pe
                    )  bonus_main_balls (bonus_balls)
                    INNER JOIN play_ticket pt ON pt.id = bonus_main_balls.ticket_id
                    INNER JOIN play_transaction tt ON tt.id = pt.transaction_id
                    INNER JOIN game_game ga ON ga.id = pt.game_id
                    WHERE entry_main_balls.id = bonus_main_balls.id
                    AND entry_main_balls.event_id = %d
                    AND entry_main_balls.is_processed = True
                    AND tt.user_id = %d
                    ORDER BY entry_main_balls.id
                '''
            query = sql % (event.balls1, event.balls1, result_email_user.event_id, user.id)

            try :
                cursor = connection.cursor()
                cursor.execute(query)
                transaction.commit()
            except IntegrityError, e:
                print e.message
                transaction.rollback()
                return False


            #load the template
            html_template = get_template('emails/results_email.html')
            text_template = get_template('emails/results_email.txt')

            html_entries = '''<tr>
            <td style="padding-left: 10px; font-size: 12px; line-height: 30px; border-left: 1px solid #ebebeb;">%d</td>
            <td style="padding-left: 10px; font-size: 12px;"><div>%s</div></td>
            <td style="padding-left: 10px; font-size: 12px; border-right: 1px solid #ebebeb;">
            <div style="color: #005b8c;">%s</div></td>
            </tr>
            '''

            numbers_html = ''
            for entry in cursor.fetchall():
                html = ''
                message = ''
                i=0
                for number in entry[4]:
                    if i < (len(entry[4])-1):
                        if number in entry[5]:
                            html += '<span><b>%d-</b></span>' % number
                        else:
                            html += '<span>%d-</span>' % number
                    else:
                        if number in entry[5]:
                            html += '<span><b>%d</b></span>' % number
                        else:
                            html += '<span>%d</span>' % number
                    i += 1

                    if len(entry[5]) == len(entry[4]):
                        message = "Congratulations you won the jackpot"
                    elif len(entry[5]) == 0:
                        message = "No match"
                    elif len(entry[5]) == 1:
                        message = "Matched %d number" % len(entry[5])
                    else:
                        message = "Matched %d numbers" % len(entry[5])

                numbers_html += html_entries % (event.id, html, message)


            amount = round(float(event.jackpot), 2)
            jackpot = "&euro; %s%s" % (intcomma(int(amount)), ("%0.2f" % amount)[-3:])

            if number_of_jackpot_winners > 0:
                winning_phrase = 'The latest <b>%s</b> %s jackpot has been won.' % (jackpot, event.game)
            else:
                winning_phrase = 'The latest <b>%s</b> %s jackpot has rolled over.' % (jackpot, event.game)

            #get games next estimated jackpots
            superenalotto_game = Game.objects.get(slug="superenalotto")
            jackpot1 = "&euro; %s%s" % (intcomma(int(superenalotto_game.jackpot)), ("%0.2f" % superenalotto_game.jackpot)[-3:])

            spanishdaily_game = Game.objects.get(slug="spanish-daily")
            jackpot2 = "&euro; %s%s" % (intcomma(int(spanishdaily_game.jackpot)), ("%0.2f" % spanishdaily_game.jackpot)[-3:])

            euromillions_game = Game.objects.get(slug="euromillions")
            jackpot3 = "&euro; %s%s" % (intcomma(int(euromillions_game.jackpot)), ("%0.2f" % euromillions_game.jackpot)[-3:])

            context = Context({
                'game': event.game,
                'user_name': user.get_full_name,
                'site_domain': site_url,
                'winning_phrase': winning_phrase,
                'balls1': event.balls1,
                'balls2': event.balls2,
                'balls3': event.balls3,
                'jackpot1': jackpot1,
                'jackpot2': jackpot2,
                'jackpot3': jackpot3,
                'user_entries': numbers_html
            })

            email = SystemEmail(
                user_id = user.id,
                subject   = 'Your %s results for %s' % (event.game, event.draw_datetime.strftime("%Y-%m-%d")),
                body_html =  html_template.render(context),
                body_text =  strip_tags(text_template.render(context)),
                to_address =  ['taffy@symfony.co.za','andrew@symfony.co.za',user.email],
                from_address =  ['info@lottostar.com'],
            )
            email.save()

            result_email_user.has_sent = True
            result_email_user.system_email = email
            result_email_user.save()

