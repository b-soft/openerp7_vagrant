from datetime import datetime
import json
from BeautifulSoup import BeautifulSoup
import urllib2
import django
from django.core.management import call_command
from django.core.management.base import AppCommand, NoArgsCommand, BaseCommand
from django.utils.html import strip_tags
from itertools import islice
import re
from apps.game.models import Game, Event

class Command(BaseCommand):

    def handle(self, *args, **options):
        game = args[0] if len(args) > 0 else False
        if game:
            current_draw = Event.objects.filter(game_id__exact=game.id,
                draw_datetime__lte=datetime.now()).order_by("-draw_datetime")[0]
            try:
                req = urllib2.Request('http://www.elgordo.com/results/lottoen.asp')
                req.add_header('User-agent', 'Mozilla 5.10')
                res = urllib2.urlopen(req)
                html = res.read()
            except urllib2.HTTPError:
                html = None

            soup = BeautifulSoup(html)
            date_soup = soup.find("div", {"class": "d_d"})
            draw_date = django.utils.html.strip_tags(date_soup)
            current_draw_date = current_draw.draw_datetime.strftime("%A, %d %B %Y")

            if draw_date.replace("&nbsp;", " ") == current_draw_date:
                print "Spanish daily la Primitiva"
                call_command('laprimitiva')
            else:
                try:
                    req = urllib2.Request('http://www.elgordo.com/results/tb-result-daily-649.asp')
                    req.add_header('User-agent', 'Mozilla 5.10')
                    res = urllib2.urlopen(req)
                    html = res.read()
                except urllib2.HTTPError:
                    html = None

                soup = BeautifulSoup(html)
                date_soup = soup.find("div", {"class": "d_d"})
                draw_date = django.utils.html.strip_tags(date_soup)
                current_draw_date = current_draw.draw_datetime.strftime("%A, %d %B %Y")

                if draw_date.replace("&nbsp;", " ") == current_draw_date:
                    print "Spanish daily Bonoloto"
                    call_command('bonoloto')
