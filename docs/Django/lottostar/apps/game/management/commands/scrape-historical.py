from datetime import datetime
import json
import urllib
from BeautifulSoup import BeautifulSoup
import urllib2
import django
from django.core.management.base import NoArgsCommand
from django.utils.html import strip_tags
from itertools import islice
import re
import time
import requests
from apps.game.models import Game, GameDrawResult, Event
from game.models import GamePrize
import settings

def take(n, iterable):
    return list(islice(iterable, n))

class Command(NoArgsCommand):

    def handle_noargs(self, **options):

        for game in Game.objects.all():

            if game.id and game.game_id is None:
                game_winning_combinations = game.gameprize_set.all()
                game_ballsets = game.ballset_set.all()

                if game.slug == "superenalotto":
                    #there is one sub game, get the ballset for sub game
                    try:
                        sub_game = Game.objects.get(id=game.id).games.get()
                        sub_game_winning_combinations =sub_game.gameprize_set.all()
                        sub_game_ballsets = sub_game.ballset_set.all()
                    except Game.DoesNotExist:
                        sub_game = None
                        sub_game_winning_combinations = None
                        sub_game_ballsets = None

                    url = 'http://www.thelotter.com/lotteryresults.aspx?pageId=7&id=149&DrawNumber=%s'
                    number = 149

                    try:
                        headers = {'content-type': 'application/json; charset=utf-8'}
                        response = requests.post('http://thelotter.com/lotteryresults.aspx/GetDrawsValueNameList', data="{lotteryRef:%d}" % number, headers=headers)
                    except urllib2.HTTPError:
                        response = None

                    draw_dates = reversed(json.loads(response.text)['d'])
                    if settings.DEBUG:
                        draw_dates = take(10, json.loads(response.text)['d'])

                    if response.status_code == 200:
                        for data in draw_dates:
                            scrape_url = url % data['Value']
                            try:
                                r = urllib2.urlopen(scrape_url)
                                html = r.read()
                            except urllib2.HTTPError:
                                html = None

                            if html is not None:
                                print scrape_url
                                soup = BeautifulSoup(html)
                                date_soup = soup.find('select', {'id': 'ctl00_ContentPlaceHolderMain_ddlDrawNumber'})
                                date_str = django.utils.html.strip_tags(date_soup).split('|')[1].strip()
                                row_date = datetime.strptime(date_str, '%d %b %Y').strftime('%Y-%m-%d')

                                draw_date = row_date.split("-")
                                draw_year = int(int(draw_date[0])); draw_month = int(draw_date[1]) ; draw_day = int(draw_date[2])
                                draw_datetime = datetime(draw_year, draw_month, draw_day, 19, 00, 00)
                                cutoff_datetime = datetime(draw_year, draw_month, draw_day, 16, 00, 00)

                                print "Running scrape for: %s (%s) " % (game.name, row_date)
                                print "Getting lottery winning numbers"

                                #Getting the main winning numbers
                                numbers_soup = soup.findAll("td", {"class": "icon_regularBall ballicon"})
                                main_numbers = []
                                for div in numbers_soup:
                                    main_numbers.append(int(django.utils.html.strip_tags(div)))
                                    #Getting the bonus winning numbers
                                bonus_soup = soup.findAll("td", {"class": "icon_bonusBall ballicon"})
                                bonus_numbers = []
                                for div in bonus_soup:
                                    bonus_numbers.append(int(django.utils.html.strip_tags(div)))
                                    #Getting the star winning numbers
                                star_soup = soup.findAll("td", {"class": "icon_additionalBall ballicon"})
                                star_numbers = []
                                for div in star_soup:
                                    star_numbers.append(int(django.utils.html.strip_tags(div)))

                                #Get a count of main and bonus balls
                                if len(game_ballsets) == 1:
                                    main_winning_entries = game_ballsets[0].main
                                    bonus_winning_entries = game_ballsets[0].bonus
                                else:
                                    main_winning_entries = game_ballsets[0].main
                                    bonus_winning_entries = game_ballsets[1].main

                                if len(main_numbers) != main_winning_entries:
                                    print "Main ball mismatch"
                                    return False

                                print bonus_winning_entries
                                    #For bonus numbers
                                if len(bonus_numbers) != bonus_winning_entries:
                                    print "Bonus ball mismatch"
                                    return False

                                if len(star_numbers) != 1:
                                    print "Stars ball mismatch"
                                    return False

                                #save the entries
                                print "Winning numbers: %s %s %s" % (main_numbers, bonus_numbers, star_numbers)
                                game_event = Event(
                                    game = game,
                                    jackpot = 0,
                                    balls1 = main_numbers,
                                    balls2 = bonus_numbers,
                                    balls3 = star_numbers,
                                    rollover = 0,
                                    entries = 0,
                                    game_status="complete",
                                    draw_datetime = draw_datetime,
                                    cutoff = cutoff_datetime
                                )
                                game_event.save()
                                game_event.has_event_result_entry = True

                                print "Getting lottery winnings"
                                script = soup.find(r'script', text=re.compile('jqGrid'))
                                prog = re.compile('(?=\'DrawRef).+?(?=DivisionToDisplay)[^}]+', re.DOTALL)
                                data_list = prog.findall(script)

                                prize_dict = []
                                mylist = [1,3,5,7,9]
                                for data in data_list:
                                    data = re.sub("\s+",'', data)
                                    values = data.replace('<div class="divPaddingFormatter">', '').replace('<divclass="divPaddingFormatter">', '').replace('</div>', '').replace('\'', '\"')
                                    j_data = "{%s}" % values
                                    results = json.loads(j_data)

                                    prize = results['LocalWinningAmount']
                                    if results['LocalWinningAmount'] == -79228162514264337593543950335:
                                        prize = 0

                                    prize_dict.append(prize)

                                count = 0
                                main_game_prize = []
                                main_game_winners = []
                                sub_game_prize = []
                                sub_game_winners = []

                                for value in prize_dict:
                                    if count in mylist:
                                        main_game_prize.append(value)
                                    else:
                                        sub_game_prize.append(value)
                                    count += 1

                                #save main game winnings
                                if len(main_game_prize) == len(game_winning_combinations):
                                    for key, prize in enumerate(main_game_prize):
                                        print "Prize per winner: %s" % prize
                                        game_draw_result = GameDrawResult(
                                            event = game_event,
                                            game_prize = game_winning_combinations[key],
                                            winners = 0,
                                            amount = main_game_prize[key]
                                        )
                                        game_draw_result.save()

                                #save sub game winnings
                                if len(sub_game_prize) == len(sub_game_winning_combinations):
                                    for key, prize in enumerate(sub_game_prize):
                                        print "Prize per winner (sub game): %s" % prize
                                        game_draw_result = GameDrawResult(
                                            event = game_event,
                                            game_prize = sub_game_winning_combinations[key],
                                            winners = 0,
                                            amount = sub_game_prize[key]
                                        )
                                        game_draw_result.save()

                                    game_event.has_event_result_winner = True
                                    game_event.game_status = "complete"
                                    game_event.save()
                            time.sleep(2)

                elif game.slug == "euromillions":
                    try:
                        headers = {'content-type': 'application/json; charset=utf-8'}
                        response = requests.post('http://thelotter.com/lotteryresults.aspx/GetDrawsValueNameList', data="{lotteryRef:99}", headers=headers)
                    except urllib2.HTTPError:
                        response = None

                    draw_dates = reversed(json.loads(response.text)['d'])
                    if settings.DEBUG:
                        draw_dates = take(10, json.loads(response.text)['d'])

                    if response.status_code == 200:
                        for data in draw_dates:
                            scrape_url = 'http://www.thelotter.com/LotteryResults.aspx?pageId=7&id=99&DrawNumber=%s' % data['Value']
                            try:
                                r = urllib2.urlopen(scrape_url)
                                html = r.read()
                            except urllib2.HTTPError:
                                html = None

                            if html is not None:
                                print scrape_url

                                soup = BeautifulSoup(html)
                                date_soup = soup.find('select', {'id': 'ctl00_ContentPlaceHolderMain_ddlDrawNumber'})

                                if str(django.utils.html.strip_tags(date_soup)):

                                    date_str = django.utils.html.strip_tags(date_soup).split('|')[1].strip()
                                    row_date = datetime.strptime(date_str, '%d %b %Y').strftime('%Y-%m-%d')

                                    draw_date = row_date.split("-")
                                    draw_year = int(int(draw_date[0])); draw_month = int(draw_date[1]) ; draw_day = int(draw_date[2])

                                    draw_datetime = datetime(draw_year, draw_month, draw_day, 21, 00, 00)
                                    cutoff_datetime = datetime(draw_year, draw_month, draw_day, 17, 00, 00)

                                    print "Running scrape for: %s (%s) " % (game.name, row_date)
                                    print "Getting lottery winning numbers"

                                    #Getting the main winning numbers
                                    numbers_soup = soup.findAll("td", {"class": "icon_regularBall ballicon"})
                                    main_numbers = []
                                    for div in numbers_soup:
                                        main_numbers.append(int(django.utils.html.strip_tags(div)))

                                    #Getting the bonus winning numbers
                                    bonus_soup = soup.findAll("td", {"class": "icon_additionalBall ballicon"})
                                    bonus_numbers = []
                                    for div in bonus_soup:
                                        bonus_numbers.append(int(django.utils.html.strip_tags(div)))

                                    #Get a count of main and bonus balls
                                    if len(game_ballsets) == 1:
                                        main_winning_entries = game_ballsets[0].main
                                        bonus_winning_entries = game_ballsets[0].bonus
                                    else:
                                        bonus_winning_entries = game_ballsets[0].main
                                        main_winning_entries = game_ballsets[1].main

                                    print main_numbers

                                    if len(main_numbers) != main_winning_entries:
                                        print "Main ball mismatch"
                                        return False

                                    #For bonus numbers
                                    if len(bonus_numbers) != bonus_winning_entries:
                                        print "Bonus ball mismatch"
                                        return False

                                    #save the entries
                                    print "Winning numbers: %s %s" % (main_numbers, bonus_numbers)
                                    game_event = Event(
                                        game = game,
                                        jackpot = 0,
                                        balls1 = main_numbers,
                                        balls2 = bonus_numbers,
                                        entries = 0,
                                        rollover = 0,
                                        game_status="complete",
                                        draw_datetime = draw_datetime,
                                        cutoff = cutoff_datetime
                                    ); game_event.save()

                                    print "Getting lottery winnings"
                                    script = soup.find(r'script', text=re.compile('jqGrid'))
                                    prog = re.compile('(?=\'DrawRef).+?(?=DivisionToDisplay)[^}]+', re.DOTALL)
                                    data_list = prog.findall(script)

                                    count = 0
                                    prize_dict = {}

                                    for data in data_list:
                                        data = re.sub("\s+",'', data)
                                        values = data.replace('<div class="divPaddingFormatter">', '').replace('<divclass="divPaddingFormatter">', '').replace('</div>', '').replace('\'', '\"')
                                        j_data = "{%s}" % values
                                        results = json.loads(j_data)

                                        prize_dict[count] = results['LocalWinningAmount']
                                        if results['LocalWinningAmount'] == -79228162514264337593543950335:
                                            prize_dict[count] = 0
                                        count += 1

                                    if len(prize_dict) == len(game_winning_combinations) or len(prize_dict) == 12:
                                        #save the winnngs
                                        for key, prize in enumerate(prize_dict.values()):
                                            print "Prize per winner: %s" % prize

                                            game_draw_result = GameDrawResult(
                                                event = game_event,
                                                game_prize = game_winning_combinations[key],
                                                winners = 0,
                                                amount = prize
                                            )
                                            game_draw_result.save()

                            time.sleep(2)

                elif game.slug == "spanish-daily":
                    url = 'http://www.thelotter.com/lotteryresults.aspx?pageId=7&id=146&DrawNumber=%s'
                    number = 146

                    print "Bonoloto scrape"
                    try:
                        headers = {'content-type': 'application/json; charset=utf-8'}
                        response = requests.post('http://thelotter.com/lotteryresults.aspx/GetDrawsValueNameList', data="{lotteryRef:%d}" % number, headers=headers)
                    except urllib2.HTTPError:
                        response = None

                    draw_dates = reversed(json.loads(response.text)['d'])
                    if settings.DEBUG:
                        draw_dates = take(10, json.loads(response.text)['d'])

                    if response.status_code == 200:
                        for data in draw_dates:
                            scrape_url = url % data['Value']
                            try:
                                r = urllib2.urlopen(scrape_url)
                                html = r.read()
                            except urllib2.HTTPError:
                                html = None

                            if html is not None:
                                print scrape_url

                                soup = BeautifulSoup(html)

                                date_soup = soup.find('select', {'id': 'ctl00_ContentPlaceHolderMain_ddlDrawNumber'})
                                date_str = django.utils.html.strip_tags(date_soup).split('|')[1].strip()
                                row_date = datetime.strptime(date_str, '%d %b %Y').strftime('%Y-%m-%d')

                                draw_date = row_date.split("-")
                                draw_year = int(int(draw_date[0])); draw_month = int(draw_date[1]) ; draw_day = int(draw_date[2])

                                draw_datetime = datetime(draw_year, draw_month, draw_day, 19, 00, 00)
                                cutoff_datetime = datetime(draw_year, draw_month, draw_day, 17, 00, 00)

                                print "Running scrape for: %s (%s) " % (game.name, row_date)
                                print "Getting lottery winning numbers"

                                #Getting the main winning numbers
                                numbers_soup = soup.findAll("td", {"class": "icon_regularBall ballicon"})
                                main_numbers = []
                                for div in numbers_soup:
                                    main_numbers.append(int(django.utils.html.strip_tags(div)))

                                #Getting the bonus winning numbers
                                bonus_soup = soup.findAll("td", {"class": "icon_bonusBall ballicon"})
                                bonus_numbers = []
                                for div in bonus_soup:
                                    bonus_numbers.append(int(django.utils.html.strip_tags(div)))

                                #Get a count of main and bonus balls
                                if len(game_ballsets) == 1:
                                    main_winning_entries = game_ballsets[0].main
                                    bonus_winning_entries = game_ballsets[0].bonus
                                else:
                                    bonus_winning_entries = game_ballsets[0].main
                                    main_winning_entries = game_ballsets[1].main

                                if len(main_numbers) != main_winning_entries:
                                    print "Main ball mismatch"
                                    return False

                                    #For bonus numbers
                                if len(bonus_numbers) != bonus_winning_entries:
                                    print "Bonus ball mismatch"
                                    return False

                                #save the entries
                                print "Winning numbers: %s" % main_numbers, bonus_numbers
                                game_event = Event(
                                    game = game,
                                    jackpot = 0,
                                    balls1 = main_numbers,
                                    balls2 = bonus_numbers,
                                    rollover = 0,
                                    entries = 0,
                                    game_status="complete",
                                    draw_datetime = draw_datetime,
                                    cutoff = cutoff_datetime
                                ); game_event.save()


                                print "Getting lottery winnings"
                                script = soup.find(r'script', text=re.compile('jqGrid'))
                                prog = re.compile('(?=\'DrawRef).+?(?=DivisionToDisplay)[^}]+', re.DOTALL)
                                data_list = prog.findall(script)

                                count = 0
                                prize_dict = {}

                                for data in data_list:
                                    data = re.sub("\s+",'', data)
                                    values = data.replace('<div class="divPaddingFormatter">', '').replace('<divclass="divPaddingFormatter">', '').replace('</div>', '').replace('\'', '\"')
                                    j_data = "{%s}" % values
                                    results = json.loads(j_data)

                                    prize_dict[count] = results['LocalWinningAmount']
                                    if results['LocalWinningAmount'] == -79228162514264337593543950335:
                                        prize_dict[count] = 0
                                    count += 1

                                if len(prize_dict) == len(game_winning_combinations):
                                    #save the winnngs
                                    for key, prize in enumerate(prize_dict.values()):
                                        print "Prize per winner: %s" % prize

                                        game_draw_result = GameDrawResult(
                                            event = game_event,
                                            game_prize = game_winning_combinations[key],
                                            winners = 0,
                                            amount = prize
                                        )
                                        game_draw_result.save()
                            time.sleep(2)


                    url = 'http://www.thelotter.com/lotteryresults.aspx?pageId=7&id=11&DrawNumber=%s'
                    number = 11

                    print "LaPrimitiva scrape"
                    try:
                        headers = {'content-type': 'application/json; charset=utf-8'}
                        response = requests.post('http://thelotter.com/lotteryresults.aspx/GetDrawsValueNameList', data="{lotteryRef:%d}" % number, headers=headers)
                    except urllib2.HTTPError:
                        response = None

                    draw_dates = reversed(json.loads(response.text)['d'])
                    if settings.DEBUG:
                        draw_dates = take(10, json.loads(response.text)['d'])

                    if response.status_code == 200:
                        for data in draw_dates:
                            scrape_url = url % data['Value']
                            try:
                                r = urllib2.urlopen(scrape_url)
                                html = r.read()
                            except urllib2.HTTPError:
                                html = None

                            if html is not None:
                                print scrape_url

                                soup = BeautifulSoup(html)

                                date_soup = soup.find('select', {'id': 'ctl00_ContentPlaceHolderMain_ddlDrawNumber'})
                                date_str = django.utils.html.strip_tags(date_soup).split('|')[1].strip()
                                row_date = datetime.strptime(date_str, '%d %b %Y').strftime('%Y-%m-%d')

                                draw_date = row_date.split("-")
                                draw_year = int(int(draw_date[0])); draw_month = int(draw_date[1]) ; draw_day = int(draw_date[2])

                                draw_datetime = datetime(draw_year, draw_month, draw_day, 19, 00, 00)
                                cutoff_datetime = datetime(draw_year, draw_month, draw_day, 17, 00, 00)

                                print "Running scrape for: %s (%s) " % (game.name, row_date)
                                print "Getting lottery winning numbers"

                                #Getting the main winning numbers
                                numbers_soup = soup.findAll("td", {"class": "icon_regularBall ballicon"})
                                main_numbers = []
                                for div in numbers_soup:
                                    main_numbers.append(int(django.utils.html.strip_tags(div)))

                                #Getting the bonus winning numbers
                                bonus_soup = soup.findAll("td", {"class": "icon_bonusBall ballicon"})
                                bonus_numbers = []
                                for div in bonus_soup:
                                    bonus_numbers.append(int(django.utils.html.strip_tags(div)))

                                #Get a count of main and bonus balls
                                if len(game_ballsets) == 1:
                                    main_winning_entries = game_ballsets[0].main
                                    bonus_winning_entries = game_ballsets[0].bonus
                                else:
                                    bonus_winning_entries = game_ballsets[0].main
                                    main_winning_entries = game_ballsets[1].main

                                if len(main_numbers) != main_winning_entries:
                                    print "Main ball mismatch"
                                    return False

                                    #For bonus numbers
                                if len(bonus_numbers) != bonus_winning_entries:
                                    print "Bonus ball mismatch"
                                    return False

                                #save the entries
                                print "Winning numbers: %s" % main_numbers, bonus_numbers
                                game_event = Event(
                                    game = game,
                                    jackpot = 0,
                                    balls1 = main_numbers,
                                    balls2 = bonus_numbers,
                                    rollover = 0,
                                    entries = 0,
                                    game_status="complete",
                                    draw_datetime = draw_datetime,
                                    cutoff = cutoff_datetime
                                ); game_event.save()


                                print "Getting lottery winnings"
                                script = soup.find(r'script', text=re.compile('jqGrid'))
                                prog = re.compile('(?=\'DrawRef).+?(?=DivisionToDisplay)[^}]+', re.DOTALL)
                                data_list = prog.findall(script)

                                count = 0
                                prize_dict = {}

                                for data in data_list:
                                    data = re.sub("\s+",'', data)
                                    values = data.replace('<div class="divPaddingFormatter">', '').replace('<divclass="divPaddingFormatter">', '').replace('</div>', '').replace('\'', '\"')
                                    j_data = "{%s}" % values
                                    results = json.loads(j_data)

                                    prize_dict[count] = results['LocalWinningAmount']
                                    if results['LocalWinningAmount'] == -79228162514264337593543950335:
                                        prize_dict[count] = 0
                                    count += 1

                                if len(prize_dict) == len(game_winning_combinations):
                                    #save the winnngs
                                    for key, prize in enumerate(prize_dict.values()):
                                        print "Prize per winner: %s" % prize

                                        game_draw_result = GameDrawResult(
                                            event = game_event,
                                            game_prize = game_winning_combinations[key],
                                            winners = 0,
                                            amount = prize
                                        )
                                        game_draw_result.save()
                            time.sleep(2)