from django.core.mail import send_mail
from django.core.management.base import NoArgsCommand
from django.db import transaction, IntegrityError, connection
from game.models import Event

@transaction.commit_manually
class Command(NoArgsCommand):
    def handle(self, *args, **options):

        events = Event.objects.filter(processed_results=True, processed_winnings=False)
        if events:
            for event in events:
                self.do_calculation(event)
        else:
            print "No unprocessed event found."

    def do_calculation(self, event):
        try:
            event = Event.objects.get(id=event.id, processed_results=True, processed_winnings=False)
        except Event.DoesNotExist, e:
            self.send_error_report(e)
            print "Event calander does not exist or winnings have been already processed."
            return False

        #update zero combination entries
        try :
            sql = self.update_zero_combination_entry(event)
            cursor = connection.cursor()
            cursor.execute(sql)
            transaction.commit()
        except IntegrityError, e:
            transaction.rollback()
            self.send_error_report(e)
            return False

        #update winning combination entries
        winning_tiers = event.game.gameprize_set.all()
        for winning in winning_tiers:
            try :
                sql = self.winning_query(winning, event)
                cursor = connection.cursor()
                cursor.execute(sql)
                transaction.commit()
            except IntegrityError, e:
                transaction.rollback()
                self.send_error_report(e)
                return False

        #update all non winning entries
        try :
            sql = self.update_non_winning_entry(event)
            cursor = connection.cursor()
            cursor.execute(sql)
            transaction.commit()
        except IntegrityError, e:
            transaction.rollback()
            self.send_error_report(e)
            return False

        #update game_event
        try :
            sql = 'UPDATE game_event SET processed_winnings = True WHERE id = %d; ' % event.id
            cursor = connection.cursor()
            cursor.execute(sql)
            transaction.commit()
        except IntegrityError, e:
            transaction.rollback()
            self.send_error_report(e)
            return False

        print "Winnings calculation completed"

        #create result mailer
        self.create_result_email(event)



    def update_non_winning_entry(self, event):
        sql = '''
                UPDATE play_entry pe SET is_processed = True
                WHERE is_processed = False AND event_id = %d;
            '''
        update_query = sql % event.id
        return update_query


    def update_zero_combination_entry(self, event):

        #check if game is a subgame
        game = event.game

        if game.game:
            main_game_ballsets = game.ballset_set.all()
            sub_game_ballsets = game.game.ballset_set.all()

            if len(main_game_ballsets) == 1 and len(sub_game_ballsets) == 1:
                balls1 = 'balls1'
                balls2 = 'balls1'
                balls3 = 'balls3'
            else:
                balls1 = 'balls1'
                balls2 = 'balls2'
                balls3 = 'balls3'
        else:
            main_game_ballsets = game.ballset_set.all()
            if len(main_game_ballsets) == 1:
                balls1 = 'balls1'
                balls2 = 'balls1'
                balls3 = None
            else:
                balls1 = 'balls1'
                balls2 = 'balls2'
                balls3 = None

        sql = self.get_non_winning_sql(balls3)
        if balls3 is not None:
            query = sql % (event.balls1, balls1, event.balls2, balls2, event.balls3, balls3, event.id, 0, 0, 0,)
        else:
            query = sql % (event.balls1,balls1,event.balls2,balls2,event.id,0,0,)

        #check if tmp_non_winning_entry exists
        try :
            sql = '''
                SELECT exists(SELECT * FROM information_schema.tables WHERE table_name='tmp_non_winning_entry')
            '''
            cursor = connection.cursor()
            cursor.execute(sql)
            transaction.commit()
        except IntegrityError, e:
            transaction.rollback()
            self.send_error_report(e)
            return False

        #update zero combination entries
        sql = ''
        if cursor.fetchone()[0]:
            sql = '''
                    DROP TABLE tmp_non_winning_entry;
           '''

        sql += query
        update_sql = '''
                UPDATE play_entry pe SET is_processed = True
                FROM tmp_non_winning_entry tmp
                WHERE pe.id = tmp.id;
        '''
        sql += update_sql

        return sql

    def winning_query(self, winning, event, limit=500):

        if len(winning.main_balls) == 1:
            main_numbers = winning.main_balls[0][1]
            bonus_numbers = winning.bonus_balls[0][1]
            star_numbers = 1
        elif len(winning.main_balls) == 2:
            main_numbers = winning.main_balls[0][1]
            bonus_numbers= winning.bonus_balls[0][1]
            star_numbers = 1
        else:
            main_numbers = winning.main_balls[0][1]
            bonus_numbers= winning.bonus_balls[0][1]
            star_numbers = 1

        #check if game is a subgame
        game = event.game
        if game.game:
            main_game_ballsets = game.ballset_set.all()
            sub_game_ballsets = game.game.ballset_set.all()

            if len(main_game_ballsets) == 1 and len(sub_game_ballsets) == 1:
                balls1 = 'balls1'
                balls2 = 'balls1'
                balls3 = 'balls3'
            else:
                balls1 = 'balls1'
                balls2 = 'balls2'
                balls3 = 'balls3'
        else:
            main_game_ballsets = game.ballset_set.all()
            if len(main_game_ballsets) == 1:
                balls1 = 'balls1'
                balls2 = 'balls1'
                balls3 = None
            else:
                balls1 = 'balls1'
                balls2 = 'balls2'
                balls3 = None

        sql = self.get_winning_sql(balls3)

        if balls3 is not None:
            query = sql % (event.balls1, balls1,
                           event.balls2, balls2,
                           event.balls3, balls3,
                           winning.id,
                           event.id,int(main_numbers),
                           int(bonus_numbers), int(star_numbers),)
        else:
            query = sql % (event.balls1,
                           balls1,
                           event.balls2,
                           balls2,
                           winning.id,
                           event.id,
                           int(main_numbers),
                           int(bonus_numbers),
                )

        sql = '''
            DROP TABLE tmp_non_winning_entry;
        '''
        sql += query
        sql_update = '''
            UPDATE account_userwallet au SET winnings = winnings + tmp.amount
            FROM tmp_non_winning_entry tmp
            WHERE au.user_id = tmp.user_id
            AND au.game_id = tmp.game_id;

            UPDATE play_entry pe SET is_processed = True, game_draw_result_id = tmp.game_drawresult_id,
            winnings = tmp.amount
            FROM tmp_non_winning_entry tmp
            WHERE pe.id = tmp.id;

            INSERT INTO system_log
            (id, user_id, system_log_type_id, description, log_vars, created_at, updated_at)
            SELECT
            nextval('system_log_id_seq') AS id,
            user_id,
            2 AS system_log_type_id,
            message,
            message_vars,
            current_timestamp AS created_at,
            current_timestamp AS updated_at
            FROM tmp_non_winning_entry;
            '''
        sql += sql_update
        return sql


    def get_non_winning_sql(self, balls3):
        sql = '''
            SELECT
                entry_main_balls.ticket_id,
                tt.user_id,
                entry_main_balls.id,
                pt.game_id,
                entry_main_balls.balls1,
                main_balls,
                bonus_balls,
                entry_main_balls.is_processed,
                entry_main_balls.message,
                CAST(ARRAY[ga.name,pt.transaction_id] as varchar[]) as message_vars
            INTO TABLE tmp_non_winning_entry
            FROM
            (   SELECT COALESCE(array_length((SELECT ARRAY(select unnest(array%s)
                INTERSECT
                SELECT unnest(%s) FROM play_entry WHERE id=pe.id)),1),0),
                    pe.id,
                    pe.ticket_id,
                    pe.balls1,
                    pe.is_processed,
                    pe.event_id,
                    CAST(('No winning in #game# (transaction: #transaction#).') AS text) AS message
                FROM play_entry pe
            )  entry_main_balls (main_balls),
            (   SELECT COALESCE(array_length((SELECT ARRAY(select unnest(array%s)
                INTERSECT
                SELECT unnest(%s) FROM play_entry WHERE id=pe.id)),1),0),
                    pe.id,
                    pe.ticket_id,
                    pe.event_id
                FROM play_entry pe
            )  bonus_main_balls (bonus_balls)

            INNER JOIN play_ticket pt ON pt.id = bonus_main_balls.ticket_id
            INNER JOIN play_transaction tt ON tt.id = pt.transaction_id
            INNER JOIN game_game ga ON ga.id = pt.game_id
            WHERE entry_main_balls.id = bonus_main_balls.id
            AND entry_main_balls.event_id = %d
            AND entry_main_balls.is_processed = False
            GROUP BY entry_main_balls.id, entry_main_balls.balls1, entry_main_balls.ticket_id, main_balls,
            bonus_balls, entry_main_balls.is_processed,entry_main_balls.message, tt.user_id, ga.name,
            pt.transaction_id, pt.game_id
            HAVING main_balls = %d AND bonus_balls = %d;
        '''

        if balls3 is not None:
            sql = '''
                SELECT
                    entry_main_balls.ticket_id,
                    tt.user_id,
                    entry_main_balls.id,
                    pt.game_id,
                    entry_main_balls.balls1,
                    main_balls,
                    bonus_balls,
                    star_balls,
                    entry_main_balls.is_processed,
                    entry_main_balls.message,
                    CAST(ARRAY[ga.name,pt.transaction_id] as varchar[]) as message_vars
                INTO TABLE tmp_non_winning_entry
                FROM
                (   SELECT COALESCE(array_length((SELECT ARRAY(select unnest(array%s)
                    INTERSECT
                    SELECT unnest(%s) FROM play_entry WHERE id=pe.id)),1),0),
                        pe.id,
                        pe.ticket_id,
                        pe.balls1,
                        pe.is_processed,
                        pe.event_id,
                        CAST(('No winning in #game# (transaction: #transaction#).') AS text) AS message
                    FROM play_entry pe
                )  entry_main_balls (main_balls),
                (   SELECT COALESCE(array_length((SELECT ARRAY(select unnest(array%s)
                    INTERSECT
                    SELECT unnest(%s) FROM play_entry WHERE id=pe.id)),1),0),
                        pe.id,
                        pe.ticket_id,
                        pe.event_id
                    FROM play_entry pe
                )  bonus_main_balls (bonus_balls),
                (   SELECT COALESCE(array_length((SELECT ARRAY(select unnest(array%s)
                    INTERSECT
                    SELECT unnest(%s) FROM play_entry WHERE id=pe.id)),1),0),
                        pe.id,
                        pe.event_id,
                        pe.ticket_id
                    FROM play_entry pe
                )  star_main_balls (star_balls)
                INNER JOIN play_ticket pt ON pt.id = star_main_balls.ticket_id
                INNER JOIN play_transaction tt ON tt.id = pt.transaction_id
                INNER JOIN game_game ga ON ga.id = pt.game_id
                WHERE entry_main_balls.id = bonus_main_balls.id
                AND entry_main_balls.id = star_main_balls.id
                AND entry_main_balls.event_id = %d
                AND entry_main_balls.is_processed = False
                GROUP BY entry_main_balls.id, entry_main_balls.balls1, entry_main_balls.ticket_id, main_balls,
                bonus_balls, star_balls, entry_main_balls.is_processed,entry_main_balls.message, tt.user_id,
                ga.name, pt.transaction_id, pt.game_id
                HAVING main_balls = %d AND bonus_balls = %d AND star_balls = %d;
            '''
        return sql

    def get_winning_sql(self, balls3):
        sql = '''
            SELECT
                entry_main_balls.ticket_id,
                tt.user_id,
                entry_main_balls.id,
                pt.game_id,
                dr.id AS game_drawresult_id,
                entry_main_balls.balls1,
                main_balls,
                bonus_balls,
                dr.amount,
                entry_main_balls.is_processed,
                entry_main_balls.message,
                CAST(ARRAY[dr.amount,ga.name,pt.transaction_id] as varchar[]) as message_vars
            INTO TABLE tmp_non_winning_entry
            FROM
            (   SELECT COALESCE(array_length((SELECT ARRAY(select unnest(array%s)
                INTERSECT
                SELECT unnest(%s) FROM play_entry WHERE id=pe.id)),1),0),
                    pe.id,
                    pe.ticket_id,
                    pe.balls1,
                    pe.is_processed,
                    pe.event_id,
                    CAST(('You won &euro;#amount# in #game# (transaction: #transaction#).') AS text) AS message
                FROM play_entry pe
            )  entry_main_balls (main_balls),
            (   SELECT COALESCE(array_length((SELECT ARRAY(select unnest(array%s)
                INTERSECT
                SELECT unnest(%s) FROM play_entry WHERE id=pe.id)),1),0),
                    pe.id,
                    pe.ticket_id,
                    pe.event_id
                FROM play_entry pe
            )  bonus_main_balls (bonus_balls)

            INNER JOIN play_ticket pt ON pt.id = bonus_main_balls.ticket_id
            INNER JOIN play_transaction tt ON tt.id = pt.transaction_id
            INNER JOIN game_gamedrawresult dr ON dr.event_id = bonus_main_balls.event_id
            INNER JOIN game_game ga ON ga.id = pt.game_id
            WHERE entry_main_balls.id = bonus_main_balls.id
            AND dr.game_prize_id = %d
            AND entry_main_balls.event_id = %d
            AND entry_main_balls.is_processed = False
            GROUP BY entry_main_balls.id, entry_main_balls.balls1, entry_main_balls.ticket_id, main_balls,
            bonus_balls, entry_main_balls.is_processed,entry_main_balls.message, tt.user_id, dr.amount,
            ga.name, pt.transaction_id, dr.id, pt.game_id
            HAVING main_balls = %d AND bonus_balls = %d;
        '''

        if balls3 is not None:
            sql = '''
                SELECT
                    entry_main_balls.ticket_id,
                    tt.user_id,
                    entry_main_balls.id,
                    pt.game_id,
                    dr.id AS game_drawresult_id,
                    entry_main_balls.balls1,
                    main_balls,
                    bonus_balls,
                    star_balls,
                    dr.amount,
                    entry_main_balls.is_processed,
                    entry_main_balls.message,
                    CAST(ARRAY[dr.amount,ga.name,pt.transaction_id] as varchar[]) as message_vars
                INTO TABLE tmp_non_winning_entry
                FROM
                (   SELECT COALESCE(array_length((SELECT ARRAY(select unnest(array%s)
                    INTERSECT
                    SELECT unnest(%s) FROM play_entry WHERE id=pe.id)),1),0),
                        pe.id,
                        pe.ticket_id,
                        pe.balls1,
                        pe.is_processed,
                        pe.event_id,
                        CAST(('You won &euro;#amount# in #game# (transaction: #transaction#).') AS text) AS message
                    FROM play_entry pe
                )  entry_main_balls (main_balls),
                (   SELECT COALESCE(array_length((SELECT ARRAY(select unnest(array%s)
                    INTERSECT
                    SELECT unnest(%s) FROM play_entry WHERE id=pe.id)),1),0),
                        pe.id,
                        pe.ticket_id,
                        pe.event_id
                    FROM play_entry pe
                )  bonus_main_balls (bonus_balls),
                (   SELECT COALESCE(array_length((SELECT ARRAY(select unnest(array%s)
                    INTERSECT
                    SELECT unnest(%s) FROM play_entry WHERE id=pe.id)),1),0),
                        pe.id,
                        pe.event_id,
                        pe.ticket_id
                    FROM play_entry pe
                )  star_main_balls (star_balls)
                INNER JOIN play_ticket pt ON pt.id = star_main_balls.ticket_id
                INNER JOIN play_transaction tt ON tt.id = pt.transaction_id
                INNER JOIN game_gamedrawresult dr ON dr.event_id = star_main_balls.event_id
                INNER JOIN game_game ga ON ga.id = pt.game_id
                WHERE entry_main_balls.id = bonus_main_balls.id
                AND entry_main_balls.id = star_main_balls.id
                AND dr.game_prize_id = %d
                AND entry_main_balls.event_id = %d
                AND entry_main_balls.is_processed = False
                GROUP BY entry_main_balls.id, entry_main_balls.balls1, entry_main_balls.ticket_id, main_balls,
                bonus_balls, star_balls, entry_main_balls.is_processed,entry_main_balls.message, tt.user_id, dr.amount,
                ga.name, pt.transaction_id, dr.id, pt.game_id
                HAVING main_balls = %d AND bonus_balls = %d AND star_balls = %d;
            '''
        return sql

    def send_error_report(self, e):
        send_mail('Calculate winnings error',str(e), 'admin@lottostar.com',
            ['taffy@symfonysl.com'], fail_silently=False)


    def create_result_email(self, event):

        sql = '''
            INSERT INTO play_resultemailuser
            (id, user_id, event_id, has_sent, has_failed)
            SELECT
                nextval('play_resultemailuser_id_seq') AS id,
                tran.user_id,
                pe.event_id,
                False AS has_sent,
                False AS has_failed
            FROM play_entry pe
            INNER JOIN play_ticket pt ON pt.id = pe.ticket_id
            INNER JOIN play_transaction tran ON tran.id = pt.transaction_id
            WHERE pe.event_id = %d AND pe.is_processed = True
            GROUP BY tran.user_id, pe.event_id
        '''

        #check if tmp_non_winning_entry exists
        try :
            cursor = connection.cursor()
            cursor.execute(sql % event.id)
            transaction.commit()
        except IntegrityError, e:
            transaction.rollback()
            self.send_error_report(e)
            return False

        print "Completed"




