from django.core.management.base import NoArgsCommand
from django.core.management.base import AppCommand

NO_LAUNCH = None
#==============================================================================
class Command(NoArgsCommand):
    option_list = AppCommand.option_list

    def handle_noargs(self, **options):
        """
        A command to execute
        """
        print "issue winnings"