from datetime import datetime
import json
from BeautifulSoup import BeautifulSoup
import urllib2
import django
from django.core.management.base import AppCommand, NoArgsCommand
from django.utils.html import strip_tags
from itertools import islice
import re
from apps.game.models import Game, GameDrawResult, Event

def take(n, iterable):
    return list(islice(iterable, n))

class Command(NoArgsCommand):

    def handle_noargs(self, **options):
        try:
            game = Game.objects.get(slug="superenalotto", active=True)
        except Game.DoesNotExist:
            game = False

        if game:
            try:
                req = urllib2.Request('http://www.giochinumerici.info/portal/page/portal/sitoinformativo/vincite/vincite/ultimoconcorso')
                req.add_header('User-agent', 'Mozilla 5.10')
                res = urllib2.urlopen(req)
                html = res.read()
            except urllib2.HTTPError:
                html = None

            if html is not None:
                current_draw = Event.objects.filter(game_id__exact=game.id,
                    draw_datetime__lte=datetime.now()).order_by("-draw_datetime")[0]

                next_draw = Event.objects.filter(game_id__exact=game.id,
                    draw_datetime__gte=datetime.now()).order_by("draw_datetime")[0]

                #get ballsets for main game
                game_winning_combinations = game.gameprize_set.all()
                game_ballsets = game.ballset_set.all()

                #there is one sub game, get the ballset for sub game
                sub_game = Game.objects.get(id=game.id).games.get()
                sub_game_winning_combinations = sub_game.gameprize_set.all()
                sub_game_ballsets = sub_game.ballset_set.all()

                soup = BeautifulSoup(html)

                if not current_draw.has_event_result_entry:
                    date_soup = soup.find('div', {'class': 'vincite_boxConcorso'})
                    draw_date = django.utils.html.strip_tags(date_soup).split()[4]
                    current_draw_date = current_draw.draw_datetime.strftime("%d/%m/%Y")

                    if draw_date.strip() == current_draw_date:
                        print "Getting lottery winning numbers"
                        #Getting the main winning numbers
                        numbers_soup = soup.findAll("tr", {"class": "vincite_boxNumeri_dati"})
                        main_numbers = []
                        for td in numbers_soup[0].findAll("td"):
                            main_numbers.append(int(django.utils.html.strip_tags(td).strip()))

                        #Getting the bonus winning numbers
                        bonus_soup = soup.findAll("td", {"class": "vincite_boxNumeri_jolly"})
                        bonus_numbers = []
                        for div in bonus_soup:
                            bonus_numbers.append(int(django.utils.html.strip_tags(div).strip()))

                        #Getting the superstar winning number
                        superstar_soup = soup.findAll("td", {"class": "vincite_boxNumeri_stella"})
                        superstar_numbers = []
                        for div in superstar_soup:
                            superstar_numbers.append(int(django.utils.html.strip_tags(div).strip()))


                        #Get a count of main and bonus balls
                        if len(game_ballsets) == 1:
                            main_winning_entries = game_ballsets[0].main
                            bonus_winning_entries = game_ballsets[0].bonus
                        else:
                            main_winning_entries = game_ballsets[0].main
                            bonus_winning_entries = game_ballsets[1].main

                        if len(main_numbers) != main_winning_entries:
                            print "Main ball mismatch"
                            return False

                            #For bonus numbers
                        if len(bonus_numbers) != bonus_winning_entries:
                            print "Bonus ball mismatch"
                            return False

                        if len(superstar_numbers) != 1:
                            print "Stars ball mismatch"
                            return False

                        if len(main_numbers) == main_winning_entries and len(bonus_numbers) == bonus_winning_entries \
                        and len(superstar_numbers) == sub_game_ballsets[0].main:
                            #save the entries
                            print "Winning numbers: %s %s %s" % (main_numbers, bonus_numbers, superstar_numbers)
                            current_draw.balls1 = main_numbers
                            current_draw.balls2 = bonus_numbers
                            current_draw.balls3 = superstar_numbers
                            current_draw.has_event_result_entry = True
                            current_draw.save()


                if current_draw.has_event_result_entry and not current_draw.has_event_result_winner:
                    print "Getting lottery winnings"
                    main_game_prize = []
                    main_game_winners = []
                    sub_game_prize = []
                    sub_game_winners = []
                    count = 0
                    for winnings in soup.findAll("tr", {"class": re.compile(
                        "vincite_boxBottom_datiGrigio|vincite_boxBottom_datiBianco")}):
                        winners_count =  strip_tags(winnings.contents[1]).strip()
                        prize_count =  strip_tags(winnings.contents[5]).strip()
                        cleaned_prize = prize_count.split()[0].replace(",",'').strip()

                        if winners_count == "nessuna":
                            winners_count = 0
                        if cleaned_prize == "-":
                            cleaned_prize = 0

                        if count <= 4:
                            main_game_winners.append(winners_count)
                            main_game_prize.append(cleaned_prize)
                        else:
                            sub_game_winners.append(winners_count)
                            sub_game_prize.append(cleaned_prize)
                        count += 1


                    #save main game winnings
                    if len(main_game_prize) == len(game_winning_combinations):
                        for key, prize in enumerate(main_game_prize):
                            if key is 0:
                                if main_game_prize[0] == '0.00' or main_game_prize[0] == '0.0' or main_game_prize[0] == 0:
                                    payout_amount = current_draw.jackpot
                                    next_draw.rollover += 1
                                else:
                                    payout_amount = main_game_prize[0]
                                    next_draw.rollover = 0
                            else:
                                payout_amount = main_game_prize[key]

                            game_draw_result = GameDrawResult(
                                event = current_draw,
                                game_prize = game_winning_combinations[key],
                                winners = int(main_game_winners[key]),
                                amount = payout_amount
                            )
                            game_draw_result.save()


                    #save sub game winnings
                    sub_game_top_prize = [0,0]
                    sub_game_top_winners = [0,0]
                    sub_game_prize = sub_game_top_prize + sub_game_prize
                    sub_game_winners = sub_game_top_winners + sub_game_winners

                    if len(sub_game_prize) > 8:
                        sub_game_prize = sub_game_prize[:-1]

                    if len(sub_game_prize) == len(sub_game_winning_combinations):
                        for key, prize in enumerate(sub_game_prize):
                            game_draw_result = GameDrawResult(
                                event = current_draw,
                                game_prize = sub_game_winning_combinations[key],
                                winners = int(sub_game_winners[key]),
                                amount = sub_game_prize[key]
                            )
                            game_draw_result.save()
                    else:
                        print "Subgame results mismatch"
                        return False

                    current_draw.has_event_result_winner = True
                    current_draw.game_status = "complete"
                    current_draw.save()

                #get the next estimate jackpot
                if next_draw.jackpot < 1 and current_draw.has_event_result_entry and current_draw.has_event_result_winner:
                    try:
                        req = urllib2.Request('http://www.giochinumerici.info/portal/page/portal/sitoinformativo/giochinumerici')
                        req.add_header('User-agent', 'Mozilla 5.10')
                        res = urllib2.urlopen(req)
                        html = res.read()
                    except urllib2.HTTPError:
                        html = None

                    if html is not None:
                        soup = BeautifulSoup(html)
                        print "Getting the next estimated jackpot"
                        next_jackpot_soup = soup.find("div", {"class": "sitoinfo_posizioneJackPot_NveNew sitoinfo_propertyJackpot_NveNew"})
                        jackpot = strip_tags(next_jackpot_soup).strip()
                        cleaned_jackpot = jackpot.replace(",","").split()[0]

                        if cleaned_jackpot > 1 and cleaned_jackpot != 'None':
                            print "Jackpot amount (%s)" % cleaned_jackpot
                            next_draw.jackpot = cleaned_jackpot
                            game.jackpot = cleaned_jackpot
                            current_draw.processed_results = True

                            current_draw.save()
                            game.save()
                            next_draw.save()



            print "Scrape completed"