from django.db import models
from vendor.countries.models import Country

class Language(models.Model):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=255)
    country = models.ForeignKey(Country, null=True, blank=True)

    created_at = models.DateTimeField(auto_now = True)
    modified_at = models.DateTimeField(auto_now_add = True)

    def __unicode__(self):
        return self.name


class Text(models.Model):
    language = models.ForeignKey(Language)
    text = models.TextField()
    msgctxt = models.TextField(blank=True, null=True)
    text_plural = models.TextField(blank=True, null=True)
    translated = models.TextField(blank=True, null=True)
    translated_plural = models.TextField(blank=True, null=True)
    description = models.TextField()

    def __unicode__(self):
        return "%s :: %s" % (self.language.name, self.text)
