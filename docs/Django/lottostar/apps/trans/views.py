from django.views.generic.base import TemplateView
from trans.functions import generate_language_file

class GenerateView(TemplateView):
    template_name = "trans/po.html"

    def render_to_response(self, context, **response_kwargs):
        generate_language_file(self.kwargs["slug"])
        return super(GenerateView, self).render_to_response(context, **response_kwargs)