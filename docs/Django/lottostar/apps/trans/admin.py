from django.contrib import admin
from django.conf import settings
from django.core.management import call_command
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from trans.functions import generate_language_file
from trans.models import Text, Language
from django import forms
from flutter_modeladmin import RemoveDeleteAdmin
from reversion import VersionAdmin

#def language_controls(obj):
#    return '<a href="/admin/trans/build/%s/" target="_blank">Generate Locale .PO</a>' % obj.code
#language_controls.short_description = 'Controls'
#language_controls.allow_tags = True
#
#class LanguageAdmin(admin.ModelAdmin):
#    list_display = ["name", "code", "country", language_controls]
#    list_editable = ["code","country"]
#
#    def changelist_view(self, request, extra_context=None):
#        for lang in settings.LANGUAGES:
#            l = Language.objects.get_or_create(name=lang[1].title(), code=lang[0])
#        return super(LanguageAdmin, self).changelist_view(request, extra_context)
#
#admin.site.register(Language, LanguageAdmin)


def text_controls(obj):
    html = '<div style="width:340px;height:25px; line-height: 25px">'
    for lang in settings.LANGUAGES:
        html += '<a style="margin:5px" href="/admin/trans/text/%s/?lang=%s" title="%s"><img src="/static/img/flags/icons/%s.png"/></a>' % (obj.id, lang[0], lang[1].title(), lang[0])
    html += "</div>"
    return html
text_controls.short_description = 'Controls'
text_controls.allow_tags = True

class TextAdmin(RemoveDeleteAdmin, VersionAdmin):
    list_display = ["text","text_plural", text_controls]
    search_fields = ["text",  "description", "translated", "translated_plural"]

    fieldsets = (
        (None, {
            'fields': ( 'text', "text_plural", "description",),
        }),
    )

    def get_form(self, request, obj=None, **kwargs):
        self.object_instance = obj
        return super(TextAdmin,self).get_form(request,obj,**kwargs)

    def formfield_for_dbfield(self, db_field, **kwargs):
        if self.object_instance.text.count("\n") > 1:
            pass
        else:
            if db_field.name == 'translated':
                kwargs['widget'] = forms.TextInput(attrs={"style":"width:50%"})
            if db_field.name == 'translated_plural':
                kwargs['widget'] = forms.TextInput(attrs={"style":"width:50%"})
        return super(TextAdmin,self).formfield_for_dbfield(db_field,**kwargs)

    def change_view(self, request, object_id, form_url='', extra_context=None):

        my_object = Text.objects.get(id=object_id)

        if my_object.text_plural:
            self.fieldsets = (
                (None, {
                    'fields': ("language","description", 'text', "text_plural",),
                    }),
                (None, {
                    'fields': ("translated", "translated_plural"),
                    }),
                )
            self.readonly_fields = ["language", "text", "text_plural", "description"]
        else:
            self.fieldsets = (
                (None, {
                    'fields': ("language","description", 'text',),
                    }),
                (None, {
                    'fields': ("translated", ),
                    }),
                )
            self.readonly_fields = ["language", "text", "text_plural", "description"]

        if request.GET.get("lang", False):
            lang = "%s" % request.GET["lang"]
            try:
                text = Text.objects.get(
                    description=my_object.description,
                    text = my_object.text,
                    text_plural = my_object.text_plural,
                    language = Language.objects.get(code=lang),
                )
            except Text.DoesNotExist:
                text = Text(
                    description=my_object.description,
                    text = my_object.text,
                    text_plural = my_object.text_plural,
                    language = Language.objects.get(code=lang),
                ); text.save()

            if my_object.language.code != lang:
                return HttpResponseRedirect("%s%s" % (reverse("admin:trans_text_changelist"), text.id))

        return super(TextAdmin, self).change_view(request, object_id, form_url='', extra_context=None)

    def changelist_view(self, request, extra_context=None):
        if not Text.objects.all().count():
            call_command('locale_clear', interactive=False)
            call_command('locale_setup', interactive=False)
        return super(TextAdmin, self).changelist_view(request, extra_context)

    def queryset(self, request):
        qs = super(TextAdmin, self).queryset(request)
        lang = request.GET.get("lang", False)
        if lang:
            return qs
        else:
            try:
                if qs.language:
                    return qs
            except:
                if request.path != reverse("admin:trans_text_changelist"):
                    return qs
                else:
                    return qs.filter(language=Language.objects.get(code="en"))

    def has_delete_permission(self, request, obj=None):
        return False

admin.site.register(Text, TextAdmin)

