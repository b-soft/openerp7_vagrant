import errno, os
import shutil
from django.core.management import call_command
from settings import PROJECT_DIR

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST:
            pass
        else: raise


def generate_language_file(lang):
    project_root = "%s/apps/" % PROJECT_DIR
    dirs = os.listdir(project_root)
    for app in dirs:
        app_path = os.path.join(project_root, app)
        locale_path = os.path.join(app_path, "locale")

        if os.path.exists(locale_path):
            shutil.rmtree(locale_path)

        if os.path.exists(app_path) and app != "trans" and not "." in app:
            mkdir_p(locale_path)
            os.chdir(app_path)
            os.system("django-admin.py makemessages -l en")



# converting dictionaries into model objects
class Struct(object):
    def __init__(self, data):
        for name, value in data.iteritems():
            setattr(self, name, self._wrap(value))

    def _wrap(self, value):
        if isinstance(value, (tuple, list, set, frozenset)):
            return type(value)([self._wrap(v) for v in value])
        else:
            return Struct(value) if isinstance(value, dict) else value
