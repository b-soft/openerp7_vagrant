from django.core.management.base import BaseCommand
from trans.models import Text, Language

class Command(BaseCommand):

    def handle(self, *args, **options):
        text_list = Text.objects.filter(language=Language.objects.get(code="en"))
        for lang in Language.objects.all():
            print "Translating Locale for %s" % lang.name,
            for text in text_list:
                google_translated = lang.name; google_translated_plural = lang.code
                t = Text.objects.get_or_create(
                    text=text.text, description=text.description,
                    text_plural=text.text_plural, msgctxt = text.msgctxt,
                    language = lang
                )[0]
                my_object = Text.objects.get(id = t.id)

                my_object.translated = google_translated
                my_object.translated_plural = google_translated_plural
                my_object.save()
            print "Done!"