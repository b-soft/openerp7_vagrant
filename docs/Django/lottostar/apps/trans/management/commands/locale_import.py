import os
from django.core.management.base import BaseCommand
from django.conf import settings
from trans.functions import mkdir_p
from trans.models import Language, Text

class Command(BaseCommand):

    def import_file(self,  locale_path, lang):
        print "Setting Up:"
        mkdir_p(locale_path)
        if os.path.exists("%s/%s/LC_MESSAGES/django.po" % (locale_path, lang.code)):
            f = open("%s/%s/LC_MESSAGES/django.po" % (locale_path, lang.code))
            sections = ("%s" % f.read()).split("\n\n")

            for i, section in enumerate(sections):
                if i:
                    description = ""; text_plural = '';  text = ''; msgctxt = ""
                    translated = ''; translated_plural = ''
                    lines = section.split("\n")
                    has_hit_translation = False
                    for ll, line in enumerate(lines):
                        if line[:1] == "#":
                            description += line.strip().replace("\\n", "\n")[1:].strip(' \t\r')
                            if line[(ll + 1)][:1] == "#":
                                description += "\n"
                        if line[:14] == 'msgid_plural "':
                            text_plural = line.replace('msgid_plural "', '').strip()[:-1].strip(' \t\r')
                        if line[:7] == 'msgid "':
                            text += line.replace('msgid "', '').strip()[:-1].replace("\\n", "\n").strip(' \t\r')
                        if line[:1] == '"':
                            if not has_hit_translation:
                                text += line.strip()[1:-1].replace("\\n", "\n").strip(' \t\r')
                            else:
                                translated += line.strip()[1:-1].replace("\\n", "\n").strip(' \t\r')
                        if line[:9] == 'msgctxt "':
                            msgctxt = line.replace('msgctxt "', '').strip()[:-1].replace("\\n", "\n").strip(' \t\r')
                        if line[:8] == 'msgstr "':
                            has_hit_translation = True
                            translated = line.replace('msgstr "', '').strip()[:-1].replace("\\n", "\n").strip(' \t\r')
                        if line[:11] == 'msgstr[0] "':
                            translated = line.replace('msgstr[0] "', '').strip()[:-1].replace("\\n", "\n").strip(' \t\r')
                        if line[:11] == 'msgstr[1] "':
                            translated_plural = line.replace('msgstr[1] "', '').strip()[:-1].replace("\\n", "\n").strip(' \t\r')

                    try:
                        updated_text = Text.objects.get(
                            description=description.strip(), text=text.strip(),
                            language=lang
                        )
                        updated_text.translated = translated
                        updated_text.translated_plural = translated_plural
                        updated_text.save()
                    except:
                        if lang.code == "en":
                            print text.strip()
                            print description.strip()

    def handle(self, *args, **options):
        locale_path = "%s/locale/" % settings.PROJECT_DIR
        for lang in Language.objects.all():
            self.import_file(locale_path, lang)
