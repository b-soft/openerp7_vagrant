from django.utils.html import escape
import os, settings, shutil
from django.core.management.base import BaseCommand
import sys
from django.conf import settings
from trans.functions import mkdir_p
from trans.models import Language, Text
from django.core import management

class Command(BaseCommand):

    def check_none(self, value):
        if not value:
            return ""
        else:
            return value

    def write_file(self, locale_path, lang):
        f = open("%s/%s/LC_MESSAGES/django.po" % (locale_path, lang.code))
        document = ''
        sections = ("%s" % f.read()).split("\n\n")

        for i, section in enumerate(sections):
            if i:
                description = ""; text_plural = '';  text = ''; msgctxt = ''
                translated = ""; translated_plural = ""
                lines = section.split("\n")
                for ll, line in enumerate(lines):
                    if line[:1] == "#":
                        description += line.strip().replace("\\n", "\n")[1:].strip(' \t\r')
                        if line[(ll + 1)][:1] == "#":
                            description += "\n"
                    if line[:14] == 'msgid_plural "':
                        text_plural = line.replace('msgid_plural "', '').strip()[:-1].strip(' \t\r')
                    if line[:7] == 'msgid "':
                        text += line.replace('msgid "', '').strip()[:-1].replace("\\n", "\n").strip(' \t\r')
                    if line[:1] == '"':
                        text += line.strip()[1:-1].replace("\\n", "\n").strip(' \t\r')
                    if line[:9] == 'msgctxt "':
                        msgctxt = line.replace('msgctxt "', '').strip()[:-1].replace("\\n", "\n").strip(' \t\r')
                    if line[:11] == 'msgstr[0] "':
                        translated = line.replace('msgstr[0] "', '').strip()[:-1].replace("\\n", "\n").strip(' \t\r')
                    if line[:11] == 'msgstr[1] "':
                        translated_plural = line.replace('msgstr[1] "', '').strip()[:-1].replace("\\n", "\n").strip(' \t\r')

                try:
                    t = Text.objects.get(
                        description=description.strip(), text=text.strip(),
                        language=lang
                    )
                    t = {
                        "text": "%s" % self.check_none(t.text),
                        "description" : "%s" % self.check_none(t.description),
                        "text_plural": "%s" % self.check_none(t.text_plural),
                        "msgctxt": "%s" % self.check_none(t.msgctxt),
                        "language": lang,
                        "translated": self.check_none(t.translated),
                        "translated_plural": self.check_none(t.translated_plural),
                    }
                except Text.DoesNotExist:
                    t = {
                        "text": "%s" % text,
                        "description" : "%s" % description,
                        "text_plural": "%s" % text_plural,
                        "msgctxt": "%s" % msgctxt,
                        "language": lang,
                        "translated": translated,
                        "translated_plural": translated_plural,
                    }

                if t:
                    if t["description"].count("\n") > 1:
                        split_lines = ("%s" % t["description"]).split("\n")
                        for l, tline in enumerate(split_lines):
                            if tline.strip(" \n\t\r") != "":
                                document += "\n#%s" % tline.strip(" \n\r\t")

                    else:
                        document += "\n#%s" % t["description"].strip(" \n\r\t")

                    if t["msgctxt"]:
                        document += '\nmsgctxt "%s"' % t["msgctxt"]

                    if t["text"].count("\n") > 1:
                        split_lines = ("%s" % t["text"]).split("\n")
                        for l, tline in enumerate(split_lines):
                            if not l:
                                document += '\nmsgid "" \n'
                                document += '"%s\\n" \n' % tline
                            else:
                                document += '"%s\\n" \n' % tline
                        split_lines = ("%s" % t["translated"]).split("\n")
                        if t["translated"].count("\n") > 1:
                            for l, tline in enumerate(split_lines):
                                if not l:
                                    document += 'msgstr "" \n'
                                    document += '"%s\\n" \n' % escape(tline.strip(" \n\r"))
                                else:
                                    document += '"%s\\n" \n' % escape(tline.strip(" \n\r"))
                        else:
                            document += 'msgstr "%s" \n' % escape(t["translated"].strip("\n\r"))
                    else:
                        if t["text_plural"]:
                            document += '\nmsgid "%s" \n' % ("%s" % t["text"]).strip("\n\t\r ")
                            document += 'msgid_plural "%s" \n' % ("%s" % t["text_plural"]).strip("\n\t\r ")
                            document += 'msgstr[0] "%s" \n' % escape(("%s" % t["translated"]).strip("\n\t\r "))
                            document += 'msgstr[1] "%s" \n' % escape(("%s" % t["translated_plural"]).strip("\n\t\r "))
                        else:
                            document += '\nmsgid "%s" \n' % ("%s" % t["text"]).strip(" \n\t\r").replace("\n", "")
                            document += 'msgstr "%s" \n' % escape(("%s" % t["translated"]).strip(" \n\t\r").replace("\n", ""))
            else:
                document = section + "\n"

        f.close()

        f = open("%s/%s/LC_MESSAGES/django.po" % (locale_path, lang.code), "w")
        f.write(document)
        f.close()

    def handle(self, *args, **options):
        locale_path = "%s/locale/" % settings.PROJECT_DIR

        if os.path.exists(locale_path):
            shutil.rmtree(locale_path)

        mkdir_p(locale_path)

        for lang in Language.objects.all():
            print "WRITING %s LOCALE PO FILES" % lang.name.upper()
            os.system("django-admin.py makemessages -l %s" % lang.code)
            self.write_file(locale_path, lang)

        print "COMPILING LOCALE MO FILES"

        os.environ["PYTHONPATH"] = settings.PROJECT_ROOT
        os.environ["DJANGO_SETTINGS_MODULE"] = "%s.settings" % settings.PROJECT_FOLDER

        os.system("django-admin.py compilemessages")

        print "COMPLETE!"
        print "-----------------------------------------------------------------"