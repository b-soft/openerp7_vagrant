import shutil
import os
from django.core.management.base import BaseCommand
from django.conf import settings
from trans.models import Language, Text

class Command(BaseCommand):

    def handle(self, *args, **options):
        project_root = "%s/apps/" % settings.PROJECT_DIR
        dirs = os.listdir(project_root)

        Text.objects.all().delete()
        Language.objects.all().delete()
        for lang in settings.LANGUAGES:
            Language.objects.get_or_create(name=lang[1].title(), code=lang[0])

        c = 0
        for app in dirs:
            app_path = os.path.join(project_root, app)
            locale_path = os.path.join(app_path, "locale")

            if os.path.exists(app_path) and app != "trans" and not "." in app:

                if os.path.exists(locale_path):
                    shutil.rmtree(locale_path)
                print "Deleted %s 'locale' folder" % app