from django.core.management import call_command
import os, sys
from django.core.management.base import BaseCommand
from django.conf import settings
from trans.functions import mkdir_p
from trans.models import Language, Text

class Command(BaseCommand):

    def handle(self, *args, **options):
        Text.objects.all().delete()
        for lang in settings.LANGUAGES:
            Language.objects.get_or_create(name=lang[1].title(), code=lang[0])

        locale_path = "%s/locale/" % settings.PROJECT_DIR
        mkdir_p(locale_path)

        if os.path.exists("%s/en/LC_MESSAGES/django.po" % locale_path):
            f = open("%s/en/LC_MESSAGES/django.po" % locale_path)

            sections = ("%s" % f.read()).split("\n\n")

            for i, section in enumerate(sections):
                if i:
                    description = ""
                    text_plural = ""
                    text = ""
                    msgctxt = ""
                    translated = ""
                    translated_plural = ""

                    lines = section.split("\n")
                    for ll, line in enumerate(lines):
                        if line[:1] == "#":
                            description += line.strip().replace("\\n", "\n")[1:].strip(' \t\r')
                            if line[(ll + 1)][:1] == "#":
                                description += "\n"
                        if line[:14] == 'msgid_plural "':
                            text_plural = line.replace('msgid_plural "', '').strip()[:-1].strip(' \t\r')
                        if line[:7] == 'msgid "':
                            text += line.replace('msgid "', '').strip()[:-1].replace("\\n", "\n").strip(' \t\r')
                        if line[:1] == '"':
                            text += line.strip()[1:-1].replace("\\n", "\n").strip(' \t\r')
                        if line[:9] == 'msgctxt "':
                            msgctxt = line.replace('msgctxt "', '').strip()[:-1].replace("\\n", "\n").strip(' \t\r')

                    Text.objects.get_or_create(
                        description=description.strip(), text=text.strip(), text_plural=text_plural.strip(), msgctxt = msgctxt.strip(),
                        language=Language.objects.get(code="en"),
                        defaults = {
                            "translated":translated,
                            "translated_plural": translated_plural
                        }
                    )
        else:
            os.chdir(locale_path)
            os.system("django-admin.py makemessages -l en --pythonpath=%s" % settings.PROJECT_DIR)

