from django.conf.urls import *
from apps.trans import views

urlpatterns = patterns('',
    url('^admin/trans/build/(?P<slug>[a-zA-Z0-9_.-]+)/$', views.GenerateView.as_view(), name='generate'),
)

