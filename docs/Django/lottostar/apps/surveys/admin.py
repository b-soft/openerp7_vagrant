from django.contrib import admin
from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.utils.text import truncate_words
from reversion import VersionAdmin
from flutter_modeladmin import RemoveDeleteAdmin

from .models import Survey, SurveyTranslation, SurveyQuestion, SurveyQuestionTranslation #, SurveyAnswer, SurveyCustomer


def get_short_text(obj):
    return truncate_words(obj.get_translation().short_text, 10)
get_short_text.short_description = "English Short Text"

'''

Related actions for language tag

'''


def survey_controls(obj):
    html = '<div style="width:340px;height:25px; line-height: 25px">'
    for lang in settings.LANGUAGES:
        html += '<a style="margin:5px" href="/admin/translate/survey/{0}/?language={1}" title="{2}"><img src="/static/img/flags/icons/{3}.png"/></a>'.format(obj.id, lang[0], lang[1].title(), lang[0])
    html += "</div>"
    return html
survey_controls.short_description = 'Languages'
survey_controls.allow_tags = True

def surveyquestion_controls(obj):
    html = '<div style="width:340px;height:25px; line-height: 25px">'
    for lang in settings.LANGUAGES:
        html += '<a style="margin:5px" href="/admin/translate/surveyquestion/{0}/?language={1}" title="{2}"><img src="/static/img/flags/icons/{3}.png"/></a>'.format(obj.id, lang[0], lang[1].title(), lang[0])
    html += "</div>"
    return html
surveyquestion_controls.short_description = 'Languages'
surveyquestion_controls.allow_tags = True

'''

Related actions

'''

#Question action for survey
# def related_question(obj):
#     my_urlsQ = ""
#     if not obj.survey_set.all().count():
#         my_urlsQ += '<a class="admin_surveyquestion" href="/admin/surveys/surveyquestion/add/?survey=%s"><img src="/static/img/icons/comment.png" alt="surveys" title="surveys"/></a>' % obj.id
#     else:
#         my_urlsQ += '<a class="admin_surveyquestion" href="/admin/surveys/surveyquestion/?survey__id__exact=%s"><img src="/static/img/icons/comment.png" alt="surveys" title="surveys"/></a>' % obj.id
#
#     return my_urlsQ
# related_question.short_description = 'Questions'
# related_question.allow_tags = True


# Answer action for survey
def related_answer(obj):
    my_urlsA = ""
    if not obj.survey_set.all().count():
        my_urlsA += '<a class="admin_surveyanswer" href="/admin/surveys/surveyanswer/add/?surveyanswer=%s"><img src="/static/img/icons/accept.png" alt="surveys" title="surveys"/></a>' % obj.id
    else:
        my_urlsA += '<a class="admin_surveyanswer" href="/admin/surveys/surveyanswer/?surveyanswer__id__exact=%s"><img src="/static/img/icons/accept.png" alt="surveys" title="surveys"/></a>' % obj.id

    return my_urlsA
related_answer.short_description = 'Answers'
related_answer.allow_tags = True


# Answer action for survey's question
def related_answerQ(obj):
    my_urlsA = ""
    if not obj.survey.surveyquestion_set.all().count():
        my_urlsA += '<a class="admin_surveyanswer" href="/admin/surveys/surveyanswer/add/?surveyanswer=%s"><img src="/static/img/icons/accept.png" alt="surveys" title="surveys"/></a>' % obj.id
    else:
        my_urlsA += '<a class="admin_surveyanswer" href="/admin/surveys/surveyanswer/?surveyanswer__id__exact=%s"><img src="/static/img/icons/accept.png" alt="surveys" title="surveys"/></a>' % obj.id

    return my_urlsA
related_answerQ.short_description = 'Answers'
related_answerQ.allow_tags = True


#Question action for answer's survey
def related_questionAn(obj):
    my_urlsQ = ""
    #obj is QuestionAnswer instance
    my_urlsQ += '<a class="admin_surveyquestion" href="/admin/surveys/surveyquestion/?surveyquestion__id__exact=%s"><img src="/static/img/icons/comment.png" alt="surveys" title="surveys"/></a>' % obj.question.id
    return my_urlsQ
related_questionAn.short_description = 'Questions'
related_questionAn.allow_tags = True


#Survey action for ...
def related_survey(obj):
    my_urlsQ = ""
    #obj is SurveyAnswer and SurveyCustomer instance
    #my_urlsQ += '<a class="admin_survey" href="/admin/surveys/survey/?survey__id__exact=%s"><img src="/static/img/icons/overlays.png" alt="surveys" title="surveys"/></a>' % obj.survey.id
    ###
    if obj.survey.surveyquestion_set.all().count():
        my_urlsQ += '<a class="admin_survey" href="/admin/surveys/survey/%s"><img src="/static/img/icons/comment.png" alt="surveys" title="surveys"/></a>' % obj.survey.id
    else:
        my_urlsQ += '<a class="admin_survey" href="/admin/surveys/survey/?surveyquestion__id__exact=%s"><img src="/static/img/icons/comment.png" alt="surveys" title="surveys"/></a>' % obj.survey.id
    ###
    return my_urlsQ
related_survey.short_description = 'Surveys'
related_survey.allow_tags = True


#Question action for survey's customer
def related_questionC(obj):
    my_urlsQ = ""
    if not obj.survey.surveycustomer_set.all().count():
        my_urlsQ += '<a class="admin_surveyquestion" href="/admin/surveys/surveyquestion/add/?surveyquestion=%s"><img src="/static/img/icons/comment.png" alt="surveys" title="surveys"/></a>' % obj.id
    else:
        my_urlsQ += '<a class="admin_surveyquestion" href="/admin/surveys/surveyquestion/?surveyquestion__id__exact=%s"><img src="/static/img/icons/comment.png" alt="surveys" title="surveys"/></a>' % obj.id

    return my_urlsQ
related_questionC.short_description = 'Questions'
related_questionC.allow_tags = True


# Answer action for survey's customer
def related_answerC(obj):
    my_urlsA = ""
    if not obj.survey.surveycustomer_set.all().count():
        my_urlsA += '<a class="admin_surveyanswer" href="/admin/surveys/surveyanswer/add/?surveyanswer=%s"><img src="/static/img/icons/accept.png" alt="surveys" title="surveys"/></a>' % obj.id
    else:
        my_urlsA += '<a class="admin_surveyanswer" href="/admin/surveys/surveyanswer/?surveyanswer__id__exact=%s"><img src="/static/img/icons/accept.png" alt="surveys" title="surveys"/></a>' % obj.id

    return my_urlsA
related_answerC.short_description = 'Answers'
related_answerC.allow_tags = True


# Customer action for survey
def related_customer(obj):
    my_urlsA = ""
    if not obj.survey_set.all().count():
        my_urlsA += '<a class="admin_surveycustomer" href="/admin/surveys/surveycustomer/add/?surveycustomer=%s"><img src="/static/img/icons/book_open.png" alt="surveys" title="surveys"/></a>' % obj.id
    else:
        my_urlsA += '<a class="admin_surveycustomer" href="/admin/surveys/surveycustomer/?surveycustomer__id__exact=%s"><img src="/static/img/icons/book_open.png" alt="surveys" title="surveys"/></a>' % obj.id

    return my_urlsA
related_customer.short_description = 'Customers'
related_customer.allow_tags = True


# Customer action for survey
def related_customerQA(obj):
    my_urlsA = ""
    #obj is SurveyQuestion and SurveyAnswer instance
    my_urlsA += '<a class="admin_surveycustomer" href="/admin/surveys/surveycustomer/?surveycustomer__id__exact=%s"><img src="/static/img/icons/book_open.png" alt="surveys" title="surveys"/></a>' % obj.author.id
    return my_urlsA
related_customerQA.short_description = 'Customers'
related_customerQA.allow_tags = True


class SurveyTextAdmin(RemoveDeleteAdmin, VersionAdmin):
    model = SurveyTranslation
    list_display = ["model", "language"]
    readonly_fields = ["model", "language"]
    fieldsets = (
        (None, {
            'fields': ('model', "language")
        }),
        ("Details", {
            'fields': ('title', 'short_text', 'meta_title', 'meta_keywords', 'meta_description',)  # 'name', "header_text")
        }),
    )

    def get_model_perms(self, request):
        return {}

    def changelist_view(self, request, extra_context=None):
        super(SurveyTextAdmin, self).changelist_view(request, extra_context=None)
        return HttpResponseRedirect(reverse("admin:surveys_survey_changelist"))

admin.site.register(SurveyTranslation, SurveyTextAdmin)


class SurveyInline(admin.StackedInline):
    model = SurveyTranslation
    extra = 1
    max_num = 1

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'language':
            kwargs['initial'] = "en"
        return super(SurveyInline, self).formfield_for_dbfield(db_field, **kwargs)


class SurveyAdmin(RemoveDeleteAdmin, VersionAdmin):
    def related_question(obj):
        my_urlsQ = ""
        if not obj.surveyquestion_set.all().count():
            my_urlsQ += '<a class="admin_surveyquestion" href="/admin/surveys/surveyquestion/add/?survey=%s"><img src="/static/img/icons/comment.png" alt="surveys" title="surveys"/></a>' % obj.id
        else:
            my_urlsQ += '<a class="admin_surveyquestion" href="/admin/surveys/surveyquestion/?survey__id__exact=%s"><img src="/static/img/icons/comment.png" alt="surveys" title="surveys"/></a>' % obj.id
        return my_urlsQ
    related_question.short_description = 'Questions'
    related_question.allow_tags = True


    # def change_view(self, request, object_id, form_url='', extra_context=None):
    #     extra_context = extra_context or {}
    #     if request.session.get("admin_survey_id", False):
    #         extra_context["survey"] = Survey.objects.get(id=request.session["admin_survey_id"])
    #     return super(SurveyQuestionAdmin, self).change_view(request, object_id, form_url, extra_context=extra_context)


    # def change_view(self, request, object_id, extra_context=None):
    #
    #     question = super(SurveyAdmin, self).change_view(request, object_id, extra_context)
    #
    #     survey = Survey.objects.get(id__exact=object_id)
    #
    #     if not request.POST.has_key('_addanother') and not request.POST.has_key('_continue'):
    #         question['Location'] = survey.get_absolute_url()
    #     return question

    list_display = ('name', get_short_text, survey_controls, related_question,) # TODO later on, add: related_customer, related_answer,
    inlines = [SurveyInline, ]

    fieldsets = (
        ('Heading', {
            'fields': ('survey_parent', 'name', )
        }),

        ('Details', {
            'fields': ('is_active', 'is_public', 'require_login', 'start', 'end',)
        }),

        ('Reward', {
            'fields': ('lottery', 'vouchers',)
        }),

        # ('Meta', {
        #     'fields': ('meta_title', 'meta_keywords', 'meta_description',)
        # }),
    )

    def get_inline_instances(self, request, obj=None):
        if obj:
            return []
        return super(SurveyAdmin, self).get_inline_instances(request, obj)

admin.site.register(Survey, SurveyAdmin)

###
class SurveyQuestionTextAdmin(RemoveDeleteAdmin, VersionAdmin):
    model = SurveyQuestionTranslation
    list_display = ["model", "language"]
    readonly_fields = ["model", "language"]
    fieldsets = (
        (None, {
            'fields': ('model', "language")
        }),
        ("Details", {
            'fields': ('content', 'choices',)  # 'name', "header_text")
        }),
    )

    def get_model_perms(self, request):
        return {}

    def changelist_view(self, request, extra_context=None):
        super(SurveyQuestionTextAdmin, self).changelist_view(request, extra_context=None)
        return HttpResponseRedirect(reverse("admin:surveys_surveyquestion_changelist"))

admin.site.register(SurveyQuestionTranslation, SurveyQuestionTextAdmin)


class SurveyQuestionInline(admin.StackedInline):
    model = SurveyQuestionTranslation
    extra = 1
    max_num = 1

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'language':
            kwargs['initial'] = "en"
        return super(SurveyQuestionInline, self).formfield_for_dbfield(db_field, **kwargs)


# def get_question(obj):
#     translated = SurveyQuestionTranslation.objects.get(model=obj, language="en")
#     return translated.content
# get_question.short_description = 'Question'


def get_question(obj):
    return truncate_words(obj.get_translation().content, 10)
get_question.short_description = "Question"


class SurveyQuestionAdmin(RemoveDeleteAdmin, VersionAdmin):
    list_display = ('get_survey_name', get_question, 'qtype', surveyquestion_controls,) # TODO later on, add: 'position', related_survey, related_customer, related_answer, related_survey
    inlines = [SurveyQuestionInline, ]
    readonly_fields = ["survey"]

    def get_survey_name(self, obj):
        return '%s' % obj.survey.name
    get_survey_name.short_description = "Survey's name"

    fieldsets = (
        ('Question', {
            'fields': ('survey', 'qtype', 'html_style', 'is_required', 'show_on_report',)   #'position',
        }),
    )

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        if request.session.get("admin_survey_id", False):
            extra_context["survey"] = Survey.objects.get(id=request.session["admin_survey_id"])
        return super(SurveyQuestionAdmin, self).change_view(request, object_id, form_url, extra_context=extra_context)

    def add_view(self, request, form_url='', extra_context=None):
        extra_context = extra_context or {}
        if request.GET.get("survey", False):
            request.session["admin_survey_id"] = request.GET["survey"]
            extra_context["survey"] = Survey.objects.get(id=request.GET["survey"])
        else:
            if request.session.get("admin_survey_id", False):
                extra_context["survey"] = Survey.objects.get(id=request.session["admin_survey_id"])
        return super(SurveyQuestionAdmin, self).add_view(request=request, form_url=form_url, extra_context=extra_context)

    def get_changelist(self, request, **kwargs):
        if request.GET.get("survey__id__exact", False):
            request.session["admin_survey_id"] = request.GET["survey__id__exact"]
        return super(SurveyQuestionAdmin, self).get_changelist(request, **kwargs)

    def queryset(self, request):
        if request.session.get("admin_survey_id", False):
            return SurveyQuestion.objects.filter(survey_id = request.session["admin_survey_id"])
        else:
            return super(SurveyQuestionAdmin, self).queryset(request)

    def save_model(self, request, obj, form, change):
        survey_id = request.session.get("admin_survey_id", False)
        if survey_id:
            obj.survey = Survey.objects.get(id=survey_id)
        return super(SurveyQuestionAdmin, self).save_model(request, obj, form, change)

    def get_inline_instances(self, request, obj=None):
        if obj:
            return []
        return super(SurveyQuestionAdmin, self).get_inline_instances(request, obj)

admin.site.register(SurveyQuestion, SurveyQuestionAdmin)

###

# class SurveyQuestionAdmin(admin.ModelAdmin):
#     list_display = ('content', 'get_survey_name', 'qtype', 'position', related_survey,) # TODO later on, add:  related_customerQA, related_answerQ,
#     #list_filter = ('content',)
#
#     def get_survey_name(self, obj):
#         return '%s' % obj.survey.name
#     get_survey_name.short_description = "Survey's name"
#
#     #readonly_fields = ['customer']
#     fieldsets = (
#         ('Question', {
#             'fields': ('survey', 'qtype', 'content', 'choices', 'html_style', 'position', 'is_required', 'show_on_report',)
#         }),
#     )


# class SurveyChoiceAdmin(admin.ModelAdmin):
#     fieldsets = (
#         ('Choice', {
#             'fields': ('question', 'text', 'order',)
#         }),
#     )


# class SurveyAnswerAdmin(admin.ModelAdmin):
#     # def related_question(obj):
#     #     return obj.question.content
#     # related_question.short_description = 'Question'
#     list_display = ('answer', 'question', related_survey, related_questionAn) #, related_customerQA
#     list_filter = ('question',)
#     #readonly_fields = ['customer']
#     fieldsets = (
#         ('Survey Answer', {
#             'fields': ('survey', 'question', 'answer',)
#         }),
#     )
#
#
# class SurveyCustomerAdmin(admin.ModelAdmin):
#     # def related_question(obj):
#     #     return obj.question.content
#     # related_question.short_description = 'Question'
#     list_display = ('customer_email_address', related_survey, related_questionC, related_answerC)
#     list_filter = ('customer_email_address',)
#     fieldsets = (
#         ('Survey Customer', {
#             'fields': ('survey', 'customer_email_address',)
#         }),
#     )

#admin.site.register(SurveyQuestion, SurveyQuestionAdmin)
# admin.site.register(SurveyAnswer, SurveyAnswerAdmin)
# admin.site.register(SurveyCustomer, SurveyCustomerAdmin)