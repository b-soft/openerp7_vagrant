from django import forms
from django.forms import models
from django.forms import ModelForm
from django.utils.safestring import mark_safe
import uuid

from .models import Survey, SurveyQuestion #, SurveyAnswer, SurveyCustomer, AnswerTextInput, AnswerRadioList, \
    #AnswerSelectOneChoice, AnswerTextArea, AnswerCheckboxList


class SurveyQuestionForm(ModelForm):
    class Meta:
        model = SurveyQuestion
        fields = ()

    def __init__(self, *args, **kwargs):
    # expects a survey object to be passed in initially
        survey = kwargs.pop('survey')
        self.survey = survey
        super(SurveyQuestionForm, self).__init__(*args, **kwargs)
        #self.uuid = random_uuid = uuid.uuid4().hex

        # add a field for each survey question, corresponding to the question
        # type as appropriate.
        data = kwargs.get('data')
        for q in survey.questions():
            if q.qtype == SurveyQuestion.TYPE_CHOICES.text_input:
                self.fields["surveyquestion_%d" % q.pk] = forms.CharField(label=q.content, widget=forms.TextInput())
            elif q.qtype == SurveyQuestion.TYPE_CHOICES.text_area:
                self.fields["surveyquestion_%d" % q.pk] = forms.CharField(label=q.content, widget=forms.Textarea())
            elif q.qtype == SurveyQuestion.TYPE_CHOICES.select_one_choice:
                question_choices = q.get_choices()
                # add an empty option at the top so that the user has to
                # explicitly select one of the options
                question_choices = tuple([('', '-------------')]) + question_choices
                self.fields["surveyquestion_%d" % q.pk] = forms.ChoiceField(label=q.content, widget=forms.Select,
                                                                            choices=question_choices)
            elif q.qtype == SurveyQuestion.TYPE_CHOICES.radio_list:
                question_choices = q.get_choices()
                self.fields["surveyquestion_%d" % q.pk] = forms.ChoiceField(label=q.content, widget=forms.RadioSelect(),
                                                                            choices=question_choices)
            elif q.qtype == SurveyQuestion.TYPE_CHOICES.checkbox_list:
                question_choices = q.get_choices()
                self.fields["surveyquestion_%d" % q.pk] = forms.MultipleChoiceField(label=q.content,
                                                    widget=forms.CheckboxSelectMultiple(), choices=question_choices)
            # if the field is required, give it a corresponding css class.
            # if q.required:
            # 	self.fields["surveyquestion_%d" % q.pk].required = True
            # 	self.fields["surveyquestion_%d" % q.pk].widget.attrs["class"] = "required"
            # else:
            # 	self.fields["surveyquestion_%d" % q.pk].required = False
            #
            # # add the category as a css class, and add it as a data attribute
            # # as well (this is used in the template to allow sorting the
            # # questions by category)
            # if q.category:
            # 	classes = self.fields["surveyquestion_%d" % q.pk].widget.attrs.get("class")
            # 	if classes:
            # 		self.fields["surveyquestion_%d" % q.pk].widget.attrs["class"] = classes + (" cat_%s" % q.category.name)
            # 	else:
            # 		self.fields["surveyquestion_%d" % q.pk].widget.attrs["class"] = (" cat_%s" % q.category.name)
            # 	self.fields["surveyquestion_%d" % q.pk].widget.attrs["category"] = q.category.name

            # initialize the form field with values from a POST request, if any.
            if data:
                self.fields["surveyquestion_%d" % q.pk].initial = data.get('surveyquestion_%d' % q.pk)

    def save(self, commit=True):
        # save the response object
        response = super(SurveyQuestionForm, self).save(commit=False)
        response.survey = self.survey
        #response.interview_uuid = self.uuid
        response.save()

        # create an answer object for each question and associate it with this
        # response.
        for field_name, field_value in self.cleaned_data.iteritems():
            if field_name.startswith("surveyquestion_"):
                # warning: this way of extracting the id is very fragile and
                # entirely dependent on the way the question_id is encoded in the
                # field name in the __init__ method of this form class.
                q_id = int(field_name.split("_")[1])
                q = SurveyQuestion.objects.get(pk=q_id)

                if q.qtype == SurveyQuestion.TYPE_CHOICES.text_input:
                    a = AnswerTextInput(surveyquestion=q)
                    a.body = field_value
                elif q.qtype == SurveyQuestion.TYPE_CHOICES.text_area:
                    a = AnswerTextArea(surveyquestion = q)
                    a.body = field_value
                elif q.qtype == SurveyQuestion.TYPE_CHOICES.select_one_choice:
                    a = AnswerSelectOneChoice(surveyquestion = q)
                    a.body = field_value
                elif q.qtype == SurveyQuestion.TYPE_CHOICES.radio_list:
                    a = AnswerRadioList(surveyquestion = q)
                    a.body = field_value
                elif q.qtype == SurveyQuestion.TYPE_CHOICES.checkbox_list:
                    a = AnswerCheckboxList(surveyquestion = q)
                    a.body = field_value
                print "creating answer to survey's question %d of type %s" % (q_id, a.surveyquestion.qtype)
                print a.surveyquestion.content
                print 'answer value:'
                print field_value
                a.response = response
                a.save()
        return response

# class SurveyQuestionForm(ModelForm):
#     class Meta:
#         model = SurveyQuestion
#         fields = ('qtype',) #'content', 'choices',)

#     text_input = forms.CharField(max_length=255)
#     text_area = forms.TextInput()
#     select_one_choice = forms.ChoiceField(choices=[(x, x) for x in range(1, 32)])
#         # radio_list
#         # radio_image_list
#         # checkbox_list
#
#
# class SurveyChoiceForm(ModelForm):
#     class Meta:
#         model = SurveyChoice
#         fields = ('text', 'order',)
#
#
# class SurveyAnswerForm(ModelForm):
#     class Meta:
#         model = SurveyAnswer
#         fields = ('answer',)
#
