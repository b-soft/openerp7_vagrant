from django.conf.urls.defaults import *

urlpatterns = patterns('apps.surveys.views',
                       url(r'^survey$', 'survey_list', name='survey_list'),
                       url(r'^admin/translate/survey/(?P<survey_id>[a-zA-Z0-9_.-]+)/$', "admin_survey_text", name="admin_survey_text"),
                       url(r'^admin/translate/surveyquestion/(?P<surveyquestion_id>[a-zA-Z0-9_.-]+)/$', "admin_surveyquestion_text", name="admin_surveyquestion_text"),
                       url(r'^survey/create/(?P<survey_id>\d+)/$', 'create_survey', name='create_survey'),
                       #url(r'^survey/questions$', 'create_survey_question', name='create_surveyquestion'),
                       #url(r'^survey/choice$', 'create_survey_question_choice', name='create_surveychoice'),
                       #url(r'^survey/answers$', 'create_survey_question_answer', name='create_surveyanswer'),
                       )
