from django.core.urlresolvers import reverse
from django.views.generic import ListView, CreateView
from django.http import HttpResponseRedirect
from .models import SurveyTranslation, Survey, SurveyQuestion, SurveyQuestionTranslation #, SurveyAnswer
from .forms import SurveyQuestionForm#, SurveyQuestionForm, SurveyAnswerForm


def admin_survey_text(request, survey_id):
    survey = Survey.objects.get(pk = int(survey_id))
    translation = SurveyTranslation.objects.get_or_create(model = survey, language=request.GET.get("language", "en"))
    return HttpResponseRedirect("/admin/surveys/surveytranslation/%s/" % translation[0].id)


def admin_surveyquestion_text(request, surveyquestion_id):
    surveyquestion = SurveyQuestion.objects.get(pk = int(surveyquestion_id))
    translation = SurveyQuestionTranslation.objects.get_or_create(model = surveyquestion, language=request.GET.get("language", "en"))
    return HttpResponseRedirect("/admin/surveys/surveyquestiontranslation/%s/" % translation[0].id)


class SurveyList(ListView):
    model = Survey
    template_name = "survey/survey_list.html"
    context_object_name = "survey_list"


class SurveyCreateView(CreateView):
    model = Survey
    template_name = "survey/create_survey.html"

    def get_form(self, form_class):
        survey_id = self.kwargs['survey_id']
        survey = Survey.objects.get(id=survey_id)
        return SurveyQuestionForm(survey=survey)

    def get_success_url(self):
        return reverse('survey_complete')

    # def get_context_data(self, **kwargs):
    #     context = super(SurveyCreateView, self).get_context_data(**kwargs)
    #     survey_id = self.kwargs['survey_id']
    #     survey = Survey.objects.get(id=survey_id)
    #     context['survey'] = survey
    #     return survey

# class SurveyQuestionCreateView(CreateView):
#     model = SurveyQuestion
#     form_class = SurveyQuestionForm
#     template_name = "survey/create_surveyquestion.html"


# class SurveyChoiceCreateView(CreateView):
#     model = SurveyChoice
#     form_class = SurveyChoiceForm
#     template_name = "survey/create_surveychoice.html"


# class SurveyAnswerCreateView(CreateView):
#     model = SurveyAnswer
#     form_class = SurveyAnswerForm
#     template_name = "survey/create_surveyanswer.html"

survey_list = SurveyList.as_view()
create_survey = SurveyCreateView.as_view()
#create_survey_question = SurveyQuestionCreateView.as_view()
#create_survey_question_choice = SurveyChoiceCreateView.as_view()
#create_survey_question_answer = SurveyAnswerCreateView.as_view()



        #
        # def get_success_url(self):
        #     return reverse('survey_complete')

# create_survey_question_answer = SurveyAnswerCreateView.as_view()
#
# def get_success_url(self):
#     return reverse('survey_complete')

