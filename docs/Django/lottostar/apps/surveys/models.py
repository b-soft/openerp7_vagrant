from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from translatable.models import TranslatableModel, get_translation_model
from model_utils import Choices
from model_utils.models import TimeFramedModel, TimeStampedModel
from autoslug.fields import AutoSlugField

from apps.game.models import Game


# T = 'text_input'
# A = 'text_area'
# S = 'select_one_choice'
# R = 'radio_list'
# C = 'checkbox_list'

# TYPE_CHOICES = Choices(
#     (T, 'Text Input'),
#     (A, 'Text Area'),
#     (S, 'Select One Choice'),
#     (R, 'Radio List'),
#     (C, 'Checkbox List'),
# )


class Survey(TranslatableModel, TimeFramedModel, TimeStampedModel):
    #admin = models.ForeignKey(User)
    name = models.CharField("survey's name", max_length=255)
    slug = AutoSlugField(populate_from='name')
    is_active = models.BooleanField('Is active', default=True, blank=True)
    is_public = models.BooleanField(default=False, blank=True)
    survey_parent = models.ForeignKey('Survey', null=True, blank=True)
    require_login = models.BooleanField('Require login', default=False)
    lottery = models.ForeignKey(Game, null=True, blank=True)
    vouchers = models.PositiveIntegerField(null=True, blank=True)

    #short_text = models.TextField('summary', max_length=500, blank=True) # drop
    #meta_title = models.CharField('Meta title', max_length=255, blank=True) # drop
    #meta_keywords = models.CharField('Meta keywords', max_length=255, blank=True) # drop
    #meta_description = models.CharField('Meta description', max_length=255, blank=True) #drop
    #header_text = models.TextField('Header text', max_length=500, blank=True) #drop

#   created_by = models.ForeignKey(User) # only updates on create.

    class Meta:
        ordering = ["name"]
        verbose_name = "Survey"
        verbose_name_plural = "Surveys"


    def __unicode__(self):
        return self.name

    def questions(self):
        return self.surveyquestion_set.all()


class SurveyTranslation(get_translation_model(Survey, "survey"), TimeStampedModel):
    title = models.CharField(max_length=500, blank=True) # force to add english  by default on create
#    summary = models.TextField('Short text', max_length=500, blank=True)
    #name = models.CharField('survey name', max_length=255)
    short_text = models.TextField('summary', max_length=500, blank=True)
    meta_title = models.CharField('Meta title', max_length=255, blank=True)
    meta_keywords = models.CharField('Meta keywords', max_length=255, blank=True)
    meta_description = models.CharField('Meta description', max_length=255, blank=True)

    #short_text = models.TextField('Short text', max_length=500, blank=True) # drop
    #header_text = models.TextField('Header text', max_length=500, blank=True) # drop

    class Meta:
        verbose_name = "Survey translation"
        verbose_name_plural = "Survey translations"

    def __unicode__(self):
        return self.model.name


def validate_list(value):
    '''
    takes a text value and verifies that there is at least one comma
    '''
    values = value.split(',')
    if len(values) < 2:
        raise ValidationError("The selected field requires an associated list of choices. Choices must contain more than one item.")

# def validate_list(value):
#     '''
#     takes a text value and verifies that there is at least one comma
#     '''
#     if not value:
#         return []
#     if len(value.split(',')) < 2:
#         raise ValidationError("The selected field requires an associated list of choices. Choices must contain more than one item.")
#     return value.split(',')


class SurveyQuestion(TranslatableModel):
    TYPE_CHOICES = Choices(
        (1, 'text_input', 'Text Input'),
        (2, 'text_area', 'Text Area'),
        (3, 'select_one_choice', 'Select One Choice'),
        (4, 'radio_list', 'Radio List'),
        (5, 'checkbox_list', 'Checkbox List'),
    )
    #
    # T = 'text_input'
    # A = 'text_area'
    # S = 'select_one_choice'
    # R = 'radio_list'
    # C = 'checkbox_list'
    #
    # TYPE_CHOICES = Choices(
    # (T, 'Text Input'),
    # (A, 'Text Area'),
    # (S, 'Select One Choice'),
    # (R, 'Radio List'),
    # (C, 'Checkbox List'),
    # )

    survey = models.ForeignKey(Survey)
    #author = models.ForeignKey(User, null=True, blank=True)
    qtype = models.PositiveSmallIntegerField('question type', choices=TYPE_CHOICES, db_index=True)
    #slug = AutoSlugField(populate_from='qtype')
    #content = models.TextField('Question', blank=True)
    choices = models.TextField('Response(s)', null=True, blank=True,
                               help_text='if the question type is "Radio List," "Select One Choice," or '
                                         '"Checkbox List" provide a comma-separated list of options for this question.')
    #TODO: check how this is implemented on http://dev.playhugelottos.com/, and check tinymce
    html_style = models.CharField(max_length=255, blank=True)
    #position = models.PositiveIntegerField()
    is_required = models.BooleanField(default=True, blank=True)
    show_on_report = models.BooleanField(default=True, blank=True)

    # def save(self, *args, **kwargs):
    #     if self.qtype == SurveyQuestion.TYPE_CHOICES.radio_list or self.qtype == SurveyQuestion.TYPE_CHOICES.select_one_choice \
    #         or self.qtype == SurveyQuestion.TYPE_CHOICES.checkbox_list:
    #         #import pdb; pdb.set_trace()
    #         validate_list(self.choices)
    #         super(SurveyQuestion, self).save(*args, **kwargs)

    def get_choices(self):

        '''
        parse the choices field and return a tuple formatted appropriately for the 'choices' argument of a form widget.
        '''


        # if not self:
        #     return []

        choices = self.choices.split(',')
        choices_list = []
        for c in choices:
            c = c.strip()
            choices_list.append((c,c))
        choices_tuple = tuple(choices_list)
        return choices_tuple

    class Meta:
        #ordering = ["content"]
        verbose_name = "Survey Question"
        verbose_name_plural = "Survey Questions"

    # def __unicode__(self):
    #     return self.get_translation(language="en").content

# class SurveyChoice(models.Model):
#     question = models.ForeignKey(SurveyQuestion)
#     text = models.CharField('choice text', max_length=500)
#     # TODO: Add a button or check box to remove the file. There are several
#     # recipes floating on internet. I like the one with a custom widget
#     order = models.IntegerField(verbose_name='order', null=True, blank=True)


class SurveyQuestionTranslation(get_translation_model(SurveyQuestion, "Survey Question"), models.Model):
    content = models.TextField('Question')
    choices = models.TextField('Response(s)',
                           help_text='if the question type is "Radio List," "Select One Choice," or '
                                     '"Checkbox List" provide a comma-separated list of options for this question.')


    ###
    # qtype = models.PositiveSmallIntegerField('question type', choices=SurveyQuestion.TYPE_CHOICES, db_index=True)
    # def save(self, *args, **kwargs):
    #     if self.qtype == SurveyQuestion.TYPE_CHOICES.radio_list or self.qtype == SurveyQuestion.TYPE_CHOICES.select_one_choice \
    #         or self.qtype == SurveyQuestion.TYPE_CHOICES.checkbox_list:
    #         #import pdb; pdb.set_trace()
    #         validate_list(self.choices)
    #         super(SurveyQuestionTranslation, self).save(*args, **kwargs)

    ###
    class Meta:
        #ordering = ["- content"]
        verbose_name = "Survey Questions"
        verbose_name_plural = "Survey Question"

    def __unicode__(self):
        return self.content


# class SurveyAnswer(models.Model):
#     survey = models.ForeignKey(Survey)
#     question = models.ForeignKey(SurveyQuestion)
#     #author = models.ForeignKey(User, null=True, blank=True)
#     #choice = models.ForeignKey(SurveyChoice)
#     answer = models.CharField(max_length=255)
    #
    # def __unicode__(self):
    #     return self.question


# these type-specific answer models use a text field to allow for flexible
# field sizes depending on the actual question this answer corresponds to. any
# "required" attribute will be enforced by the form.
# class AnswerTextInput(SurveyAnswer):
#     body = models.TextField(blank=True, null=True)
#
#
# class AnswerTextArea(SurveyAnswer):
#     body = models.TextField(blank=True, null=True)
#
#
# class AnswerSelectOneChoice(SurveyAnswer):
#     body = models.TextField(blank=True, null=True)
#
#
# class AnswerRadioList(SurveyAnswer):
#     body = models.TextField(blank=True, null=True)
#
#
# class AnswerCheckboxList(SurveyAnswer):
#     body = models.TextField(blank=True, null=True)
#
#
# class SurveyCustomer(models.Model):
#     survey = models.ForeignKey(Survey)
#     customer = models.ForeignKey(User, null=True, blank=True)
#     customer_email_address = models.EmailField()
    #choice = models.ForeignKey(SurveyChoice)
    #answer =  mSurveyCustomerodels.CharField(max_length=255)
    #
    # def __unicode__(self):
    #     return self.question

#lass SurveyCustomerResonse(models.Model):
 #   SurveyCustomer
  #  question_id
    # response
    #

    #     return self.question                     # def __unicode__(self):                       #                                              #answer =  models.CharField(max_length=255)    #choice = models.ForeignKey(SurveyChoice)      customer_email_address = models.EmailField(    customer = models.ForeignKey(User, null=Tru    survey = models.ForeignKey(Survey)