from django.views.generic.base import TemplateView
from decorators import is_not_logged_in, is_logged_in
from django.contrib.auth.decorators import login_required
from django.contrib.auth import views as auth_views
from apps.account import views
from django.conf.urls import *


urlpatterns = patterns('',
    ## Authentication
    url(r'^login/$', is_not_logged_in(views.login), name='auth_login'),

    url(r'^logout/$', login_required(auth_views.logout), {'template_name': 'registration/logout.html'}, name='auth_logout'),

    url(r'^register/$', is_not_logged_in(views.RegistrationView.as_view()), name='register'),

    url(r'^register/complete/$', TemplateView.as_view(template_name="registration/registration_complete.html"), name='register_complete'),

    url(r'^register/suspended/$', TemplateView.as_view(template_name="registration/registration_suspended.html"), name='register_suspended'),

    url(r'^forgot-password/$', is_not_logged_in(views.PasswordResetView.as_view(), alt="account_profile_password"), name='auth_forgot_password'),

    url(r'^forgot-password/complete/$', is_not_logged_in( TemplateView.as_view(template_name="registration/forgot_password_completed.html"), alt="account_profile_password"), name='auth_forgot_password_complete'),

    url(r'^activate/resend/$', views.ActivateEmailResendView.as_view(), name='auth_activation_resend'),

    url(r'^activate/resend/complete/$', views.ActivateEmailResendCompletedView.as_view(), name='auth_activation_resend_complete'),

    url(r'^activate/resend/failed/$', TemplateView.as_view(template_name="registration/activation_failed.html"), name='auth_activation_failed'),

    url(r'^activate/(?P<activation_key>\w+)/$', views.ActivateEmailView.as_view(), name='registration_activate'),

    url(r'^accounts/$', views.AccountView.as_view(), name='account_index'),

    url(r'^accounts/profile/$', is_logged_in(views.ProfileView.as_view()), name='account_profile'),

    url(r'^accounts/profile/complete/$', is_logged_in(views.ProfileCompletedView.as_view()),name='account_profile_completed'),

    url(r'^accounts/profile/email/$', is_logged_in(views.UpdateEmailView.as_view()), name='account_profile_email'),

    url(r'^accounts/profile/email/complete/$', is_logged_in(TemplateView.as_view(template_name="registration/profile_email_complete.html")), name='account_profile_email_complete'),

    url(r'^accounts/profile/password/$', is_logged_in(views.UpdatePasswordView.as_view()), name='account_profile_password'),

    url(r'^accounts/profile/password/complete/$', is_logged_in(TemplateView.as_view(template_name="registration/profile_password_complete.html")), name='account_profile_password_complete'),

    url(r'^accounts/transaction/limit/$', is_logged_in(views.TransactionSettingsView.as_view()), name='transaction_limit_settings'),

    url(r'^accounts/deposit/limit/$', views.create_limit, {'limit_type': 'deposit'}, name='create-deposit-limit'),

    url(r'^accounts/entry/limit/$', views.create_limit, {'limit_type': 'entry'}, name='create-entry-limit'),

    url(r'^accounts/update-status/$', is_logged_in(views.UpdateStatusView.as_view()), name='account_exclude_me'),

    url(r'^accounts/history/(?P<pk>[a-zA-Z0-9_.-]+)/$', is_logged_in(views.HistoryDetailsView.as_view()), name='account_history_details'),

    url(r'^accounts/history/(?P<pk>[a-zA-Z0-9_.-]+)/entries/(?P<product_id>[a-zA-Z0-9_.-]+)/$', is_logged_in(views.HistoryDetailsView.as_view()), name='account_history_details'),

    url(r'^accounts/history/$', is_logged_in(views.HistoryListView.as_view()), name='account_history'),

    url(r'^accounts/balance/$', is_logged_in(views.BalanceView.as_view()), name='account_balance'),

    url(r'^accounts/claim/winnings/$', is_logged_in(views.ClaimWinningsView.as_view()), name='account_claim_winnings'),

    url(r'^accounts/notification/$', is_logged_in(views.NotificationView.as_view()), name='account_notifications'),

    url(r'^accounts/history/replay/(?P<pk>\d+)$', is_logged_in(views.replay_entries), name='replay-entries'),

    url(r'^accounts/transaction/(?P<transaction_id>\d+)/product/(?P<product_id>\d+)/entries/$',
        is_logged_in(views.product_history), name='product_history'),

    ## leave this last
    (r'', include('registration.urls')),
)
