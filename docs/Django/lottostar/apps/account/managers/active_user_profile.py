from django.db import models

class ActiveUserProfileManager(models.Manager):
    def get_query_set(self):
        return super(ActiveUserProfileManager, self).get_query_set().filter(status='active')


