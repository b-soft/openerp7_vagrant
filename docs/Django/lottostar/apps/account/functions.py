import hashlib
import urllib
import urllib2
import re
from django.contrib.auth.models import User
import random
import calendar
import datetime
from django.conf import settings
import os, errno
from account.models.restriction import BannedIp, UserRestrictedLog
from system.models.log import Log
from vendor.countries.models import Country


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST:
            pass
        else: raise

def remove_image(image, delete_main=True):
    cache_dir = '%s/cache/%s/' % (settings.MEDIA_ROOT, image.user.id) ; mkdir_p(cache_dir)
    file_name = image.image.name.replace("photo/%s/" % image.user.id, "")

    full_path_o = "%s%so_%s" % (cache_dir, image.id, file_name)
    full_path_l = "%s%sl_%s" % (cache_dir, image.id, file_name)
    full_path_t = "%s%st_%s" % (cache_dir, image.id, file_name)
    full_path_p = "%s%sp_%s" % (cache_dir, image.id, file_name)

    if delete_main:
        image.image = None
        image.save()
        try:
            os.remove(image.image.path)
        except:
            pass

    try:
        os.remove(full_path_o)
    except:
        pass
    try:
        os.remove(full_path_l)
    except:
        pass
    try:
        os.remove(full_path_t)
    except:
        pass
    try:
        os.remove(full_path_p)
    except:
        pass



def add_months(sourcedate,months):
    month = sourcedate.month - 1 + months
    year = sourcedate.year + month / 12
    month = month % 12 + 1
    day = min(sourcedate.day,calendar.monthrange(year,month)[1])
    return datetime.date(year,month,day)



def generate_username():
    username = ("user_%s") % (str(random.randint(0,1000000)))
    try:
        User.objects.get(username=username)
        return generate_username()
    except User.DoesNotExist:
        return username


def generate_activation_key(username):
    salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
    if isinstance(username, unicode):
        username = username.encode('utf-8')
    return hashlib.sha1(salt+username).hexdigest()


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def update_user_profile(user, vars, description, notes):
    profile = user.profile
    profile.status = "suspended"
    user_restricted_log = UserRestrictedLog(
        user = user,
        notes = notes
    )
    system_log = Log(
        user = user,
        system_log_type_id = 3,
        description = description,
        log_vars = vars
    )
    profile.save()
    user_restricted_log.save()
    system_log.save()


def check_for_banned_ip(ip_address,user):
    banned_ips = BannedIp.objects.all()
    if banned_ips:
        for banned_ip in banned_ips:
            if re.findall(banned_ip.ip_address, ip_address):
                message ='SUSPENDED:  New Customer - registration ip (%s) is not allowed to play.'
                notes = message % ip_address
                vars = [ip_address]
                update_user_profile(user, vars,message, notes)

def check_allowed_country(profile):
    country = Country.on_site.get(iso=profile.country)
    if not country.is_allowed:
        message ='SUSPENDED:  New Customer - registration country (%s) is not allowed to play.'
        notes = message % profile.country
        vars = [profile.country]
        update_user_profile(profile.user, vars, message, notes)

def verify_with_maxmind(user, ip_address, geoip_record):
    params = urllib.urlencode(dict(i=ip_address, city=geoip_record['city'], region=geoip_record['region_name'],
        country=geoip_record['country_name'], license_key = 'b0auVAfWoN6i'))

    try:
        url = urllib2.urlopen('https://minfraud.maxmind.com/app/ccv2r', params)
        result =  url.read()
    except urllib2.HTTPError:
        result = None

    if result is not None:
        country_match = result.split(';')[1]
        if country_match.split('=')[1] == "No":
            message ='SUSPENDED:  New Customer - registration ip location (%s) does not match registration country (%s).'
            notes = message % (ip_address, geoip_record['country_name'])
            vars = [ip_address, geoip_record['country_name']]
            update_user_profile(user, vars, message, notes)

        anonymous_proxy = result.split(';')[4]
        if anonymous_proxy.split('=')[1] == "Yes":
            message ='SUSPENDED:  New Customer - registered using an annonymous proxy (%s).'
            notes = message % ip_address
            vars = [ip_address]
            update_user_profile(user, vars, message, notes)


def save_into_stystem_log(user_id,description,vars,type):
    system_log = Log(
        user_id = user_id,
        system_log_type_id = type,
        description = description,
        log_vars = vars
    )
    system_log.save()