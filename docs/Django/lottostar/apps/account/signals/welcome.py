from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.dispatch.dispatcher import Signal
from django.utils.safestring import mark_safe
from utils.mailer import SendMail
from django.utils.translation import ugettext_lazy as _


def handle_welcome_email(sender, **kwargs):
    user = kwargs['user']
    profile = user.profile

    site = Site.objects.get_current()
    site_url = str('%s%s' % ('http://', site.domain))

    login_link = "http://%s%s" % (site.domain, reverse('auth_login', args=(), kwargs={}))

    link = '<a href="%s">%s</a>' % (login_link, _('click Here').title())
    link_raw = '<a href="%s">%s</a>' % (login_link, login_link)

    SendMail(
        slug="welcome",
        to=[user.email],
        user=user,
        site=Site.objects.get_current(),
        site_url=site_url,
        link=mark_safe(link),
        link_raw=mark_safe(link_raw),
    ).send()

send_welcome_email = Signal(providing_args=["user"])
send_welcome_email.connect(handle_welcome_email)