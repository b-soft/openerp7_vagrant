from django.dispatch import receiver
from django.db.models.signals import pre_save
from django.contrib.sites.models import Site

#TODO: Olwethu: 16/09/2013: fix import errors with SendMail
# from apps.utils.mailer import SendMail
from account.models.profile import UserProfile

@receiver(pre_save, sender=UserProfile, dispatch_uid='account.UserProfile')
def banned_user_email(sender, instance, **kwargs):
    """
    send email to user notifying them that the user account has been banned.
    """
    if not kwargs['raw']:
        try:
            profile = UserProfile.objects.get(id=instance.id)
            # if user account has been banned make user inactive.
            if instance.status != profile.status and instance.status == 'banned':
                instance.user.is_active = False
                instance.user.save()
            site = Site.objects.get_current()
            site_url = '{0}{1}'.format('http://', site.domain)
            # SendMail(
            #     slug="banned-user",
            #     to=[instance.user.email],
            #     status=instance.status,
            #     user=instance.user,
            #     site=site,
            #     site_url=site_url,
            # ).send()
        except UserProfile.DoesNotExist:
            pass
