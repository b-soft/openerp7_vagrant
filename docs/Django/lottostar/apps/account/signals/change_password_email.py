from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.dispatch.dispatcher import Signal
from django.utils.safestring import mark_safe
from utils.mailer import SendMail
from django.utils.translation import ugettext_lazy as _


def handle_change_password_email(sender, **kwargs):
    user = kwargs['user']

    site = Site.objects.get_current()
    site_url = str(('%s%s') % ('http://', site.domain ))
    password=kwargs['password']

    link = '<a href="%s%s">%s</a>' % (site_url, reverse("auth_login"), _('Click here').title())

    link_password = '<a href="%s%s">%s</a>' % (site_url, reverse("auth_forgot_password"), _('Click Here').title())
    link_contact = '<a href="%s%s">%s</a>' % (site_url, reverse("contact"), _('Contact us').title())

    SendMail(
        to=[user.email],
        slug="update-password",
        password=password,
        user=user,
        site_url=site_url,
        link=mark_safe(link),
        link_contact=mark_safe(link_contact),
        link_password=mark_safe(link_password)
    ).send()


change_password_email = Signal(providing_args=["user"])
change_password_email.connect(handle_change_password_email)