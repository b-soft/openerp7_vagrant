from django.contrib.sites.models import Site
from django.dispatch.dispatcher import Signal
from utils.mailer import SendMail

def handle_cancellation_request(sender, **kwargs):
    user = kwargs['user']

    site = Site.objects.get_current()
    site_url = str('%s%s' % ('http://', site.domain))

    SendMail(
        slug="cancellation-request",
        to=[user.email],
        user=user,
        site_url="http://%s" % site_url,
    ).send()


send_cancellation_request = Signal(providing_args=["user"])
send_cancellation_request.connect(handle_cancellation_request)