from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.dispatch.dispatcher import Signal
from django.utils.safestring import mark_safe
from utils.mailer import SendMail
from django.utils.translation import ugettext_lazy as _

def handle_activate_email(sender, **kwargs):
    user = kwargs['user']

    site = Site.objects.get_current()
    site_url = str(('%s%s') % ('http://', site.domain ))

    link = '<a href="%s%s">%s</a>' % (site_url, reverse("auth_login"), _('Click here').title())
    link_password = '<a href="%s%s">%s</a>' % (
        site_url, reverse("auth_forgot_password"), _('reset your password').title())
    link_contact = '<a href="%s%s">%s</a>' % (site_url, reverse("contact"), _('contact us').title())

    SendMail(
        slug="welcome",
        to=[user.email],
        user=user,
        site_url=site_url,
        link=mark_safe(link),
        link_contact=mark_safe(link_contact),
        link_password=mark_safe(link_password),
    ).send()

send_activate_email = Signal(providing_args=["user"])
send_activate_email.connect(handle_activate_email)