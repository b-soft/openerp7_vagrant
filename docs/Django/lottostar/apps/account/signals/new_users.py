from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.sites.models import Site
from django.contrib.auth.models import User
from django.contrib.auth.models import Group


@receiver(post_save, sender=User, dispatch_uid='auth.User')
def remove_delete_permissions(sender, instance, created, **kwargs):
    """
    for every user that register's on the site remove the ability to delete any object.
    """
    if not kwargs['raw']:
        if created:
            try:
                group = Group.objects.get(name='Site Users')
                instance.groups.add(group)
                instance.save()
            except Group.DoesNotExist:
                pass

