from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.dispatch.dispatcher import Signal
from django.utils.safestring import mark_safe
from utils.mailer import SendMail
from django.utils.translation import ugettext_lazy as _



def handle_change_email(sender, **kwargs):
    user = kwargs['user']
    profile = user.profile

    site = Site.objects.get_current()
    site_url = '%s%s' % ('http://', site.domain )

    activate_link = "%s%s" % (site_url, reverse('registration_activate', args=(), kwargs={'activation_key': profile.activation_key}))
    link = '<a href="%s">%s</a>' % (activate_link, _('click Here').title())
    link_raw = '<a href="%s">%s</a>' % (activate_link, activate_link)

    SendMail(
        slug="update-email",
        to=[user.email],
        user=user,
        key_expires=profile.key_expires,
        activation_key=profile.activation_key,
        site_url=site_url,
        link=mark_safe(link),
        link_raw=mark_safe(link_raw),
    ).send()

send_change_email = Signal(providing_args=["user"])
send_change_email.connect(handle_change_email)