from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.dispatch.dispatcher import Signal
from django.utils.safestring import mark_safe
from utils.mailer import SendMail
from django.utils.translation import ugettext_lazy as _


def handle_register_email(sender, **kwargs):
    user = kwargs['user']
    profile = user.profile

    site = Site.objects.get_current()
    site_url = str('%s%s' % ('http://', site.domain))

    activate_link = "http://%s%s" % (site.domain,
                                     reverse('registration_activate', args=(),
                                         kwargs={'activation_key': profile.activation_key}))

    link = '<a href="%s">%s</a>' % (activate_link, _('click Here').title())
    link_raw = '<a href="%s">%s</a>' % (activate_link, activate_link)

    SendMail(
        slug="register",
        to=[user.email],
        user=user,
        site=Site.objects.get_current(),
        key_expires=profile.key_expires,
        activation_key=profile.activation_key,
        site_url=site_url,
        link=mark_safe(link),
        link_raw=mark_safe(link_raw),
    ).send()

send_register_email = Signal(providing_args=["user"])
send_register_email.connect(handle_register_email)