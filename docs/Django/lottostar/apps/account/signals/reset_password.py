from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.dispatch.dispatcher import Signal
from django.utils.http import int_to_base36
from django.utils.safestring import mark_safe
from utils.mailer import SendMail
from django.utils.translation import ugettext_lazy as _



def handle_resend_password_email(sender, **kwargs):
    user = kwargs['user']

    site = Site.objects.get_current()
    site_url = str(('%s%s') % ('http://', site.domain ))

    token_generator = PasswordResetTokenGenerator()

    link = '<a href="%s%s">%s</a>' % ( site_url, reverse('auth_password_reset_confirm',
        args=(),
        kwargs={'uidb36': int_to_base36(user.id),
                'token': token_generator.make_token(user)
        }), _('click Here To Reset').title())

    SendMail(slug="forgot-password",
        to=[user.email], user=user,
        link=mark_safe(link),
        site_url=mark_safe(site_url),
    ).send()


resend_password_email = Signal(providing_args=["user"])
resend_password_email.connect(handle_resend_password_email)