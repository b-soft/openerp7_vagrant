from django import forms
from django.contrib.auth.hashers import UNUSABLE_PASSWORD
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User

#------------------------------------------------------------

class GetUserByEmailForm(forms.Form):
    error_messages = {
        'unknown': _("Whoops! We can't find that email address on our system."),
        'unusable': _("The user account associated with this e-mail address cannot reset the password."),
        'already_activated': _('This account has already been activated.'),
        }
    email = forms.EmailField(label=_("E-mail"), max_length=150)

    def clean_email(self):
        """ Validates that an active user exists with the given email address. """
        email = self.cleaned_data["email"]
        self.users_cache = User.objects.filter(email__iexact=email)
        if not len(self.users_cache):
            raise forms.ValidationError(self.error_messages['unknown'])
        if any((user.password == UNUSABLE_PASSWORD)
            for user in self.users_cache):
            raise forms.ValidationError(self.error_messages['unusable'])

        self.user = self.users_cache[0]
        return email