from django.contrib.auth.models import User
from account.forms.base_registration import BaseRegistrationForm
from django.utils.translation import ugettext_lazy as _

class EmailForm(BaseRegistrationForm):
    def __init__(self, *args, **kwargs):
        super(EmailForm, self).__init__(*args, **kwargs)
        self.fields.keyOrder = ['email', ]

    class Meta:
        model = User
        fields = ('email',)

    def clean_email(self):
        pass