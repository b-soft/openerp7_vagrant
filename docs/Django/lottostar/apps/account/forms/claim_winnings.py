import json
from django import forms
from django.forms.models import ModelForm
from django.utils.translation import ugettext_lazy as _
from account.models.redeem_log import RedeemLog
from account.models.userwallet import UserWallet
from system.models.log import Log


class ClaimWinningsForm(ModelForm):
    winnings = forms.CharField(
        widget=forms.TextInput(attrs={'readonly':'readonly'})
    )

    class Meta:
        model = UserWallet
        fields = ['winnings']

    def save(self, request, profile_callback=None):
        if self.cleaned_data['winnings']:
            user=request.user
            profile = user.profile
            profile.play_credits += float(self.cleaned_data['winnings'])

            for wallet in UserWallet.objects.filter(user=request.user):
                if wallet.winnings > 0:
                    redeem_log = RedeemLog()
                    redeem_log.user_wallet=wallet
                    redeem_log.winnings=wallet.winnings
                    redeem_log.vouchers=0
                    redeem_log.play_credits=wallet.winnings
                    redeem_log.save()

                    vars=['&euro;','%s'%wallet.winnings,'%s'%wallet.game.name]
                    system_log = Log()
                    system_log.system_log_type_id=2
                    system_log.user_id=user.id
                    system_log.description="Converted #currency##amount# winnings to play credits - #game#"
                    system_log.log_vars=vars
                    system_log.save()

                    wallet.winnings=0
                    wallet.save()

            profile.save()
        return user.profile