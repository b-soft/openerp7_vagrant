from django.contrib.auth.models import User
from django.forms import forms
import pygeoip
import simplejson
from account.forms.base_registration import BaseRegistrationForm
from account.functions import generate_username, get_client_ip, check_for_banned_ip, check_allowed_country, verify_with_maxmind
from account.models.profile import UserProfile
from account.signals import send_welcome_email
from django.utils.translation import ugettext_lazy as _
from settings import GEOIP_URL
from vendor.countries.models import Country


class RegisterForm(BaseRegistrationForm):
    required_css_class = "required"
    error_css_class = "error"
    #exclude = 'username'

    def __init__(self, *args, **kwargs):
        #self.request = kwargs.pop('request')
        super(RegisterForm, self).__init__(*args, **kwargs)

        self.fields.keyOrder = ['gender', 'first_name', 'last_name', 'dob', 'country', 'email', 'password', 'tos']

    def clean(self):
        if 'first_name' in self.cleaned_data and 'last_name' in self.cleaned_data and 'dob' in self.cleaned_data:
            existing = UserProfile.objects.filter(user__first_name__iexact=self.cleaned_data['first_name'],
                user__last_name__iexact=self.cleaned_data['last_name'], country=self.cleaned_data['country'],
                dob=self.cleaned_data['dob'])
            if existing.exists():
                raise forms.ValidationError(_("An account with similar first name, last name, country and dob details exists."))

        return self.cleaned_data

    def save(self, request, profile_callback=None):

        user = User.objects.create_user(
            username=generate_username(),
            email=self.cleaned_data['email'],
            password=self.cleaned_data['password']
        )
        user.is_active = True
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']

        profile = user.profile
        profile.gender = self.cleaned_data['gender']
        profile.dob = self.cleaned_data['dob']
        profile.country = self.cleaned_data['country']

        #save geoip data
        gi = pygeoip.GeoIP(GEOIP_URL)
        ip_address = request.client_ip

        profile.ip_address = ip_address
        geoip_record = gi.record_by_addr(ip_address)

        if geoip_record:
            country = Country.on_site.get(iso=geoip_record['country_code'])
            if country:
                profile.country = country.iso
            profile.geo_location = simplejson.dumps(geoip_record)
            profile.city = geoip_record['city']
            profile.postal_code = geoip_record['postal_code']

        user.save()
        profile.save()

        #verify new registration
        check_allowed_country(profile)
        if geoip_record:
            verify_with_maxmind(user, ip_address, geoip_record)
        check_for_banned_ip(ip_address, user)

        send_welcome_email.send(sender=None, user=user)
