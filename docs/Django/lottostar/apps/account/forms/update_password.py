from django import forms
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
#------------------------------------------------------------
from account.signals.change_password_email import change_password_email

class UpdatePasswordForm(forms.Form):
    old_password = forms.CharField(label=_("Old password"), widget=forms.PasswordInput, required=False)
    new_password = forms.CharField(label=_("New password"), widget=forms.PasswordInput, required=False)
    confirm_password = forms.CharField(label=_("Confirm password"), widget=forms.PasswordInput, required=False)

    def clean(self):
        if self.cleaned_data["old_password"] != "" and self.cleaned_data["new_password"] != "" and self.cleaned_data["confirm_password"] != "":
            if self.request.user.check_password(self.cleaned_data["old_password"]):
                if self.cleaned_data["new_password"] != self.cleaned_data["confirm_password"]:
                    self._errors["new_password"] = "incorrect"
                    self._errors["confirm_password"] = "incorrect"
                    raise forms.ValidationError("Your new password and confirmed password did not match.")
            else:
                self._errors["old_password"] = "incorrect"
                raise forms.ValidationError("Your old password was incorrect.")
        else:
            if self.cleaned_data["old_password"] == "" and self.cleaned_data["new_password"] == "" and self.cleaned_data["confirm_password"] == "":
                raise forms.ValidationError("Your password was not changed.")
            else:
                self._errors["old_password"] = "incorrect"; self._errors["new_password"] = "incorrect"; self._errors["confirm_password"] = "incorrect"
                messages.error(self.request, "One of the fields was blank. Please try again.")
                raise forms.ValidationError("blank field.")
        return self.cleaned_data

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(UpdatePasswordForm, self).__init__(*args, **kwargs)

    def save(self):
        self.request.user.set_password(self.cleaned_data["new_password"])
        self.request.user.save()

        change_password_email.send(sender=None, user=self.request.user, password=self.cleaned_data["new_password"])

        messages.success(self.request, 'Your password was successfully saved.')

