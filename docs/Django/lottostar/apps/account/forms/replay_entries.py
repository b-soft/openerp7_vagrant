from datetime import datetime
from django import forms

from game.models import Event

class ReplayEntriesForm(forms.Form):
    def __init__(self, entries_qs, *args, **kwargs):
        entries_field = forms.ModelMultipleChoiceField(required=False,
            widget=forms.CheckboxSelectMultiple(attrs={'class': 'transaction-entries'}), queryset=entries_qs)
        self.base_fields['entries'] = entries_field
        super(ReplayEntriesForm, self).__init__(*args, **kwargs)


class DrawDateForm(forms.Form):
    events = forms.ModelChoiceField(
        required=True, queryset=Event.objects.filter(draw_datetime__gt=datetime.now(), cutoff__gt=datetime.now()))
