from django.forms import ModelForm

from account.models import UserWallet


class UserCreditForm(ModelForm):
    class Meta:
        model = UserWallet
        fields = ('credits',)