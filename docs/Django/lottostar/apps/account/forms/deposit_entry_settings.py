from django.forms.models import ModelForm
from django.contrib.sites.models import Site

from utils.mailer import SendMail
from account.models import Restriction


class LimitRestrictionForm(ModelForm):
    class Meta:
        model = Restriction
        fields = ['period', 'value']

    def save(self, limit_type, is_active, user, commit=False, send_mail=False):
        instance = super(LimitRestrictionForm, self).save(commit=commit)
        instance.user = user
        instance.type = limit_type
        instance.is_active = is_active
        instance.save()
        if send_mail:
            slug = 'entry-limit'
            if limit_type == 'deposit':
                slug = 'deposit-limit'
            site = Site.objects.get_current()
            site_url = '{0}{1}'.format('http://', site.domain)
            SendMail(
                slug=slug, to=[user.email], user=user, limit_duration=self.cleaned_data.get("period"),
                limit_amount=self.cleaned_data.get("value"), site_url=site_url,
            ).send()
