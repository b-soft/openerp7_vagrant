from datetime import date
from django import forms
from django.contrib.auth.models import User
from account.forms.base_registration import attrs_dict
from account.functions import generate_activation_key
from account.signals.change_email import send_change_email
from django.utils.translation import ugettext_lazy as _

class UpdateEmailForm(forms.Form):
    email = forms.EmailField(widget=forms.TextInput(attrs=dict(attrs_dict, maxlength=255)), label=_("E-mail"), required=True)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(UpdateEmailForm, self).__init__(*args, **kwargs)

    def clear(self):
        user = User.objects.filter(email=self.cleaned_data['email']).exclude(id=self.request.user.id)
        if user:
            self._errors["email"] = "This email address already exists."
        return self.cleaned_data

    def save(self, user, profile_callback=None):
        user.email = self.cleaned_data.get('email')
        user.is_active = False
        user.save()

        profile = user.profile
        profile.key_expires = date.today()
        profile.activation_key = generate_activation_key(user.username)
        profile.key_expires = date.today()
        profile.save()

        send_change_email.send(sender=self, user=user)

        return profile
