from django.contrib.auth.models import User
from account.forms.base_registration import BaseRegistrationForm
from django.utils.translation import ugettext_lazy as _

class ProfileForm(BaseRegistrationForm):
    required_css_class = "required"
    error_css_class = "error"
    #exclude = 'username'

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(ProfileForm, self).__init__(*args, **kwargs)

        self.fields.keyOrder = ['title', 'gender', 'first_name', 'last_name', 'dob',
                                'address', 'suburb', 'city', 'postal_code', 'country', 'email', 'preferred_number']

    def clean_email(self):
        user = User.objects.filter(email=self.cleaned_data['email']).exclude(id=self.request.user.id)
        if user:
            self._errors["email"] = "Email exists."
        return self.cleaned_data

    def save(self, user, profile_callback=None):

        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.save()

        profile = user.profile

        profile.preferred_number = self.cleaned_data['preferred_number']
        profile.gender = self.cleaned_data['gender']
        profile.dob = self.cleaned_data['dob']
        profile.country = self.cleaned_data['country']
        profile.title = self.cleaned_data['title']
        profile.address = self.cleaned_data['address']
        profile.suburb = self.cleaned_data['suburb']
        profile.city = self.cleaned_data['city']
        profile.postal_code = self.cleaned_data['postal_code']

        profile.save()

        return profile