from django import forms
from django.contrib.auth.forms import AuthenticationForm as AuthAuthenticationForm
from django.utils.translation import ugettext_lazy as _

#------------------------------------------------------------

class AuthenticationForm(AuthAuthenticationForm):
    username = forms.EmailField(label=_("Email"),widget=forms.TextInput(attrs={"maxlength":75,"class":"input-large"}))
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput(attrs={"class":"input-large"}))

    def clean(self):
        cleaned_data = super(AuthenticationForm, self).clean()

        if self.user_cache:
            if any(status == self.user_cache.profile.status for status in ['canceled', 'suspended']):
                raise forms.ValidationError(_(u"Account has been {0} please contact site administrator.".format(self.user_cache.profile.status)))
        return cleaned_data
