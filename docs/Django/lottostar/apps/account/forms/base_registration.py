import datetime
from django.db.models.query_utils import Q
import re
from django import forms
from django.contrib.sites.models import Site
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.forms.extras.widgets import SelectDateWidget
from account.models.profile import UserProfile
from vendor.countries.models import Country

#------------------------------------------------------------

attrs_dict = { 'class': 'dropdown' }

#------------------------------------------------------------

class BaseRegistrationForm(forms.Form):

    TITLE_CHOICES = (
        ('Mr', _('Mr')),
        ('Mrs', _('Mrs')),
        ('Miss', _('Miss')),
        )

    LIMIT_PERIOD_CHOICES = (
        ('daily', _('Daily')),
        ('weekly', _('Weekly')),
        ('monthly', _('Monthly')),
        )

    GENDER_CHOICES = (
        ('m', _('Male')),
        ('f', _('Female')),
    )


    AGE_TO = datetime.datetime.today().year

    title = forms.ChoiceField(label=_(u'Title'), choices=TITLE_CHOICES, required=True, widget=forms.Select(attrs=attrs_dict))
    gender = forms.ChoiceField(label=_(u'Gender'), choices=GENDER_CHOICES, required=True, widget=forms.Select(attrs=attrs_dict))

    username = forms.RegexField(regex=r'^[\w.@+-]+$', max_length=30, label=_("Preferred Name"), error_messages={'invalid': _("This value may contain only letters, numbers and @/./+/-/_ characters.")})

    tos = forms.BooleanField(widget=forms.CheckboxInput(attrs={"data-customforms":"disabled"}), label=mark_safe(_(u'I agree to the <a href="/terms-and-conditions/" class="btn-terms">Terms and Conditions</a>.')), error_messages={'required': _("You must agree to the terms to apply")})

    first_name = forms.CharField(label=_(u'First name'), max_length=30)
    last_name = forms.CharField(label=_(u'Last name'), max_length=75)
    preferred_number = forms.CharField(label=_(u'Preferred Number'), max_length=75)

    dob = forms.DateField(label=_(u'Date of Birth'), widget=SelectDateWidget(years=sorted(range(1910, AGE_TO), reverse=True), attrs={"DATE_INPUT_FORMATS": "j N, Y", "class": "inline left small"}), required=True)

    address = forms.CharField(label=_(u'Address'), max_length=255, required=False)
    suburb = forms.CharField(label=_(u'Suburb'), max_length=255, required=False)
    city = forms.CharField(label=_(u'City'), max_length=255, required=False)
    postal_code = forms.CharField(label=_(u'Postal code'), max_length=255, required=False)

    country = forms.ChoiceField(label=_(u'Country'), choices=(), widget=forms.Select(attrs={'class':'profile-country width-long'}), initial="ZA")

    email = forms.EmailField(widget=forms.TextInput(attrs=dict(attrs_dict, maxlength=255)), label=_("E-mail"))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'dropdown', 'autocomplete': 'off',}, render_value=False), label=_("Password"))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'dropdown', 'autocomplete': 'off',}, render_value=False), label=_("Password (again)"))

    def clean_username(self):
        """ Validate that the username is alphanumeric and is not already in use. """
        existing = User.objects.filter(username__iexact=self.cleaned_data['username'])
        if existing.exists():
            raise forms.ValidationError(_("This username already exists."))
        else:
            return self.cleaned_data['username']

    def clean_email(self):
        if User.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(_(u'Email address is already in use.'))
        return self.cleaned_data['email']

    def clean_preferred_number(self):
        if 'preferred_number' in self.cleaned_data:
            preferred_number = UserProfile.objects.filter(
                ~Q(user_id = self.request.user.id),
                preferred_number__iexact=self.cleaned_data['preferred_number'],
                country__iexact=self.cleaned_data['country']
            )
            if preferred_number:
                raise forms.ValidationError(_(u'This preferred number has already been registered.'))

            preferred_number = re.sub('[^0-9]', '', self.cleaned_data['preferred_number'])
            if not preferred_number:
                raise forms.ValidationError(_("Invalid preferred number."))
            else:
                return self.cleaned_data['preferred_number']


    def __init__(self, *args, **kwargs):
        super(BaseRegistrationForm, self).__init__(*args, **kwargs)
        choices = [(country.iso, unicode(country)) for country in Country.on_site.all()]
        self.fields['country'].choices = choices

