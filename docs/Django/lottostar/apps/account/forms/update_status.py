from datetime import datetime
from django import forms
from django.forms.models import ModelForm
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from account.functions import save_into_stystem_log
from account.models.profile import UserProfile
from system.models.log import Log


class UpdateStatusSettingsForm(ModelForm):
    #eish no more html on python code...me want to be on the templates.
    STATUS_CHOICE = (
        ('active', mark_safe(_(u'<b>Registered</b><div style="margin:0 0 10px 20px;">You are registered.</div>'))),
        ('deactivated', mark_safe(_(u'<b>Deactivate</b><div style="margin:0 0 10px 20px;">You account will be deactivated.</div>'))),
        ('suspended', mark_safe(_(u'<b>Temporary suspension</b><div style="margin:0 0 10px 20px;">Your account will be suspended for '
                                  u'6 months before you can unlock it again.</div>'))),
        )
    status = forms.TypedChoiceField(
        choices=STATUS_CHOICE, widget=forms.RadioSelect
    )

    class Meta:
        model = UserProfile
        fields = ['status']

    def save(self, user, profile_callback=None):
        """
        this method is used for user's self suspension. the account will be active with certain page restrictions.
        """
        old_status = user.profile.status
        if old_status != self.cleaned_data['status']:
            if self.cleaned_data['status'] == 'deactivated':
                user.is_active = False
                user.save()

            profile = user.profile
            profile.status = self.cleaned_data['status']
            profile.monitor_status = datetime.now()
            profile.save()

            description = "Account status changed from #old_status# to #new_status#"
            vars= [old_status, profile.status]
            save_into_stystem_log(user.id, description,vars,2)
