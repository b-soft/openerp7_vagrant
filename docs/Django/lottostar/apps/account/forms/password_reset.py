from datetime import date
from django.forms import forms
from account.forms.get_user_by_email import GetUserByEmailForm
from account.functions import generate_activation_key
from account.signals.reset_password import resend_password_email
from django.utils.translation import ugettext_lazy as _

class PasswordResetForm(GetUserByEmailForm):

    def save(self):
        resend_password_email.send(sender=None, user=self.user)
        return self.user

class EmailActivateForm(GetUserByEmailForm):

    def clean_email(self):
        super(EmailActivateForm,self).clean_email()

        if self.user.profile.activation_key == "ALREADY_ACTIVATED":
            raise forms.ValidationError(self.error_messages['already_activated'])

        if not self.user.profile.activation_key:
            raise forms.ValidationError(_("Invalid key error"))

    def save(self):
        profile = self.user.profile
        profile.activation_key = generate_activation_key(username=self.user.username)
        profile.key_expires=date.today()
        profile.save()
        return self.user
