from django.forms.models import ModelForm
from django.utils.translation import ugettext_lazy as _
from account.models import Restriction

class EntrySettingsForm(ModelForm):
    class Meta:
        model = Restriction
        fields = ['period', 'value']