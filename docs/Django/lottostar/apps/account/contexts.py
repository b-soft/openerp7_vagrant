from django.http import HttpResponse
from account.models import UserProfile, UserWallet
from apps.game.models import Product
from django.utils.translation import get_language_info, activate

from django.conf import settings

def account_settings(request):
    context = {}
    context["user_play_credits"] = 0
    default_lang = "en"
    up = False

    if request.user.is_authenticated():
        try:
            up = UserProfile.objects.get(user=request.user)
        except:
            up = False
        if up:
            default_lang = up.preferred_language
            context["user_play_credits"] = up.play_credits

        vouchers = 0
        points = 0
        winnings = 0
        for product in Product.objects.all():
            user_wallet, created = UserWallet.objects.get_or_create(user=request.user, product=product, game=product.get_product_game().game)
            if user_wallet:
                vouchers += int(user_wallet.vouchers)
                points += int(user_wallet.points)
                winnings += int(user_wallet.winnings)

        context["user_play_vouchers"] = vouchers
        context["user_play_points"] = points
        context["user_play_winnings"] = winnings
    else:
        pass


    if request.GET.get("lang", False):
        is_lang = False
        for lang in settings.LANGUAGES:
            if request.GET["lang"] == lang[0]:
                is_lang = True

        if is_lang == True:
            default_lang = request.GET["lang"]
            request.session["set_lang"] = default_lang

            if up:
                up.preferred_language = default_lang
                up.save()

    else:
        if "set_lang" in request.session:
            default_lang = request.session["set_lang"]

    activate(default_lang)
    context["set_lang"] = default_lang
    context["lang"] = get_language_info(default_lang)

    return context