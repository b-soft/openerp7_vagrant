from datetime import datetime
from django.views.generic.edit import FormView
from django.contrib.formtools.wizard.views import SessionWizardView
from django.shortcuts import redirect
from django.contrib.auth.models import User

from account.forms.replay_entries import ReplayEntriesForm
from play.models import Entry, ReplayEntries
from game.models import Event

# class ReplayEntriesFormView(FormView):
#     form_class = ReplayEntriesForm
#     template_name = "account/history_details.html"
#
#     def get_form(self, form_class):
#         pk = self.kwargs.get('pk', None)
#         if pk:
#             entries_qs = Entry.objects.filter(ticket__transaction__pk=pk).order_by("event__draw_datetime")
#         else:
#             entries_qs = Entry.objects.none()
#         return form_class(entries_qs, **self.get_form_kwargs())
#
#     def form_valid(self, form):
#         """
#         save the replayed entries on a temporary table
#         """


def replay_entries(request, pk):
    if request.method == 'POST':
        user = User.objects.get(id=request.user.id)
        entries_id = request.POST.getlist('entry', None)
        if entries_id:
            entries_id = map(int, entries_id)
            #safe check to ensure that all entries are linked to the transaction pk provided.
            entries = Entry.objects.filter(id__in=entries_id, ticket__transaction__id=pk)
            if entries:
                try:
                    # replay entries for the next draw date
                    next_draw_event = Event.objects.filter(
                        draw_datetime__gt=datetime.now(), cutoff__gt=datetime.now()).order_by('draw_datetime')[0]
                except IndexError:
                    next_draw_event = None
                if next_draw_event:
                    entries_list = []
                    append_entry = entries_list.append
                    for entry in entries:
                        append_entry(ReplayEntries(user=user, entry=entry, event=next_draw_event))
                    ReplayEntries.objects.bulk_create(entries_list)
    return redirect('checkout')

#replay_entries = ReplayEntriesFormView.as_view()