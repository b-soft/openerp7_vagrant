from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView

class AccountView(TemplateView):
    template_name = "account/index.html"

    def get_context_data(self, **kwargs):
        context = super(AccountView, self).get_context_data(**kwargs)
        return context

    def render_to_response(self, context, **response_kwargs):
        if self.request.session.get("is_in_checkout", False):
            del self.request.session["is_in_checkout"]
            self.request.session.save()
            return HttpResponseRedirect(reverse("checkout"))
        return super(AccountView, self).render_to_response(context, **response_kwargs)
