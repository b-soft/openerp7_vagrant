from django.views.generic import ListView
from account.models.userwallet import UserWallet

class BalanceView(ListView):
    template_name = "account/account_balance.html"

    def get_queryset(self):
        return UserWallet.objects.filter(user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(BalanceView, self).get_context_data(**kwargs)
        wallet_list = self.get_queryset()

        vouchers = 0
        winnings = 0
        points = 0
        for wallet in wallet_list:
            vouchers += wallet.vouchers
            winnings += wallet.winnings
            points += wallet.points

        context["total_vouchers"] = vouchers
        context["total_winnings"] = winnings
        context["total_points"] = points
        context["wallet_list"] = wallet_list

        return context
