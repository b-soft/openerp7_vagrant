from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect
from django.views.generic import ListView
from django.views.generic.edit import FormView
from account.forms.claim_winnings import ClaimWinningsForm
from account.models.userwallet import UserWallet

class ClaimWinningsView(FormView):
    model = UserWallet
    template_name = "account/claim_winnings_form.html"
    form_class = ClaimWinningsForm

    def get_context_data(self, **kwargs):
        context = super(ClaimWinningsView, self).get_context_data(**kwargs)
        winnings = 0
        for wallet in UserWallet.objects.filter(user=self.request.user):
            winnings += wallet.winnings
        context["winnings"] = winnings
        return context

    def get_initial(self):
        initial = super(ClaimWinningsView, self).get_initial()
        winnings = 0
        for wallet in UserWallet.objects.filter(user=self.request.user):
            winnings += wallet.winnings
        initial['winnings'] = winnings
        return initial

    def get_success_url(self):
        return reverse("account_balance")

    def form_invalid(self, form):
        return super(ClaimWinningsView, self).form_invalid(form)

    def form_valid(self, form):
        winnings=0
        for wallet in UserWallet.objects.filter(user=self.request.user):
            winnings += wallet.winnings
        if float(winnings) != float(form.cleaned_data["winnings"]):
            messages.error(self.request, 'Winnings claim amount is not correct.')
            return HttpResponseRedirect(reverse("account_claim_winnings"))
        form.save(self.request)
        messages.success(self.request, 'Your winnings claim is successfully processed.')
        return super(ClaimWinningsView, self).form_valid(form)
