from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic.edit import FormView
from django.contrib.auth.models import User
from account.forms.update_email import UpdateEmailForm

class UpdateEmailView(FormView):
    model = User
    template_name = 'registration/profile_email.html'
    form_class = UpdateEmailForm

    def get_form_kwargs(self):
        kwargs = super(UpdateEmailView, self).get_form_kwargs()
        kwargs["request"] = self.request
        return kwargs

    def get_initial(self):
        initial = super(UpdateEmailView, self).get_initial()
        initial['email'] = self.request.user.email
        return initial

    def get_success_url(self):
        return reverse("account_profile_email_complete")

    def form_valid(self, form):

        if form.cleaned_data["email"] == self.request.user.email:
            messages.error(self.request, 'Your email address is the same.')
            return HttpResponseRedirect(reverse("account_profile_email"))

        messages.success(self.request,
            'Please activate your new email address by clicking the link within the email sent to you')

        form.save(self.request.user)

        return super(UpdateEmailView, self).form_valid(form)