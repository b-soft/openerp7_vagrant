from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic.edit import FormView
from account.forms.register import RegisterForm

class RegistrationView(FormView):
    form_class = RegisterForm
    template_name = "registration/registration_form.html"

    def get_success_url(self):
        user = self.request.user
        profile = user.profile

        if profile.status == 'suspended':
            return reverse('register_suspended')

        if self.request.session.get("is_in_checkout", False):
            del self.request.session["is_in_checkout"]
            self.request.session.save()
            return reverse("checkout")

        return reverse('register_complete')

    def form_valid(self, form):

        user = form.save(self.request)

        new_user = authenticate(username=self.request.POST['email'],
                                password=self.request.POST['password'])

        login(self.request, new_user)

        return HttpResponseRedirect(self.get_success_url())
