from django.contrib import messages
from django.core.urlresolvers import reverse
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView
from django.contrib.auth.models import User
from account.forms.email_activate import EmailActivateForm

class ActivateEmailResendView(FormView):
    model=User
    form_class=EmailActivateForm
    template_name="registration/activation_form.html"

    def get_success_url(self):
        return reverse("auth_activation_resend_completed")

    def form_valid(self, form):
        user  = form.save()
        messages.success(self.request, 'Your activation email was successfully sent.')
        return super(ActivateEmailResendView, self).form_valid(form)


class ActivateEmailResendCompletedView(TemplateView):
    template_name = "registration/activation_complete.html"
