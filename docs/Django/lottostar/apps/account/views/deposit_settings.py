from datetime import datetime, timedelta
from django.contrib import messages
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.views.generic.edit import FormView
from account.models import Restriction
from account.forms import DepositSettingsForm
from django.utils.translation import ugettext_lazy as _

#------------------------------------------------------------
from utils.mailer import SendMail


class DepositSettingsView( FormView):
    model = Restriction
    template_name = "account/deposit_limit_form.html"
    form_class = DepositSettingsForm

    restriction = {}

    def get_object(self):
        try:
            return_obj = Restriction.objects.filter(
                user = self.request.user,
                type = "deposit",
            ).order_by("-id")[:1].get()
        except:
            return_obj = False
        self.restriction = return_obj
        return return_obj

    def get_context_data(self, **kwargs):
        context = super(DepositSettingsView, self).get_context_data(**kwargs)
        context["restriction"] = self.get_object()
        return context

    def get_success_url(self):
        return reverse("deposit_limit_settings")

    def form_invalid(self, form):
        return super(DepositSettingsView, self).form_invalid(form)

    def form_valid(self, form):
        create_restriction = False

        if not self.get_object():
            create_restriction = True
        else:
            restriction_list = Restriction.objects.filter(
                user = self.request.user,
                type = "deposit",
                value__lte = form.cleaned_data["value"],
                created_at__gt = (datetime.now() - timedelta(hours=24)),
                created_at__lt = datetime.now()
            )
            if restriction_list:
                create_restriction = False
            else:
                if not int(form.cleaned_data["value"]):
                    create_restriction = False
                else:
                    create_restriction = True

            if not self.restriction:
                self.get_object()

            if self.restriction:
                if self.restriction.period != form.cleaned_data["period"]:
                    create_restriction = False

            if not create_restriction:
                messages.error(self.request, _('You are still in your 24 hour cooling off period.'))


        if create_restriction:
            restriction = form.save(commit=False)
            restriction.user = self.request.user
            restriction.type = "deposit"
            restriction.value = form.cleaned_data["value"]
            restriction.save()
            messages.success(self.request, _('Your settings were updated successfully.'))

            site = Site.objects.get_current()
            site_url = '%s%s' % ('http://', site.domain )

            SendMail(
                slug="deposit-limit",
                to=[self.request.user.email],
                user=self.request.user,
                limit_duration = form.cleaned_data["period"],
                limit_amount = form.cleaned_data["value"],
                site_url=site_url,
            ).send()

        return super(DepositSettingsView, self).form_valid(form)
