from django.core import paginator
from django.core.paginator import PageNotAnInteger, EmptyPage, Paginator
from django.views.generic import TemplateView, ListView, DetailView
from game.models import Product, ProductGameBoost
from play.models import Transaction, Entry, Ticket
from play.models.transaction import TransactionVoucher
from account.forms.replay_entries import ReplayEntriesForm#, DrawDateForm
from django.views.generic.edit import FormView
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from play.models.entries import BasketEntry


class HistoryListView(ListView):
    template_name = "account/history.html"
    model = Transaction
    paginate_by = 10
    context_object_name = "history_list"

    def get_queryset(self):
        queryset = super(HistoryListView, self).get_queryset()
        queryset = queryset.filter(user=self.request.user).order_by("-created_at")
        return queryset


#XXX: this should be a list view of entries, like ProductHistoryListView
class HistoryDetailsView(DetailView):
    template_name = "account/history_details.html"
    model = Transaction
    context_object_name = "history"

    def get_context_data(self, **kwargs):
        context = super(HistoryDetailsView, self).get_context_data(**kwargs)
        transaction = self.get_object()
        entries_qs = Entry.objects.filter(ticket__transaction=transaction).order_by("ticket__product")
        paginator = Paginator(entries_qs, 10)
        context["entry_count"] = entries_qs.count()
        page = int(self.request.GET.get('page', 1))
        context["page"] = page
        try:
            entry_list = paginator.page(page)
        except PageNotAnInteger:
            entry_list = paginator.page(1)
        except EmptyPage:
            entry_list = paginator.page(paginator.num_pages)

        context["entry_list"] = entry_list
        return context


class ProductHistoryListView(ListView):
    template_name = "account/history_details.html"
    model = Entry
    paginate_by = 10

    def get_queryset(self):
        queryset = super(ProductHistoryListView, self).get_queryset()
        transaction_id = self.kwargs.get('transaction_id', None)
        product_id = self.kwargs.get('product_id', None)
        if product_id and transaction_id:
            queryset = queryset.filter(
                ticket__transaction__id=transaction_id, ticket__product__id=product_id).order_by('event__draw_datetime')
        return queryset

    def get_context_data(self, **kwargs):
        context = super(ProductHistoryListView, self).get_context_data(**kwargs)
        context['history'] = Transaction.objects.get(id=self.kwargs.get('transaction_id'))
        return context

product_history = ProductHistoryListView.as_view()
