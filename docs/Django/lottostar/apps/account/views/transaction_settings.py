from datetime import timedelta, datetime
from django.contrib import messages
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect
from django.views.generic.edit import FormView
from account.models.restriction import Restriction
from account.forms import TransactionSettingsForm


#------------------------------------------------------------
class TransactionSettingsView(FormView):
    model = Restriction
    template_name = "account/transaction_limit_form.html"
    form_class = TransactionSettingsForm

    def get_context_data(self, **kwargs):
        context = super(TransactionSettingsView, self).get_context_data(**kwargs)
        context["is_house_setting"] = False

        now = datetime.now()
        date = now - timedelta(hours=24)

        try:
            context["restriction"] = Restriction.objects.filter(type="transaction", created_at__lte=date).latest('id')
        except Restriction.DoesNotExist:
            context["restriction"] = Site.objects.get_current()
            context["is_house_setting"] = True

        return context

    def get_success_url(self):
        return reverse("transaction_limit_settings")

    def form_invalid(self, form):
        return super(TransactionSettingsView, self).form_invalid(form)

    def form_valid(self, form):
        restriction = form.save(commit=False)
        restriction.user = self.request.user
        restriction.type = "transaction"
        restriction.save()
        messages.success(self.request, 'Your settings were updated successfully. '
                                       'Please note: The changes will be effective after 24 hours.')
        return super(TransactionSettingsView, self).form_valid(form)

