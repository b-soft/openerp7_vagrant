from django.core.urlresolvers import reverse
from django.views.generic.edit import FormView
from django.contrib.auth.models import User
from account.forms.update_password import UpdatePasswordForm

class UpdatePasswordView(FormView):
    model = User
    form_class = UpdatePasswordForm
    template_name = "registration/profile_password.html"

    def get_success_url(self):
        return reverse("account_profile_password_complete")

    def get_form_kwargs(self):
        kwargs = super(UpdatePasswordView, self).get_form_kwargs()
        kwargs["request"] = self.request
        return kwargs

    def form_valid(self, form):
        form.save()
        return super(UpdatePasswordView, self).form_valid(form)
