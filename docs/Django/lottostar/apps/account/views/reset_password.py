from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic.edit import FormView
from account.forms.password_reset import PasswordResetForm

class PasswordResetView(FormView):
    form_class = PasswordResetForm
    template_name = "registration/forgot_password_form.html"

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(reverse("auth_forgot_password_complete"))
