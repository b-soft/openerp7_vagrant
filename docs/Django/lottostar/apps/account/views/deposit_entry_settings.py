from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from django.views.generic.edit import CreateView
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponseRedirect

from account.forms import LimitRestrictionForm
from account.models import Restriction


class RestrictionCreateView(CreateView):
    model = Restriction
    form_class = LimitRestrictionForm
    template_name = 'account/limit_form.html'
    success_url = reverse_lazy('account_index')

    def get_object(self, queryset=None):
        return Restriction.get_latest_limit(user=self.request.user, type=self.kwargs.get('limit_type'))

    def form_valid(self, form):
        user = self.request.user
        latest_limit = self.get_object()
        limit_type = self.kwargs.get('limit_type')
        if latest_limit:
            limit_value = form.cleaned_data.get('value')
            if limit_value < latest_limit.value or latest_limit.value == 0:
                # cooling period does not apply to decreasing the limit value or setting a limit for the first time
                form.save(limit_type=limit_type, is_active=True, user=user, send_mail=True)
            else:
                # check if user has an limit update pending if true user cannot update limit
                if Restriction.update_limit_not_allowed(user, limit_type):
                    messages.error(self.request, _('You are still in your 24 hour cooling off period.'))
                else:
                    form.save(limit_type=limit_type, is_active=False, user=user, send_mail=True)
                    messages.success(self.request, _('Your settings were updated successfully.'))
        else:
            form.save(limit_type=limit_type, is_active=True, user=user, send_mail=True)

        return HttpResponseRedirect(self.success_url)

    def get_context_data(self, **kwargs):
        context = super(RestrictionCreateView, self).get_context_data(**kwargs)
        limit_type = self.kwargs.get('limit_type', None)
        context['limit_type'] = limit_type.title() if limit_type else None
        return context

create_limit = RestrictionCreateView.as_view()
