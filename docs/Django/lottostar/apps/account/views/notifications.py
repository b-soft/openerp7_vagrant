from django.views.generic.list import ListView
from system.models.log import Log
from django.core.paginator import PageNotAnInteger, EmptyPage, Paginator

class NotificationView(ListView):
    template_name = "account/notifications.html"
    model = Log
    context_object_name = "log_list"

    def get_context_data(self, **kwargs):
        context = super(NotificationView, self).get_context_data(**kwargs)
        paginator = Paginator(Log.objects.filter(user=self.request.user).order_by("-created_at"), 10)

        page = int(self.request.GET.get('page', 1))
        context["page"] = page
        try:
            log_list = paginator.page(page)
        except PageNotAnInteger:
            log_list = paginator.page(1)
        except EmptyPage:
            log_list = paginator.page(paginator.num_pages)

        context["log_list"] = log_list
        return context