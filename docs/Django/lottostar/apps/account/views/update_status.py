from django.contrib import messages
from django.contrib.auth import logout
from django.core.urlresolvers import reverse
from django.views.generic.edit import FormView
from account.forms import UpdateStatusSettingsForm

class UpdateStatusView(FormView):
    template_name = "account/update_status_form.html"
    form_class = UpdateStatusSettingsForm

    def get_success_url(self):
        return reverse("account_exclude_me")

    def get_initial(self):
        initial = super(UpdateStatusView, self).get_initial()
        user = self.request.user
        profile = user.profile
        initial['status'] = profile.status
        return initial

    def form_invalid(self, form):
        messages.error(self.request, "Please correct the following errors.")
        return super(UpdateStatusView, self).form_invalid(form)

    def form_valid(self, form):
        form.save(self.request.user)
        #log out banned users
        if not self.request.user.is_active:
            logout(self.request)
        messages.success(self.request, 'Your profile was updated successfully.')
        return super(UpdateStatusView, self).form_valid(form)
    #------------------------------------------------------------
