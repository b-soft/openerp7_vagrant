from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic.base import RedirectView
from account.signals import send_activate_email
from account.models.profile import UserProfile

class ActivateEmailView(RedirectView):
#    template_name="registration/activation_complete.html"

    def get(self, request, *args, **kwargs):
        try:
            profile = UserProfile.objects.get(activation_key=self.kwargs['activation_key'])
        except UserProfile.DoesNotExist:
            profile = False

        print UserProfile.objects.all()

        if profile:
            user = profile.user
            user.is_active = True
            user.save()

            profile.activation_key = 'ALREADY_ACTIVATED'
            profile.status = 'active'
            profile.save()

            messages.success(self.request, 'Your account was successfully activated.')
            send_activate_email.send(sender=None, user=user)

            if request.user.is_authenticated():
                return HttpResponseRedirect(reverse('account_profile'))
            else:
                return HttpResponseRedirect(reverse('auth_login'))
        else:
            return HttpResponseRedirect(reverse('auth_activation_failed'))
