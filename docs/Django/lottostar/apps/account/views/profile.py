from django.contrib import messages
from django.core.urlresolvers import reverse
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView
from account.forms.profile import ProfileForm

class ProfileView(FormView):
    template_name = "registration/profile.html"
    form_class = ProfileForm

    def get_form_kwargs(self):
        kwargs = super(ProfileView, self).get_form_kwargs()
        kwargs["request"] = self.request
        return kwargs

    def get_initial(self):
        initial = super(ProfileView, self).get_initial()
        user = self.request.user
        profile = user.profile

        initial['first_name'] = user.first_name
        initial['last_name'] = user.last_name
        initial['preferred_number'] = profile.preferred_number
        initial['email'] = profile.user.email
        initial['title'] = profile.title
        initial['gender'] = profile.gender
        initial['dob'] = profile.dob
        initial['address'] = profile.address
        initial['suburb'] = profile.suburb
        initial['city'] = profile.city
        initial['postal_code'] = profile.postal_code
        initial['country'] = profile.country


        return initial

    def get_success_url(self):
        return reverse("account_profile_completed")

    def form_invalid(self, form):
        return super(ProfileView, self).form_invalid(form)

    def form_valid(self, form):
        form.save(self.request.user)
        messages.success(self.request, 'Your profile was updated successfully.')
        return super(ProfileView, self).form_valid(form)


class ProfileCompletedView(TemplateView):
    template_name = "registration/profile_completed.html"