from datetime import datetime, timedelta
from django.db import transaction
from django.core.management.base import NoArgsCommand
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse

from utils.mailer import SendMail

from account.functions import generate_activation_key


class Command(NoArgsCommand):
    """
    Command for sending notifications about user profile status updates.
    """
    help = 'Send notifications for profile status update.'

    @transaction.commit_on_success
    def handle_noargs(self, **options):
        """
        loop through all the users on the site and find those that have suspended their accounts, check if the minimum
        required suspension period has passed and send them a notification re-activate their accounts.

        create email template with slug: "self-suspend-reactivate".
        """
        site = Site.objects.get_current()
        site_url = '{0}{1}'.format('http://', site.domain)
        for user in User.objects.iterator():
            if user.profile.monitor_status and not user.profile.reactive_request_sent:
                if(datetime.now() - user.profile.monitor_status) >= timedelta(days=182):
                    if not user.profile.activation_key or user.profile.activation_key == 'ALREADY_ACTIVATED':
                        user.profile.activation_key = generate_activation_key(user.username)
                    activation_link = '{0}{1}{2}'.format('http://', site.domain,
                            reverse('registration_activate', kwargs={'activation_key': user.profile.activation_key}))
                    link = '<a href="{0}">{1}</a>'.format(activation_link, _('click Here').title())
                    link_raw = '<a href="{0}">{1}</a>'.format(activation_link, activation_link)
                    SendMail(
                        slug="self-suspend-reactivate",
                        to=[user.email],
                        user=user,
                        site=site,
                        site_url=site_url,
                        link=mark_safe(link),
                        link_raw=mark_safe(link_raw),
                    ).send()
                    user.profile.monitor_status = None
                    user.profile.reactive_request_sent = True
                    user.profile.save()
