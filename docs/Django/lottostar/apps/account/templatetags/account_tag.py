from django import template
from registration.models import User

register = template.Library()

@register.filter
def admin_label_user(value, request):
    try:
        user = User.objects.get(id=request.session["admin_user_id"])
        return '%s %s' % (user.first_name, user.last_name)
    except:
        return "No user selected"