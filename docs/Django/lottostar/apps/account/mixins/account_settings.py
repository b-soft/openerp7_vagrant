from django.core.urlresolvers import reverse
from django.contrib import messages
from account.models import UserProfile

class AccountSettingsMixin(object):
    model = UserProfile

    def get_form_kwargs(self):
        kwargs = super(AccountSettingsMixin, self).get_form_kwargs()
        kwargs["request"] = self.request
        return kwargs

    def get_success_url(self):
        return reverse("account_profile_completed")

    def form_invalid(self, form):
        messages.error(self.request, "Please correct the following errors.")
        return super(AccountSettingsMixin, self).form_invalid(form)

    def form_valid(self, form):
        form.save(self.request.user)
        messages.success(self.request, 'Your profile was updated successfully.')
        return super(AccountSettingsMixin, self).form_valid(form)
