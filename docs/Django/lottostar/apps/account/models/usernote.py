from django.db import models
from django.contrib.auth.models import User

class UserNote(models.Model):
    user = models.ForeignKey(User)
    description =  models.TextField(blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "%s" % self.description

    class Meta:
        app_label = "auth"

