import hashlib, os
from datetime import date
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
import settings

from json_field.fields import JSONField
from account.functions import generate_activation_key
from account.managers import ActiveUserProfileManager

@models.permalink
def get_image_path(self, filename):
    fileName, fileExtension = os.path.splitext(filename)
    m = hashlib.md5()
    m.update(fileName)
    new_file_name = "%s%s" % (m.hexdigest(),fileExtension)
    return "photo/%d/%s" % (self.user.id, new_file_name)

class UserProfile(models.Model):

    TITLE_CHOICES = (
        ('Mr', _('Mr')),
        ('Mrs', _('Mrs')),
        ('Miss', _('Miss')),
    )

    GENDER_CHOICES = (
        ('m', _('Male')),
        ('f', _('Female')),
    )

    STATUS_CHOICES = (
        ('active', _('Active')),
        ('suspended', _('Suspended')),
        ('banned', _('Banned')),
        ('deactivated', _('Deactivated')),
    )

    objects = models.Manager()
    active = ActiveUserProfileManager()

    user = models.OneToOneField(User)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default="active")
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, null=True, blank=True)

    title = models.CharField(max_length=20, choices=TITLE_CHOICES, null=True, blank=True)
    dob = models.DateField(blank=True, null=True)
    preferred_number = models.CharField(max_length=255, null=True, blank=True)

    ip_address = models.CharField(max_length=75, blank=True, null=True)
    geo_location = JSONField(blank=True, null=True)

    address = models.CharField(max_length=255, null=True, blank=True)
    suburb = models.CharField(max_length=255, null=True, blank=True)
    city = models.CharField(max_length=255, null=True, blank=True)
    postal_code = models.CharField(max_length=255, null=True, blank=True)
    country = models.CharField(max_length=255, null=True, blank=True)
    preferred_language = models.CharField(max_length=50, choices=settings.LANGUAGES, default='en')

    newsletter = models.BooleanField(default=False)

    activation_key = models.CharField(max_length=50, blank=True, null=True)
    key_expires = models.DateTimeField(blank=True, null=True)

    play_credits = models.FloatField(max_length=20, default=0)
    points = models.IntegerField(max_length=20, default=0)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    monitor_status = models.DateTimeField(blank=True, null=True)
    reactive_request_sent = models.BooleanField(default=False)

    class Meta:
        app_label = "auth"

    def __unicode__(self):
        return "%s %s" % (self.user.first_name, self.user.last_name)

    def get_age(self):
        if self.dob:
            today = date.today()
            y = today.year - self.dob.year
            if today.month < self.dob.month or today.month == self.dob.month and today.day < self.dob.day:
                y -= 1
            return y
        else:
            return 'None'

    @property
    def get_gender(self):
        """
        gender is not a required field therefore we must accommodate users that have not entered this field.
        this is a property field, ie(how to call: profile.get_gender, result: Male)
        """
        if self.gender == 'male':
            return 'Male'
        elif self.gender == 'female':
            return 'Female'
        else:
            return None

    def save(self, *args, **kwargs):

        if self.activation_key is None:
            username = self.user.username
            self.activation_key = generate_activation_key(username=username)
            self.key_expires=date.today()

        if self.key_expires is None:
            self.default_package = date.today()

        return super(UserProfile, self).save(*args, **kwargs)

    @property
    def get_full_name_or_email(self):
        """
        Returns the full name of the user, or if none is supplied will return the email
        """
        user = self.user
        if user.first_name or user.last_name:
            # We will return this as translated string. Maybe there are some
            # countries that first display the last name.
            name = _("%(first_name)s %(last_name)s") % \
                {'first_name': user.first_name,
                 'last_name': user.last_name}
        else:
            # Fallback to the user's email
            name = "%(email)s" % {'email': user.email}
        return name.strip()

User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])
User.get_profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])

from account.signals.banned_profile import banned_user_email as _banned_user_email
