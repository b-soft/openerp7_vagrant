from django.db import models
from django.contrib.auth.models import User
from game.models import Game, Product


class UserWallet(models.Model):
    user = models.ForeignKey(User)

    product = models.ForeignKey(Product, blank=True, null=True)
    game = models.ForeignKey(Game)

    winnings = models.FloatField(max_length=20, default=0, blank=True, null=True)
    vouchers = models.IntegerField(max_length=10, default=0, blank=True, null=True)
    points = models.IntegerField(max_length=10, default=0, blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = "auth"
