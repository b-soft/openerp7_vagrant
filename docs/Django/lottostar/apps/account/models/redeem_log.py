from django.db import models
from django.utils.translation import ugettext_lazy as _
from account.models.userwallet import UserWallet

class RedeemLog(models.Model):
    user_wallet = models.ForeignKey(UserWallet)
    winnings = models.FloatField(max_length=20, default=0, blank=True, null=True)
    vouchers = models.IntegerField(max_length=10, default=0, blank=True, null=True)
    play_credits = models.FloatField(max_length=20, default=0, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    class Meta:
        verbose_name = "Redeem Log"
        verbose_name_plural = "Redeem Logs"
        app_label = "auth"