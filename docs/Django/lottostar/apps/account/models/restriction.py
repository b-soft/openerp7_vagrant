from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from model_utils import Choices
from datetime import datetime, timedelta
import settings

class Restriction(models.Model):
    TYPE_CHOICES = (
        ('transaction', _('Transaction limit')),
        ('deposit',     _('Deposit limit')),
        ('entry',       _('Entry limit')),
    )
    LIMIT_PERIOD_CHOICES = (
        ('daily',       _('Daily')),
        ('weekly',      _('Weekly')),
        ('monthly',     _('Monthly')),
    )

    user = models.ForeignKey(User)
    type = models.CharField(max_length=50, choices=TYPE_CHOICES, default="deposit")
    period = models.CharField(max_length=10, choices=LIMIT_PERIOD_CHOICES, null=False, blank=False)
    value = models.IntegerField(null=False, blank=False, default=0, verbose_name=_("Amount"), validators=[MaxValueValidator(9999999),MinValueValidator(0)])
    created_at = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        app_label = "auth"

    @classmethod
    def get_latest_limit(cls, user, type):
        """
        get the latest transaction limit model for the user, that has been created past the defined cooling period
        """
        try:
            # cooling period for limit update is 24 hours
            cooling_period = getattr(settings, 'COOLING_PERIOD', 0)
            update_cooling_period = datetime.now() - timedelta(hours=cooling_period)
            filters = {
                'user': user,
                'type': type,
                'created_at__lte': update_cooling_period
            }
            return cls.objects.filter(**filters).latest('created_at')
        except cls.DoesNotExist:
            return None

    @classmethod
    def update_limit_not_allowed(cls, user, type):
        # cooling period for limit update is 24 hours
        cooling_period = getattr(settings, 'COOLING_PERIOD', 0)
        update_cooling_period = datetime.now() - timedelta(hours=cooling_period)
        filters = {
            'user': user,
            'type': type,
            'created_at__gt': update_cooling_period
        }
        return cls.objects.filter(**filters).exists()


class UserRestrictedLog(models.Model):
    STATUS = Choices(
        (1, 'active', 'Active'),
        (2, 'suspended', 'Suspended'),
        (3, 'banned', 'Banned'),
    )

    REASON = Choices(
        (1, 'country not allowed', 'Country not allowed'),
        (2, 'IP location does not match registration country'),
        (3, 'duplicate account', 'Duplicate account'),
    )

    status = models.IntegerField('Status', choices=STATUS, default=STATUS.active, db_index=True)
    reason = models.IntegerField('Reason for status change', choices=REASON, db_index=True)
    user = models.ForeignKey(User, blank=True, null=True)
    notes = models.TextField(max_length=500, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = "auth"
        verbose_name = "User restricted log"
        verbose_name_plural = "User restricted logs"



class BannedIp(models.Model):
    ip_address = models.CharField(help_text='ONLY ENTER to the third quadrant, etc 242.14.51', max_length=75, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.ip_address

    class Meta:
        app_label = "auth"
        verbose_name = "Banned IP"
        verbose_name_plural = "Banned IPs"