from django.contrib import admin
from account.models.restriction import UserRestrictedLog


class UserRestrictedLogAdmin(admin.ModelAdmin):

    list_display = ('status', 'user',)
    list_filter = ('status',)
    fieldsets = (
        ('UserRestrictedLog', {
            'fields': ('status', 'reason', 'user', 'notes')
        }),

    )
admin.site.register(UserRestrictedLog, UserRestrictedLogAdmin)



