from django.contrib import admin
from account.models.restriction import UserRestrictedLog
from django.utils.translation import ugettext_lazy as _
from reversion import VersionAdmin
from flutter_modeladmin import RemoveDeleteAdmin

class UserRestrictedLogAdmin(RemoveDeleteAdmin, VersionAdmin):
    def customer_name(self, obj):
        return "%s %s <br><div style=\"font-size:10px;color:#999999;\">(%s)</div>" % (obj.user.first_name, obj.user.last_name, obj.created_at.strftime("%d-%m-%Y %H:%M:%S"))
    customer_name.short_description = 'Customer'
    customer_name.allow_tags = True

    list_display = ["customer_name", "notes", 'created_at']
    search_fields = ["user"]
    list_filter = ["user"]
    list_per_page = 20

    def get_model_perms(self, request): #hide from admin list
        return {}

admin.site.register(UserRestrictedLog, UserRestrictedLogAdmin)