from django.contrib import admin
from account.models.restriction import BannedIp
from flutter_modeladmin import RemoveDeleteAdmin
from reversion import VersionAdmin

class BannedIpAdmin(RemoveDeleteAdmin, VersionAdmin):
    list_display = ["ip_address", 'created_at']
    search_fields = ["ip_address"]
    list_filter = ["ip_address"]
    list_per_page = 20
admin.site.register(BannedIp, BannedIpAdmin)