from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from apps.account.models.profile import UserProfile
from flutter_modeladmin import RemoveDeleteAdmin
from reversion import VersionAdmin

class UserProfileAdmin(RemoveDeleteAdmin, VersionAdmin):

    fieldsets = (
      #  (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('status', 'gender', 'title')}),
        (_('Permissions'), {'fields': ('dob',)}),
     #   (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        )

    list_display = ["user", "status", "gender"]
    search_fields = ["user"]
    list_filter = ["user"]
    list_per_page = 20

    def get_model_perms(self, request): #hide from admin list
        return {}

admin.site.register(UserProfile, UserProfileAdmin)