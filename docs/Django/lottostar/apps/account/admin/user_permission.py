from django.contrib import admin
from django.contrib.auth.admin import GroupAdmin
from django.contrib.auth.models import User, Group, Permission
from django.utils.translation import ugettext_lazy as _
import reversion
from flutter_modeladmin import RemoveDeleteAdmin

admin.site.unregister(User)
admin.site.unregister(Group)

class UserPermission(Group):
    class Meta:
        proxy = True
        app_label = 'auth'
        verbose_name = _('User Permission')
        verbose_name_plural = _('User Permissions')



class LottoFlutterGroupAdmin(GroupAdmin, RemoveDeleteAdmin):
    pass
admin.site.register(UserPermission, LottoFlutterGroupAdmin)
reversion.register(Group)
reversion.register(Permission)