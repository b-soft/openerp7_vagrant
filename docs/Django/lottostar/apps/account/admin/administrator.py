from django.contrib import admin
from django.contrib.auth.models import User, Group
from django.core.urlresolvers import reverse
from django.forms.util import ErrorList
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from registration.models import RegistrationProfile
from account.models import UserWallet, Restriction
from account.models.profile import UserProfile
from flutter_modeladmin import RemoveDeleteAdmin
from django import forms
import reversion

admin.site.unregister(User)
admin.site.unregister(Group)

class RegistrationInline(admin.StackedInline):
    model = RegistrationProfile

class ProfileInline(admin.StackedInline):
    model = UserProfile
    fieldsets = (
        (_('Details'), {'fields': ('status', 'gender', 'title', 'dob', 'preferred_language', 'preferred_number',
                                   'address', 'suburb', 'city', 'country', 'postal_code')}),
        (_('Account'), {'fields': ('play_credits', 'points')}),
        )
    can_delete = False

class UserWalletInline(admin.StackedInline):
    model = UserWallet
    extra = 0

class RestrictionInline(admin.StackedInline):
    model = Restriction
    extra = 1

class Administrator(User):
    class Meta:
        proxy = True
        app_label = 'auth'
        verbose_name = _("Administrator")
        verbose_name_plural = _("Administrators")

class AdminForm(forms.ModelForm):
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    email = forms.EmailField(required=True)

    def clean(self):
        cleaned_data = self.cleaned_data

        cleaned_data["first_name"] = self.cleaned_data["first_name"].strip()
        cleaned_data["last_name"] = self.cleaned_data["last_name"].strip()

        if not cleaned_data["first_name"]:
            self._errors["first_name"] = ErrorList([u"Error"])

        if not cleaned_data["last_name"]:
            self._errors["last_name"] = ErrorList([u"Error"])

        return cleaned_data

    class Meta:
        model = Administrator


class AdministratorAdmin(RemoveDeleteAdmin):
    inlines = [ProfileInline, UserWalletInline, RestrictionInline]
    list_filter = ["is_active", "is_superuser"]
    form = AdminForm
    fieldsets = (
     #   (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active','is_superuser',
                                       'groups', 'user_permissions')}),
     #   (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        )

    def response_change(self, request, obj):
        if "_addanother" in request.POST:
            return HttpResponseRedirect(reverse("admin:auth_administrator_add"))
        if "_save" in request.POST:
            return HttpResponseRedirect(reverse("admin:auth_administrator_changelist"))
        return super(AdministratorAdmin, self).response_change(request, obj)

    def save_model(self, request, obj, form, change):
        obj.is_staff = True
        obj.save()

    def queryset(self, request):
        return User.objects.filter(is_staff=True)

    def name_and_email(self, obj):
        if obj.first_name or obj.last_name:
            return_value = "%s %s <br><div style=\"font-size:10px;color:#999999;\">%s</div>" % (obj.first_name, obj.last_name, obj.email)
        else:
            return_value = "%s <br><div style=\"font-size:10px;color:#999999;\">%s</div>" % (obj.username, obj.email)
        return return_value
    name_and_email.short_description = 'Customer'
    name_and_email.allow_tags = True

    list_display = ("name_and_email", "is_active", "last_login")
    list_per_page = 20

def user_unicode(self):
    if self.first_name or self.last_name:
        return_value = '%s %s' % (self.first_name, self.last_name)
    else:
        return_value = self.username
    return return_value

User.__unicode__ = user_unicode

admin.site.register(Administrator, AdministratorAdmin)
reversion.register(User) #reversion does not work with proxy models, register parent model