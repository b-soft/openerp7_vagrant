from django.contrib import admin
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django import forms
from django.forms.util import ErrorList
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from account.admin.administrator import ProfileInline
from account.models import Restriction
from account.models.usernote import UserNote
from flutter_modeladmin import RemoveDeleteAdmin
from account.models.userwallet import UserWallet
from play.models.transaction import Transaction

class RestrictionInline(admin.StackedInline):
    model = Restriction
    extra = 1

class Customer(User):
    class Meta:
        proxy = True
        app_label = 'auth'
        verbose_name = _("Customer")
        verbose_name_plural = _("Customers")

def customer_controls(obj):
    if obj.profile.status == "active":
        text = '<img src="/static/img/flag_green.gif"/ title="Change status" alt="Change status">'
    elif obj.profile.status == "suspended":
        text = '<img src="/static/img/flag_orange.gif" title="Change status" alt="Change status"/>'
    else:
        text = '<img src="/static/img/flag_red.gif" title="Change status" alt="Change status"/>'

    return '<a style="margin:5px" href="%s?user__id__exact=%s" title="Wallet" alt="Wallet"><img src="/static/img/wallet.gif"/></a>'\
            '<a style="margin:5px" href="%s?user__id__exact=%s" title="Transaction" alt="Transaction"><img src="/static/img/cart.png"/></a>'\
            '<a style="margin:5px" href="%s?user__id__exact=%s" title="Activity" alt="Activity"><img src="/static/img/16-clock.png"/></a>'\
            '<a style="margin:5px" href="%s?user__id__exact=%s" title="User note" alt="User note"><img src="/static/img/comment.gif"/></a>'\
            '<a style="margin:5px" href="%s?user__id__exact=%s" title="Responsible gambling" alt="Responsible gambling"><img src="/static/img/wrench.png"/></a>'\
           % (reverse("admin:auth_userwallet_changelist"), obj.id,
              reverse("admin:play_transaction_changelist"), obj.id,
              reverse("admin:system_log_changelist"), obj.id,
              reverse("admin:auth_usernote_changelist"), obj.id,
              reverse("admin:auth_restriction_changelist"), obj.id)
customer_controls.short_description = "Controls"
customer_controls.allow_tags = True

class CustomerForm(forms.ModelForm):
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    email = forms.EmailField(required=True)

    def clean(self):
        cleaned_data = self.cleaned_data
        cleaned_data["first_name"] = self.cleaned_data["first_name"].strip()
        cleaned_data["last_name"] = self.cleaned_data["last_name"].strip()

        if not cleaned_data["first_name"]:
            self._errors["first_name"] = ErrorList([u"Error"])

        if not cleaned_data["last_name"]:
            self._errors["last_name"] = ErrorList([u"Error"])

        return cleaned_data

    class Meta:
        model = Customer

class CustomerAdmin(RemoveDeleteAdmin):
    inlines = [ProfileInline]
    list_filter = ["is_active"]
    form = CustomerForm
    fieldsets = (
        (_('Personal info'), {'fields': ('is_active', 'first_name', 'last_name', 'email')}),
        )

    def has_add_permission(self, request):
        return False

    def save_model(self, request, obj, form, change):
        obj.is_staff = False
        obj.is_superuser = False
        obj.save()

    def response_change(self, request, obj):
        if "_addanother" in request.POST:
            return HttpResponseRedirect(reverse("admin:auth_customer_add"))
        if "_save" in request.POST:
            return HttpResponseRedirect(reverse("admin:auth_customer_changelist"))
        return super(CustomerAdmin, self).response_change(request, obj)

    def queryset(self, request):
        return User.objects.filter(is_staff=False, is_superuser=False)

    def name_and_email(self, obj):
        if obj.first_name or obj.last_name:
            return_value = "%s %s <br><div style=\"font-size:10px;color:#999999;\">%s</div>" % (obj.first_name, obj.last_name, obj.email)
        else:
            return_value = "%s <br><div style=\"font-size:10px;color:#999999;\">%s</div>" % (obj.username, obj.email)
        return return_value
    name_and_email.short_description = 'Customer'
    name_and_email.allow_tags = True

    def total_cash_spend(self, obj):
        total=0
        for transaction in Transaction.objects.filter(user_id=obj.id):
            total += transaction.play_credits
        return "%s %s" % ('&euro;', total)
    total_cash_spend.short_description = 'Total Cash Spend'
    total_cash_spend.allow_tags = True

    def unclaimed_winnings(self, obj):
        winnings=0
        for wallet in UserWallet.objects.filter(user_id=obj.id):
            winnings += wallet.winnings
        return "%s %s" % ('&euro;', winnings)
    unclaimed_winnings.short_description = 'Unclaimed winnings'
    unclaimed_winnings.allow_tags = True


    def play_credits(self, obj):
        return "%s %s" % ('&euro;', obj.profile.play_credits)
    play_credits.short_description = 'Play credits'
    play_credits.allow_tags = True

    def user_note(self, obj):
        return UserNote.objects.filter(user_id=obj.id).latest('created_at')
    user_note.short_description = 'Notes'
    user_note.allow_tags = True

    def mobile_number(self, obj):
        return obj.profile.preferred_number
    mobile_number.short_description = 'Mobile number'
    mobile_number.allow_tags = True

    def user_langauge(self, obj):
        return '<img src="/static/img/flags/icons/%s.png"/ title="Language" alt="Language">' % obj.profile.preferred_language
    user_langauge.short_description = 'Language'
    user_langauge.allow_tags = True

    def last_played_on(self, obj):
        try:
            trans_obj= Transaction.objects.filter(user_id=obj.id).order_by('-id')
        except Transaction.DoesNotExist:
            trans_obj = False

        last_played ="Never played"
        if trans_obj:
            last_played = trans_obj[0].created_at.strftime("%d-%m-%Y %H:%M:%S")
        return "%s" % last_played
    last_played_on.short_description = 'Last played on'
    last_played_on.allow_tags = True

    search_fields = ['id', 'first_name', 'last_name', 'email']
    list_display = ("name_and_email", "total_cash_spend", "unclaimed_winnings", "play_credits", "last_played_on",
                    "user_langauge", "mobile_number", "is_active", "user_note", "last_login", customer_controls)
    list_per_page = 20

    def restriction_log(self, obj):
        url = reverse("admin:account_userrestrictedlog_changelist")
        lookup = u"user__id__exact"
        text = u"add"
        return u"<a href='%s?%s=%d'>%s</a>" % (url, lookup, obj.pk, text)
    restriction_log.short_description = 'add'
    restriction_log.allow_tags = True


def user_unicode(self):
    if self.first_name or self.last_name:
        return_value = '%s %s' % (self.first_name, self.last_name)
    else:
        return_value = self.username
    return return_value

User.__unicode__ = user_unicode

admin.site.register(Customer, CustomerAdmin)
