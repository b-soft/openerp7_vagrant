from django import http
from django.core import urlresolvers
from django.contrib.auth import logout
import settings
from django.contrib.auth.decorators import login_required


class IPRequestMiddleware(object):

    """
    IP middleware
    """

    def process_request(self, request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[-1].strip()
        else:
            ip = request.META.get('REMOTE_ADDR')

        if ip == '127.0.0.1':
            ip = '41.164.141.98'

        request.client_ip = ip

def _get_view_name(view):
    """
    Resolve a view function or instance to a name.

    This implementation's fallback matches that of `contrib.admindocs.views.views_index()`.
    """
    return '{0}.{1}'.format(view.__module__, getattr(view, '__name__', view.__class__.__name__))

def _resolve(request):
    """
    Helper to resolve the `request` to a full `ResolverMatch`.

    This mimics `django.core.handlers.BaseHandler.get_response()`'s logic, but
    doesn't discard the `ResolverMatch` instance's additional metadata by
    unpacking it.
    """
    resolver = urlresolvers.get_resolver(urlresolvers.get_urlconf())
    return resolver.resolve(request.path_info)


class LottoFlutterLoginMiddleware(object):
    """
    This implements lotto flutter's login policy.

    At the moment, all views will be wrapped in Django's `login_required` decorator.
    """

    def process_view(self, request, view_func, view_args, view_kwargs):
        # Re-resolve the request, so that we can access the url_name.
        # This is necessary to disambiguate generic views, in particular.
        match = _resolve(request)

        if not request.user.is_authenticated():
            return None

        else: #if user is authenticated check account status
            profile_status = request.user.profile.status
            if not self.check_profile_restriction(_get_view_name(view_func), match.url_name, profile_status,request):
                return None # load requested page if the user has not been restricted
            else:
                #TODO: Olwethu: 16/09/2013: what pages to redirect suspended users to. Test with 401 page
                return http.HttpResponseForbidden('<h1>Forbidden</h1>')

    def check_profile_restriction(self, view_name, url_name, profile_status, request):
        """
        """
        restrict = False

        # restrict access to these pages for suspended users
        restrict_access = [
            'account_profile',
            'account_profile_password',
            'account_profile_email',
            'transaction_limit_settings',
            'deposit_limit_settings',
            'entry_limit_settings',
            'account_exclude_me',
            'checkout',
            'paycredit',
        ]

        if profile_status == 'suspended':
            #add request winnings urls for suspended users
            self_suspended_restriction = restrict_access + []
            if url_name in set(self_suspended_restriction):
                restrict = True

        elif profile_status == 'self_suspended':
            if url_name in set(restrict_access):
                restrict = True

        # if auth check failed prevent banned users from accessing any part of the site (authentication safe guard)
        elif profile_status == 'banned':
            logout(request)
            restrict = True

        return restrict

