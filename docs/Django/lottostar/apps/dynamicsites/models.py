from django.contrib.sites.models import Site
from django.db.models import FileField, CharField, IntegerField
from dynamicsites.fields import SubdomainListField, FolderNameField

"""
Monkey-patch the Site object to include a list of subdomains
Future ideas include:
* Site-enabled checkbox
* Site-groups
* Account subdomains (ala basecamp)
"""

FolderNameField(blank=True).contribute_to_class(Site, 'folder_name')
SubdomainListField(blank=True).contribute_to_class(Site, 'subdomains')
CharField("Title", max_length=255, null=True, blank=True).contribute_to_class(Site,'title')
CharField("Company Title", max_length=255, null=True, blank=True).contribute_to_class(Site,'company')
FileField("Logo",max_length=255, null=True, blank=True, upload_to='logo/').contribute_to_class(Site, 'logo')
CharField("Google Analytics Code", max_length=255, null=True, blank=True).contribute_to_class(Site, 'ga_code')
CharField("Meta Description", max_length=255, null=True, blank=True).contribute_to_class(Site, 'meta_description')
CharField("Main Site", max_length=255, null=True, blank=True).contribute_to_class(Site, 'main_site')
CharField("Prefix", max_length=255, null=True, blank=True).contribute_to_class(Site, 'prefix')

CharField("Period", max_length=255, null=True, blank=True, default="monthly").contribute_to_class(Site, 'period')
IntegerField("Transaction value", default=3000, null=True, blank=True).contribute_to_class(Site, 'transaction_value')
IntegerField("Deposit value", default=3000, null=True, blank=True).contribute_to_class(Site, 'deposit_value')
IntegerField("Entry value", default=100, null=True, blank=True).contribute_to_class(Site, 'entry_value')
IntegerField("Cooling off period", default=24, null=True, blank=True).contribute_to_class(Site, 'cooling_off_period')


@property
def has_subdomains(self):
    return len(self.subdomains)

@property
def default_subdomain(self):
    """
    Return the first subdomain in self.subdomains or '' if no subdomains defined
    """
    if len(self.subdomains):
        if self.subdomains[0]=="''":
            return ''
        return self.subdomains[0]
    return ''

Site.has_subdomains = has_subdomains
Site.default_subdomain = default_subdomain

