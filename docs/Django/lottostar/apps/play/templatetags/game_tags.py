import datetime
from django import template
from django.db.models.aggregates import Count
from django.template.base import TemplateSyntaxError, VariableDoesNotExist, Node
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe
from game.models import Event, Game
from play.models.transaction import Ticket, Entry
from system.functions import search_and_replace
from django.core.urlresolvers import reverse_lazy

register = template.Library()

@register.filter
def get_range(value):
    return range(1, int(value)+1)

@register.filter
def get_dict(dictionary, value):
    try:
        return int(dictionary[value])
    except:
        return dictionary[value]

@register.filter
def add_page(value, page):
    value = ((page-1) * 25) + value
    return value

@register.filter
def style_balls(value, class_name):
    if not value:
        value = ""
    new_value = ""
    if isinstance(value, list):
        for item in value:
            new_value += "<div class='%s'>%s</div>" % (class_name, item)
        return mark_safe(new_value)
    return value

@register.filter
def name_boost_value(value, boost_list):
    if not int(value):
        value = _("Standard")
    return value


@register.filter
def format_days(value):
    days = value.split("-")
    html = ""
    for x, day in enumerate(days):
        if x:
            if x == len(days) - 2:
                html += ", "
            else:
                html += " & "
        html += day.title()
    return html


@register.filter
def makeclock(time_string):
    clock = ""
    for letter in time_string:
        if letter == ":":
            clock += "<span class='sep'></span>"
        else:
            clock += "<span>%s</span>" % letter
    return mark_safe(clock)

@register.filter
def get_draw_datetime(draw):
    return Event.objects.get(pk=int(draw)).draw_datetime

@register.simple_tag
def is_ball_selected(session,get_product, get_ballset, get_ticket, get_number, exclude_selection):
    #force format
    get_product = int(get_product)
    get_ballset = int(get_ballset)
    get_ticket = int(get_ticket)
    get_number = int(get_number)

    return_class = 'id="ball-%s-%s-%s-%s"' % (get_product, get_ticket, get_ballset, get_number)
    cart = session.get("tickets",{})

    if cart.get(get_ticket, False):
        if cart[get_ticket].get(get_ballset, False):
            if get_number in cart[get_ticket][get_ballset]:
                return_class += ' class="on"'

    addon = session.get("addons",{})

    if exclude_selection == "true":
        if addon.get(get_ticket, False):
            if get_number == addon[get_ticket]:
                return_class += ' class="off"'
    return return_class

@register.simple_tag
def is_scheduled(schedule, session, day):
    is_available = False

    for event in schedule:
        if day == event.params["dow"]:
            is_available = True

    scheduled = session.get("draws", {})

    days = scheduled.get("days", "").split("-")

    if is_available:
        return_value = ""
        for session_day in days:
            if session_day == day:
                return_value = "checked"
    else:
        return_value = "disabled"
    return return_value


@register.simple_tag
def get_schedule(session, event):
    return_value = ""
    scheduled = session.get("draws", {})
    if scheduled.get("start_event", False):
        if int(scheduled['start_event']) == int(event.id):
            return_value = "selected='selected'"
    return return_value


def get_first_key(dictionary):
    for item in dictionary:
        return dictionary[item]

@register.simple_tag
def is_addon_selected(session, get_ticket, get_number=False, exclude_selection="false"):
    return_value = ""
    addon = session.get("addons", {})
    if addon.get(get_ticket, False):
        if addon[get_ticket] == get_number:
            return_value = 'selected="selected"'
        else:
            return_value = "class='active'"
    return return_value


@register.filter
def get_addon_list(addon_id, ballset_addon):
    return ballset_addon[int(addon_id)]


class WidthRatioPlusOneNode(Node):
    def __init__(self, val_expr, max_expr, max_width):
        self.val_expr = val_expr
        self.max_expr = max_expr
        self.max_width = max_width

    def render(self, context):
        try:
            value = self.val_expr.resolve(context)
            max_value = self.max_expr.resolve(context)
            max_width = int(self.max_width.resolve(context))
        except VariableDoesNotExist:
            return ''
        except (ValueError, TypeError):
            raise TemplateSyntaxError("widthratio final argument must be an number")
        try:
            value = float(value)
            max_value = float(max_value)
            ratio = (value / max_value) * max_width
            ratio += 1
        except ZeroDivisionError:
            return '1'
        except (ValueError, TypeError):
            return ''
        return str(int(round(ratio)))

@register.tag
def widthratio_plusone(parser, token):
    bits = token.contents.split()
    if len(bits) != 4:
        raise TemplateSyntaxError("widthratio takes three arguments")
    tag, this_value_expr, max_value_expr, max_width = bits

    return WidthRatioPlusOneNode(parser.compile_filter(this_value_expr),
        parser.compile_filter(max_value_expr),
        parser.compile_filter(max_width))

@register.simple_tag
def get_entries(entry, ticket_list, session):
    return_value = "--"
    ticket_id = int(entry["ticket"])
    for ticket in ticket_list:
        if int(ticket["ticket"]) == ticket_id:
            entries = int(ticket["number_of_tickets"])

            if not entries:
                if session["tickets"].get(ticket_id, False):
                    return_value = _("Incomplete")
                else:
                    return_value = "--"
            else:
                if entries < 2:
                    return_value = _("%s entry" % entries)
                else:
                    return_value = _("%s entries" % entries)
    return return_value


@register.simple_tag
def get_tickets(transaction_id):
    ticket_list = Ticket.objects.filter(transaction_id=int(transaction_id))
    transaction_entry_list = []
    for ticket in ticket_list:
        entry_list = Entry.objects.filter(ticket=ticket).values('event__game', 'event', 'ticket__product').annotate(Count('event__game'))
        product = ticket.product.id
        entry_count = 0
        game_name = ''
        for entry in entry_list:
            entry_count += int(entry['event__game__count'])
            game_name = Game.objects.filter(id=int(entry['event__game'])).values_list('name', flat=True)[0]
        event_dates = Event.objects.filter(id__in=entry_list.values_list('event__id', flat=True)).values_list('draw_datetime', flat=True)

        if event_dates:
            if len(event_dates) > 1:
                date_str = '{0} - {1}'.format(event_dates[0].strftime("%d %b %Y"), event_dates.order_by('draw_datetime')[0].strftime("%d %b %Y"))
            else:
                date_str = '{0}'.format(event_dates[0].strftime("%d %b %Y"))
            trans_entries = {
                'event': date_str,
                'entries': entry_count,
                'game': game_name,
                'product': product
            }
            transaction_entry_list.append(trans_entries)
    #we should always try to keep html on the templates. simply return a list of items needed to do the same thing on the template
    html_str = ''
    for transaction_entry in transaction_entry_list:
        url = reverse_lazy('product_history', kwargs={'transaction_id': transaction_id, 'product_id': transaction_entry['product']})
        html_str += '<a style="font-size: 12px;" href={0}> x {1} {2} ({3})</a></br>'.format(
            url, transaction_entry['entries'], transaction_entry['game'], transaction_entry['event'],)
    return html_str

@register.simple_tag
def get_transaction_total(transaction_id):
    entries_list = Entry.objects.filter(ticket__transaction=int(transaction_id))
    count_vouchers=0
    total=0
    for entry in entries_list:
        if entry.price > 0:
            total += entry.price
        else:
            count_vouchers+=1

    price_html=''
    if total > 0:
        price_html += '<div>&euro; %s</div>' % total

    if count_vouchers > 0:
        if count_vouchers == 1:
            voucher_text = "Voucher"
        else:
            voucher_text = "Vouchers"
        price_html += '<div>%s %s</div>' % (count_vouchers, voucher_text)
    return price_html


@register.simple_tag
def string_replace(text, vars):
    return search_and_replace(text, vars)
