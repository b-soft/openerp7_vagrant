from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.db import models
from django.utils.translation import ugettext_lazy as _
from djorm_pgarray.fields import ArrayField
from json_field import JSONField
from model_utils import Choices
from game.models import Game, Event, GameDrawResult, Product, ProductGame
from djorm_expressions.models import ExpressionManager
from model_utils.models import TimeStampedModel


class Transaction(models.Model):
    PAYMENT_STATUS_CHOICES = Choices(
        (0, 'pending', _('Pending')),
        (1, 'completed', _('Completed')),
        (2, 'cancelled', _('Cancelled')),
    )

    TRANSACTION_TYPE_CHOICES = Choices(
        (0, 'pending', _("Pending")),
        (1, 'purchase_play_credit', _("Purchase Play Credit")),
        (2, 'entry_purchased', _("Entry Purchase")),
        (3, 'cancelled', _("Cancelled")),
    )

    site = models.ForeignKey(Site)
    user = models.ForeignKey(User)

    transaction_type = models.CharField(db_index=True, max_length=2, choices=TRANSACTION_TYPE_CHOICES)
    payment_status = models.CharField(max_length=2, choices=PAYMENT_STATUS_CHOICES)

    total_value = models.DecimalField(max_digits=20, decimal_places=2)
    discount = models.DecimalField(max_digits=6, decimal_places=2)
    play_credits = models.DecimalField(max_digits=20, decimal_places=2) #how many play credits were used.
    total_paid = models.DecimalField(max_digits=20, decimal_places=2) #the actual amount paid
    credits_bought = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True) #total number of play credits bought
    total_vouchers_used = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True) #total cost of vouchers used

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Transaction"
        verbose_name_plural = "Transactions"
        app_label = "play"

    def get_transaction_type_display(self):
        return self.TRANSACTION_TYPE_CHOICES._choices[int(self.transaction_type)][1]

    def get_payment_status_display(self):
        return self.PAYMENT_STATUS_CHOICES._choices[int(self.payment_status)][1]

class TransactionVoucher(models.Model):
    transaction = models.ForeignKey(Transaction)

    product = models.ForeignKey(Product)
    game = models.ForeignKey(Game)

    vouchers = models.IntegerField(max_length=6)
    single_voucher_cost = models.DecimalField(max_digits=6, decimal_places=2)
    total_voucher_cost = models.DecimalField(max_digits=6, decimal_places=2)
    total_game_cost = models.DecimalField(max_digits=6, decimal_places=2)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Transaction Vouchers"
        verbose_name_plural = "Transaction Voucher"
        app_label = "play"


class Ticket(models.Model):
    transaction = models.ForeignKey(Transaction)
    product = models.ForeignKey(Product)
    game = models.ForeignKey(ProductGame)
    deck = models.ForeignKey('Deck')
    selected_numbers = JSONField(null=True, blank=True)
    addons = models.PositiveSmallIntegerField(null=True, blank=True) #ticket superstar value
    used_vouchers = models.IntegerField(null=True, blank=True)
    total_cost = models.DecimalField(max_digits=12, decimal_places=2) #total cost of the ticket
    total_entries = models.IntegerField(blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Ticket"
        verbose_name_plural = "Tickets"
        app_label = "play"


class Entry(models.Model):
    ticket = models.ForeignKey(Ticket)
    event = models.ForeignKey(Event)

    boost_value = models.IntegerField(max_length=6)
    balls1 = ArrayField(dbtype="int", null=True, blank=True, db_index=True)
    balls2 = ArrayField(dbtype="int", null=True, blank=True, db_index=True)
    balls3 = ArrayField(dbtype="int", null=True, blank=True, db_index=True)

    price = models.DecimalField(max_digits=6, decimal_places=2)
    game_draw_result = models.ForeignKey(GameDrawResult, null=True, blank=True)
    winnings = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True)
    is_processed = models.BooleanField(default=False, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = ExpressionManager()

    class Meta:
        verbose_name = "Entry"
        verbose_name_plural = "Entries"
        app_label = "play"


class ReplayEntries(models.Model):
    entry = models.ForeignKey('play.Entry')
    user = models.ForeignKey(User)
    event = models.ForeignKey(Event)
    is_processed = models.BooleanField(default=False)

    class Meta:
        verbose_name = "ReplayEntries"
        verbose_name_plural = "Replay Entries"
        app_label = "play"


class Deck(TimeStampedModel):
    transaction = models.ForeignKey('play.Transaction')
    start_on = models.ForeignKey('game.Event')
    product = models.ForeignKey('game.Product')
    product_game = models.ForeignKey('game.ProductGame')
    jackpot = models.DecimalField(max_digits=20, decimal_places=2)
    boost = models.PositiveIntegerField(max_length=10, null=True, blank=True)
    weeks = models.PositiveIntegerField(max_length=10, null=True, blank=True)
    draw_days = ArrayField(dbtype="varchar(255)", null=True, blank=True)
    total_cost = models.DecimalField(max_digits=10, decimal_places=2)
    addons = JSONField(blank=True, null=True)
    tickets = JSONField(blank=True, null=True)
    total_entries = models.IntegerField(blank=True, null=True)

    class Meta:
        app_label = "play"
