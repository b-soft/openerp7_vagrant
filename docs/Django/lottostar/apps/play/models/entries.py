from django.contrib.auth.models import User
from django.db import models
from game.models import ProductGame, Product
from json_field import JSONField

class BasketEntry(models.Model):
    user = models.ForeignKey(User)

    product = models.ForeignKey(Product)
    game = models.ForeignKey(ProductGame)

    boost_value = models.IntegerField(max_length=6)
    balls = JSONField(blank=True, null=True)
    addons = JSONField(blank=True, null=True)
    draws = JSONField(blank=True, null=True)

    prize = models.FloatField(default=0, blank=True, null=True) #jackpot

    cost = models.FloatField(default=0, blank=True, null=True)
    tickets = models.IntegerField(max_length=6, blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)


    class Meta:
        verbose_name = "Basket Entry"
        verbose_name_plural = "Basket Entries"
        app_label = "play"
