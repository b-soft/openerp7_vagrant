from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _

class TransactionType(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "Transaction type"
        verbose_name_plural = "Transaction types"
        app_label = "play"

