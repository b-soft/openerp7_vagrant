from django.contrib.auth.models import User
from django.db import models
from game.models import Event
from play.models import SystemEmail

class ResultEmailUser(models.Model):
    user = models.ForeignKey(User)
    event = models.ForeignKey(Event)
    system_email = models.ForeignKey(SystemEmail, null=True, blank=True)

    has_sent = models.BooleanField(default=False, db_index=True)
    has_failed = models.BooleanField(default=False, db_index=True)

    class Meta:
        app_label = "play"
