from django.db import models
from djorm_pgarray.fields import ArrayField
from django.contrib.auth.models import User


class SystemEmail(models.Model):
    user = models.ForeignKey(User, related_name='email_user')
    subject   = models.CharField(max_length=255)
    body_html = models.TextField(blank=True, null=True)
    body_text = models.TextField(blank=True, null=True)
    to_address = ArrayField(dbtype="varchar(255)", null=True, blank=True)
    from_address = ArrayField(dbtype="varchar(255)", null=True, blank=True)
    is_sent = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now = True)
    modified_at = models.DateTimeField(auto_now_add = True)

    def __unicode__(self):
        return self.subject

    class Meta:
        app_label = "play"

