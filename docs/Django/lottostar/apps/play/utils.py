import itertools
import settings
from django.contrib.sites.models import Site
from django.template.loader import render_to_string
from django.utils.encoding import force_unicode
from django.core.mail import EmailMultiAlternatives
from django.core.urlresolvers import reverse
from django.db.models import Sum


def single_ballset_combinations(selected_numbers, game_main):
    """
    Creates a unique combination of numbers from the selected numbers of a ballset, This will give us the total number
    of tickets that can be purchased with the selected numbers.

    arguments:
        selected_numbers: this should be an iterable. ie;[1, 2, 3]
        game_main: integer value used to identify @login_requiredthe minimum selection of balls for a valid ticket.
                   ie; 6 (set by admin known as 'main' on the admin site)

    returns:
        combinations: a list of all of the possible combinations of selected_numbers numbers
        ie; [(1,2), (2,3), (1,3)]
    """
    return list(itertools.combinations(selected_numbers, game_main))

def create_combined_selected_numbers(first_selected_numbers, second_selected_numbers):
    """
    combine two lists into a 2-D list

    arguments:
        first_selected_numbers: ie; [1, 2, 3]
        second_selected_numbers: ie; [2, 3]

    returns:
        combined_selected_numbers: ie;[[1, 2, 3], [2, 3]]
    """
    combined_selected_numbers = []
    combined_selected_numbers.append(list(first_selected_numbers))
    combined_selected_numbers.append(list(second_selected_numbers))
    return combined_selected_numbers

def multiple_ballset_combinations(first_ballset_selected_numbers, first_ballset_main, second_ballset_selected_numbers, second_ballset_main):
    """
    Create a unique combination of numbers from the selected numbers of each ballset, Combine these two ballsets
    combinations to create a single list with unique rows.

    arguments:
        first_ballset_selected_numbers: a list of all the numbers selected on the first ballset.
        ie; [(1, 2, 3), (1, 3, 2), (3, 2, 1)...]
        second_ballset_selected_numbers: a list of all the numbers selected on the second ballset
        ie; [(1, 2), (1, 3), (3, 2)]

    returns:@login_required
        combinations: this is a list that combines the 'first_ballset_selected_numbers' with
        'second_ballset_selected_numbers' in a unique way. ie;
         [
            [[1, 2, 3], [1, 2]], #row contains arrays for 1st and 2nd ballset
            [[1, 2, 3], [1, 3]],
            [[1, 2, 3], [3, 2]],
            [[1, 3, 2], [1, 2]],
            [[1, 3, 2], [1, 3]],
            [[1, 3, 2], [3, 2]],
            .........
        ]
    """
    first_ballset_selected_numbers = single_ballset_combinations(first_ballset_selected_numbers, first_ballset_main)
    second_ballset_selected_numbers = single_ballset_combinations(second_ballset_selected_numbers, second_ballset_main)
    combinations = []
    for first_selected_numbers_combination in first_ballset_selected_numbers:
        for second_selected_numbers_combination in second_ballset_selected_numbers:
            combinations.append(create_combined_selected_numbers(first_selected_numbers_combination,
                                                                 second_selected_numbers_combination))
    return combinations

def email_transaction(user, transaction, recipient_list=None):
    """
    used to email transaction record to a user.
    """
    total_entries = transaction.deck_set.all().aggregate(Sum('total_entries'))
    site = Site.objects.get_current()
    my_orders = '{0}{1}{2}'.format('http://', site.domain, reverse('account_history'))
    account_balance = '{0}{1}{2}'.format('http://', site.domain, reverse('account_balance'))
    contact_us = '{0}{1}{2}'.format('http://', site.domain, reverse('contact'))
    email_content = {
        "user": user, "transaction": transaction, "decks": transaction.deck_set.all(),
        "buy_play_credits": transaction.credits_bought, "used_vouchers": transaction.total_vouchers_used,
        "total_entries": total_entries['total_entries__sum'], 'my_orders': my_orders,
        'account_balance': account_balance, 'contact_us': contact_us
    }

    subject = 'Purchase Confirmation (Transaction #{0})'.format(transaction.id)
    template_name = 'emails/html/transaction_complete.html'
    message_html = render_to_string(template_name, email_content)
    if not recipient_list:
        recipient_list = [user.email]
    send_html_mail(recipient_list=recipient_list, subject=subject, message_html=message_html)

def not_blank(email):
    if email:
        return True
    else:
        return False

def clean_recipients(recipient_list):
    """
    Remove blank recipients before trying to send to them
    """
    recipients = filter(not_blank, recipient_list)
    final_list = ()
    for recipient in recipients:
        if not recipient in final_list:
            final_list += (recipient,)

    return final_list

def send_html_mail(recipient_list, subject, message_html, message=None, from_email=None):
    """
    Function to send HTML e-mails. message is used for sending an email with plain text.
    """
    #if from_email is not provided, default to the site's email.
    if not from_email:
        from_email = getattr(settings, 'DEFAULT_FROM_EMAIL', None)
    # need to do this in case subject used lazy version of ugettext
    subject = force_unicode(subject)
    message = force_unicode(message)
    recipient_list = clean_recipients(recipient_list)

    email = EmailMultiAlternatives(subject, message, from_email, recipient_list)
    email.attach_alternative(message_html, "text/html")
    email.send()

    return True
