from django import forms
import settings
from apps.game.models import Product


class ProcessForm(forms.Form):
    total_cost = forms.CharField(widget=forms.HiddenInput)


class CheckoutForm(forms.Form):
    buy_play_credits = forms.FloatField(widget=forms.TextInput(attrs={"onkeypress": "return numbersonly(event)"}), required=False, initial=10.00)

    def clean_buy_play_credits(self):
        buy_play_credits = self.cleaned_data['buy_play_credits']
        play_with_credits = self.data.get('play_with_credits', False)
        user = self.data.get('user', 'AnonymousUser')
        if user != 'AnonymousUser' and play_with_credits:
            minimum_play_credits = getattr(settings, 'MINIMUM_PLAY_CREDITS', None)
            if buy_play_credits < minimum_play_credits:
                raise forms.ValidationError('Minimum of {0} is allowed'.format(minimum_play_credits))
        return buy_play_credits


class PlayCreditForm(forms.Form):
    buy_play_credits = forms.FloatField(widget=forms.TextInput(attrs={"onkeypress": "return numbersonly(event)"}), required=False, initial=10.00)


class GameOverviewForm(forms.Form):
    games = forms.ModelMultipleChoiceField(queryset=Product.objects.all(), widget=forms.CheckboxSelectMultiple(), required=False)
    from_date = forms.DateField(input_formats=("%d-%m-%Y",), required=False)
    to_date = forms.DateField(input_formats=("%d-%m-%Y",), required=False)


class ResendTransactionEmailForm(forms.Form):
    to = forms.EmailField()
