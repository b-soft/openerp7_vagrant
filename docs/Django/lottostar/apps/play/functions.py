from __future__ import division
from django.contrib.auth.decorators import login_required
from django.contrib.humanize.templatetags.humanize import intcomma
from django.template.defaultfilters import floatformat
import math
from account.models import UserWallet, Restriction
from apps.account.models.profile import UserProfile
from game.models import GamePrize, ProductGameBoost, Product
from play.models import BasketEntry
from datetime import timedelta, datetime
from play.models import ReplayEntries
from django.db.models import Sum

def factorial(n):
    if not n:
        return 1
    else:
        return n * factorial(n-1)

def factorial_probability(x, y):
    return factorial(x) / (factorial(y) * factorial(x - y) )


def get_total_number_of_tickets(args, the_ticket=0):
    game_session = args["session"]
    cart = game_session.get("tickets", {})
    draws = game_session.get("draws", {})
    addon_value = game_session.get("addons", {})
    boost_value = game_session.get("boost_value", 0)

    if boost_value:
        for boost in args["boost"]:
            if int(boost.prefix) == int(boost_value):
                game_price = float(args["game"].game.price) + float(boost.price)
    else:
        game_price = args["game"].game.price

    addon_price = 0
    if args["addon"]:
        addon_price = args["addon"][0].price

    ticket_cost = []
    total_number_of_tickets = 0

    for ticket_no in range(1,args["game"].entries+1):
        total_ticket_list = []
        ticket = cart.get(int(ticket_no), {})
        if ticket:
            for params in args["ballset"]:
                selected_numbers = ticket.get(int(params.id), [])

                if len(selected_numbers) >= int(params.main):
                    ticket_total = factorial_probability(len(selected_numbers), params.main)
                else:
                    ticket_total = 0
                total_ticket_list.append(ticket_total)

            number_of_tickets = 0
            if total_ticket_list:
                number_of_tickets = reduce(lambda x, y: x*y, total_ticket_list)
                total_number_of_tickets += number_of_tickets

            if addon_price and addon_value.get(ticket_no):
                ticket_cost.append({
                    "ticket": ticket_no,
                    "number_of_tickets": number_of_tickets,
                    "total_price": number_of_tickets * (game_price + addon_price)
                })
            else:
                ticket_cost.append({
                    "ticket": ticket_no,
                    "number_of_tickets": number_of_tickets,
                    "total_price": number_of_tickets * game_price
                })

    total_cost_of_tickets = float(0)
    for ticket in ticket_cost:
        total_cost_of_tickets += float(ticket["total_price"])

    if draws.get("duration", False):
        try:
            total_weeks = int(draws.get("duration", 1))
        except:
            total_weeks = 1
    else:
        total_weeks = 1

    if draws.get("days", False):
        total_days = len(draws["days"].split("-"))
    else:
        total_days = len(args["schedule"])

    final_total = total_cost_of_tickets * (total_days * total_weeks)

    return total_number_of_tickets, floatformat(final_total, 2), ticket_cost


def get_deck_probability(args, ball):
    minwin_tier = GamePrize.objects.filter(game = args["game"]).order_by("-tier")[:1].get()

    b = 1
    for tier_ball in minwin_tier.main_balls:
        if int(tier_ball[0]) == (ball.id):
            b = tier_ball[1]

    k = ball.main
    n = ball.ball_max

    return_value = (factorial_probability(k, b) * factorial_probability((n-k), (k-b))) / factorial_probability(n, k)

    return return_value


def get_ticket_percentage(args):
    total_number_of_tickets, total_cost_of_tickets, ticket_cost = get_total_number_of_tickets(args)

    ballset_total_list = []

    for i, ball in enumerate(args["ballset"]):
        ballset_total_list.append( get_deck_probability(args, ball) )

    probability = reduce(lambda x, y: x*y, ballset_total_list)
    probability_of_one_ticket = probability * 100

    percentage = floatformat(probability_of_one_ticket * total_number_of_tickets, 0)

    weeks = int(args["draws"].get("duration",1))
    days = int(len(args["draws"].get("days", get_dow(args["product"].id)).split("-")))

    percentage = int(weeks * days) * int(percentage)

    if percentage >= 100:
        percentage = 99

    if total_number_of_tickets != 0 and percentage == 0:
        return 1

    return percentage


def get_game_percentage(args):
    percentage = int(get_ticket_percentage(args))
    if percentage >= 100:
        percentage = 99
    return percentage


def get_game_cost(args):
    total_number_of_tickets, total_cost_of_tickets, ticket_cost = get_total_number_of_tickets(args)
    return intcomma(total_cost_of_tickets)

def get_game_tickets(args):
    total_number_of_tickets, total_cost_of_tickets, ticket_cost = get_total_number_of_tickets(args)
    return int(total_number_of_tickets)

def get_game_ticket_entries(args):
    total_number_of_tickets, total_cost_of_tickets, ticket_cost = get_total_number_of_tickets(args)
    return ticket_cost

# intervene entry selection prior to sessioning
def is_exceeding_entry_limit(request, cart, product_id, edit_id):
    return_value = False
    return return_value


def get_session_details(product_slug, session, session_id, edit_id=False, use_product_id=False):
    args = {}

    if not use_product_id:
        product = Product.objects.get(slug=product_slug)
    else:
        product = Product.objects.get(id=product_slug)
    args["product"] = product

    if edit_id:
        game_session = session.get(session_id, {}).get(edit_id, {})
    else:
        game_session = session.get(session_id, {}).get(product.id, {})

    if edit_id:
        args["id"] = edit_id

    args["session"] = game_session
    args["boost_value"] = game_session.get("boost_value", 0)
    args["page"] = game_session.get("page", 1)

    args["addons"] = game_session.get("addons", {})
    args["tickets"] = game_session.get("tickets", {})
    args["draws"] = game_session.get("draws", {})

    game = product.productgame_set.all().select_related("game")[:1].get()
    args["game"] = game

    args["page_total"] = int(math.floor(game.entries / 4))

    ballset = game.game.ballset_set.all().order_by("-main")
    args["ballset"] = ballset

    args["boost"] = ProductGameBoost.objects.filter(product_game = game).order_by("boost")
    args["addon"] = game.game.games.all()

    schedule = game.game.eventrule_set.filter(schedule="weekly")
    args["schedule"] = schedule

    args["remaining_time"] = product.get_remaining_time()

    ballset_addon = {}
    for item in args["addon"]:
        ballset_addon[int(item.id)] = item.ballset_set.all().order_by("-main")
    args["ballset_addon"]  = ballset_addon

    total_number_of_tickets, total_cost_of_tickets, ticket_cost = get_total_number_of_tickets(args)
    total_cost_of_tickets = total_cost_of_tickets.replace(',', '.')
    try:
        args["total_cost"] = float(floatformat(total_cost_of_tickets,2))
    except:
        args["total_cost"] = 0.00

    try:
        args["total_entries"] = int(total_number_of_tickets) * (int(args["draws"]["duration"]) * len(args["draws"]["days"].split("-")))
    except:
        args["total_entries"] = int(total_number_of_tickets)

    args["total_entries_per_draw"] = int(total_number_of_tickets)

    args["percentage"] = get_ticket_percentage(args)
    args["ticket_cost"] = ticket_cost

    entries_list = []
    for i in range(1, game.entries + 1):
        entries_list.append({"ticket": i})

    args["entry_list"] = entries_list

    if not args["draws"]:
        dow = ""
        for x, day in enumerate(schedule):
            if x:
                dow += "-"
            dow += day.params["dow"]

        draws = {
            "duration": 1,
            "start_event": game.game.get_events()[:1].get().id,
            "days": dow
        }
        game_session["draws"] = draws
        args["draws"] = draws

    if game_session.get("boost_value", 0):
        return_jackpot = float(game.game.jackpot) + float(ProductGameBoost.objects.get(prefix = game_session.get("boost_value", 0), product_game=game).boost)
    else:
        return_jackpot = game.game.jackpot

    args["jackpot"] = intcomma(floatformat(return_jackpot, 2))

    args["jackpot_float"] = '{:.2f}'.format(return_jackpot)

    args["exclude_selection"] = "false"
    if args["addon"]:
        for addon_item in args["addon"]:
            if addon_item.exclude_selection:
                args["exclude_selection"] = "true"
    return args


def get_game_json(product_id, edit_id, session, return_value):
    if edit_id:
        args = get_session_details(product_id, session, "cart_edit", edit_id, True)
    else:
        args = get_session_details(product_id, session, "cart_play", product_id, True)

    context = {
        "jackpot":args["jackpot"],
        "jackpot_float":args["jackpot_float"],
        "boost_value":args["boost_value"],
        "draws":args["draws"],
        "page":args["page"],
        "tickets":args["tickets"],
        "percentage":args["percentage"],
        "total_cost":args["total_cost"],
        "total_cost_format":intcomma(floatformat(args["total_cost"],2)),
        "total_entries":args["total_entries"],
        "exclude_selection": args["exclude_selection"],
        "remaining_time":args["remaining_time"],
        "entries": get_game_ticket_entries(args),
        "status":return_value
    }
    return context


#todo: Validate user's entry limit
def td_format(td_object):
    seconds = int(td_object.total_seconds())
    periods = [
        ('year',        60*60*24*365),
        ('month',       60*60*24*30),
        ('day',         60*60*24),
        ('hour',        60*60),
        ('minute',      60),
        ('second',      1)
    ]

    strings=[]
    for period_name,period_seconds in periods:
        if seconds > period_seconds:
            period_value , seconds = divmod(seconds,period_seconds)
            if period_value == 1:
                strings.append("%s %s" % (period_value, period_name))
            else:
                strings.append("%s %ss" % (period_value, period_name))

    return ", ".join(strings)

def return_edit(value):
    if value == "":
        return 0
    else:
        return int(value)

def fetch_game(request, slug):
    if request.session.get("cart_edit_id", False):
        game = get_session_details(slug, request.session, "cart_edit", int(request.session["cart_edit_id"]))
    else:
        game = get_session_details(slug, request.session, "cart_play")
    return game


def update_session_details(request, object_id, object_value, product_id, edit_id=False):
    if edit_id:
        if request.session.get("cart_edit", False):
            if request.session["cart_edit"].get(edit_id, False):
                request.session["cart_edit"][edit_id][object_id] = object_value
            else:
                request.session["cart_edit"][edit_id] = {object_id: object_value}
        else:
            request.session["cart_edit"] = {edit_id: {object_id: object_value} }
    else:
        if request.session.get("cart_play", False):
            if request.session["cart_play"].get(product_id, False):
                request.session["cart_play"][product_id][object_id] = object_value
            else:
                request.session["cart_play"][product_id] = {object_id: object_value}
        else:
            request.session["cart_play"] = {product_id: {object_id: object_value} }
    request.session.save()
    return True


def get_dow(product_id):
    try:
        event_list = Product.objects.get(id=product_id).productgame_set.all().select_related("game")[:1].get().game.eventrule_set.filter(schedule="weekly")
    except:
        event_list = []

    dow = ""
    for x, day in enumerate(event_list):
        if x:
            dow += "-"
        dow += "%s" % day.params["dow"]
    return dow


def pickle(json_value):
    new_item = {}
    for item in json_value:
        if type(json_value[item]) == dict:
            try:
                new_item[int(item)] = pickle(json_value[item])
            except:
                new_item[item] = pickle(json_value[item])
        else:
            try:
                new_item[int(item)] = json_value[item]
            except:
                new_item[item] = json_value[item]
    return new_item


def get_basket_item(request, id):
    fallback = False
    basket = {}
    if request.user.is_authenticated():
        try:
            item = BasketEntry.objects.get(pk=id)
            basket = {int(item.id): {
                    "tickets": pickle(item.balls),
                    "draws": pickle(item.draws),
                    "addons": pickle(item.addons),
                    "boost_value": item.boost_value,
                }}
        except BasketEntry.DoesNotExist:
            fallback = True
    else:
        fallback = True
    if fallback:
        basket_list = request.session["basket"]
        for item in basket_list:
            if int(basket_list[item]["id"]) == int(id):
                basket = {int(id): basket_list[item]}
    return basket


def convert_true_false(value):
    if value == "true":
        value = True
    else:
        value = False
    return value



def get_final_cost(request):
    context={}
    total_cost = 0
    total_entries = 0
    total_entries_per_draw = 0
    is_authenticated = request.user.is_authenticated()

    if is_authenticated:
        basket = BasketEntry.objects.filter(user=request.user)
    else:
        basket = request.session.get("basket", {})

    basket_list = []
    for item in basket:
        if is_authenticated:
            session = {"model": {item.id : {
                "id": item.id,
                "product": item.product,
                "product_slug": item.product.slug,
                "boost_value": item.boost_value,
                "tickets": pickle(item.balls),
                "addons": pickle(item.addons),
                "draws": pickle(item.draws),
                }}}
            args = get_session_details(item.product.slug, session, "model", item.id)
        else:
            bitem = basket[item]
            session = {"basket": {int(bitem["id"]) : {
                "id": int(bitem["id"]),
                "product_slug": bitem["product_slug"],
                "boost_value": bitem["boost_value"],
                "tickets": bitem["tickets"],
                "addons": bitem["addons"],
                "draws": bitem["draws"],
                }}}
            args = get_session_details(bitem["product_slug"], session, "basket", int(bitem["id"]))

        total_entries += int(args["total_entries"])
        total_entries_per_draw += int(args["total_entries_per_draw"])
        basket_list.append(args)

    product_list = []
    in_product_list = []
    for item in basket_list:
        total_cost = 0
        if not item["product"] in in_product_list:
            for loop_item in basket_list:
                if item["product"] == loop_item["product"]:
                    total_cost += float(loop_item["total_cost"])
            product_list.append({"product": item["product"], "total_cost": total_cost})
            in_product_list.append(item["product"])

    context.update(get_final_cost_context(request, basket_list, product_list, total_entries, total_entries_per_draw))

    return context



def get_voucher_list(request, product_list):
    sel_vouchers = []
    context = {}
    total = 0.0
    for product in product_list:
        if request.user.is_authenticated():
            if UserWallet.objects.filter(product = product["product"], user_id=request.user).count():
                wallet = UserWallet.objects.get(product = product["product"], user_id=request.user)

                game_total = float(product["total_cost"])
                game_price = float(wallet.product.get_game().price)

                used_vouchers = 0
                vouchers = int(wallet.vouchers)
                if vouchers >= 1:
                    for voucher in range(1, vouchers+1):
                        if (game_total - game_price) >= 0:
                            game_total -= game_price
                            used_vouchers += 1

                remaining_vouchers = wallet.vouchers - used_vouchers
                used_vouchers_cost = int(used_vouchers) * game_price

                total += (float(product["total_cost"]) - float(used_vouchers_cost))

                sel_vouchers.append({
                    "product" : wallet.product,
                    "vouchers" : used_vouchers,
                    "of_vouchers" : wallet.vouchers,
                    "voucher_cost" : game_price,
                    "used_cost" : used_vouchers_cost,
                    "game_cost": float(product["total_cost"]),
                    "total_cost": float(product["total_cost"]) - float(used_vouchers_cost)
                })
            else:
                total += float(product["total_cost"])
        else:
            total += float(product["total_cost"])

    context["voucher_list"] = sel_vouchers
    return context, total



def get_final_cost_context(request, basket_list, product_list, total_entries, total_entries_per_draw):
    context = {}

    if request.session.get("buy_play_credits", False):
        buy_play_credits = float("%.2f" % request.session.get("buy_play_credits", 0.00))
    else:
        buy_play_credits = 0.00

    voucher_context, total_cost = get_voucher_list(request, product_list)

    total_vouchers_used_cost = 0
    for vouchers in voucher_context.values():
        for voucher_item in vouchers:
            total_vouchers_used_cost += voucher_item['used_cost']

    discount = floatformat(request.session.get("discount", 0.00), 2)
    voucher = floatformat(request.session.get("voucher", 0.00), 2)

    if request.user.is_authenticated():
        try:
            play_credits = UserProfile.objects.get(user=request.user).play_credits
        except UserProfile.DoesNotExist:
            UserProfile.objects.create(user=request.user).save()
            play_credits = UserProfile.objects.get(user=request.user).play_credits
    else:
        play_credits = 0.00

    if float(play_credits) >= (float(total_cost) - float(voucher) - float(discount)):
        buy_play_credits = 0.00

    context["game_list"] = basket_list
    context["total_entries_per_draw"] = total_entries_per_draw
    context["total_entries"] = total_entries
    #why is this here and not on the view? please no more casting Decimal as a float defeats the purpose of using Decimal
    # try:
    #     replayed_entries = ReplayEntries.objects.filter(user=request.user, is_processed=False)
    #     replayed_entries_total = replayed_entries.aggregate(Sum('entry__price'))['entry__price__sum']
    #     context['replayed_entries_total'] = replayed_entries_total
    #     context['replayed_entries_count'] = replayed_entries.count()
    #     total_cost += float(replayed_entries_total)
    # except KeyError:
    #     pass

    # display the total cost of the entries when the play credits and vouchers have been deducted.
    total_cost += total_vouchers_used_cost

    context["total_cost"] = floatformat(total_cost, 2)

    context["discount"] = floatformat(discount, 2)
    if total_cost:
        context["discount_percentage"] = ((float(discount) / float(total_cost)) * 100)
    context["vouchers"] = floatformat(total_vouchers_used_cost)

    if request.user.is_authenticated():
        if float(total_cost) > float(play_credits):
            context["play_with_credits"] = True

    if float(play_credits) >= (float(total_cost) - float(voucher) - float(discount)):
        if float(float(total_cost) - float(voucher) - float(discount)) <= 0:
            play_credits = 0.00
        else:
            play_credits = floatformat((float(total_cost) - float(voucher) - float(discount)), 2)
    else:
        if (float(total_cost) - float(voucher) - float(discount)) <= 0:
            play_credits = 0.00

    context["play_credits"] = floatformat(play_credits,2)
    context["buy_play_credits"] = floatformat(buy_play_credits,2)

    payment_due = (float(total_cost) - (float(total_vouchers_used_cost) + float(discount) + float(play_credits))) + float(buy_play_credits)

    if payment_due <= 0:
        payment_due = 0

    # TODO: Olwethu: 10/10/2013: fix this
    context["payment_due"] = floatformat(payment_due,2)

    context.update(voucher_context)
    return context