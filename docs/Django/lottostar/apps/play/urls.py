from django.conf.urls import *
from django.views.generic import TemplateView
from apps.play import views
from django.views.decorators.cache import never_cache

#checkout
urlpatterns = patterns('play.views.checkout',
    url('^checkout/$', never_cache(views.checkout.CheckoutView.as_view()), name='checkout'),
    url('^checkout/set/pay-credits/$', "paycredit", name='paycredit'),
    url('^checkout/success/$', 'checkout_success', name='checkout_complete'),
    url('^checkout/fail/$', TemplateView.as_view(template_name="play/checkout/fail.html"), name='checkout_fail'),
    url('^checkout/test/email/(?P<transaction_id>\d+)/$', 'test_email_transaction', name='test_email_transaction'),
)

#games
urlpatterns += patterns('play.views.game',
    url('^game/ticket/toggle/$', "game_ticket_toggle", name='game_ticket_toggle'),
    url('^game/ticket/clear/$', "game_ticket_clear", name='game_ticket_clear'),

    url('^game/ticket/quickpick/$', "game_ticket_quickpick", name='game_ticket_quickpick'),
    url('^game/ticket/quickpick-all/$', "game_ticket_quickpick_all", name='game_ticket_quickpick_all'),

    url('^game/ticket/addon/$', "game_ticket_addon", name='game_ticket_addon'),
    url('^game/ticket/addon-all/$', "game_ticket_addon_all", name='game_ticket_addon_all'),

    url('^game/ticket/boost/$', "game_ticket_boost", name='game_ticket_boost'),

    url('^game/set/draw/$', "game_set_draw", name='game_set_draw'),
    url('^game/set/page/$', "game_set_page", name='game_set_page'),
    url('^game/get/time/$', "game_get_time", name='game_get_time'),

    url('^game/(?P<slug>[a-zA-Z0-9_.-]+)/$', never_cache(views.game.PlayView.as_view()), kwargs={"reset_edit_session": True}, name='game'),
    url('^game/(?P<slug>[a-zA-Z0-9_.-]+)/edit/(?P<id>[a-zA-Z0-9_.-]+)/$', never_cache(views.game.EditResetView.as_view()), name='game_reset_edit'),
    url('^game/(?P<slug>[a-zA-Z0-9_.-]+)/edit/$', never_cache(views.game.PlayView.as_view()), name='game_edit'),

    url('^game/remove/basket-entry/$', "deleteEntries", name='basket_entry_delete'),
)

#transaction
urlpatterns += patterns('play.views.transactions',
    url(r'^transaction/replay-entries/delete/$', 'delete_replay_entries', {'pk': None}, name='delete_replay_entries'),
    url(r'^transaction/resend/email/(?P<transaction_id>\d+)/$', 'resend_email', name='resend-email'),
    url(r'^transaction/resend/email/successful/(?P<transaction_id>\d+)/$', 'resend_email_success',
        name='resend_email_success'),
)
