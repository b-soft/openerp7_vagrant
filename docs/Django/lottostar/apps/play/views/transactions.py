from django.views.generic import DetailView, DeleteView, FormView, TemplateView
from django.core.urlresolvers import reverse_lazy, reverse
from django.http import HttpResponseRedirect
from django.template.loader import render_to_string

from play.models.transaction import Transaction, Entry, ReplayEntries
from play.forms.game import ResendTransactionEmailForm
from play.utils import email_transaction

class TransactionDetailView(DetailView):
    model = Transaction
    template_name = 'play/transactions/transaction_detail.html'

    def get_context_data(self, **kwargs):
        context = super(TransactionDetailView, self).get_context_data(**kwargs)
        context['transaction_entries'] = Entry.objects.filter(ticket__transaction=self.object).order_by('event__draw_datetime')
        return context

#TODO: Olwethu: delete view if for a single object, need to change it to allow deleting multiple objects
class ReplayEntriesDeleteView(DeleteView):
    model = ReplayEntries
    success_url = reverse_lazy('checkout')

    def get_object(self, queryset=None):
        """
        check if a single item is being deleted if not delete all replay entries that have not been processed.
        """
        try:
            obj = super(ReplayEntriesDeleteView, self).get_object(queryset=None)
        except AttributeError:
            if queryset is None:
                queryset = self.get_queryset()
            return None #queryset.filter(user=self.request.user, is_processed=False)
        return obj

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        delete = super(ReplayEntriesDeleteView, self).delete(request, *args, **kwargs)
        return HttpResponseRedirect(self.get_success_url())

delete_replay_entries = ReplayEntriesDeleteView.as_view()


class ResendTransactionEmailFormView(FormView):
    template_name = 'play/checkout/resend_transaction_email.html'
    form_class = ResendTransactionEmailForm

    def get_initial(self):
        initial = super(ResendTransactionEmailFormView, self).get_initial()
        user = self.request.user
        initial['to'] = user.email
        return initial

    def get_success_url(self):
        return reverse('resend_email_success', kwargs={'transaction_id': self.kwargs.get('transaction_id')})

    def form_valid(self, form):
        user = self.request.user
        recipient_list = [user.email]
        email = form.cleaned_data.get('to')
        if user.email != email:
            recipient_list = [email]
        transaction_id = self.kwargs.get('transaction_id')
        try:
            transaction = Transaction.objects.get(id=transaction_id)
            email_transaction(user, transaction, recipient_list)
        except Transaction.DoesNotExist:
            pass
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(ResendTransactionEmailFormView, self).get_context_data(**kwargs)
        context['transaction_id'] = self.kwargs.get('transaction_id')
        return context

resend_email = ResendTransactionEmailFormView.as_view()


class ResendTransactionEmailTemplateView(TemplateView):
    template_name = 'play/checkout/resend_transaction_email_success.html'

    def get_context_data(self, **kwargs):
        context = super(ResendTransactionEmailTemplateView, self).get_context_data(**kwargs)
        context['transaction_id'] = self.kwargs.get('transaction_id')
        return context

resend_email_success = ResendTransactionEmailTemplateView.as_view()
