from django.contrib import messages
import json
import math
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.views.decorators.csrf import requires_csrf_token
from datetime import datetime
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import FormView
from apps.game.models import  Product
from apps.game.forms import ToggleForm, AddonForm, TicketClearForm, BoostForm, BaseGameForm, NumbersForm, PageForm, DrawForm
from play.forms.game import ProcessForm
from play.functions import get_basket_item, get_session_details,  update_session_details, pickle, get_final_cost, get_game_json, is_exceeding_entry_limit
from django.utils.translation import ugettext_lazy as _
from play.models import BasketEntry


@requires_csrf_token
def game_ticket_toggle(request):
    return_value = "true"
    context = {}
    game_args = {}
    form = ToggleForm(request.POST)
    if form.is_valid():
        edit_id = form.cleaned_data["edit_id"]
        if edit_id:
            edit_id = int(edit_id)
        product_id = int(form.cleaned_data["product"])
        ballset_id = int(form.cleaned_data["ballset"])
        ticket_no = int(form.cleaned_data["ticket"])
        number = int(form.cleaned_data["number"])
        limit = int(form.cleaned_data["limit"])

        try:
            if edit_id:
                cart = request.session.get("cart_edit", {}).get(edit_id, {"tickets": {}})["tickets"]
            else:
                cart = request.session.get("cart_play", {}).get(product_id, {"tickets": {}})["tickets"]
        except:
            cart = {}
        is_ticked = False
        if form.cleaned_data["is_ticked"]:
            is_ticked = True


        if ticket_no and ballset_id and number:
            ticket = cart.get(ticket_no, {})
            if ticket:
                existing_numbers = ticket.get(ballset_id, [])
                if existing_numbers:
                    if not is_ticked:
                        if limit and not number in existing_numbers and len(existing_numbers) < limit:
                            existing_numbers.append(number)
                        else:
                            if not limit:
                                existing_numbers.append(number)
                    else:
                        if number in existing_numbers:
                            existing_numbers.remove(number)
                            return_value = "false"
                    cart[ticket_no][ballset_id] = existing_numbers
                else:
                    cart[ticket_no][ballset_id] = [number]
            else:
                cart[ticket_no] = { ballset_id: [number] }

        has_selection = False
        for ballset in cart[ticket_no]:
            if cart[ticket_no][ballset]:
                has_selection = True
        if not has_selection:
            del cart[ticket_no]


        if not is_exceeding_entry_limit(request, cart, product_id, edit_id):
            update_session_details(request, "tickets", cart, product_id, edit_id)
            game_args = get_game_json(product_id, edit_id, request.session, return_value)
            context.update(game_args)
        else:
            context.update({"status":"limit"})

        #todo: update addon if excluded selection
        if game_args["exclude_selection"] == "true":
            try:
                if edit_id:
                    addon = request.session.get("cart_edit", {}).get(edit_id, {"addons": {}})["addons"]
                else:
                    addon = request.session.get("cart_play", {}).get(product_id, {"addons": {}})["addons"]
            except:
                addon = {}

            if addon:
                if addon.get(ticket_no, False) in cart.get(ticket_no, {}).get(ballset_id, []):
                    del addon[ticket_no]
                    update_session_details(request, "addons", addon, product_id, edit_id)
    else:
        context.update({"status":"false"})
        print form.errors

    return HttpResponse(json.dumps(context), mimetype="application/json")


@requires_csrf_token
def game_ticket_addon(request):
    context = {}
    form = AddonForm(request.POST)
    if form.is_valid():
        edit_id = form.cleaned_data["edit_id"]
        if edit_id:
            edit_id = int(edit_id)
        product_id = int(form.cleaned_data["product"])
        number = int(form.cleaned_data["number"])
        ticket_no = int(form.cleaned_data["ticket"])
        try:
            if edit_id:
                addon = request.session.get("cart_edit", {}).get(edit_id, {"addons": {}})["addons"]
            else:
                addon = request.session.get("cart_play", {}).get(product_id, {"addons": {}})["addons"]
        except:
            addon = {}

        addon[ticket_no] = number

        #todo: exclude selection if number is in ticket selection

        update_session_details(request, "addons", addon, product_id, edit_id)
        context.update(get_game_json(product_id, edit_id, request.session, "ok"))
    else:
        context.update({"status":"false"})
        print form.errors

    return HttpResponse(json.dumps(context), mimetype="application/json")


@requires_csrf_token
def game_ticket_clear(request):
    context = {}
    form = TicketClearForm(request.POST)
    if form.is_valid():
        edit_id = form.cleaned_data["edit_id"]
        if edit_id:
            edit_id = int(edit_id)
        product_id = form.cleaned_data["product"]
        ticket_no = form.cleaned_data["ticket"]

        if edit_id and edit_id != "":
            try:
                del request.session["cart_edit"][edit_id]["tickets"][ticket_no]
            except:
                pass
            try:
                del request.session["cart_edit"][edit_id]["addons"][ticket_no]
            except:
                pass
        else:
            try:
                del request.session["cart_play"][product_id]["tickets"][ticket_no]
            except:
                pass
            try:
                del request.session["cart_play"][product_id]["addons"][ticket_no]
            except:
                pass
        request.session.save()

        context.update(get_game_json(product_id, edit_id, request.session, "ok"))
    else:
        context.update({"status":"false"})
        print form.errors

    return HttpResponse(json.dumps(context), mimetype="application/json")


@requires_csrf_token
def game_ticket_quickpick(request):
    context = {}
    form = NumbersForm(request.POST)
    if form.is_valid():
        edit_id = form.cleaned_data["edit_id"]
        if edit_id:
            edit_id = int(edit_id)
        product_id = int(form.cleaned_data["product"])
        ticket_no = int(form.cleaned_data["ticket"])
        selections = form.cleaned_data["numbers"]

        try:
            if edit_id:
                cart = request.session.get("cart_edit", {}).get(edit_id, {"tickets": {}})["tickets"]
            else:
                cart = request.session.get("cart_play", {}).get(product_id, {"tickets": {}})["tickets"]
        except:
            cart = {}

        if product_id and ticket_no and selections:
            try:
                ballset_list = Product.objects.get(id=product_id).productgame_set.all()[:1].get().game.ballset_set.all().order_by("-main")
            except:
                ballset_list = False

            if ballset_list:
                for bindex, number in enumerate(selections.split("_")):
                    val_list = number.split('-')
                    numbers = []
                    for val in val_list:
                        numbers.append(int(val))

                    get_ballset = int(ballset_list[bindex].id)

                    if cart.get(ticket_no, False):
                        if cart[ticket_no].get(get_ballset, False):
                            del cart[ticket_no][get_ballset]
                        cart[ticket_no][get_ballset] = numbers
                    else:
                        cart[ticket_no] = { get_ballset: numbers }

        if not is_exceeding_entry_limit(request, cart, product_id, edit_id):
            update_session_details(request, "tickets", cart, product_id, edit_id)
            context.update(get_game_json(product_id, edit_id, request.session, "ok"))
        else:
            context.update({"status":"limit"})

        #todo: exclude selection if number is in ticket selection

    else:
        context.update({"status":"false"})
        print form.errors
    return HttpResponse(json.dumps(context), mimetype="application/json")


@requires_csrf_token
def game_ticket_boost(request):
    context = {}
    form = BoostForm(request.POST)
    if form.is_valid():
        edit_id = form.cleaned_data["edit_id"]
        if edit_id:
            edit_id = int(edit_id)
        product_id = int(form.cleaned_data["product"])
        boost = int(form.cleaned_data["boost"])

        if edit_id:
            if request.session.get("cart_edit", False):
                if request.session["cart_edit"].get(edit_id, False):
                    request.session["cart_edit"][edit_id]["boost_value"] = boost
                else:
                    request.session["cart_edit"] = {edit_id: {"boost_value": boost}}
            else:
                request.session["cart_edit"] = {edit_id: {"boost_value": boost}}
        else:
            if request.session.get("cart_play", False):
                if request.session["cart_play"].get(product_id, False):
                    request.session["cart_play"][product_id]["boost_value"] = boost
                else:
                    request.session["cart_play"] = {product_id: {"boost_value": boost}}
            else:
                request.session["cart_play"] = {product_id: {"boost_value": boost}}
        request.session.save()

        context.update(get_game_json(product_id, edit_id, request.session, "ok"))
    else:
        context.update({"status":"false"})
        print form.errors

    return HttpResponse(json.dumps(context), mimetype="application/json")


@requires_csrf_token
def game_ticket_quickpick_all(request):
    context = {}
    form = BaseGameForm(request.POST)
    if form.is_valid():
        edit_id = form.cleaned_data["edit_id"]
        if edit_id:
            edit_id = int(edit_id)
        product_id = int(form.cleaned_data["product"])

        if product_id:
            try:
                game = Product.objects.get(id=product_id).productgame_set.all()[:1].get()
            except:
                game = False

            try:
                if edit_id:
                    cart = request.session.get("cart_edit", {}).get(edit_id, {"tickets": {}})["tickets"]
                else:
                    cart = request.session.get("cart_play", {}).get(product_id, {"tickets": {}})["tickets"]
            except:
                cart = {}

            if game:
                ballset_list = game.game.ballset_set.all().order_by("-main")
            else:
                ballset_list = False

            if ballset_list:
                for ticket_no in range(1, int(game.entries)):
                    selections = request.POST.get("number%s" % ticket_no, False)
                    if selections:
                        qform = NumbersForm(data={
                            "product": product_id,
                            "edit_id": edit_id,
                            "ticket": ticket_no,
                            "numbers": selections
                        })
                        if qform.is_valid():
                            for bindex, number in enumerate(selections.split("_")):
                                val_list = number.split('-')
                                numbers = []
                                for val in val_list:
                                    numbers.append(int(val))

                                get_ballset = int(ballset_list[bindex].id)

                                if cart.get(int(ticket_no), False):
                                    if cart[int(ticket_no)].get(get_ballset, False):
                                        del cart[int(ticket_no)][get_ballset]
                                    cart[int(ticket_no)][get_ballset] = numbers
                                else:
                                    cart[int(ticket_no)] = { get_ballset: numbers }
                        else:
                            qform.errors
            if not is_exceeding_entry_limit(request, cart, product_id, edit_id):
                update_session_details(request, "tickets", cart, product_id, edit_id)
                context.update(get_game_json(product_id, edit_id, request.session, "ok"))

                #todo: check addon in ticket selection

            else:
                context.update({"status":"limit"})
    else:
        context.update({"status":"false"})
        print form.errors

    return HttpResponse(json.dumps(context), mimetype="application/json")


@requires_csrf_token
def game_ticket_addon_all(request):
    context = {}
    form = BaseGameForm(request.POST)
    if form.is_valid():
        edit_id = form.cleaned_data["edit_id"]
        if edit_id:
            edit_id = int(edit_id)
        product_id = int(form.cleaned_data["product"])

        if product_id:
            try:
                game = Product.objects.get(id=product_id).productgame_set.all()[:1].get()
            except:
                game = False

            try:
                if edit_id:
                    addons = request.session.get("cart_edit", {}).get(edit_id, {"addons": {}})["addons"]
                else:
                    addons = request.session.get("cart_play", {}).get(product_id, {"addons": {}})["addons"]
            except:
                addons = {}

            for ticket_no in range(1, int(game.entries)):
                if request.POST.get("number%s" % ticket_no, None):
                    qform = NumbersForm(data={
                        "product": product_id,
                        "edit_id": edit_id,
                        "ticket": ticket_no,
                        "numbers": request.POST.get("number%s" % ticket_no, None)
                    })
                    if qform.is_valid():
                        get_number = qform.cleaned_data["numbers"]
                        if addons.get(int(ticket_no), False):
                            del addons[int(ticket_no)]
                        addons[int(ticket_no)] = int(get_number)
                    else:
                        print qform.errors
            update_session_details(request, "addons", addons, product_id, edit_id)
            context.update(get_game_json(product_id, edit_id, request.session, "ok"))
        else:
            context.update({"status":"false"})
    else:
        context.update({"status":"false"})
        print form.errors

    return HttpResponse(json.dumps(context), mimetype="application/json")


@requires_csrf_token
def game_get_time(request):
    context = {}
    form = BaseGameForm(request.POST)
    if form.is_valid():
        edit_id = form.cleaned_data["edit_id"]
        if edit_id:
            edit_id = int(edit_id)
        product_id = int(form.cleaned_data["product"])

        if product_id:
           context = get_game_json(product_id, edit_id, request.session, "ok")
    return HttpResponse(context.get("remaining_time", "00:00:00"))


@requires_csrf_token
def game_set_page(request):
    context = {}
    form = PageForm(request.POST)
    if form.is_valid():
        edit_id = form.cleaned_data["edit_id"]
        if edit_id:
            edit_id = int(edit_id)
        product_id = int(form.cleaned_data["product"])
        page = int(form.cleaned_data["page"])

        if edit_id:
            if request.session.get("cart_edit", False):
                if request.session["cart_edit"].get(edit_id, False):
                    request.session["cart_edit"][edit_id]["page"] = page
                else:
                    request.session["cart_edit"] = {edit_id: {"page": page}}
            else:
                request.session["cart_edit"] = {edit_id: {"page": page}}
        else:
            if request.session.get("cart_play", False):
                if request.session["cart_play"].get(product_id, False):
                    request.session["cart_play"][product_id]["page"] = page
                else:
                    request.session["cart_play"] = {product_id: {"page": page}}
            else:
                request.session["cart_play"] = {product_id: {"page": page}}

        request.session.save()
        context.update(get_game_json(product_id, edit_id, request.session, "ok"))
    else:
        context.update({"status":"false"})
        print form.errors

    return HttpResponse(json.dumps(context), mimetype="application/json")

@requires_csrf_token
def game_set_draw(request):
    context = {}
    form = DrawForm(request.POST)
    if form.is_valid():
        edit_id = form.cleaned_data["edit_id"]
        if edit_id:
            edit_id = int(edit_id)
        product_id = int(form.cleaned_data["product"])
        duration = int(form.cleaned_data["duration"])
        start_event_id = int(form.cleaned_data["start"])
        days = form.cleaned_data["days"]

        if duration != "" and not int(duration):
            duration = 1

        if duration >= 52:
            duration = 52

        if start_event_id:
            draws = {
                "duration": duration,
                "start_event": start_event_id,
                "days": days
            }

            if edit_id:
                if request.session.get("cart_edit", False):
                    if request.session["cart_edit"].get(edit_id, False):
                        request.session["cart_edit"][edit_id]["draws"] = draws
                    else:
                        request.session["cart_edit"] = {edit_id: {"draws": draws}}
                else:
                    request.session["cart_edit"] = {edit_id: {"draws": draws}}
            else:
                if request.session.get("cart_play", False):
                    if request.session["cart_play"].get(product_id, False):
                        request.session["cart_play"][product_id]["draws"] = draws
                    else:
                        request.session["cart_play"] = {product_id: {"draws": draws}}
                else:
                    request.session["cart_play"] = {product_id: {"draws": draws}}

            request.session.save()
            context.update(get_game_json(product_id, edit_id, request.session, "ok"))
        else:
            context.update({"status":"false"})
    else:
        context.update({"status":"false"})
        print form.errors

    return HttpResponse(json.dumps(context), mimetype="application/json")


class ProcessGameView(FormView):
    form_class = ProcessForm
    args = {}
    ticket_errors = []
    page = 1
    total_cost = 0
    total_entries = 0
    game_list = []
    basket_list = {}

    def has_ticket_errors(self):
        if self.request.session.get("cart_edit_id", False):
            self.args = get_session_details(self.kwargs["slug"], self.request.session, "cart_edit", self.request.session["cart_edit_id"])
        else:
            self.args = get_session_details(self.kwargs["slug"], self.request.session, "cart_play")

        self.page = 1
        ticket_errors = []

        for ticket_no in self.args["tickets"]:
            for ball in self.args["ballset"]:
                if len(self.args["tickets"][ticket_no].get(int(ball.id), [])) < ball.main:
                    if ticket_no not in ticket_errors:
                        ticket_errors.append(ticket_no)
        if ticket_errors:
            self.page = int(math.ceil(ticket_errors[0] / 4))

        self.ticket_errors = ticket_errors

        if not self.args["tickets"]:
            skip_no_ticket = False
            if self.request.user.is_authenticated() and BasketEntry.objects.filter(user=self.request.user).count():
                skip_no_ticket = True
            #
            if not skip_no_ticket:
                basket = self.request.session.get("basket", [])
                if not basket:
                    messages.error(self.request, _("No tickets have been entered"))
                    self.ticket_errors = True
        else:
            if self.ticket_errors:
                ticket_str = ""
                for x, item in enumerate(ticket_errors):
                    if x != len(ticket_errors) and x:
                        ticket_str += ", "
                    ticket_str += "ticket %s" % item

                messages.error(self.request, _("Please make sure the following tickets are completely filled in: %s" % ticket_str) )
        return self.ticket_errors

    def process_entries(self):

        self.basket_list = self.request.session.get("basket", {})

        if self.args["tickets"]:
            if self.request.session.get("cart_edit_id", False):
                if self.args.get("id", False):
                    if self.request.user.is_authenticated():
                        try:
                            bentry = BasketEntry.objects.get(user=self.request.user, id=self.args["id"], product=self.args["product"], game=self.args["game"])
                        except BasketEntry.DoesNotExist:
                            bentry = False

                        if bentry:
                            bentry.boost_value = self.args["boost_value"]
                            bentry.balls = self.args["tickets"]
                            bentry.addons = self.args["addons"]
                            bentry.draws = self.args["draws"]

                            bentry.prize = self.args["jackpot_float"]
                            bentry.cost = self.args["total_cost"]
                            bentry.cost = self.args["total_entries"]
                            bentry.save()
                    else:
                        for item in self.basket_list:
                            if self.basket_list[item]["id"] == self.args["id"]:
                                self.basket_list[item] = {
                                    "id": self.args["id"],
                                    "product": self.args["product"].id,
                                    "product_slug": self.args["product"].slug,
                                    "tickets": self.args["tickets"],
                                    "addons" : self.args["addons"],
                                    "boost_value" : self.args["boost_value"],
                                    "draws": self.args["draws"]
                                }
            else:
                self.basket_list[len(self.basket_list)] = {
                    "id": len(self.basket_list) + 1,
                    "product": self.args["product"].id,
                    "product_slug": self.args["product"].slug,
                    "tickets": self.args["tickets"],
                    "addons" : self.args["addons"],
                    "boost_value" : self.args["boost_value"],
                    "draws": self.args["draws"]
                }

            if self.request.session.get("cart_edit_id", False):
                edit_id = int(self.request.session["cart_edit_id"])
                if self.request.session["cart_edit"].get(edit_id, False):
                    del self.request.session["cart_edit"][edit_id]
                    del self.request.session["cart_edit_id"]
            else:
                if self.request.session.get("cart_play", {}).get(self.args["product"].id, False):
                    del self.request.session["cart_play"][self.args["product"].id]


    def get_shopping_cart(self):
        game_list = []
        delete_que = []
        self.total_cost = 0
        self.total_entries = 0
        is_authenticated = self.request.user.is_authenticated()

        # save into database and delete out of basket session if logged in
        for x, item in enumerate(self.basket_list):
            basket_item = self.basket_list[item]
            session = {"basket": {basket_item["id"]: basket_item}}

            item_args = get_session_details(basket_item["product_slug"], session, "basket", basket_item["id"])

            self.total_cost += float(item_args["total_cost"])
            self.total_entries += int(item_args["total_entries"])

            if is_authenticated:
                bmodel = BasketEntry(
                    user = self.request.user,

                    product = item_args["product"],
                    game = item_args["game"],

                    boost_value = item_args["boost_value"],

                    balls = json.dumps(item_args["tickets"]),
                    addons = json.dumps(item_args["addons"]),
                    draws = json.dumps(item_args["draws"]),

                    prize = item_args["jackpot_float"],
                    cost = item_args["total_cost"],
                    tickets = item_args["total_entries"],
                )
                bmodel.save()
                delete_que.append(item)
            else:
                game_list.append(item_args)

        if is_authenticated:
            for x, item in enumerate(delete_que):
                del self.basket_list[item]

            for game in BasketEntry.objects.filter(user=self.request.user):
                session = {"model": {game.id : {
                    "id": game.id,
                    "product": game.product,
                    "product_slug": game.product.slug,
                    "boost_value" : game.boost_value,
                    "tickets": pickle(game.balls),
                    "addons" : pickle(game.addons),
                    "draws": pickle(game.draws),
                    }}}
                args = get_session_details(game.product.slug, session, "model", game.id)
                game_list.append(args)

                self.total_cost += float(args["total_cost"])
                self.total_entries += int(args["total_entries"])

        self.game_list = game_list

        self.request.session["basket"] = self.basket_list
        self.request.session.save()
        return self.game_list


    def form_valid(self, form):
        if not self.has_ticket_errors():
            self.process_entries()
            self.get_shopping_cart()
        else:
            return super(ProcessGameView, self).form_invalid(form)
        return HttpResponseRedirect(self.get_success_url())


    def form_invalid(self, form):
        if self.ticket_errors:
            if self.request.user.is_authenticated():
                if not BasketEntry.objects.filter(user=self.request.user).count():
                    if self.request.session.get("cart_edit_id", False):
                        return HttpResponseRedirect(reverse("game_edit", kwargs={"slug":self.kwargs["slug"]} ) )
                    else:
                        return HttpResponseRedirect(reverse("game", kwargs={"slug":self.kwargs["slug"]} ) )
                else:
                    if self.request.session.get("cart_edit_id", False):
                        return HttpResponseRedirect(reverse("game_edit", kwargs={"slug":self.kwargs["slug"]} ) )
            else:
                if self.request.session.get("cart_edit_id", False):
                    return HttpResponseRedirect(reverse("game_edit", kwargs={"slug":self.kwargs["slug"]} ) )
                else:
                    return HttpResponseRedirect(reverse("game", kwargs={"slug":self.kwargs["slug"]} ) )
        return super(ProcessGameView, self).form_invalid(form)

    def get_success_url(self):
        return reverse("checkout")



class PlayView(ProcessGameView):
    model = Product
    template_name = "play/game/game.html"
    context_object_name = "product"
    args = {}

    def get_context_data(self, **kwargs):
        context = super(PlayView, self).get_context_data(**kwargs)
        context["timestamp"] = (datetime.now()).strftime('%Y-%m-%dT%H:%M:%SZ')

        if self.kwargs.get("reset_edit_session", False):
            if self.request.session.get("cart_edit_id", False):
                del self.request.session["cart_edit_id"]
                self.request.session.save()

        if self.request.session.get("cart_edit_id", False):
            context.update(get_session_details(self.kwargs["slug"], self.request.session, "cart_edit", self.request.session["cart_edit_id"]))
        else:
            context.update(get_session_details(self.kwargs["slug"], self.request.session, "cart_play"))

        return context



class EditResetView(ProcessGameView):
    template_name = "play/game/game.html"

    def render_to_response(self, context, **response_kwargs):
        self.request.session["cart_edit_id"] = int(self.kwargs["id"])
        if self.request.session.get("cart_edit", False):
            self.request.session["cart_edit"].update(get_basket_item(self.request, int(self.kwargs["id"])))
        else:
            self.request.session["cart_edit"] = get_basket_item(self.request, int(self.kwargs["id"]))

        self.request.session.save()
        return HttpResponseRedirect(reverse("game_edit", kwargs={"slug": self.kwargs["slug"]}))



@requires_csrf_token
def deleteEntries(request):
    context={}
    if request.is_ajax():
        if request.method == 'POST':
            slug = request.POST['slug']
            id = int(request.POST['id'])
            product = Product.objects.get(slug=slug)

            if request.user.is_authenticated():
                try:
                    bentry = BasketEntry.objects.get(user=request.user, id=id, product=product)
                except BasketEntry.DoesNotExist:
                    bentry = False

                if bentry:
                    bentry.delete()

            else:
                new_basket_list = {}
                basket_list = request.session.get("basket", {})

                for x, item in enumerate(basket_list):
                    if int(basket_list[item]["id"]) != int(id):
                        new_basket_list[basket_list[item]["id"]] = basket_list[item]

                request.session["basket"] = new_basket_list
                request.session.save()

    context = get_final_cost(request)

    return render_to_response('play/partials/checkout_final_prices.html', context, context_instance=RequestContext(request))