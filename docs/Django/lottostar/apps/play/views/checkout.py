from __future__ import division
import itertools
import json
from datetime import timedelta, datetime
from dateutil.relativedelta import relativedelta
from django.contrib import messages
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.template import RequestContext
from django.views.generic import  FormView
from django.shortcuts import get_object_or_404, render_to_response
from apps.account.models import UserProfile, UserWallet, Restriction
from apps.game.models import Event, Game, ProductGame
from play.forms.game import CheckoutForm, PlayCreditForm
from play.functions import pickle, get_session_details, get_final_cost, get_final_cost_context
from play.models.transaction import Ticket, Transaction, Entry, TransactionVoucher, Deck
from play.models.entries import BasketEntry, Product
from play.utils import *
from django.views.generic import TemplateView
from promotions.functions import get_offer_helper, clean_checkout_session
from promotions.models import Offer, OfferCustomer
from django.db.models import F
from system.models.log import Log
from utils.mailer import SendMail
from play.models import ReplayEntries
from django.db.models import Sum
import settings

from django.contrib.auth.views import logout



class CheckoutView(FormView):
    form_class = CheckoutForm
    template_name = "play/checkout/checkout.html"
    game_list = {}
    basket_list = []
    total_cost = 0
    total_entries = 0
    total_entries_per_draw = 0
    product_list = []
    in_product_list = []
    buy_play_credits = 0
    play_credits = 0
    discount = 0
    vouchers = 0
    payment_due = 0
    voucher_list = []
    restriction = False
    limit_message = ""
    limit = {"period": "monthly", "entries_left": 0}

    def check_transaction_limit(self, type):
        if self.request.user.is_authenticated():
            daily_limit = datetime.now() - timedelta(hours=24)
            weekly_limit = datetime.now() - timedelta(weeks=1)
            monthly_limit = datetime.now() - relativedelta(months=1)

            daily_filter = {'created_at__gte': daily_limit}
            weekly_filter = {'created_at__gte': weekly_limit}
            monthly_filter = {'created_at__gte': monthly_limit}

            transaction_type = {
                'deposit': Transaction.TRANSACTION_TYPE_CHOICES.purchase_play_credit,
                'entry': Transaction.TRANSACTION_TYPE_CHOICES.entry_purchased
            }
            transaction_limit = Restriction.get_latest_limit(self.request.user, type)
            if not transaction_limit or transaction_limit.value == 0:
                return None

            transaction_list = Transaction.objects.filter(
                user=self.request.user, transaction_type=transaction_type[type])
            if transaction_limit.period == "daily":
                transaction_list = transaction_list.filter(**daily_filter)
            if transaction_limit.period == "weekly":
                transaction_list = transaction_list.filter(**weekly_filter)
            if transaction_limit.period == "monthly":
                transaction_list = transaction_list.filter(**monthly_filter)
            if type == 'deposit':
                deposit_total = transaction_list.aggregate(Sum('total_paid'))
                deposit_limit = transaction_limit.value
                if deposit_total['total_paid__sum']:
                    deposit_limit = transaction_limit.value - deposit_total['total_paid__sum']
                return deposit_limit
            else:
                total_trans_entries = Entry.objects.filter(
                    ticket__transaction__id__in=transaction_list.values_list('id', flat=True)).count()
                entries_count = total_trans_entries + self.total_entries
                if entries_count > transaction_limit.value:
                    amount_over = entries_count - transaction_limit.value
                    self.limit["amount_over"] = amount_over
                    self.limit_message = _("Your entry limit of {0} has been exceeded. Please remove or update your\
                        game(s) to proceed.".format(self.limit["period"]))
                    return self.limit_message
            return None
        return None

    def fetch_basket(self):
        is_authenticated = self.request.user.is_authenticated()
        game_list = []
        delete_que = []

        self.basket_list = self.request.session.get("basket", {})

        for x, item in enumerate(self.basket_list):
            basket_item = self.basket_list[item]
            session = {"basket": {basket_item["id"]: basket_item}}
            item_args = get_session_details(basket_item["product_slug"], session, "basket", basket_item["id"])
            if is_authenticated:
                bmodel = BasketEntry(
                    user=self.request.user, product=item_args["product"], game=item_args["game"],
                    boost_value=item_args["boost_value"], balls=json.dumps(item_args["tickets"]),
                    addons=json.dumps(item_args["addons"]), draws=json.dumps(item_args["draws"]),
                    prize=item_args["jackpot_float"], cost=item_args["total_cost"],
                    tickets=item_args["total_entries_per_draw"],
                )
                bmodel.save()
                delete_que.append(item)
            else:
                game_list.append(item_args)

        if is_authenticated:
            for x, item in enumerate(delete_que):
                del self.basket_list[item]
            self.request.session["basket"] = self.basket_list
            basket = BasketEntry.objects.filter(user=self.request.user)
        else:
            basket = self.request.session.get("basket", {})

        # basket entry is a deck with tickets
        self.basket_list = []
        for item in basket:
            if is_authenticated:
                session = {"model": {item.id : {
                    "id": item.id,
                    "product": item.product,
                    "product_slug": item.product.slug,
                    "boost_value" : item.boost_value,
                    "tickets": pickle(item.balls),
                    "addons" : pickle(item.addons),
                    "draws": pickle(item.draws),
                }}}
                args = get_session_details(item.product.slug, session, "model", item.id)
            else:
                bitem = basket[item]
                session = {"basket": {int(bitem["id"]) : {
                    "id": int(bitem["id"]),
                    "product_slug": bitem["product_slug"],
                    "boost_value" : bitem["boost_value"],
                    "tickets": bitem["tickets"],
                    "addons" : bitem["addons"],
                    "draws": bitem["draws"],
                }}}
                args = get_session_details(bitem["product_slug"], session, "basket", int(bitem["id"]))

            self.total_entries_per_draw += int(args["total_entries_per_draw"])
            self.total_entries += int(args["total_entries"])
            self.basket_list.append(args)

        self.product_list = []
        self.in_product_list = []
        for item in self.basket_list:
            total_cost = 0

            for loop_item in self.basket_list:
                if item["product"] == loop_item["product"]:
                    total_cost += float(loop_item["total_cost"])

            if not item["product"] in self.in_product_list:
                self.in_product_list.append(item["product"])
                start_event = Event.objects.get(pk=item["draws"]["start_event"])
                draw_days = item['draws']['days'].title().replace('-', ', ')
                self.product_list.append(
                    {"product": item["product"], "total_cost": total_cost, "start_on": start_event.draw_datetime,
                     "draw_days": draw_days, "total_entries": item['total_entries']})


    def get(self, request, *args, **kwargs):
        self.fetch_basket()
        return super(CheckoutView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.fetch_basket()
        return super(CheckoutView, self).post(request, *args, **kwargs)

    def get_initial(self):
        initial = super(CheckoutView, self).get_initial()
        initial.update(get_final_cost_context(self.request, self.basket_list, self.product_list, self.total_entries, self.total_entries_per_draw))
        return initial

    def form_valid(self, form):
        if self.request.user.is_authenticated():
            if not self.check_transaction_limit('entry'):
                objects = get_final_cost_context(self.request, self.basket_list, self.product_list, self.total_entries, self.total_entries_per_draw)

                # add the replayed entries to the total cost of the transaction
                # replay_entries = ReplayEntries.objects.filter(user=self.request.user, is_processed=False)
                # replay_entries_total = replay_entries.aggregate(Sum('price'))
                # if replay_entries_total['price__sum']:
                #     objects["total_cost"] += replay_entries_total['price__sum']

                #delete transaction, buy_play_credits on session
                try:
                    del self.request.session['transaction']
                    del self.request.session['deposit']
                except KeyError:
                    pass

                # safe guard to ensure that the user does not exceed deposit limit
                if form.cleaned_data["buy_play_credits"]:
                    buy_play_credits = float(form.cleaned_data["buy_play_credits"])
                else:
                    buy_play_credits = 0.00
                deposit_limit = self.check_transaction_limit('deposit')
                # if the amount deposited by the user exceeds the deposit limit clear it
                if deposit_limit and deposit_limit < buy_play_credits:
                    messages.info(self.request, 'Deposit limit exceeded. Max allowed deposit is: {:.2f}'.format(deposit_limit))
                    buy_play_credits = 0.00

                product_dict = create_entry_purchase(
                    request=self.request,
                    user=self.request.user,
                    total_cost=float(objects["total_cost"]),
                    payment_due=float(objects["payment_due"]),
                    buy_play_credits=buy_play_credits,
                    play_credits=float(objects["play_credits"]),
                    discount=float(objects["discount"]),
                    product_list=self.product_list,
                    status=Transaction.PAYMENT_STATUS_CHOICES.completed,
                    used_vouchers_cost=float(objects["vouchers"])
                )
                transaction = self.request.session.get('transaction', None)
                if transaction:
                    transaction_entries = Entry.objects.filter(
                        ticket_id__in=Ticket.objects.filter(transaction=transaction).values_list('id', flat=True))
                    for product, vouchers in product_dict.iteritems():
                        entries = transaction_entries.filter(ticket__product=product)
                        voucher_entries = entries.values_list('id', flat=True)[:vouchers]
                        if voucher_entries:
                            Entry.objects.filter(id__in=voucher_entries).update(price=0)

                #TODO Olwethu: 30/09/2013: fix to allow adding replayed entries to current transaction.
                # for product in Product.objects.all():
                #     """
                #     regroup replay entries by the product and create a ticket for these entries, link these tickets to
                #     the current transaction.
                #     """
                #     product_entries = ReplayEntries.objects.filter(
                #         entry__ticket__product=product, entry__ticket__game__game__id__in=ProductGame.objects.filter(
                #             product=product).values_list('id', flat=True))
                #     if product_entries.exists():
                #         ticket, created = Ticket.objects.get_or_create(
                #             transaction=transaction, product=product, game=ProductGame.objects.get(product=product)
                #         )
                #         entries_list = []
                #         append_entry = entries_list.append
                #         for product_entry in product_entries:
                #             append_entry(Entry(
                #                 ticket=ticket, event=product_entry.event, boost_value=product_entry.entry.boost_value,
                #                 balls1=product_entry.entry.balls1, balls2=product_entry.entry.balls2,
                #                 balls3=product_entry.entry.balls3))
                #         Entry.objects.bulk_create(entries_list)

                if self.request.session.get("buy_play_credits", False):
                    del self.request.session["buy_play_credits"]
            else:
                messages.error(self.request, self.limit_message)
                # check if anonymous user has a basket pending, if true user has to be authenticated to proceed
                if self.request.session.get('basket', False):
                    # redirect to checkout page after authentication.
                    self.request.session['LOGIN_REDIRECT_URL'] = reverse('checkout')
                    logout(self.request)
        return super(CheckoutView, self).form_valid(form)

    def get_success_url(self):
        if self.request.user.is_authenticated():
            return_url = reverse("checkout_complete")
        else:
            return_url = reverse("auth_login")
            self.request.session["is_in_checkout"] = True
            self.request.session.save()
        return return_url

    def discounted_total_cost_helper(self, cash_total_cost, discount, threshold):
        """
        helper function to get the  discounted total cost and discount applied to the transaction.
        """
        if threshold != 0 and cash_total_cost > threshold:
            discounted_cost = threshold - (threshold * discount)
            discounted_total_cost = (cash_total_cost - threshold) + discounted_cost
            discount = (threshold * discount)
        else:
            discounted_total_cost = cash_total_cost - (cash_total_cost * discount)
            discount = (cash_total_cost * discount)
        return (discounted_total_cost, discount)

    def calculate_discounted_total_cost(self, total_cost, used_play_credits, discount, threshold, total_vouchers_used, for_product):
        """
        calculates the total amount to be paid after vouchers, play credits and discount has been applied.
        """
        cash_total_cost = total_cost - (total_vouchers_used + used_play_credits)
        discounted_total_cost, discount = self.discounted_total_cost_helper(cash_total_cost, discount, threshold)
        self.request.session['discounted_total_cost'] = discounted_total_cost
        self.request.session['discount'] = discount
        return (discounted_total_cost, discount)

    def get_context_data(self, **kwargs):
        context = super(CheckoutView, self).get_context_data(**kwargs)
        context["game_list"] = self.basket_list
        context["total_cost"] = self.total_cost
        context["total_entries"] = self.total_entries
        context["total_entries_per_draw"] = self.total_entries_per_draw
        context["product_list"] = self.in_product_list
        context["products"] = self.product_list
        context['deposit_limit'] = self.check_transaction_limit('deposit')
        context['minimum_play_credits'] = getattr(settings, 'MINIMUM_PLAY_CREDITS', None)
        context.update(get_final_cost_context(self.request, self.basket_list, self.product_list, self.total_entries, self.total_entries_per_draw))
        if self.request.user.is_authenticated():
            try:
                offer_customer = self.request.user.offercustomer_set.all().order_by('-created')[0]
                if offer_customer.status == OfferCustomer.STATUS.active:
                    type = offer_customer.offer.params.get('type', 0)
                    type = int(type)
                    if type == 4 and offer_customer.offer.check_offer_frequency(self.request.user): #discount reward type
                            for_product = offer_customer.offer.params.get('for_product', None)
                            offer_discount = offer_customer.offer.params.get('discount', 0)
                            offer_discount = float(offer_discount)
                            threshold = offer_customer.offer.params.get('threshold', 0)
                            if threshold != '':
                                threshold = int(threshold)
                            else:
                                threshold = 0
                            discounted_total_cost, discount = self.calculate_discounted_total_cost(
                                float(context["total_cost"]), float(context['play_credits']), offer_discount, threshold,
                                float(context['vouchers']), for_product)
                            context['discounted_total_cost'] = discounted_total_cost
                            context['discount'] = discount
            except IndexError:
                clean_checkout_session(self.request)
        context["transaction_limit"] = self.check_transaction_limit('entry')
        context['total_vouchers'] = 0
        for basket_vouchers in context['voucher_list']:
            context['total_vouchers'] += basket_vouchers['vouchers']

        return context

def paycredit(request):
    form = PlayCreditForm(request.POST)
    if form.is_valid():
        request.session["buy_play_credits"] = form.cleaned_data["buy_play_credits"]
        request.session.save()
    context = get_final_cost(request)
    return render_to_response('play/partials/checkout_final_prices.html', context, context_instance=RequestContext(request))


class CheckoutSuccessTemplateView(TemplateView):
    """
    displays successful checkout page and checks for a cross sell promotions.
    """
    template_name = 'play/checkout/success.html'

    def voucher_bonus(self, transaction, offer):
        """
        check is the user has been accepted an offer for free vouchers.
        params: dict containing the (buy_entries, in_lottery, earn_vouchers, for_product)
            buy_entries: the minimum number of entries to get the offer.
            in_lottery: the game in which the entries have to come from.
            earn_voucher: the amount of vouchers earned for buy_entries.
            for_product: the product the earn_voucher can be used on

        Arguments:
            entries: the ticket entries the voucher should be applied too.
            offer: the offer accepted by the user.
        """
        game = offer.params.get('in_lottery', None)
        if not game or game == "":
            offer_entries = Entry.objects.filter(
                ticket__in=Ticket.objects.filter(transaction=transaction)).count()
        else:
            offer_entries = Entry.objects.filter(
                ticket__in=Ticket.objects.filter(transaction=transaction, game__game=game)).count()
        try:
            earn_voucher = offer.params.get('earn_voucher', 1)
            if earn_voucher == "":
                earn_voucher = 1
            earn_voucher = int(earn_voucher)
            buy_entries = offer.params.get('buy_entries', 1)
            if buy_entries == "":
                buy_entries = 1
            buy_entries = int(buy_entries)
            vouchers = (offer_entries / buy_entries) * earn_voucher
            for_product = offer.params.get('for_product', None)
            if for_product:
                for_product = int(for_product)
                product = Product.objects.get(id=for_product)
                try:
                    wallet = UserWallet.objects.get(product=product)
                    wallet.vouchers = F('vouchers') + vouchers
                    wallet.save()
                except UserWallet.DoesNotExist:
                    # assumes that every product has a single game. TODO: fix this quick
                    game = ProductGame.objects.filter(product=product).get().game
                    UserWallet.objects.create(product=product, game=game, point=0.0, vouchers=vouchers)
        except ArithmeticError:
            pass

    def deposit_bonus(self, offer):
        """
        applies deposit bonus
        """
        deposit = self.request.session.get('deposit', None)
        if deposit:
            try:
                bonus = 1 + offer.params.get('bonus', 0)
                threshold = offer.params.get('threshold', None)
                if threshold == "":
                    threshold = None
                if threshold and deposit > threshold:
                    bonus_deposit = threshold * bonus
                else:
                    bonus_deposit = deposit * bonus
                self.request.user.profile.play_credits = F('play_credits') + bonus_deposit
                clean_checkout_session(self.request)
            except ArithmeticError:
                pass

    def get_context_data(self, **kwargs):
        context = super(CheckoutSuccessTemplateView, self).get_context_data(**kwargs)
        #ensures that the user gets a single offer at a time.
        stored_offer = self.request.session.get('offer', None)
        if not stored_offer:
            context['initial_offer'] = get_offer_helper(self.request, Offer.ENGAGE.custom)
        # delete offer stored on the session if it has been used.
        elif OfferCustomer.objects.filter(
                customer=self.request.user, offer=stored_offer, status=OfferCustomer.STATUS.expired).exists():
            try:
                del self.request.session['offer']
            except KeyError:
                pass
        try:
            offer_customer = self.request.user.offercustomer_set.all().order_by('-created')[0]
            reward_type = offer_customer.offer.params.get('type', 0)
            reward_type = int(reward_type)
            latest_transaction = self.request.user.transaction_set.filter(
                    payment_status=Transaction.PAYMENT_STATUS_CHOICES.completed).order_by('-created_at')[0]
            if offer_customer.offer.check_offer_frequency(self.request.user):
                if type == 3: #Voucher Bonus reward type
                    self.voucher_bonus(latest_transaction, offer_customer.offer)
                elif type == 2: #Deposit Bonus reward type
                    self.deposit_bonus(offer_customer.offer)
                if reward_type == 3 or reward_type == 2:
                    #update customer offer table
                    offer_customer.transaction = latest_transaction
                    for_product = offer_customer.offer.params.get('for_product', None)
                    if for_product and for_product is not " ":
                        offer_customer.product = Product.objects.get(id=int(for_product))
                    #if the offer is valid for a single transaction set customer offer status as expired.
                    if offer_customer.offer.period == 1:
                        offer_customer.status = OfferCustomer.STATUS.expired
                    offer_customer.save()
                    clean_checkout_session(self.request)
        except IndexError:
            pass
        return context

checkout_success = CheckoutSuccessTemplateView.as_view()
#======================================================================================================================
# Helper functions for creating transactions and ticket entries.
#======================================================================================================================


def create_entry_purchase(request=None, user=None, total_cost=0.00, discount=0.00, play_credits=0.00,\
                          buy_play_credits=0.00, payment_due=0.00, product_list=[], \
                          status=Transaction.TRANSACTION_TYPE_CHOICES.pending, used_vouchers_cost=0):
    if user.is_authenticated():
        if not buy_play_credits and play_credits:
            buy_play_credits = 0.00
        else:
            #creates a separate transaction for purchasing play credits.
            transaction = Transaction.objects.create(
                site=Site.objects.get_current(), user=user, payment_status=status, discount=0, play_credits=0,
                transaction_type=Transaction.TRANSACTION_TYPE_CHOICES.purchase_play_credit, total_paid=buy_play_credits,
                total_value=buy_play_credits
            )

        if buy_play_credits:
            profile = UserProfile.objects.get(user=user)
            #https://docs.djangoproject.com/en/dev/ref/models/instances/#updating-attributes-based-on-existing-fields
            profile.play_credits = F('play_credits') + buy_play_credits
            profile.save()

        #set play credits value to the play credits the user has initially
        transaction = Transaction.objects.create(
            site=Site.objects.get_current(), user=user, payment_status=status, discount=discount, total_paid=payment_due,
            transaction_type=Transaction.TRANSACTION_TYPE_CHOICES.entry_purchased, play_credits=play_credits,
            total_value=total_cost, total_vouchers_used=used_vouchers_cost
        )

        if request:
            #store transaction, buy_play_credits on session
            request.session['transaction'] = transaction
            request.session['deposit'] = buy_play_credits

        products_used_vouchers = {}
        games_total = {}
        for product in product_list:
            if UserWallet.objects.filter(product=product["product"], user_id=user).count():
                wallet = UserWallet.objects.get(product=product["product"], user_id=user)

                game_total = float(product["total_cost"])
                game = wallet.product.get_game()
                game_price = float(game.price)

                used_vouchers = 0
                vouchers = int(wallet.vouchers) # total_paid, discount = discount_offer(
                if vouchers >= 1:
                    #http://wiki.python.org/moin/PythonSpeed/PerformanceTips#Use_xrange_instead_of_range
                    for voucher in xrange(1, vouchers+1):
                        if (game_total - game_price) >= 0:
                            game_total -= game_price
                            used_vouchers += 1

                remaining_vouchers = wallet.vouchers - used_vouchers
                wallet.vouchers = remaining_vouchers
                wallet.save()

                products_used_vouchers.update({product['product'].id: used_vouchers})
                # ticket total price with vouchers and play credits used deducted.
                games_total.update({product['product'].id: game_total})

        if play_credits:
            profile = UserProfile.objects.get(user=user)
            profile.play_credits = F('play_credits') - float(play_credits)
            profile.save()

        tickets = []
        for basket_item in BasketEntry.objects.filter(user=user):
            start_event = get_object_or_404(Event, pk=basket_item.draws["start_event"])
            weeks = int(basket_item.draws.get('duration'))
            draw_days = basket_item.draws['days'].title().replace('-', ',').split(',')
            deck_attrs = {
                'transaction': transaction, 'start_on': start_event, 'product': basket_item.product, 'weeks': weeks,
                'product_game': basket_item.game, 'jackpot': basket_item.prize, 'boost': int(basket_item.boost_value),
                'draw_days': draw_days, 'total_cost': basket_item.cost, 'addons': basket_item.addons,
                'tickets': basket_item.balls, 'total_entries': basket_item.tickets,
            }
            #create a deck from the basket item
            deck = Deck.objects.create(**deck_attrs)

            for ticket_no, selected_numbers in deck.tickets.iteritems():
                try:
                    addons = deck.addons[ticket_no]
                except KeyError:
                    addons = 0
                ticket_attrs = {
                    'transaction': transaction, 'product': deck.product, 'game': deck.product_game, 'deck': deck,
                    'addons': int(addons), 'used_vouchers': products_used_vouchers[deck.product.id],
                    'total_cost': deck.total_cost, 'selected_numbers': selected_numbers.values(),
                }
                tickets.append(Ticket(**ticket_attrs))

        if tickets:
            Ticket.objects.bulk_create(tickets)
            entries_bulk = []
            for ticket in Ticket.objects.filter(transaction=transaction):
                entries = create_ticket_entries(ticket, ticket.deck.start_on)
                entries_bulk += entries
            Entry.objects.bulk_create(entries_bulk, batch_size=10000)

        try:
            offer_customer = request.user.offercustomer_set.all().order_by('created')[0]
            type = offer_customer.offer.params.get('type', None)
            type = int(type)
            if type == 4 and offer_customer.offer.check_offer_frequency(request.user): #discount reward type
                for_product = offer_customer.offer.params.get('for_product', None)

                # get the actual value paid by the customer; this is the final value once all discounts and specials
                # the customer has have been applied.
                # total_paid = request.session.get('discounted_total_cost', transaction.total_value)
                # discount = request.session.get('discount', 0)

                #update transaction
                # transaction.discount = discount
                # transaction.total_paid = total_paid
                #store the total amount of vouchers used on the transaction
                # transaction.total_vouchers_used = sum(products_used_vouchers.values())
                # transaction.save()

                #update customer offer table
                offer_customer.transaction = transaction
                if for_product and for_product is not " ":
                    offer_customer.product = Product.objects.get(id=int(for_product))

                #if the offer is valid for a single transaction set customer offer status as expired.
                if offer_customer.offer.period == 1:
                    offer_customer.status = OfferCustomer.STATUS.expired

                offer_customer.save()
                clean_checkout_session(request)
        except IndexError:
            pass

        vars=[transaction.id]
        system_log = Log()
        system_log.system_log_type_id = 1
        system_log.user_id = user.id
        system_log.description = "Entry purchase - #transaction#"
        system_log.log_vars = vars
        system_log.save()

    #clearing your basket
    BasketEntry.objects.filter(user=user).delete()

    # send purchase completed email
    if user.is_authenticated():
        email_transaction(user, transaction)

    return products_used_vouchers

def create_ticket_entries(ticket, start_event):
    #create a list of draw days the user has selected for the tickets
    draws = map(lambda draw_day: draw_day.lower(), ticket.deck.draw_days) #set all days to lowercase
    #gets start event from the start date selected
    week = {0: 'monday', 1: 'tuesday', 2: 'wednesday', 3: 'thursday', 4: 'friday', 5: 'saturday', 6: 'sunday'}

    # stores a list of events based on the number of weeks selected for the tickets
    draws_events = []

    #calculates the number of events based on the duration (ie; a week, a month) the tickets are valid for
    if start_event.draw_datetime <= datetime.now():
        new_date = datetime.now()
    else:
        new_date = start_event.draw_datetime

    selection_duration = new_date + timedelta(weeks=ticket.deck.weeks)

    events = Event.objects.filter(
        draw_datetime__gte=new_date, draw_datetime__lte=selection_duration,
        game=ticket.deck.product_game.game).order_by('draw_datetime')
    events = events[:len(events)-1] #removes rollover to another week

    for e in events: #get events that happen on the draw days the user has selected to play tickets on
        if week[e.draw_datetime.weekday()] in draws:
            draws_events.append(e)

    entries = []
    addon_value = pickle(ticket.deck.addons).get(ticket.addons, [])
    if addon_value:
        addon_value = [addon_value]

    #check if this game has a single ballset.
    if len(ticket.selected_numbers) == 1:
        selected_numbers = ticket.selected_numbers.pop()
        main = ticket.deck.product_game.game.ballset_set.values_list('main', flat=True)[0]
        combinations = single_ballset_combinations(selected_numbers, main)
        for combination in combinations:
            combination = list(combination)
            #create a entry for each combination, assign the appropriate event for each draw day selected.
            for draw_event in draws_events:
                entries.append(Entry(ticket=ticket, event=draw_event, balls1=combination, balls2=addon_value,
                    price=draw_event.game.price, boost_value=ticket.deck.boost))

    else: #ticket with more than 1 ballset.
        #[[first ballset selected numbers], [second ballset selected numbers]]
        selected_numbers = ticket.selected_numbers
        ballset1_main = ticket.deck.product_game.game.ballset_set.values_list('main', flat=True)[1]
        ballset2_main = ticket.deck.product_game.game.ballset_set.values_list('main', flat=True)[0]
        combinations = multiple_ballset_combinations(
            selected_numbers[1], ballset1_main, selected_numbers[0], ballset2_main
        )
        for combination in combinations:
            for draw_event in draws_events:
                entries.append(Entry(ticket=ticket, event=draw_event, balls1=combination[0], balls2=combination[1],
                      balls3=addon_value, price=draw_event.game.price, boost_value=ticket.deck.boost))
        ticket.total_entries = len(entries)
        ticket.save()
    return entries

from django.shortcuts import render, get_object_or_404
def test_email_transaction(request, transaction_id):
    """
    used to email transaction record to a user.
    """
    user = request.user
    transaction = get_object_or_404(Transaction, pk=transaction_id)

    total_entries = transaction.deck_set.all().aggregate(Sum('total_entries'))
    site = Site.objects.get_current()
    my_orders = '{0}{1}{2}'.format('http://', site.domain, reverse('account_history'))
    account_balance = '{0}{1}{2}'.format('http://', site.domain, reverse('account_balance'))
    contact_us = '{0}{1}{2}'.format('http://', site.domain, reverse('contact'))
    email_content = {
        "user": user, "transaction": transaction, "decks": transaction.deck_set.all(),
        "buy_play_credits": transaction.credits_bought, "used_vouchers": transaction.total_vouchers_used,
        "total_entries": total_entries['total_entries__sum'], 'my_orders': my_orders,
        'account_balance': account_balance, 'contact_us': contact_us

    }
    return render(request, 'emails/html/transaction_complete.html', email_content)
