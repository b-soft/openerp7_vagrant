from django.contrib import admin
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from play.models.transaction import Transaction, ReplayEntries
from flutter_modeladmin import RemoveDeleteAdmin
from reversion import VersionAdmin
from play.models.transaction_type import TransactionType

class TransactionAdmin(RemoveDeleteAdmin, VersionAdmin):
    change_list_template = "admin/transaction/change_list.html"
    change_form_template = "admin/transaction/change_form.html"

    def get_changelist(self, request, **kwargs):
        request.session["customer_user_id"] = request.GET.get("user__id__exact", request.session.get("customer_user_id", False))
        return super(TransactionAdmin, self).get_changelist(request, **kwargs)

    def get_extra_context(self, request):
        extra_context = {}
        if request.session.get("customer_user_id", False):
            customer = User.objects.get(pk=request.session["customer_user_id"])
            if customer.first_name or customer.last_name:
                extra_context['customer'] = "%s %s" % (customer.first_name, customer.last_name)
            else:
                extra_context['customer'] = "%s" % customer.username
            extra_context['customer_id'] = customer.id
        return extra_context

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context.update(self.get_extra_context(request))
        return super(TransactionAdmin, self).changelist_view(request, extra_context=extra_context)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context.update(self.get_extra_context(request))
        return super(TransactionAdmin, self).change_view(request=request, object_id=object_id, form_url=form_url, extra_context=extra_context)

    def add_view(self, request, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context.update(self.get_extra_context(request))
        return super(TransactionAdmin, self).add_view(request=request, form_url=form_url, extra_context=extra_context)

    def has_add_permission(self, request):
        return False

    def get_transaction_type(self, obj):
        return obj.TRANSACTION_TYPE_CHOICES._choices[int(obj.transaction_type)][1]
    get_transaction_type.short_description = 'Transaction type'
    get_transaction_type.allow_tags = True

    def get_payment_status(self, obj):
        return obj.PAYMENT_STATUS_CHOICES._choices[int(obj.payment_status)][1]
    get_payment_status.short_description = 'Payment status'

    def total_trans_value(self, obj):
        return '&euro;%s' % obj.total_value
    total_trans_value.short_description = 'Total value'
    total_trans_value.allow_tags = True

    def name_and_email(self, obj):
        return "%s %s <br><div style=\"font-size:10px;color:#999999;\">%s</div>" % (obj.user.first_name, obj.user.last_name, obj.user.email)
    name_and_email.short_description = 'Customer'
    name_and_email.allow_tags = True

    def save_model(self, request, obj, form, change):
        if not change:
            user_id = request.session.get("customer_user_id", False)
            if user_id:
                obj.user = User.objects.get(id=user_id)
        return super(TransactionAdmin, self).save_model(request, obj, form, change)

    list_display = ["id", "name_and_email", "get_transaction_type", "total_trans_value", "get_payment_status", "created_at"]
    list_per_page = 20


class ReplayEntriesAdmin(RemoveDeleteAdmin, VersionAdmin):
    pass

admin.site.register(Transaction, TransactionAdmin)
admin.site.register(ReplayEntries, ReplayEntriesAdmin)
