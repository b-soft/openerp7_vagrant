from django.db import transaction
from django.core.management.base import NoArgsCommand
from django.contrib.auth.models import Group, Permission

class Command(NoArgsCommand):
    """
    Command for settings up different user groups and their permissions
    """
    help = 'Revoke delete permissions user group.'

    @transaction.commit_on_success
    def handle_noargs(self, **options):
        """
        create group for users that register on the site and revoke all delete permissions.
        """
        group = Group.objects.get_or_create(name='Site Users')
        for permission in Permission.objects.filter(codename__startswith='delete_'):
            group.permissions.add(permission)
        group.save()