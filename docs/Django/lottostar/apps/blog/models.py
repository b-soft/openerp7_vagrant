from django.contrib.sites.models import Site
from django.db import models
from django.contrib.auth.models import User
from translatable.models import TranslatableModel, get_translation_model

# Statuses
LIVE_STATUS = 1
DRAFT_STATUS = 2
HIDDEN_STATUS = 3

class Tag(TranslatableModel):
    date_created = models.DateTimeField(auto_now=True, editable=False)
    date_modified = models.DateTimeField(auto_now_add=True, editable=False)

    def __unicode__(self):
        return self.get_translation(language="en").name

class TagTranslation(get_translation_model(Tag, "tag")):
    name = models.CharField(max_length=255)
    slug = models.SlugField(unique=True)

    def __unicode__(self):
        return self.model.get_translation(language="en").name

    class Meta:
        ordering = ['name']
        verbose_name = "Tag"
        verbose_name_plural = "Tags"


class Category(TranslatableModel):
    site = models.ManyToManyField(Site)
    order = models.IntegerField(max_length=3, default=0)
    date_created = models.DateTimeField(auto_now=True, editable=False)
    date_modified = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"

    def __unicode__(self):
        return self.get_translation(language="en").name


class CategoryTranslation(get_translation_model(Category, "category")):
    name = models.CharField(max_length=255)
    slug = models.SlugField(unique=True)

    def __unicode__(self):
        return self.model.get_translation(language="en").name

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"


class Live(models.Manager):
    def get_query_set(self):
        return super(Live, self).get_query_set().filter(status__exact=LIVE_STATUS)


class Post(TranslatableModel):
    site = models.ManyToManyField(Site)
    post_status = (
        (LIVE_STATUS, 'Live'),
        (DRAFT_STATUS, 'Draft'),
        (HIDDEN_STATUS,'Hidden'),
    )
    photo = models.ImageField("Photo", upload_to="blog/", blank=True, null=True)
    status = models.IntegerField(choices=post_status)
    author = models.ForeignKey(User)
    categories = models.ManyToManyField(Category)
    tags = models.ManyToManyField(Tag)

    post_comments = models.BooleanField(default=True)

    date_created = models.DateTimeField(auto_now=True, editable=False)
    date_modified = models.DateTimeField(auto_now_add=True, editable=False)
    date_published = models.DateTimeField()

    # manager
    live = Live()
    objects = models.Manager()

    class Meta:
        ordering = ['-date_published']
        verbose_name = "Post"
        verbose_name_plural = "Posts"

    def __unicode__(self):
        return self.get_translation(language="en").title


class PostTranslation(get_translation_model(Post, "post")):
    title = models.CharField(max_length=255)
    slug = models.SlugField()
    body = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return self.model.get_translation(language="en").title

    class Meta:
        verbose_name = "Post"
        verbose_name_plural = "Posts"


class PostComment(models.Model):
    post = models.ForeignKey(PostTranslation)
    user = models.ForeignKey(User)
    post_status = (
        (LIVE_STATUS, 'Approved'),
        (DRAFT_STATUS, 'Waiting'),
        (HIDDEN_STATUS,'Declined'),
        )
    status = models.IntegerField(choices=post_status)
    comment = models.TextField()

    date_created = models.DateTimeField(auto_now=True, editable=False)
    date_modified = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        ordering = ['-date_created']
        verbose_name = "Comment"
        verbose_name_plural = "Comments"

    def __unicode__(self):
        return self.post.title
