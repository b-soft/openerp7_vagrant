from django.contrib import admin
from django import forms
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect
from blog.models import Post, Category, Tag, PostComment, PostTranslation, TagTranslation, CategoryTranslation
from django.conf import settings
from flutter_modeladmin import RemoveDeleteAdmin
from reversion import VersionAdmin


def tags(obj):
    return ", ".join([x.get_translation(language="en").name for x in obj.tags.all()])

def categories(obj):
    return ", ".join([c.get_translation(language="en").name for c in obj.categories.all()])

def title(obj):
    return obj.get_translation(language="en").title

def name(obj):
    return obj.get_translation(language="en").name

def slug(obj):
    return obj.get_translation(language="en").slug

def author(obj):
    return obj.author.get_full_name()


class PostTextAdmin(RemoveDeleteAdmin, VersionAdmin):
    model = PostTranslation
    list_display = ["model", "language"]
    readonly_fields = ["model", "language"]

    fieldsets = (
        (None, {
            'fields': ('model', "language")
        }),
        ("Details", {
            'fields': ('title','slug',)
        }),
        ("Content", {
            'fields': ('body', )
        }),
    )

    class Media:
        js = (
            "/static/js/tinymce/tinymce.min.js",
            "/static/js/tinymce.js",
        )

    def get_model_perms(self, request):
        return {}

    def changelist_view(self, request, extra_context=None):
        return HttpResponseRedirect(reverse("admin:blog_post_changelist"))

admin.site.register(PostTranslation, PostTextAdmin)

def post_controls(obj):
    html = '<div style="width:340px;height:25px; line-height: 25px">'
    for lang in settings.LANGUAGES:
        html += '<a style="margin:5px" href="/blog/admin/translate/post/%s/?language=%s" title="%s"><img src="/static/img/flags/icons/%s.png"/></a>' % (obj.id, lang[0], lang[1].title(), lang[0])
    html += "</div>"
    return html
post_controls.short_description = 'Controls'
post_controls.allow_tags = True

class PostInlineAdmin(admin.StackedInline):
    model = PostTranslation
    extra = 1
    max_num = 1
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'language':
            kwargs['initial'] = "en"
        return super(PostInlineAdmin, self).formfield_for_dbfield(db_field, **kwargs)

class PostAdmin(RemoveDeleteAdmin, VersionAdmin):
    inlines = [PostInlineAdmin,]

    fieldsets = (
        (None, {
            'fields': ('site','photo',('status','author'),'date_published',),
        }),
        ("Tags and Categories", {
            'fields': (('categories', 'tags'),)
        }),
    )
    list_display = (title, 'status', author, 'date_published', tags, categories, post_controls)
    list_filter = ('status', 'tags', 'categories')

    def get_inline_instances(self, request, obj=None):
        inline_instances = super(PostAdmin, self).get_inline_instances(request, obj)
        if obj:
            return []
        return inline_instances


    def save_model(self, request, obj, *args, **kwargs):
        obj.author = request.user
        super(PostAdmin, self).save_model(request, obj, *args, **kwargs)
admin.site.register(Post, PostAdmin)


def category_controls(obj):
    html = '<div style="width:340px;height:25px; line-height: 25px">'
    for lang in settings.LANGUAGES:
        html += '<a style="margin:5px" href="/blog/admin/translate/category/%s/?language=%s" title="%s"><img src="/static/img/flags/icons/%s.png"/></a>' % (obj.id, lang[0], lang[1].title(), lang[0])
    html += "</div>"
    return html
category_controls.short_description = 'Controls'
category_controls.allow_tags = True

class CategoryInlineAdmin(admin.StackedInline):
    model = CategoryTranslation
    extra = 1
    max_num = 1
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'language':
            kwargs['initial'] = "en"
        return super(CategoryInlineAdmin, self).formfield_for_dbfield(db_field, **kwargs)

class CategoryAdmin(RemoveDeleteAdmin, VersionAdmin):
    inlines = [CategoryInlineAdmin, ]
    list_display = (name, slug,'order',category_controls)
    list_editable = ('order',)

    def get_inline_instances(self, request, obj=None):
        inline_instances = super(CategoryAdmin, self).get_inline_instances(request, obj)
        if obj:
            return []
        return inline_instances
admin.site.register(Category, CategoryAdmin)


class CategoryTextAdmin(RemoveDeleteAdmin, VersionAdmin):
    model = CategoryTranslation
    list_display = ["model", "language"]
    readonly_fields = ["model", "language"]

    fieldsets = (
        (None, {
            'fields': ('model', "language")
        }),
        ("Details", {
            'fields': ('name','slug',)
        }),
    )

    def get_model_perms(self, request):
        return {}

    def changelist_view(self, request, extra_context=None):
        return HttpResponseRedirect(reverse("admin:blog_category_changelist"))
admin.site.register(CategoryTranslation, CategoryTextAdmin)


def tag_controls(obj):
    html = '<div style="width:340px;height:25px; line-height: 25px">'
    for lang in settings.LANGUAGES:
        html += '<a style="margin:5px" href="/blog/admin/translate/tag/%s/?language=%s" title="%s"><img src="/static/img/flags/icons/%s.png"/></a>' % (obj.id, lang[0], lang[1].title(), lang[0])
    html += "</div>"
    return html
tag_controls.short_description = 'Controls'
tag_controls.allow_tags = True

class TagInlineAdmin(admin.StackedInline):
    model = TagTranslation
    extra = 1
    max_num = 1

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'language':
            kwargs['initial'] = "en"
        return super(TagInlineAdmin, self).formfield_for_dbfield(db_field, **kwargs)

class TagAdmin(RemoveDeleteAdmin, VersionAdmin):
    inlines = [TagInlineAdmin, ]
    list_display = [name, slug, tag_controls]

    def get_inline_instances(self, request, obj=None):
        inline_instances = super(TagAdmin, self).get_inline_instances(request, obj)
        if obj:
            return []
        return inline_instances

admin.site.register(Tag, TagAdmin)


class TagTextAdmin(RemoveDeleteAdmin, VersionAdmin):
    model = TagTranslation
    list_display = ["model", "language"]
    readonly_fields = ["model", "language"]

    fieldsets = (
        (None, {
            'fields': ('model', "language")
        }),
        ("Details", {
            'fields': ('name','slug',)
        }),
    )

    def get_model_perms(self, request):
        return {}

    def changelist_view(self, request, extra_context=None):
        return HttpResponseRedirect(reverse("admin:blog_tag_changelist"))
admin.site.register(TagTranslation, TagTextAdmin)
