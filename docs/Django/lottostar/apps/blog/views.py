import calendar
from django.contrib import messages
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from blog.models import DRAFT_STATUS, PostTranslation, CategoryTranslation, TagTranslation
from apps.blog.models import LIVE_STATUS
from models import PostComment
from forms import CommentForm
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView

from django.utils.translation import ugettext_lazy as _

from django.template.defaultfilters import title

from models import Category, Post, Tag


def admin_post_text(request, post_id):
    post = Post.objects.get(pk = post_id)
    translate = PostTranslation.objects.get_or_create(model=post, language=request.GET.get("language", "en"), defaults={"title":"", "slug":"", "body":""})
    return HttpResponseRedirect("/admin/blog/posttranslation/%s/" % translate[0].id)

def admin_category_text(request, category_id):
    category = Category.objects.get(pk = category_id)
    translate = CategoryTranslation.objects.get_or_create(model=category, language=request.GET.get("language", "en"), defaults={"name":"", "slug":""})
    return HttpResponseRedirect("/admin/blog/categorytranslation/%s/" % translate[0].id)

def admin_tag_text(request, tag_id):
    tag = Tag.objects.get(pk = tag_id)
    translate = TagTranslation.objects.get_or_create(model=tag, language=request.GET.get("language", "en"), defaults={"name":"", "slug":""})
    return HttpResponseRedirect("/admin/blog/tagtranslation/%s/" % translate[0].id)


class PostListView(ListView):
    template_name= "blog/post_list.html"
    model = Post
    context_object_name = "post_list"

    def get_queryset(self):
        return Post.objects.filter(status=LIVE_STATUS, site=Site.objects.get_current()).order_by("-date_published")

    def get_context_data(self, **kwargs):
        context = super(PostListView, self).get_context_data(**kwargs)
        context["title"] = "Our Latest Entries"
        return context


class PostDetailView(DetailView):
    template_name= "blog/post.html"
    model = PostTranslation
    context_object_name = "post"

    def get_context_data(self, **kwargs):
        context = super(PostDetailView, self).get_context_data(**kwargs)
        context["form"] = CommentForm
        return context


class PostCommentCreateView(CreateView):
    template_name= "blog/post.html"
    form_class = CommentForm
    model = PostComment

    def get_form_kwargs(self):
        kwargs = super(PostCommentCreateView, self).get_form_kwargs()
        if self.request.user.is_authenticated():
            kwargs['instance'] = PostComment()
            kwargs['instance'].post = PostTranslation.objects.get(slug=self.kwargs['slug'])
            kwargs['instance'].user = self.request.user
            kwargs['instance'].status = DRAFT_STATUS
        return kwargs

    def form_invalid(self, form):
        messages.error(self.request, _("You did not enter in a valid message"))
        return HttpResponseRedirect(reverse('blog_post', kwargs={ 'slug' : self.kwargs['slug'] }))

    def form_valid(self, form):
        messages.success(self.request, _("Your comment was successfully submitted"))
        return super(PostCommentCreateView,self).form_valid(form)

    def get_success_url(self):
        return reverse('blog_post', args=(), kwargs={ 'slug' : self.kwargs['slug'] })


class MonthListView(ListView):
    template_name= "blog/post_list.html"
    model = PostComment

    def get_queryset(self):
        month_list = dict((v.lower(),k) for k,v in enumerate(calendar.month_name))
        post_list = Post.objects.filter(
            site=Site.objects.get_current(),
            date_published__year=self.kwargs.get("year"),
            date_published__month=month_list[self.kwargs.get("month")]
        )
        return post_list

    def get_context_data(self, **kwargs):
        context = super(MonthListView, self).get_context_data(**kwargs)
        context["title"] = "%s %s" % (title(self.kwargs.get("month")), self.kwargs.get("year"))
        return context


class CategoryListView(ListView):
    template_name= "blog/post_list.html"
    model = Post
    context_object_name = "post_list"

    def get_queryset(self):
        post_list = False
        try:
            post_list = Post.objects.filter(site=Site.objects.get_current(), categories__translations=CategoryTranslation.objects.filter(slug=self.kwargs.get("slug")))
        except Category.MultipleObjectsReturned:
            print "Multiple Categories Returned for slug '%s'" % self.kwargs.get("slug")
        return post_list

    def get_context_data(self, **kwargs):
        context = super(CategoryListView, self).get_context_data(**kwargs)
        try:
            context["title"] = CategoryTranslation.objects.get(slug=self.kwargs.get("slug"))
        except CategoryTranslation.MultipleObjectsReturned:
            print "Multiple Categories Returned for slug '%s'" % self.kwargs.get("slug")
        return context


class TagListView(ListView):
    template_name= "blog/post_list.html"
    model = Post
    context_object_name = "post_list"

    def get_queryset(self):
        post_list = False
        try:
            post_list = Post.objects.filter(site=Site.objects.get_current(), tags=Tag.objects.get(translations__slug=self.kwargs.get("slug")))
        except Tag.MultipleObjectsReturned:
            print "Multiple Tags Returned for slug '%s'" % self.kwargs.get("slug")
        return post_list

    def get_context_data(self, **kwargs):
        context = super(TagListView, self).get_context_data(**kwargs)
        try:
            context["title"] = Tag.objects.get(translations__slug=self.kwargs.get("slug"))
        except Category.MultipleObjectsReturned:
            print "Multiple Tags Returned for slug '%s'" % self.kwargs.get("slug")
        return context
