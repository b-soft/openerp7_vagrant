from django.conf.urls import *
from apps.blog import views

urlpatterns = patterns('apps.blog.views',
    url(r'^admin/translate/post/(?P<post_id>[a-zA-Z0-9_.-]+)/$', "admin_post_text", name="admin_post_text"),
    url(r'^admin/translate/category/(?P<category_id>[a-zA-Z0-9_.-]+)/$', "admin_category_text", name="admin_category_text"),
    url(r'^admin/translate/tag/(?P<tag_id>[a-zA-Z0-9_.-]+)/$', "admin_tag_text", name="admin_tag_text"),

    url('^$', views.PostListView.as_view(), name='blog'),
    url('^category/(?P<slug>[a-zA-Z0-9_.-]+)/$', views.CategoryListView.as_view(), name='blog_category'),
    url('^tag/(?P<slug>[a-zA-Z0-9_.-]+)/$', views.TagListView.as_view(), name='blog_tag'),
    url('^(?P<slug>[a-zA-Z0-9_.-]+)/comment/$', views.PostCommentCreateView.as_view(), name='blog_comment_post'),
    url('^(?P<year>[a-zA-Z0-9_.-]+)/(?P<month>[a-zA-Z0-9_.-]+)/$', views.MonthListView.as_view(), name='blog_month'),
    url('^(?P<slug>[a-zA-Z0-9_.-]+)/$', views.PostDetailView.as_view(), name='blog_post'),
)
