from django import forms
from models import PostComment

class CommentForm(forms.ModelForm):
    comment = forms.CharField(widget=forms.Textarea(attrs={"style":"width:98%; height: 100px;"}))
    class Meta:
        model = PostComment
        fields = ['comment',]
