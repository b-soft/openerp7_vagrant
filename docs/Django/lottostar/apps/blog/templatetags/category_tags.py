import datetime, twitter, re, os
from blog.models import Category, Post
from django.core.cache import cache

from django import template
from django.conf import settings

register = template.Library()


@register.filter
def exists(path):
    if os.path.exists(path):
        return True
    else:
        return False


# --- Category List
class CategoryNode(template.Node):
    def render(self, context):
        context['category_list'] = Category.objects.all().order_by("order")
        return ''

def get_categories(parser, token):
    return CategoryNode()

register.tag('get_categories', get_categories)


# --- History of Posts List

class HistoryNode(template.Node):
    def render(self, context):
        year_list = []
        for year in Post.objects.filter(status=1).dates('date_published', 'year'):
            month_list = []
            for month in Post.objects.filter(status=1,date_published__year=year.year).dates('date_published', 'month'):
                month_list.append({"title":month.month})
            year_list.append({"title":year.year, "month_list": month_list})
        context['year_list'] = year_list
        return ''

def get_post_years(parser, token):
    return HistoryNode()

register.tag('get_post_years', get_post_years)


# --- Last 5 Posts
class PostNode(template.Node):
    def render(self, context):
        context['latest_post_list'] = Post.objects.filter(status=1).order_by('-date_published')[:5]
        return ''

def get_latest_posts(parser, token):
    return PostNode()

register.tag('get_latest_posts', get_latest_posts)


# --- filters
@register.filter
def name_month(value):
    return datetime.date(int(datetime.datetime.now().year), value, 1).strftime("%B")

def url_target_blank(text):
    words = re.findall("@(\w+)",text)
    for word in words:
        text = text.replace(("@%s" % word), ('<a href="http://twitter.com/%s">@%s</a>' % (word, word)))
    return text.replace('<a ', '<a target="_blank" ')
url_target_blank = register.filter(url_target_blank)

