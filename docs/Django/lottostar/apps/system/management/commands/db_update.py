from django.core.management.base import NoArgsCommand
from django.db import transaction, IntegrityError, connection

@transaction.commit_manually
class Command(NoArgsCommand):
    def handle(self, *args, **options):

        try :
            sql = '''
                ALTER TABLE game_productgameboost ADD COLUMN prefix integer;
                ALTER TABLE game_productgameboost ADD COLUMN boost numeric(20, 2);
            '''
            cursor = connection.cursor()
            cursor.execute(sql)
            transaction.commit()
        except IntegrityError, e:
            transaction.rollback()

        print "Database successfully updated"

