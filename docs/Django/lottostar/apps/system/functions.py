import json
import urllib2
from BeautifulSoup import BeautifulSoup
from django.utils.html import strip_tags
import re

def search_and_replace(s, vars):
    result = re.findall ( '#(.*?)#', s, re.MULTILINE)
    new_string=s
    i=0
    for variable in result:
        new_string = s.replace('#%s#' % variable, vars[i])
        s=new_string
        i+=1
    return new_string


def parse_url(type, url):
    source_url=''
    if type == "facebook":
        source_url = "http://graph.facebook.com/?id=%s" % url
    elif type == "twitter":
        source_url = "http://cdn.api.twitter.com/1/urls/count.json?url=%s" % url
    elif type == "googleplus":
        source_url = "https://plusone.google.com/u/0/_/+1/fastbutton?url=%s&count=true" % url

    if type == "twitter" or type == "facebook":
        req = urllib2.Request(source_url)
        req.add_header('User-agent', 'Mozilla 5.10')
        res = urllib2.urlopen(req)
        content = res.read()
        result = json.loads(content)
        if type == "twitter":
            count = result['count']
        else:
            try:
                count=result['shares']
            except :
                count=0
        return count
    else:
        req = urllib2.Request(source_url)
        req.add_header('User-agent', 'Mozilla 5.10')
        res = urllib2.urlopen(req)
        html = res.read()
        soup = BeautifulSoup(html)
        content = soup.find('div', {'id': 'aggregateCount'})
        count = strip_tags(content)
    return count