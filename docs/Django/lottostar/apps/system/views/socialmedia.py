from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.views.decorators.csrf import csrf_exempt, requires_csrf_token
from system.functions import parse_url
from system.models.seometadata import SeoMetadataTranslation, SeoMetadata


@csrf_exempt
@requires_csrf_token
def get_socialmedia_feeds(request):
    context = {}
    if request.is_ajax():
        if request.method == 'POST':
            type = request.POST['type']
            url = request.POST['url']
            context['result'] = parse_url(type, url)
    return render_to_response('social.html', context, context_instance=RequestContext(request))


def admin_seometadata_text(request, seometadata_id):
    seometa = SeoMetadata.objects.get(pk = seometadata_id)
    seo = SeoMetadataTranslation.objects.get_or_create(model = seometa, language=request.GET.get("language", "en"), defaults={"title": "", "description": "", "keywords":""})
    return HttpResponseRedirect("/admin/system/seometadatatranslation/%s/" % seo[0].id)
