from django.conf.urls import *
urlpatterns = patterns('apps.system.views.socialmedia',
    url(r'^admin/translate/seometadata/(?P<seometadata_id>[a-zA-Z0-9_.-]+)/$', "admin_seometadata_text", name="admin_seometadata_text"),

    url('^socialmedia/feeds/$', "get_socialmedia_feeds", name='get_socialmedia_feeds'),
)
