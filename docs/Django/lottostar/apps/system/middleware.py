from django import http
from django.contrib.sites.models import Site
from django.http import HttpResponsePermanentRedirect
from django.core.urlresolvers import resolve
from django.core import urlresolvers
from django.http.response import HttpResponse
from django.utils.http import urlquote
from django.utils.safestring import mark_safe
import re
from system.models.noindex import Noindex
from system.models.seometadata import SeoMetadata, SeoMetadataTranslation
from system.models.siteredirect import SiteRedirect

class SystemRedirectMiddleware(object):
    """
    Implementation of 301 redirects
    """
    def process_request(self, request):
        from_url = request.get_full_path()
        try:
            redirect_object = SiteRedirect.objects.get(from_url=from_url)
        except SiteRedirect.DoesNotExist:
            redirect_object = None

        site = Site.objects.get_current()
        if redirect_object:
            new_uri = '%s://%s%s' % (
                request.is_secure() and 'https' or 'http',
                site.domain,
                urlquote(redirect_object.to_url)
            )

            return HttpResponsePermanentRedirect(new_uri)


class SystemNoindexMiddleware(object):
    """
    Implementation of no index
    """
    def process_response(self, request, response):
        from_url = request.get_full_path()
        try:
            redirect_object = Noindex.objects.get(noindex_url=from_url)
        except Noindex.DoesNotExist:
            redirect_object = None
        if redirect_object:
            response['X-Robots-Tag'] = 'noindex'

        return response


class SystemSEOMiddleware(object):
    """
    Implementation SEO Meta
    """
    def process_template_response(self, request, response):
        from_url = request.get_full_path()
        SEO_CONTENT_DICT = {}

        lang = 'en'
        if "set_lang" in request.session:
            lang = request.session["set_lang"]

        try:
            seo_mata = SeoMetadata.objects.get(url=from_url)
        except SeoMetadata.DoesNotExist:
            seo_mata = None

        if seo_mata:
            try:
                translate = SeoMetadataTranslation.objects.get(model=seo_mata,
                    language=lang)
            except SeoMetadataTranslation.DoesNotExist:
                translate = None

            if translate:
                SEO_CONTENT_DICT['title'], SEO_CONTENT_DICT['keywords'], SEO_CONTENT_DICT['description'] = translate.title, translate.keywords, translate.description

        response.context_data['seo_meta_data'] = SEO_CONTENT_DICT
        return response

