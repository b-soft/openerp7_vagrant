from django.contrib import admin
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from system.functions import search_and_replace
from system.models.log import Log

class LogAdmin(admin.ModelAdmin):
    change_list_template = "admin/log/change_list.html"
    change_form_template = "admin/log/change_form.html"

    def get_changelist(self, request, **kwargs):
        request.session["customer_user_id"] = request.GET.get("user__id__exact", request.session.get("customer_user_id", False))
        return super(LogAdmin, self).get_changelist(request, **kwargs)

    def get_extra_context(self, request):
        extra_context = {}
        if request.session.get("customer_user_id", False):
            customer = User.objects.get(pk=request.session["customer_user_id"])
            if customer.first_name or customer.last_name:
                extra_context['customer'] = "%s %s" % (customer.first_name, customer.last_name)
            else:
                extra_context['customer'] = "%s" % customer.username
            extra_context['customer_id'] = customer.id
        return extra_context

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context.update(self.get_extra_context(request))
        return super(LogAdmin, self).changelist_view(request, extra_context=extra_context)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context.update(self.get_extra_context(request))
        return super(LogAdmin, self).change_view(request=request, object_id=object_id, form_url=form_url, extra_context=extra_context)

    def add_view(self, request, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context.update(self.get_extra_context(request))
        return super(LogAdmin, self).add_view(request=request, form_url=form_url, extra_context=extra_context)

    def save_model(self, request, obj, form, change):
        if not change:
            user_id = request.session.get("customer_user_id", False)
            if user_id:
                obj.user = User.objects.get(id=user_id)
        return super(LogAdmin, self).save_model(request, obj, form, change)

    def notes(self, obj):
        return search_and_replace(obj.description, obj.log_vars)
    notes.short_description = 'Description'
    notes.allow_tags = True

    def customer_name(self, obj):
        return "%s %s <br><div style=\"font-size:10px;color:#999999;\">(%s)</div>" % (obj.user.first_name, obj.user.last_name, obj.created_at.strftime("%d-%m-%Y %H:%M:%S"))
    customer_name.short_description = 'Customer'
    customer_name.allow_tags = True

    list_display = ["customer_name", "notes", "system_log_type"]
    list_per_page = 20

    def has_add_permission(self, request):
        return False

    def get_model_perms(self, request): #hide from admin list
        return {}

admin.site.register(Log, LogAdmin)