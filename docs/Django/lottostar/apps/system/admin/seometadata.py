from django.core.urlresolvers import reverse
from django.db import models
from django.contrib import admin
from django.forms import forms
from django.http import HttpResponseRedirect
from django.utils.text import truncate_words
from reversion import VersionAdmin
from flutter_modeladmin import RemoveDeleteAdmin
from system.models.seometadata import SeoMetadata, SeoMetadataTranslation
from django.conf import settings

def seo_controls(obj):
    html = '<div style="width:340px;height:25px; line-height: 25px">'
    for lang in settings.LANGUAGES:
        html += '<a style="margin:5px" href="/admin/translate/seometadata/%s/?language=%s" title="%s"><img src="/static/img/flags/icons/%s.png"/></a>' % (obj.id, lang[0], lang[1].title(), lang[0])
    html += "</div>"
    return html
seo_controls.short_description = 'Controls'
seo_controls.allow_tags = True

class SEOTextTranslationAdmin(RemoveDeleteAdmin, VersionAdmin):
    model = SeoMetadataTranslation
    list_display = ["model", "language"]
    readonly_fields = ["model", "language"]
    fieldsets = (
        (None, {
            'fields': ('model', "language")
        }),
        ("Details", {
            'fields': ('title','description', 'keywords')
        }),
    )

    def get_model_perms(self, request):
        return {}

    def changelist_view(self, request, extra_context=None):
        return HttpResponseRedirect(reverse("admin:system_seometadata_changelist"))

admin.site.register(SeoMetadataTranslation, SEOTextTranslationAdmin)


def get_title(obj):
    return truncate_words(obj.get_translation().title, 10)
get_title.short_description = "English Title"

class SEOTextTranslationInline(admin.StackedInline):
    model = SeoMetadataTranslation
    extra = 1
    max_num = 1

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'language':
            kwargs['initial'] = "en"
        return super(SEOTextTranslationInline, self).formfield_for_dbfield(db_field, **kwargs)



class SEOMetaDataAdmin(RemoveDeleteAdmin, VersionAdmin):
    list_display = (get_title, 'url',seo_controls, )
    inlines = [SEOTextTranslationInline,]

    formfield_overrides = {
        models.TextField: {'widget': forms.TextInput(attrs={"style":"width: 50%"})},
    }

    def get_inline_instances(self, request, obj=None):
        inline_instances = super(SEOMetaDataAdmin, self).get_inline_instances(request, obj)
        if obj:
            return []
        return inline_instances

admin.site.register(SeoMetadata, SEOMetaDataAdmin)

