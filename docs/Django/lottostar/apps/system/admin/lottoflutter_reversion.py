from django.utils.translation import ugettext_lazy as _
from django.contrib import admin

from reversion.models import Revision, Version

from flutter_modeladmin import RemoveDeleteAdmin


class LottoFlutterRevision(Revision):
    class Meta:
        proxy = True
        app_label = 'system'
        verbose_name = _('Revision')
        verbose_name_plural = _('Revisions')

class LottoFlutterRevisionAdmin(RemoveDeleteAdmin):
    list_display = ("user", "date_created", "comment")



    def has_add_permission(self, request):
        return False

admin.site.register(LottoFlutterRevision, LottoFlutterRevisionAdmin)

class LottoFlutterVersion(Version):
    class Meta:
        proxy = True
        app_label = 'system'
        verbose_name = _('Version')
        verbose_name_plural = _('Versions')

class LottoFlutterVersionAdmin(RemoveDeleteAdmin):
    list_display = ("revision", "object", "changes")

    def changes(self, obj):
        return obj.revision.comment
    changes.allow_tags = True
    changes.short_description = 'Model Changes'

    def has_add_permission(self, request):
        return False
admin.site.register(LottoFlutterVersion, LottoFlutterVersionAdmin)
