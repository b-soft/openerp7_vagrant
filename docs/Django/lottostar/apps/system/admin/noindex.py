from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from system.models.noindex import Noindex

class NoindexAdmin(admin.ModelAdmin):
    list_display = ["noindex_url"]
    list_per_page = 20
admin.site.register(Noindex, NoindexAdmin)