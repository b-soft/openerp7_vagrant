from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from system.models.siteredirect import SiteRedirect

class SiteRedirectAdmin(admin.ModelAdmin):
    list_display = ["from_url", "to_url"]
    list_per_page = 20
admin.site.register(SiteRedirect, SiteRedirectAdmin)