from django.db import models
from django.utils.translation import ugettext_lazy as _

class LogType(models.Model):
    name =  models.CharField(max_length=255, blank=False, null=False)
    slug = models.SlugField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "System Log Type"
        verbose_name_plural = "System Log Types"
        app_label = "system"

