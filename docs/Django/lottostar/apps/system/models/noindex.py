from django.db import models
from django.utils.translation import ugettext_lazy as _

class Noindex(models.Model):
    noindex_url =  models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = "Site Noindex"
        verbose_name_plural = "Site Noindex"
        app_label = "system"