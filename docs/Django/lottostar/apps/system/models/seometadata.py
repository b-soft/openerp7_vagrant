from django.contrib.sites.models import Site
from django.utils.translation import ugettext_lazy as _
from translatable.models import TranslatableModel, get_translation_model
from django.db import models

class SeoMetadata(TranslatableModel):
    site = models.ManyToManyField(Site, blank=True, null=True)
    url = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = _("Seo Meta Data")
        verbose_name_plural = _("Seo Meta Data")
        app_label = "system"

    def __unicode__(self):
        return self.get_translation(language="en").title

class SeoMetadataTranslation(get_translation_model(SeoMetadata, "seometadata")):
    title       = models.CharField(max_length=255, blank=True, null=True)
    keywords    = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = _("Seo Meta Data")
        verbose_name_plural = _("Seo Meta Data")
        app_label = "system"

    def __unicode__(self):
        return self.title
