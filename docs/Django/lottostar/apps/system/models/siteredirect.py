from django.db import models
from django.utils.translation import ugettext_lazy as _

class SiteRedirect(models.Model):
    from_url =  models.CharField(max_length=255, blank=True, null=True)
    to_url =  models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = "Site Redirect"
        verbose_name_plural = "Site Redirects"
        app_label = "system"