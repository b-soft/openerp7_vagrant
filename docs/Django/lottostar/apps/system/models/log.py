from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from djorm_pgarray.fields import ArrayField
from system.models import LogType


class Log(models.Model):
    user = models.ForeignKey(User)
    system_log_type = models.ForeignKey(LogType)
    description =  models.TextField(blank=True, null=True)
    log_vars =  ArrayField(dbtype="varchar(255)", null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    class Meta:
        verbose_name = "System Log"
        verbose_name_plural = "System Logs"
        app_label = "system"