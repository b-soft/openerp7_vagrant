from datetime import datetime
# from django.db.models import F
from django.db.models import Max

from promotions.models import Offer
# from apps.account.models import UserWallet
# from play.models.transaction import Ticket, Entry
# from game.models import Product, ProductGame

def get_offer_helper(request, engage):
    """
    helper function to get the offer to display on the modal, engage controls how the offer will be applied.
    """
    current_offer = None
    customer = request.user
    if customer.is_authenticated():
        offers_list = []
        # Retention offers are dynamically offered to customers on login.
        offers = Offer.get_available_offer(customer, engage)
        if offers:
            for offer in offers:
                #TODO: Olwethu: 22/08/2013: check what last played is used for
                #if offer.get_last_transaction_by_customer(customer) and offer.get_last_played_game(customer) and offer.check_params(customer):
                if offer.get_last_transaction_by_customer(customer) and offer.check_params(customer):
                    offers_list.append(offer.id)
            try:
                current_offer = offers.filter(id__in=offers_list)[0]
            except IndexError:
                pass
        #store offer on session
        request.session['offer'] = current_offer
    return current_offer

def is_offer_valid(offer, customer):
    """
    checks if the offer stored on the session is still valid.
    """
    now = datetime.now()
    if offer.status == Offer.STATUS.active and (offer.start <= now and offer.end >= now) and \
        offer.get_last_transaction_by_customer(customer) and offer.check_params(customer):
        try:
            #check the first offer is always the one with the highest priority.
            max_priority = Offer.objects.all().aggregate(Max('priority'))['priority__max']
            if max_priority == offer.priority:
                return True
        except KeyError:
            pass
    return False

def clean_checkout_session(request):
    #delete transaction, deposit on session
    try:
        del request.session['transaction']
    except KeyError:
        pass
    try:
        del request.session['discounted_total_cost']
    except KeyError:
        pass
    try:
        del request.session['deposit']
    except KeyError:
        pass
    try:
        del request.session['discount']
    except KeyError:
        pass
