from django import forms

from model_utils import Choices

from vendor.countries.models import Country
from promotions.models import Offer, OfferCustomer
from game.models import Product, Game
import settings


BONUS_CHOICE = CHOICES = Choices(
    (0.25, 'quarter', '25%'),
    (0.5, 'half', '50%'),
    (0.75, 'three_quarter', '75%'),
    (1, 'full', '100'),
)


class OfferAdminForm(forms.ModelForm):
    CHOICES = Choices(
        (0, 'anytime', 'Anytime'),
        (7, 'one_week', '1 Week'),
        (21, 'three_weeks', '3 Weeks'),
        (42, 'six_weeks', '6 Weeks'),
        (90, 'three_months', '3 Months'),
        (180, 'six_months', '6 Months'),
        (365, 'one_year', '1 Year')
    )


    registration_period = forms.ChoiceField(
        label='Customer must have been registered for', choices=CHOICES, required=True)
    inactive_period = forms.ChoiceField(
        label='Customer must have been inactive for', choices=CHOICES, required=True)
    countries = forms.ModelMultipleChoiceField(
        label='Countries:', queryset=Country.objects.all(), required=False,
        help_text='Leave empty if not applicable or offer applies to all countries.')
    #TODO: Olwethu: 21/08/2013: remove languages from parameters.
    # languages = forms.ChoiceField(
    #     label='Language', choices=settings.LANGUAGES,
    #     help_text='Leave empty if not applicable or offer applies to all languages.')
    gender = forms.ChoiceField(choices=(('any_gender', 'Any Gender'), ('m', 'Male'), ('f', 'Female')), required=True)

    def __init__(self, *args, **kwargs):
        self.base_fields['type'].required = True
        super(OfferAdminForm, self).__init__(*args, **kwargs)
        if 'instance' in kwargs:
            reload_fields = ['registration_period', 'inactive_period', 'countries', 'languages', 'gender', 'type']
            for field in reload_fields: #populate declared fields with previously entered values
                if self.instance.params.has_key(field) and self.fields.has_key(field):
                    if field == 'countries':
                        self.fields[field].initial = [country_code for country_code in self.instance.params[field]]
                    else:
                        self.fields[field].initial = self.instance.params[field]

    class Meta:
        model = Offer
        fields = [
            'status', 'priority', 'name', 'description', 'engage', 'period', 'frequency',
            'start', 'end', 'type', 'product'
        ]


class LottoPointsIncludeForm(forms.Form):
    """
    inclusion form for LottoPoints Bonus.
    """
    CHOICE = (
        ('anytime', 'Anytime'),
        ('one_week', '1 Week'),
        ('three_weeks', '3 Weeks'),
        ('six_weeks', '6 Weeks'),
        ('three_months', '3 Months'),
    )

    min_tickets = forms.CharField(label='Minimum tickets', initial=1)
    for_product = forms.ModelChoiceField(label='In', queryset=Product.objects.all(), empty_label='Any Lotto')
    last_played = forms.ChoiceField(label='Last played', choices=CHOICE)
    bonus = forms.ChoiceField(label='Bonus', choices=BONUS_CHOICE)


class DepositBonusInclusionForm(forms.Form):
    """
    inclusion form for Deposit Bonus
    """
    threshold = forms.DecimalField(label='Threshold', decimal_places=2)
    bonus = forms.ChoiceField(label='Bonus', choices=BONUS_CHOICE)


class VoucherBonusInclusionForm(forms.Form):
    """
    inclusion form for Voucher Bonus
    """
    buy_entries = forms.CharField(label='Buy entries', required=True, initial=1)
    in_lottery = forms.ModelChoiceField(
        label='in lottery', queryset=Game.objects.all(), empty_label='Please select')
    earn_vouchers = forms.CharField(label='Earn vouchers', required=True, initial=1)
    for_product = forms.ModelChoiceField(label='for product', queryset=Product.objects.all(), empty_label='Please select')


class DiscountInclusionForm(forms.Form):
    """
    inclusion form for Discount
    """
    DISCOUNT = BONUS_CHOICE
    threshold = forms.DecimalField(label='Threshold', decimal_places=2)
    for_product = forms.ModelChoiceField(label='for product', queryset=Product.objects.all(), empty_label='Please select')
    discount = forms.ChoiceField(label='Discount', choices=DISCOUNT)


class WinningsBonusInclusionForm(forms.Form):
    """
    inclusion form for Winnings Bonus
    """
    TIERS = (
        ('all_tiers', 'All Tiers'),
        ('jackpot', 'Jackpot'),
        ('top_2_Tiers', 'Top 2 Tiers'),
        ('all_exclude_jackpot', 'All tiers except Jackpot'),
        ('all_exclude_top_2', 'All tiers except top 2'),
    )

    tiers = forms.ChoiceField(label='Tiers', choices=TIERS, initial='')
    bonus = forms.ChoiceField(label='Bonus', choices=BONUS_CHOICE)
    max_bonus = forms.CharField(label='Maximum bonus', initial=1000)


class PurchaseRebateInclusionForm(forms.Form):
    """
    inclusion form for Purchase Rebate
    """
    REBATE = BONUS_CHOICE
    for_product = forms.ModelChoiceField(label='for product', queryset=Product.objects.all(), empty_label='Please select')
    rebate = forms.ChoiceField(label='Rebate', choices=REBATE)


class AcceptOfferForm(forms.ModelForm):
    class Meta:
        model = OfferCustomer
