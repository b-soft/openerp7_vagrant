from django.db.models.signals import post_save
from django.dispatch import receiver

from promotions.models import Offer
from promotions.crypto import create_offer_token

@receiver(post_save, sender=Offer)
def save_offer_token(sender, instance, created, **kwargs):
    """
    fire signal as soon as the offer is created to generate a unique token to associate with this offer.
    """
    if created:
        token = create_offer_token(instance)
        instance.token = token
        instance.save()