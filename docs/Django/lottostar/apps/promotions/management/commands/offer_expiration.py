from datetime import datetime, timedelta
from django.core.management.base import NoArgsCommand
from django.db.models import Q

from promotions.models import OfferCustomer, Offer


class Command(NoArgsCommand):
    """
    Command for setting offers as expired.
    """
    help = 'Set passed offers to expired'

    verbosity = 1

    def handle_noargs(self, **options):
        """
        Check what offers have end dates that have passed and mark them as closed, set all customer offers as expired
        for these offers.
        """
        self.verbosity = int(options.get('verbosity', 1))

        now = datetime.now()
        closed_offers = Offer.objects.filter(end__lt=now).update(status=Offer.STATUS.closed)
        OfferCustomer.objects.filter(offer__status=Offer.STATUS.closed).update(status=OfferCustomer.STATUS.expired)
        self.write('Number of offers than have been closed: {0!r}\n'.format(closed_offers))

        #if the period the offer should apply to user has passed marked customer offer as expired.
        offer_customers = OfferCustomer.objects.filter(status=OfferCustomer.STATUS.active)
        if offer_customers:
            for offer_customer in offer_customers:
                if offer_customer.created <= (now - timedelta(days=offer_customer.offer.period)):
                    offer_customer.status = OfferCustomer.STATUS.expired
                    offer_customer.save()

    def write(self, s):
        if 1 <= self.verbosity:
            self.stdout.write(s)
