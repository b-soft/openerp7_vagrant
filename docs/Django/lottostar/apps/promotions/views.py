import json
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
from django.http import Http404

from promotions.forms import DepositBonusInclusionForm, VoucherBonusInclusionForm, DiscountInclusionForm
from promotions.models import Offer, OfferCustomer


def append_form_fields(request):
    data = {}
    include_form = {
        # 1: LottoPointsIncludeForm,
        2: DepositBonusInclusionForm,
        3: VoucherBonusInclusionForm,
        4: DiscountInclusionForm,
        # 5: WinningsBonusInclusionForm,
        # 6: PurchaseRebateInclusionForm
    }
    selected_reward_type = request.GET.get('item', None)
    offer_id = request.GET.get('offer_id', None)
    initial_data = {}
    if selected_reward_type:
        form = include_form[int(selected_reward_type)]
        if offer_id:
            try:
                offer = Offer.objects.get(id=int(offer_id))
                for key in form.base_fields.keys():
                    if offer.params.has_key(key):
                        initial_data.update({key: offer.params[key]})
                #display previously entered reward type attributes
                form = form(initial_data)
            except Offer.DoesNotExist:
                pass
        ctx = {'form': form}
        data = render_to_string('admin/promotions/offer/includes/_include_form.html', ctx)
        return HttpResponse(data)
    return HttpResponse(json.dumps(data), "application/json")

@login_required
def reject_initial_offer(request):
    """
    user has rejected the initial promotions offer.
    """
    (status, offer) = create_customer_offer_helper(request, OfferCustomer.STATUS.expired)
    try:
        del request.session['offer']
        request.session['offer'] = offer
    except KeyError:
        request.session['offer'] = offer
    return HttpResponse(status=status)

@login_required
def accept_offer(request):
    """
    user has accepted the initial promotions offer.
    """
    (status, offer) = create_customer_offer_helper(request, OfferCustomer.STATUS.active)
    try:
        del request.session['offer']
        request.session['offer'] = offer
    except KeyError:
        request.session['offer'] = offer
    return HttpResponse(status=status)

def create_customer_offer_helper(request, status):
    """
    helper function to create a customer offer with the provided status.
    """
    http_status = 204
    if request.method == 'POST':
        initial_offer_id = request.POST.get('offer_id', None)
        initial_offer = request.session.get('offer', None)
        if initial_offer_id:
            try:
                offer = Offer.objects.get(id=int(initial_offer_id))
                #validate that the offer submitted is the initial offer presented to the user.
                if initial_offer and initial_offer.id == offer.id:
                    offer_customer, created = OfferCustomer.objects.get_or_create(offer=offer, customer=request.user)
                    offer_customer.status = status
                    offer_customer.save()
                    http_status = 201
            except Offer.DoesNotExist:
                pass
    return (http_status, offer_customer.offer)

def get_active_offer(request):
    """
    if the user has accepted an offer on this session return it.
    """
    customer_offers = OfferCustomer.objects.filter(
        customer=request.user, status=OfferCustomer.STATUS.active).order_by('offer__priority')
    try:
        customer_offer = customer_offers[0]
        ctx = {'offer_id': customer_offer.offer.id}
        return HttpResponse(json.dumps(ctx), "application/json")
    except IndexError:
        pass
    return HttpResponse(json.dumps(None), "application/json")


def validate_offer_token(url):
    """
    check the token in the url belongs to an offer. This will be used to validate offer tokens sent on email as links.
    """
    is_valid = False
    token = url #TODO: Olwethu: 21:08/2013: extract token from the url
    try:
        offer = Offer.objects.get(token=token)
        is_valid = True
    except (Offer.DoesNotExist, Offer.MultipleObjectsReturned):
        raise Http404
    return is_valid

