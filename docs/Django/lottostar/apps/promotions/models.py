from datetime import datetime, timedelta
from django.db import models
from django.contrib.auth.models import User

from translatable.models import TranslatableModel, get_translation_model
from model_utils.models import TimeFramedModel, TimeStampedModel
from model_utils import Choices
from json_field import JSONField

from trans.models import Text
from play.models.transaction import Transaction
from vendor.countries.models import Country


class Offer(TranslatableModel, TimeFramedModel, TimeStampedModel):
    STATUS = Choices(
        (1,'pending', 'Pending'),
        (2, 'active', 'Active'),
        (3, 'closed', 'Closed'),
    )

    ENGAGE = Choices(
        (1, 'retention', 'Retention'),
        (2, 'conversion', 'Conversion'),
        (3, 'custom', 'Custom'),
    )

    TYPE = Choices(
        # (1, 'lottopoints_bonus', 'LottoPoints Bonus'),
        (2, 'deposit_bonus', 'Deposit Bonus'),
        (3, 'voucher_bonus', 'Voucher Bonus'),
        (4, 'discount', 'Discount'),
        # (5, 'winnings_bonus', 'Winnings Bonus'),
        # (6, 'purchase_rebate', 'Purchase Rebate'),
    )

    PRIORITY = Choices((1), (2), (3))

    PERIOD = Choices(
        (1, 'one_transaction', '1 Transaction'),
        (2, 'one_day', '1 Day'),
        (3, 'one_week', '1 Week'),
        (4, 'three_weeks', '3 Weeks'),
        (5, 'six_weeks', '6 Weeks'),
    )

    FREQUENCY = Choices(
        (1, 'daily', 'Every Day'),
        (2, 'weekly', 'Every Week'),
        (3, 'three_weeks', 'Every 3 Weeks'),
        (4, 'twelve_weeks', 'Every 12 Weeks'),
        (5, 'constantly', 'Constantly'),
    )

    admin = models.ForeignKey(User, blank=True, null=True)
    name = models.CharField(help_text='Internal name ie not customer facing.', max_length=255)
    description = models.TextField(help_text='Internal description ie not customer facing.')
    params = JSONField()

    status = models.IntegerField(choices=STATUS, default=STATUS.pending, db_index=True)
    engage = models.IntegerField(
        help_text='The engage option controls how the offer will be applied: Retention offers are dynamically offered\
         to customers on login. Conversion offers are applied to specific users from the backend. Custom offers are \
         applied via specifically developed processes.',
        choices=ENGAGE, default=ENGAGE.retention, db_index=True)
    type = models.IntegerField('Reward Type', choices=TYPE, blank=True, null=True, db_index=True)
    priority = models.IntegerField(choices=PRIORITY, default=1, db_index=True)
    period = models.IntegerField(
        help_text='The period is the length of time the offer will continue to apply to the customer.',
        choices=PERIOD, default=PERIOD.one_transaction, db_index=True)
    frequency = models.IntegerField(help_text='The frequency controls how often the customer can receive the offer.',
                                    choices=FREQUENCY, default=FREQUENCY.daily, db_index=True)
    offered = models.IntegerField(default=0)
    token = models.CharField(max_length=128)
    #when this is null the offer applies to all products
    product = models.ForeignKey('game.Product', null=True, blank=True, db_index=True,
                                help_text="leave field blank to apply offer to all products.")

    def __unicode__(self):
        return self.name

    @classmethod
    def get_available_offer(cls, customer, engage=None):
        """
        get available promotion offers for a product and sort them by priority if product id is given, else get all the
        active offers the user qualifies for.
        """
        now = datetime.now()
        filters = {
            'status': Offer.STATUS.active,
            'start__lte': now,
            'end__gte': now,
        }
        #exclude offers the user has accepted or rejected.
        excludes = {
            'id__in': OfferCustomer.objects.filter(customer=customer).values_list('offer', flat=True)
        }
        offers = cls.objects.filter(**filters).exclude(**excludes).order_by('-priority')
        # The engage option controls how the offer will be applied.
        if engage:
            offers = offers.filter(engage=engage).order_by('-priority')
        return offers

    def check_params(self, customer):
        params = {
            'gender': self.check_gender(customer),
            'registration_period': self.check_profile_age(customer),
            'countries': self.check_locale(customer),
            #'language': self.check_language(customer),
        }
        try:
            if set(params.values()).pop() == True:
                return True
            return False
        except KeyError:
            return False

    def check_language(self, customer):
        """
        verify that the user's preferred language is valid to get the offer.
        """
        language = self.params.get('languages', None)
        if customer.profile.preferred_language == language:
            return True
        return False

    def check_gender(self, customer):
        """
        check if the customer's gender is an allowed parameter.
        """
        genders = {'m': 'Male', 'f': 'Female'}
        allowed_gender = self.params.get('gender')
        if allowed_gender == 'any_gender' or customer.profile.get_gender == None:
            return True
        gender = customer.profile.get_gender
        if gender == genders[allowed_gender]:
            return True
        return False

    def check_profile_age(self, customer):
        """
        check if the user's profile has been active(registered) for the duration on the offer.
        """
        registration_period = self.params.get('registration_period', None)
        if registration_period:
            registration_period = int(registration_period)
            if registration_period == 0:
                return True
            registration_date = datetime.now() - timedelta(days=registration_period)
            return customer.profile.created_at > registration_date
        return False

    def check_locale(self, customer):
        """
        check if the user's country is a valid option on the offer allowed locales.
        """
        #TODO: Olwethu: 22/08/2013: What should happen when the user has not filled in a country.
        if not customer.profile.country:
            return True
        countries = self.params.get('countries', None)
        if countries:
            if countries == '':
                return True
            countries = Country.objects.filter(iso__in=countries)
            return countries.filter(translations__name=customer.profile.country).exists()
        #check if this is a empty list of countries
        elif isinstance(countries, list) and not countries:
            return True
        return False

    def check_ratio(self, customer):
        bonus = self.params['bonus']
        if bonus == 'R':
            pass
            ratio = customer.get_retention_ratio()
            if ratio[bonus] <= 0:
                return True
        return False

    def get_last_transaction_by_customer(self, customer):
        """
        check the last transaction date by the customer is more than the required number of inactive days to receive an
        offer.
        """
        inactive_days = self.params.get('inactive_period', 0)
        inactive_days = int(inactive_days)
        if inactive_days == 0:
            return True
        try:
            #XXX: Olwethu: 19/08/2013:change "payment_status --> completed"
            last_transaction_date = Transaction.objects.filter(user=customer, payment_status='pending').order_by('created_at').values_list('created_at', flat=True)[0]
            last_active_date = datetime.now() - timedelta(days=inactive_days)
            return last_transaction_date > last_active_date
        except IndexError:
            return False

    # def get_last_played_game(self, customer):
    #     """
    #     check the last game played by the customer.
    #     """
    #     last_played_days = self.params.get('played', 20)
    #     if not last_played_days:
    #         return True
    #     try:
    #         #XXX: Olwethu: 19/08/2013:change "payment_status --> completed"
    #         last_transaction_date = Transaction.objects.filter(user=customer, payment_status='pending').order_by('created_at').values_list('created_at', flat=True)[0]
    #         last_played_date = datetime.now() - timedelta(days=last_played_days)
    #         return last_transaction_date > last_played_date
    #     except IndexError:
    #         pass

    @classmethod
    def check_offer_frequency(cls, customer):
        """
        get the last time the user accepted this offer and check if user can get it again by comparing the this date
        with the frequency(number of days that must pass for a user to be offer the same offer) at which the offer is
        to available to a user.

        last_time_received: last time the user received the offer.
        """
        can_get_offer = False
        filters = {
            'id__in': OfferCustomer.objects.filter(customer=customer).values_list('customer', flat=True)
        }
        try:
            last_accepted_offer = cls.objects.filter(**filters).order_by('created')[0]
            last_time_received = datetime.now() - timedelta(days=last_accepted_offer.frequency)
            can_get_offer = last_accepted_offer.created > last_time_received
        except IndexError:
            pass
        return can_get_offer


class OfferTranslation(get_translation_model(Offer, "offer"), TimeStampedModel):
    title = models.CharField(
        help_text='Displayed on lightbox and/or in promotion boxes.', max_length=255, blank=False, null=False)
    content = models.TextField(
        'Explanation', help_text='Displayed in lightbox and/or as informational description in emails.')


class OfferCustomer(TimeStampedModel):
    STATUS = Choices(
        (1, 'active', 'Active'),
        (2, 'expired', 'Expired'),
    )
    customer = models.ForeignKey(User) #should be user profile
    offer = models.ForeignKey('promotions.Offer')
    status = models.IntegerField(choices=STATUS, default=STATUS.active, db_index=True)
    product = models.ForeignKey('game.Product', blank=True, null=True)
    transaction = models.ForeignKey(Transaction, blank=True, null=True)

    def __unicode__(self):
        return self.customer.email

import promotions.signals as _promotion_signals
