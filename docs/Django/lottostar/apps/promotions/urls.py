from django.conf.urls import patterns, url

urlpatterns = patterns('apps.promotions.views',
    url(r'^append/offer/form/$', 'append_form_fields', name='append-form-fields'),
    url(r'^reject/initial/offer/$', 'reject_initial_offer', name='reject-initial-offer'),
    url(r'^accept/initial/offer/$', 'accept_offer', name='accept-offer'),
    url(r'^get/active/offer/$', 'get_active_offer', name='get-active-offer'),
)
