from hashlib import md5

HashKey ='1988A69BB01D0904A9AF9C909EB718BA'

def create_offer_token(offer):
    """
    Creates a token for an offer by using the attributes of the offer.
    """
    data_string = "{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}".format(
        offer.id,
        offer.name,
        offer.description,
        offer.engage,
        offer.period,
        offer.priority,
        offer.frequency,
        offer.params
    )
    #ASCii encode the hash string
    hash_string = data_string.encode("ascii")

    # md5 instance
    encoder = md5()
    encoder.update("{0}|{1}".format((HashKey).encode("ascii"), hash_string))
    HashValue = encoder.hexdigest()
    return HashValue
