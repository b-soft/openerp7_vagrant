import json
from django import template
from django.db.models import Q
from datetime import datetime

from fancy_tag import fancy_tag

from promotions.forms import *
from promotions.models import Offer, OfferCustomer
from promotions.functions import get_offer_helper, is_offer_valid
from django.template import RequestContext
#from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS

register = template.Library()


@fancy_tag(register, takes_context=True)
def get_initial_offer(context):
    """
    get the initial offer to display to the user, this is the offer with the highest priority.
    """
    request = context['request'] #request = RequestContext(request, context['request']) #request = context['request']
    #request = context.get('request')
    #if request is None:
    #    return ''
    if request.user.is_authenticated():
        customer = request.user
        initial_offer = request.session.get('offer', None)

        #if user accepted an offer and it is still active do not load any new offers.
        try:
            offer_customer = OfferCustomer.objects.filter(customer=customer, status=OfferCustomer.STATUS.active).latest('created')
            if is_offer_valid(offer_customer.offer, customer):
                request.session['offer'] = offer_customer.offer
                return None
            else:
                offer_customer.status = OfferCustomer.STATUS.expired
                try:
                    del request.session['offer']
                except KeyError:
                    pass
        except OfferCustomer.DoesNotExist:
            pass

        try:
            initial_offer = Offer.objects.get(id=initial_offer.id)
            #if user was presented an offer but closed browser/logged off before accepting it, display offer again.
            if is_offer_valid(initial_offer, customer):
                #check if the user has accepted or reject the initial offer
                if OfferCustomer.objects.filter(customer=customer, offer=initial_offer).exists():
                    return None #if true do not load initial offer modal
                return initial_offer
            else:
                #if the offer stored on the session has been changed and user does not qualify remove it from session.
                del request.session['offer']
        except (Offer.DoesNotExist, AttributeError): #catch when the initial offer is None
            try:
                del request.session['offer']
            except KeyError:
                pass
    offer = get_offer_helper(request, Offer.ENGAGE.retention)
    return offer

@register.inclusion_tag('promotions/_product_available_offer.html', takes_context=True)
def get_products_promotion_offers(context, product_list):
    """
    inclusion tag for getting all active offers for a product the user can get.
    """
    customer = context['request'].user
    ctx = {}
    if customer.is_authenticated():
        #check what offers the user qualifies for and present the one with the highest priority.
        offers_list = []
        try:
            latest_taken_offer = OfferCustomer.objects.filter(
                customer=customer, status=OfferCustomer.STATUS.active).order_by('offer__priority')[0]
            try:
                ctx['taken_offer'] = Offer.objects.get(id=latest_taken_offer.offer.id)
            except Offer.DoesNotExist:
                pass
        except IndexError:
            pass
        available_offers = Offer.get_available_offer(customer)
        #get all offers for the products on the checkout page and those that apply to all products
        available_offers = available_offers.filter(Q(product__in=product_list)|Q(product__isnull=True))
        if available_offers:
            for offer in available_offers:
                if offer.get_last_transaction_by_customer(customer) and offer.check_params(customer):
                    offers_list.append(offer.id)
        ctx['offers'] = available_offers.filter(id__in=offers_list).order_by('priority')
        return ctx
    return None
