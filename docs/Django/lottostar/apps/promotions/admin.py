from django.contrib import admin
from django.db.models import F

from reversion import VersionAdmin

from promotions.models import Offer, OfferCustomer, OfferTranslation
from promotions.forms import OfferAdminForm
from flutter_modeladmin import RemoveDeleteAdmin


try:
    # Globally disable delete selected
    admin.site.disable_action('delete_selected')
except KeyError:
    pass

class OfferTranslationInline(admin.StackedInline):
    model = OfferTranslation
    max_num = 1
    extra = 1


class OfferAdmin(RemoveDeleteAdmin, VersionAdmin):
    form = OfferAdminForm
    list_display = ["name", "description", "priority", "status"]
    list_filter = ["priority", "status"]
    inlines = [OfferTranslationInline,]
    fieldsets = (
        ('Offer', {
            'fields': ('status', 'priority', 'name', 'description', 'product')
        }),
        ('Setup', {
            'fields': ('engage', 'period', 'frequency', 'start', 'end')
        }),
        ('Params', {
            'classes': ('params',),
            'fields': ('registration_period', 'inactive_period', 'countries', 'gender', 'type')
        }),
    )

    def save_model(self, request, obj, form, change):
        params_json = {}
        params_list = [
            'registration_period', 'inactive_period', 'countries', 'languages', 'gender', 'type',
            'min_tickets', 'in_lottery', 'bonus', 'threshold', 'bonus', 'buy_entries',
            'for_product', 'earn_vouchers', 'discount',
        ]
        for param in params_list:
            if request.POST.has_key(param) and param != 'countries':
                params_json.update({param: request.POST[param]})
            elif form.cleaned_data.has_key(param):
                if param == 'countries':
                    params_json.update({param: request.POST.getlist(param)})
                else:
                    params_json.update({param: form.cleaned_data[param]})
        obj.params = params_json
        priority = form.cleaned_data['priority']
        #check if an offer with the same priority exist if true increment its priority and those that have a higher
        # priority
        if Offer.objects.filter(priority=priority).exists():
            Offer.objects.filter(priority__gte=priority).update(priority=F('priority') + 1)
        #TODO: Olwethu: 06/09/2013: save the admin user that created the offer on the offer table admin field
        obj.save()

class OfferCustomerAdmin(RemoveDeleteAdmin, VersionAdmin):
    pass

admin.site.register(Offer, OfferAdmin)
admin.site.register(OfferCustomer, OfferCustomerAdmin)
