from django.conf.urls.defaults import *
from apps.faq import views

urlpatterns = patterns('faq.views',
    url(r'^admin/translate/faq/(?P<faq_id>[a-zA-Z0-9_.-]+)/$', "admin_faq_text", name="admin_faq_text"),
    url(r'^admin/translate/faqcategory/(?P<category_id>[a-zA-Z0-9_.-]+)/$', "admin_faqcategory_text", name="admin_faqcategory_text"),

    url('^$', views.FAQView.as_view(), name='faq'),
    url('^question/(?P<slug>[a-zA-Z0-9_.-]+)/$', views.FAQQuestionView.as_view(), name='faq_question'),
)
