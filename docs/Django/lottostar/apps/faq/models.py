from django.db import models
from translatable.models import TranslatableModel, get_translation_model


class FAQCategory(TranslatableModel):
    order = models.IntegerField(max_length=3, default=0)
    date_created = models.DateTimeField(auto_now=True, editable=False)
    date_modified = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        ordering = ["order"]
        verbose_name = "FAQ Category"
        verbose_name_plural = "FAQ Categories"

    def __unicode__(self):
        try:
            return self.get_translation(language="en").title
        except:
            return ""


class FAQCategoryTranslation(get_translation_model(FAQCategory, "faq_category")):
    title = models.CharField(max_length=255)

    def __unicode__(self):
        try:
            return self.model.get_translation(language="en").title
        except:
            return ""

    class Meta:
        verbose_name = "FAQ Category"
        verbose_name_plural = "FAQ Categories"



class FAQ(TranslatableModel):
    category = models.ForeignKey(FAQCategory, verbose_name="Category")
    slug = models.CharField(max_length=255)
    order = models.IntegerField(max_length=3, default=0)
    date_created = models.DateTimeField(auto_now=True, editable=False)
    date_modified = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        ordering = ["order"]

    def __unicode__(self):
        try:
            return self.get_translation(language="en").question
        except:
            return ""

    class Meta:
        verbose_name = "FAQ"
        verbose_name_plural = "FAQs"


class FAQTranslation(get_translation_model(FAQ, "faq")):
    question = models.CharField(max_length=255)
    answer = models.TextField()

    def __unicode__(self):
        try:
            return self.model.get_translation(language="en").question
        except:
            return ""

    class Meta:
        verbose_name = "FAQ"
        verbose_name_plural = "FAQs"
