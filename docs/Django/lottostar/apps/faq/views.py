from django.http import HttpResponseRedirect
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from apps.faq.models import FAQCategory, FAQ, FAQTranslation, FAQCategoryTranslation

def admin_faq_text(request, faq_id):
    faq = FAQ.objects.get(pk = faq_id)
    translate = FAQTranslation.objects.get_or_create(model = faq, language=request.GET.get("language", "en"))
    return HttpResponseRedirect("/admin/faq/faqtranslation/%s/" % translate[0].id)

def admin_faqcategory_text(request, category_id):
    faq = FAQCategory.objects.get(pk = category_id)
    translate = FAQCategoryTranslation.objects.get_or_create(model = faq, language=request.GET.get("language", "en"))
    return HttpResponseRedirect("/admin/faq/faqcategorytranslation/%s/" % translate[0].id)


class FAQView(ListView):
    template_name = "faq/faq.html"
    model = FAQCategory
    context_object_name = "category_list"


class FAQQuestionView(DetailView):
    template_name = "faq/faq_item.html"
    model = FAQ
