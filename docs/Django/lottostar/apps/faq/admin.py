from django.contrib import admin
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from apps.faq.models import FAQTranslation, FAQ, FAQCategoryTranslation, FAQCategory
from django.conf import settings
from flutter_modeladmin import RemoveDeleteAdmin
from reversion import VersionAdmin

def title(obj):
    return obj.get_translation(language="en").title

def faqcategory_controls(obj):
    html = '<div style="width:340px;height:25px; line-height: 25px">'
    for lang in settings.LANGUAGES:
        html += '<a style="margin:5px" href="/faq/admin/translate/faqcategory/%s/?language=%s" title="%s"><img src="/static/img/flags/icons/%s.png"/></a>' % (obj.id, lang[0], lang[1].title(), lang[0])
    html += "</div>"
    return html
faqcategory_controls.short_description = 'Controls'
faqcategory_controls.allow_tags = True

class FAQCategoryInline(admin.StackedInline):
    model = FAQCategoryTranslation
    extra = 1
    max_num = 1

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'language':
            kwargs['initial'] = "en"
        return super(FAQCategoryInline, self).formfield_for_dbfield(db_field, **kwargs)


class FAQCategoryAdmin(RemoveDeleteAdmin, VersionAdmin):
    list_display = (title,'order', faqcategory_controls)
    list_editable = ["order"]
    inlines = [FAQCategoryInline, ]

    def get_inline_instances(self, request, obj=None):
        inline_instances = super(FAQCategoryAdmin, self).get_inline_instances(request, obj)
        if obj:
            return []
        return inline_instances

admin.site.register(FAQCategory, FAQCategoryAdmin)

class FAQCategoryTextAdmin(RemoveDeleteAdmin, VersionAdmin):
    model = FAQCategoryTranslation
    list_display = ["model", "language"]
    readonly_fields = ["model", "language"]
    fieldsets = (
        (None, {
            'fields': ('model', "language")
        }),
        ("Details", {
            'fields': ('title',)
        }),
    )

    def has_add_permission(self, request):
        return False

    def get_model_perms(self, request):
        return {}

    def changelist_view(self, request, extra_context=None):
        return HttpResponseRedirect(reverse("admin:faq_faqcategory_changelist"))

admin.site.register(FAQCategoryTranslation, FAQCategoryTextAdmin)


def faq_controls(obj):
    html = '<div style="width:340px;height:25px; line-height: 25px">'
    for lang in settings.LANGUAGES:
        html += '<a style="margin:5px" href="/faq/admin/translate/faq/%s/?language=%s" title="%s"><img src="/static/img/flags/icons/%s.png"/></a>' % (obj.id, lang[0], lang[1].title(), lang[0])
    html += "</div>"
    return html
faq_controls.short_description = 'Controls'
faq_controls.allow_tags = True

def question(obj):
    return obj.get_translation(language="en").question

def answer(obj):
    return obj.get_translation(language="en").answer


class FAQTextAdmin(RemoveDeleteAdmin, VersionAdmin):
    model = FAQTranslation
    list_display = ["model", "language"]
    readonly_fields = ["model", "language"]
    fieldsets = (
        (None, {
            'fields': ('model', "language")
        }),
        ("Details", {
            'fields': ('question','answer',
            )
        }),
    )
    class Media:
        js = (
            "/static/js/tinymce/tinymce.min.js",
            "/static/js/tinymce.js",
        )

    def get_model_perms(self, request):
        return {}

    def changelist_view(self, request, extra_context=None):
        return HttpResponseRedirect(reverse("admin:faq_faq_changelist"))

admin.site.register(FAQTranslation, FAQTextAdmin)

class FAQInline(admin.StackedInline):
    model = FAQTranslation
    extra = 1
    max_num = 1

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'language':
            kwargs['initial'] = "en"
        return super(FAQInline, self).formfield_for_dbfield(db_field, **kwargs)


class FAQAdmin(RemoveDeleteAdmin, VersionAdmin):
    list_display = (question, answer, 'order', 'category', faq_controls)
    list_filter = ("category",)
    list_editable = ["order"]
    inlines = [FAQInline,]

    def get_inline_instances(self, request, obj=None):
        inline_instances = super(FAQAdmin, self).get_inline_instances(request, obj)
        if obj:
            return []
        return inline_instances

admin.site.register(FAQ, FAQAdmin)