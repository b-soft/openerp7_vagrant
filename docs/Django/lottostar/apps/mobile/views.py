# Create your views here.

from django.core.urlresolvers import reverse
from django.views.generic import ListView, CreateView

from .models import Publisher
from .forms import PublisherForm


class PublisherList(ListView):
    model = Publisher
    template_name = "mobile/publisher_list.html"
    context_object_name = "publisher_list"


class PublisherCreateView(CreateView):
    model = Publisher
    form_class = PublisherForm
    template_name = "mobile/create_publisher.html"

    def get_success_url(self):
        return reverse('register_complete')