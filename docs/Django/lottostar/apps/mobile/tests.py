"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
import random
import string
from django.test import TestCase
from django.test.client import Client
from django.core.urlresolvers import reverse

from apps.mobile.models import Publisher
from apps.mobile.factories import UserFactory, PublisherFactory


class PublisherTest(TestCase):
    model = Publisher

    def create_random_publisher_name(self, length):
        """
        create random publisher name with a max of 10 characters.
        """
        return ''.join(random.choice(string.lowercase) for i in range(length))

    def setUp(self):
        self.user = UserFactory(username='nganpet')
        self.client = Client()
        #authenticate client
        self.client.login(username=self.user.username)
        # Publisher.objects.create(name="Gutembert", city="Cape Town")
        # Publisher.objects.create(name="Taffty", city="Harare")
        self.publishers = []
        for i in xrange(3):
            form_data = {
            'name': self.create_random_publisher_name(10),
            'address': 'test address',
            'city': 'cape town',
            'state_province': 'western cape',
            'country': 'south africa',
            'website': 'http://www.publisher%s.com' % i
        }
        self.publishers.append(PublisherFactory(**self.form_data)) #create a list of 3 publishers


    def test_valid_create_publisher(self):
        """
        calls the create view for new publisher.
        """
        url = reverse('create_publisher')
        form_data = {
            'name': 'publisher',
            'address': 'test address',
            'city': 'cape town',
            'state_province': 'western cape',
            'country': 'south africa',
            'website': 'http://www.publisher.com'
        }
        response = self.client.post(url, form_data)
        print 'response: %s' % response.__dict__
        self.assertEqual(response.status_code, 200) #test if post successful


    def test_publisher_list(self):
        """
        Publishers town is correctly identified.
        """
        url = reverse('publisher_list')
        response = self.client.get(url, kwargs={'object_list': self.publishers})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['request'].object_list.count(), 3)