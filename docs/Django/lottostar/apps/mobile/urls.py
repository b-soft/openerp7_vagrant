from django.conf.urls import patterns, url

from apps.mobile.views import PublisherList, PublisherCreateView

urlpatterns = patterns('',
    url(r'^mobile/publishers/$', PublisherList.as_view(), name='publisher_list'),
    url(r'^mobile/create/publishers/$', PublisherCreateView.as_view(), name='create_publisher'),
)
