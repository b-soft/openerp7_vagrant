from django.contrib.auth.models import User

import factory

from apps.mobile.models import Publisher


class UserFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = User
    FACTORY_DJANGO_GET_OR_CREATE = ('username',)


class PublisherFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Publisher
    FACTORY_DJANGO_GET_OR_CREATE = ('name', 'city', 'address', 'state_province', 'country', 'website',)
