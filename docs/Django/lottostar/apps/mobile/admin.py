from django.contrib import admin
from .models import Publisher

class PublisherAdmin(admin.ModelAdmin):

    list_display = ('name', 'address', 'city',)
    list_filter = ('name',)
    fieldsets = (
        ('Publisher', {
            'fields': ('name', 'address', 'city', 'state_province', 'country', 'website',)
        }),
        
    )    

admin.site.register(Publisher, PublisherAdmin)
