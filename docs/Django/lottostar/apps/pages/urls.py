from django.conf.urls import *
from django.views.generic import TemplateView
from apps.pages import views
from pages.views import HomeDetailView

handler403 = "pages.views.server_error_page_403"
handler404 = "pages.views.server_error_page_404"
handler500 = "pages.views.server_error_page_500"

urlpatterns = patterns('pages.views',
    url(r'^admin/translate/emailcontent/(?P<emailcontent_id>[a-zA-Z0-9_.-]+)/$', "admin_emailcontent_text", name="admin_emailcontent_text"),
    url(r'^admin/translate/blockcontent/(?P<blockcontent_id>[a-zA-Z0-9_.-]+)/$', "admin_blockcontent_text", name="admin_blockcontent_text"),
    url(r'^admin/translate/pagegroup/(?P<pagegroup_id>[a-zA-Z0-9_.-]+)/$', "admin_pagegroup_text", name="admin_pagegroup_text"),
    url(r'^admin/translate/page/(?P<page_id>[a-zA-Z0-9_.-]+)/$', "admin_page_text", name="admin_page_text"),

    url(r'^$', HomeDetailView.as_view(template_name="pages/home.html"), name="home"),

    url(r'^404/$', "server_error_page_404", name="server_404"),
    url(r'^403/$', "server_error_page_403", name="server_403"),
    url(r'^500/$', "server_error_page_500", name="server_500"),

    url(r'^contact/$', views.ContactFormView.as_view(), name="contact"),
    url(r'^contact/success/$', TemplateView.as_view(template_name="pages/contact_success.html"), name="contact_success"),

    url(r'^blank/(?P<slug>[a-zA-Z0-9_-]+)/$', views.PageBlankView.as_view(), name="page_blank"),
    url(r'^(?P<slug>[a-zA-Z0-9_-]+)/$', views.PageDetailView.as_view(), name="page"),
)