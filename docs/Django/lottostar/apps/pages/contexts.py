from django.contrib.sites.models import Site
import twitter
from django.conf import settings
from pages.models import HomeConfig
from django.core.cache import cache
from settings import CACHE_TIME

def global_settings(request):
    context = {}
    context["slug"] = ("%s" % request.path).split("/")[1]

#    if not "admin" in context["slug"] and context["slug"] != "":
#        if not cache.has_key('tweet_list'):
#            api = twitter.Api(
#                consumer_key=settings.TWITTER_CONSUMER_KEY,
#                consumer_secret=settings.TWITTER_CONSUMER_SECRET,
#                access_token_key=settings.TWITTER_ACCESS_TOKEN_KEY,
#                access_token_secret=settings.TWITTER_ACCESS_TOKEN_SECRET,
#            )
#
#            twitter_agent = api.GetUserTimeline()
#
#            if twitter_agent:
#                tweets = []
#                c = 0
#                for text in twitter_agent:
#                    if c <= settings.TWITTER_COUNT:
#                        t = "%s" % text.text; tweets.append({"text": t.strip()})
#                    c += 1
#                cache.set('tweet_list', tweets, settings.TWITTER_TIMEOUT)
#            else:
#                tweets = []
#        else:
#            tweets = cache.get('tweet_list')
#        context["tweet_list"] = tweets

    site = Site.objects.get_current()
    cache_key = '%s_%s' % ('home_config', site.id)
    cache_time = 60 * 60
    result = cache.get(cache_key)
    if not result:
        try:
            result = HomeConfig.objects.get(site=site)
            cache.set(cache_key, result, cache_time)
        except:
            pass

    context['config'] = result
    return context