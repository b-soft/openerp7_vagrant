from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.views.generic import FormView, TemplateView, DetailView
from django.views.generic.list import ListView
from blog.models import Post
from pages.forms import ContactForm
from pages.models import Contact, Page, PageTranslation, BlockContentTranslation, BlockContent, EmailContent, EmailContentTranslation, PageGroup, PageGroupTranslation
from play.models import Entry


def admin_page_text(request, page_id):
    page = Page.objects.get(pk = page_id)
    translation = PageTranslation.objects.get_or_create(model = page, language=request.GET.get("language", "en"))
    return HttpResponseRedirect("/admin/pages/pagetranslation/%s/" % translation[0].id)

def admin_pagegroup_text(request, pagegroup_id):
    pagegroup = PageGroup.objects.get(pk = pagegroup_id)
    translation = PageGroupTranslation.objects.get_or_create(model = pagegroup, language=request.GET.get("language", "en"))
    return HttpResponseRedirect("/admin/pages/pagegrouptranslation/%s/" % translation[0].id)

def admin_blockcontent_text(request, blockcontent_id):
    blockcontent = BlockContent.objects.get(pk = blockcontent_id)
    page = BlockContentTranslation.objects.get_or_create(model = blockcontent, language=request.GET.get("language", "en"))
    return HttpResponseRedirect("/admin/pages/blockcontenttranslation/%s/" % page[0].id)

def admin_emailcontent_text(request, emailcontent_id):
    emailcontent = EmailContent.objects.get(pk = emailcontent_id)
    page = EmailContentTranslation.objects.get_or_create(model = emailcontent, language=request.GET.get("language", "en"))
    return HttpResponseRedirect("/admin/pages/emailcontenttranslation/%s/" % page[0].id)


class PageDetailView(TemplateView):
    template_name = "pages/page.html"

    def get_object(self):
        try:
            translation = PageTranslation.objects.get(slug=self.kwargs["slug"])
            return translation.model
        except:
            try:
                return Page.objects.get(display=True, slug=self.kwargs["slug"])
            except Page.DoesNotExist:
                raise Http404()

    def get_context_data(self, **kwargs):
        context = super(PageDetailView, self).get_context_data(**kwargs)
        context["page"] = self.get_object()
        return context


class PageBlankView(DetailView):
    model = Page
    template_name = "pages/page_blank.html"
    context_object_name = "page"

    def get_object(self, queryset=None):
        try:
            return Page.objects.get(slug=self.kwargs["slug"])
        except Page.DoesNotExist:
            return HttpResponseRedirect(reverse("home"))


class ContactFormView(FormView):
    model = Contact
    template_name = 'pages/contact.html'
    form_class = ContactForm

    def get_context_data(self, **kwargs):
        context = super(ContactFormView, self).get_context_data(**kwargs)
        try:
            context["page"] = Page.objects.get(slug="contact")
        except:
            pass
        return context

    def get_success_url(self):
        self.done()
        return reverse("contact_success")

    def form_valid(self, form):
        form.save(self.request)
        return super(ContactFormView, self).form_valid(form)

    def done(self, **kwargs):
        context = super(ContactFormView, self).get_context_data(**kwargs)
        context["success_msg"] = "Contact us form received"
        return context


class HomeDetailView(ListView):
    template_name="pages/home.html"
    model = Post
    context_object_name = "blog_list"

    def get_queryset(self):
        return Post.objects.filter(status=1,site=Site.objects.get_current()).order_by("-date_published")[:3]

def server_error_page(request, error):
    response = render(request, "%s.html" % error)
    response.status_code = int(error)
    return response

def server_error_page_500(request, error="500"):
    return server_error_page(request, error)

def server_error_page_404(request, error="404"):
    return server_error_page(request, error)

def server_error_page_403(request, error="403"):
    return server_error_page(request, error)


