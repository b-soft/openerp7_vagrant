from django.contrib import admin
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.utils.text import truncate_words
from pages.models import EmailContent,  HomeConfig, HomeSlider, PageTranslation, Page, Contact, PageGroup, PageGroupTranslation, EmailContentTranslation, Complaint, HomeProduct, BlockContent, BlockContentTranslation, HomeBlockContent
import settings
from flutter_modeladmin import RemoveDeleteAdmin
from reversion import VersionAdmin


def get_title(obj):
    return truncate_words(obj.get_translation().title, 10)
get_title.short_description = "English Title"

def get_subject(obj):
    return truncate_words(obj.get_translation().subject, 10)
get_subject.short_description = "English Subject"


def pagegroup_controls(obj):
    html = '<div style="width:340px;height:25px; line-height: 25px">'
    for lang in settings.LANGUAGES:
        html += '<a style="margin:5px" href="/admin/translate/pagegroup/%s/?language=%s" title="%s"><img src="/static/img/flags/icons/%s.png"/></a>' % (obj.id, lang[0], lang[1].title(), lang[0])
    html += "</div>"
    return html
pagegroup_controls.short_description = 'Controls'
pagegroup_controls.allow_tags = True

class PageGroupTextAdmin(RemoveDeleteAdmin, VersionAdmin):
    model = PageGroupTranslation
    list_display = ["model", "language"]
    readonly_fields = ["model", "language"]
    prepopulated_fields = {'slug': ('title',)}
    fieldsets = (
        (None, {
            'fields': ('model', "language")
        }),
        ("Details", {
            'fields': ('title','slug')
        }),
    )

    def get_model_perms(self, request):
        return {}

    def changelist_view(self, request, extra_context=None):
        return HttpResponseRedirect(reverse("admin:pages_pagegroup_changelist"))

admin.site.register(PageGroupTranslation, PageGroupTextAdmin)

class PageGroupInline(admin.StackedInline):
    model = PageGroupTranslation
    extra = 1
    max_num = 1

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'language':
            kwargs['initial'] = "en"
        return super(PageGroupInline, self).formfield_for_dbfield(db_field, **kwargs)

class PageGroupAdmin(RemoveDeleteAdmin, VersionAdmin):
    list_display = (get_title, 'slug', 'display',pagegroup_controls, )
    inlines = [PageGroupInline,]

    def get_inline_instances(self, request, obj=None):
        inline_instances = super(PageGroupAdmin, self).get_inline_instances(request, obj)
        if obj:
            return []
        return inline_instances

admin.site.register(PageGroup, PageGroupAdmin)

#-----------------------------------------------------------

def page_controls(obj):
    html = '<div style="width:340px;height:25px; line-height: 25px">'
    for lang in settings.LANGUAGES:
        html += '<a style="margin:5px" href="/admin/translate/page/%s/?language=%s" title="%s"><img src="/static/img/flags/icons/%s.png"/></a>' % (obj.id, lang[0], lang[1].title(), lang[0])
    html += "</div>"
    return html
page_controls.short_description = 'Controls'
page_controls.allow_tags = True

class PageTextAdmin(RemoveDeleteAdmin, VersionAdmin):
    model = PageTranslation
    list_display = ["model", "slug", "language"]
    readonly_fields = ["model", "language"]
    prepopulated_fields = {'slug': ('title',)}
    fieldsets = (
        (None, {
            'fields': ('model', "language")
        }),
        ("Details", {
            'fields': ('title', 'subtitle','slug')
        }),
        ("Content", {
            'fields': ('content', )
        }),
        ("SEO", {
            'classes': ('collapse',),
            'fields': ('meta_title', 'meta_keywords',"meta_description")
        }),
    )

    class Media:
        js = (
            "/static/js/tinymce/tinymce.min.js",
            "/static/js/tinymce.js",
        )

    def get_model_perms(self, request):
        return {}

    def changelist_view(self, request, extra_context=None):
        return HttpResponseRedirect(reverse("admin:pages_page_changelist"))

admin.site.register(PageTranslation, PageTextAdmin)

class PageInline(admin.StackedInline):
    model = PageTranslation
    extra = 1
    max_num = 1

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'language':
            kwargs['initial'] = "en"
        return super(PageInline, self).formfield_for_dbfield(db_field, **kwargs)


class PageAdmin(RemoveDeleteAdmin, VersionAdmin):
    search_fields = ["name", "slug"]
    list_display = ('name', get_title, 'slug','group', 'display', page_controls,)
    list_editable = ('display',)
    list_filter = ('display','group',)
    inlines = [PageInline,]
    prepopulated_fields = {'slug': ('name',)}

    def get_inline_instances(self, request, obj=None):
        inline_instances = super(PageAdmin, self).get_inline_instances(request, obj)
        if obj:
            return []
        return inline_instances

admin.site.register(Page, PageAdmin)

#-----------------------------------------------------------

def blockcontent_controls(obj):
    html = '<div style="width:340px;height:25px; line-height: 25px">'
    for lang in settings.LANGUAGES:
        html += '<a style="margin:5px" href="/admin/translate/blockcontent/%s/?language=%s" title="%s"><img src="/static/img/flags/icons/%s.png"/></a>' % (obj.id, lang[0], lang[1].title(), lang[0])
    html += "</div>"
    return html
blockcontent_controls.short_description = 'Controls'
blockcontent_controls.allow_tags = True


class BlockContentInline(admin.StackedInline):
    model = BlockContentTranslation
    extra = 1
    max_num = 1

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'language':
            kwargs['initial'] = "en"
        return super(BlockContentInline, self).formfield_for_dbfield(db_field, **kwargs)


class BlockContentAdmin(RemoveDeleteAdmin, VersionAdmin):
    search_fields = ["title",]
    list_display = ('title', get_title, blockcontent_controls)
    inlines = [BlockContentInline,]

    def get_inline_instances(self, request, obj=None):
        inline_instances = super(BlockContentAdmin, self).get_inline_instances(request, obj)
        if obj:
            return []
        return inline_instances

admin.site.register(BlockContent,  BlockContentAdmin)

class BlockContentTextAdmin(RemoveDeleteAdmin, VersionAdmin):
    model = PageTranslation
    list_display = ["model", "language"]
    readonly_fields = ["model", "language"]

    fieldsets = (
        (None, {
            'fields': ('model', "language")
        }),
        ("Details", {
            'fields': ('title',)
        }),
        ("Content", {
            'fields': ('content', )
        }),
    )

    class Media:
        js = (
            "/static/js/tinymce/tinymce.min.js",
            "/static/js/tinymce.js",
        )

    def get_model_perms(self, request):
        return {}

    def changelist_view(self, request, extra_context=None):
        return HttpResponseRedirect(reverse("admin:pages_blockcontent_changelist"))

admin.site.register(BlockContentTranslation, BlockContentTextAdmin)

#-----------------------------------------------------------

def emailcontent_controls(obj):
    html = '<div style="width:340px;height:25px; line-height: 25px">'
    for lang in settings.LANGUAGES:
        html += '<a style="margin:5px" href="/admin/translate/emailcontent/%s/?language=%s" title="%s"><img src="/static/img/flags/icons/%s.png"/></a>' % (obj.id, lang[0], lang[1].title(), lang[0])
    html += "</div>"
    return html
emailcontent_controls.short_description = 'Controls'
emailcontent_controls.allow_tags = True

class EmailContentInline(admin.StackedInline):
    model = EmailContentTranslation
    extra = 1
    max_num = 1

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'language':
            kwargs['initial'] = "en"
        return super(EmailContentInline, self).formfield_for_dbfield(db_field, **kwargs)


class EmailContentAdmin(RemoveDeleteAdmin, VersionAdmin):
    search_fields = ["name", "slug"]
    list_display = ('name', get_subject, 'slug', emailcontent_controls)
    prepopulated_fields = {'slug': ('name',)}

    inlines = [EmailContentInline, ]

    def get_inline_instances(self, request, obj=None):
        inline_instances = super(EmailContentAdmin, self).get_inline_instances(request, obj)
        if obj:
            return []
        return inline_instances

    class Media:
        js = (
            "/static/js/tinymce/tinymce.min.js",
            "/static/js/tinymce.js",
        )
admin.site.register(EmailContent, EmailContentAdmin)


class EmailContentTextAdmin(RemoveDeleteAdmin, VersionAdmin):
    model = EmailContentTranslation
    list_display = ["model", "language"]
    readonly_fields = ["model", "language"]

    fieldsets = (
        (None, {
            'fields': ('model', "language")
        }),
        ("Details", {
            'fields': ('subject',)
        }),
        ("Content", {
            'fields': ('content', )
        }),
    )

    class Media:
        js = (
            "/static/js/tinymce/tinymce.min.js",
            "/static/js/tinymce.js",
        )

    def get_model_perms(self, request):
        return {}

    def changelist_view(self, request, extra_context=None):
        return HttpResponseRedirect(reverse("admin:pages_emailcontent_changelist"))

admin.site.register(EmailContentTranslation, EmailContentTextAdmin)

#-----------------------------------------------------------

class ContactAdmin(RemoveDeleteAdmin, VersionAdmin):
    search_fields = ['name','last_name', 'email', 'language']
    list_display = ('site', 'name', 'last_name', 'email','language', )
    list_filter = ('language','site',)
admin.site.register(Contact, ContactAdmin)

#-----------------------------------------------------------

class ComplaintAdmin(RemoveDeleteAdmin):
    search_fields = ['name','last_name', 'email', 'ticket_type', 'language']
    list_display = ('site','name', 'last_name', 'email','ticket_type', 'language')
    list_filter = ('site', 'language',)
admin.site.register(Complaint, ComplaintAdmin)

#-----------------------------------------------------------

class HomeSliderAdmin(admin.TabularInline):
    model = HomeSlider

class HomeProductAdmin(admin.TabularInline):
    model = HomeProduct

class HomeBlockContentAdmin(admin.TabularInline):
    model = HomeBlockContent

#-----------------------------------------------------------

class HomeConfigAdmin(RemoveDeleteAdmin, VersionAdmin):
    list_display = ["site",]
    inlines = [HomeSliderAdmin, HomeProductAdmin, HomeBlockContentAdmin]
    fieldsets = (
        (None, {
            'fields': ('site',)
        }),
        ("Social Links", {
            'fields': (('facebook','twitter',), ('googleplus',"yandex",), 'bebo')
        }),
        ("Important Notices", {
            'classes': ('collapse',),
            'fields': ('security', 'twitter_security',)
        }),
        ("Global Content", {
            'classes': ('collapse',),
            'fields': ('footer_text1', 'footer_text2', 'blank_checkout' )
        }),
        ("Global SEO", {
            'classes': ('collapse',),
            'fields': ('meta_title', 'meta_keywords',"meta_description")
        }),
    )

#    def changelist_view(self, request, extra_context=None):
#        try:
#            location = '/admin/pages/homeconfig/%d' % HomeConfig.objects.filter()[0].id
#            return HttpResponseRedirect(location)
#        except:
#            return super(HomeConfigAdmin, self).changelist_view(request, extra_context)
admin.site.register(HomeConfig, HomeConfigAdmin)

