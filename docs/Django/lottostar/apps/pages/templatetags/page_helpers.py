from django import template
from django.utils.safestring import mark_safe
from pages.models import Page

register = template.Library()

@register.simple_tag
def get_page_link(slug):
    try:
        page = Page.objects.get(slug=slug)
        trans = page.get_translation()
        return mark_safe('<a href="/%s/">%s</a>' % (trans.slug, trans.title))
    except:
        pass
    return ""