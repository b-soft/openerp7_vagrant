from django.contrib.sites.models import Site
from django.db import models
from djorm_pgarray.fields import ArrayField
from registration.models import User
from translatable.models import TranslatableModel, get_translation_model
from game.models import Product
import settings

class PageGroup(TranslatableModel):
    site = models.ManyToManyField(Site)
    slug =  models.SlugField(max_length=150,blank=False, null=False)
    display = models.BooleanField(default=True)
    order = models.IntegerField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now = True)
    modified_at = models.DateTimeField(auto_now_add = True)

    def __unicode__(self):
        try:
            return self.get_translation(language="en").title
        except:
            return ""


    class Meta:
        verbose_name = "Page Group"
        verbose_name_plural = "Page Groups"
        ordering = ["order"]


class PageGroupTranslation(get_translation_model(PageGroup, "pagegroup")):
    title = models.CharField(max_length=150,blank=False, null=False)
    slug = models.SlugField(max_length=150,blank=False, null=False)

    created_at = models.DateTimeField(auto_now = True)
    modified_at = models.DateTimeField(auto_now_add = True)

    def __unicode__(self):
        try:
            return self.model.get_translation(language="en").title
        except:
            return ""

    class Meta:
        verbose_name = "Page Group"
        verbose_name_plural = "Page Groups"


class Page(TranslatableModel):
    site = models.ManyToManyField(Site)
    name = models.CharField(max_length=100, blank=False, null=False)
    slug = models.SlugField(max_length=150,blank=False, null=False)
    display = models.BooleanField(default=True)

    group = models.ForeignKey(PageGroup, blank=True, null=True)

    created_at = models.DateTimeField(auto_now = True)
    modified_at = models.DateTimeField(auto_now_add = True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "Pages"
        verbose_name_plural = "Pages"


class PageTranslation(get_translation_model(Page, "page")):
    title = models.CharField(max_length=150, blank=False, null=False)
    subtitle = models.CharField(max_length=150, blank=False, null=False)
    slug =  models.SlugField(max_length=150,blank=False, null=False)
    content = models.TextField(blank=False, null=False)

    meta_title = models.CharField("Title",max_length=255, null=True, blank=True)
    meta_description = models.CharField("Description",max_length=255, null=True, blank=True)
    meta_keywords = models.CharField("Keywords",max_length=255, null=True, blank=True)

    created_at = models.DateTimeField(auto_now = True)
    modified_at = models.DateTimeField(auto_now_add = True)

    def __unicode__(self):
        return self.model.name

    class Meta:
        verbose_name = "Pages"
        verbose_name_plural = "Pages"


class Contact(models.Model):
    site = models.ForeignKey(Site)
    name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    telephone = models.CharField(max_length=255)
    message = models.CharField(max_length=255)
    language = models.CharField(choices=settings.LANGUAGES, max_length=255)

    created_at = models.DateTimeField(auto_now = True)
    modified_at = models.DateTimeField(auto_now_add = True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "Contact Message"
        verbose_name_plural = "Contact Messages"
        ordering = ['-created_at']


class Complaint(models.Model):
    site = models.ForeignKey(Site)
    name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    complaint = models.CharField(max_length=255)
    language = models.CharField(choices=settings.LANGUAGES, max_length=255)
    ticket_type = models.CharField(choices=settings.TICKETS, max_length=255)

    created_at = models.DateTimeField(auto_now = True)
    modified_at = models.DateTimeField(auto_now_add = True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "Complaint Message"
        verbose_name_plural = "Complaint Messages"
        ordering = ['-created_at']


class EmailContent(TranslatableModel):
    site = models.ManyToManyField(Site)
    name =  models.CharField(max_length=100, blank=False, null=False)
    slug =  models.SlugField(max_length=150,blank=False, null=False)

    created_at = models.DateTimeField(auto_now = True)
    modified_at = models.DateTimeField(auto_now_add = True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "Email Template"
        verbose_name_plural = "Email Templates"


class EmailContentTranslation(get_translation_model(EmailContent, "emailcontent")):
    subject = models.CharField(max_length=150)
    content = models.TextField(blank=False, null=False)

    created_at = models.DateTimeField(auto_now = True)
    modified_at = models.DateTimeField(auto_now_add = True)

    class Meta:
        verbose_name = "Email Template"
        verbose_name_plural = "Email Templates"

    def __unicode__(self):
        return self.model.name


class BlockContent(TranslatableModel):
    title = models.CharField(max_length=150, blank=False, null=False)
    created_at = models.DateTimeField(auto_now = True)
    modified_at = models.DateTimeField(auto_now_add = True)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = "Block Content"
        verbose_name_plural = "Block Content"


class BlockContentTranslation(get_translation_model(BlockContent, "blockcontent")):
    title = models.CharField(max_length=150)
    content = models.TextField()

    created_at = models.DateTimeField(auto_now = True)
    modified_at = models.DateTimeField(auto_now_add = True)

    def __unicode__(self):
        return self.model.title

    class Meta:
        verbose_name = "Block Content"
        verbose_name_plural = "Block Content"


class HomeConfig(models.Model):
    site = models.ForeignKey(Site)
    facebook = models.CharField(max_length = 255,blank=True, null=True)
    twitter = models.CharField(max_length = 255,blank=True, null=True)
    googleplus = models.CharField(max_length = 255,blank=True, null=True)
    yandex = models.CharField(max_length = 255,blank=True, null=True)
    bebo = models.CharField(max_length = 255,blank=True, null=True)

    footer_text1 = models.ForeignKey(BlockContent, verbose_name="Footer Text 1", related_name="footer_text1",blank=True, null=True)
    footer_text2 = models.ForeignKey(BlockContent, verbose_name="Footer Text 2", related_name="footer_text2",blank=True, null=True)

    security = models.ForeignKey(BlockContent, verbose_name="Security Important Notice", related_name="security",blank=True, null=True)
    twitter_security = models.ForeignKey(BlockContent, verbose_name="Twitter Important Notice", related_name="twitter_security",blank=True, null=True)

    blank_checkout = models.ForeignKey(Page, verbose_name="Blank Checkout", related_name="blank_checkout",blank=True, null=True)

    meta_title = models.CharField("Title", max_length=255, null=True, blank=True)
    meta_description = models.CharField("Description", max_length=255, null=True, blank=True)
    meta_keywords = models.CharField("Keywords", max_length=255, null=True, blank=True)

    created_at = models.DateTimeField(auto_now = True)
    modified_at = models.DateTimeField(auto_now_add = True)

    class Meta:
        verbose_name = "Home / Site Settings"
        verbose_name_plural = "Home / Site Settings"

    def __unicode__(self):
        return "Home Settings"


class HomeSlider(models.Model):
    home = models.ForeignKey(HomeConfig)

    image = models.FileField(verbose_name="Image", upload_to='upload/slider/', blank=True)
    page = models.ForeignKey(Page, null=True, blank=True)
    link = models.CharField(max_length=255, null=True, blank=True)
    order = models.IntegerField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now = True)
    modified_at = models.DateTimeField(auto_now_add = True)

    class Meta:
        ordering = ['order']


class HomeBlockContent(models.Model):
    home = models.ForeignKey(HomeConfig)
    description = models.ForeignKey(BlockContent)
    page = models.ForeignKey(Page)
    order = models.IntegerField(null=True, blank=True, default=0)

    created_at = models.DateTimeField(auto_now = True)
    modified_at = models.DateTimeField(auto_now_add = True)

    class Meta:
        ordering = ["order"]


class HomeProduct(models.Model):
    home = models.ForeignKey(HomeConfig)
    product = models.ForeignKey(Product)


class Advert(TranslatableModel):
    site = models.ManyToManyField(Site)
    display = models.BooleanField(default=True)
    order = models.IntegerField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now = True)
    modified_at = models.DateTimeField(auto_now_add = True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "Advert"
        verbose_name_plural = "Adverts"


class AdvertTranslation(get_translation_model(Advert, "advert")):
    title = models.CharField(max_length=150, blank=False, null=False)
    subtitle = models.CharField(max_length=150, blank=False, null=False)
    slug =  models.SlugField(max_length=150,blank=False, null=False)
    content = models.TextField(blank=False, null=False)

    meta_title = models.CharField("Title",max_length=255, null=True, blank=True)
    meta_description = models.CharField("Description",max_length=255, null=True, blank=True)
    meta_keywords = models.CharField("Keywords",max_length=255, null=True, blank=True)

    created_at = models.DateTimeField(auto_now = True)
    modified_at = models.DateTimeField(auto_now_add = True)

