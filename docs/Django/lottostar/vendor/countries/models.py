from django.contrib.sites.managers import CurrentSiteManager
from django.contrib.sites.models import Site
from django.db import models
from django.utils.translation import ugettext_lazy as _
from translatable.models import TranslatableModel, get_translation_model

class Country(TranslatableModel):
    iso = models.CharField(_('ISO alpha-2'), max_length=2, primary_key=True)
    iso3 = models.CharField(_('ISO alpha-3'), max_length=3, null=True)
    is_allowed = models.BooleanField(default=True)
    site = models.ManyToManyField(Site)

    objects = models.Manager()
    on_site = CurrentSiteManager()

    def __unicode__(self):
        if self.has_translations():
            return self.translations.get(language=self.translations.values_list('language', flat=True)[0]).name
        return self.iso

    class Meta:
        verbose_name = "Countries"
        verbose_name_plural = "Countries"
        ordering = ['translations__name']

class CountryTranslation(get_translation_model(Country, "country")):
    name = models.CharField(_('Country name'), max_length=255)
    slug = models.SlugField()
    def __unicode__(self):
        return self.name

class State(TranslatableModel):
    country = models.ForeignKey(Country)
    code = models.CharField(_('Code'), max_length=20, blank=False, null=True)

    def __unicode__(self):
        return self.get_translation().name

class StateTranslation(get_translation_model(State, "state")):
    name = models.CharField(_('State or region name'), max_length=255)
    slug = models.SlugField()



