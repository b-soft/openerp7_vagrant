from django.contrib import admin
from countries.models import State, StateTranslation, Country, CountryTranslation

def title(obj):
    return obj.get_translation("eng").name

def slug(obj):
    return obj.get_translation("eng").slug


class CountryInlineAdmin(admin.StackedInline):
    model = CountryTranslation
    extra = 1

class CountryAdmin(admin.ModelAdmin):
    model = Country
    list_filter = ("is_allowed",)
    list_display = (title, "iso", "is_allowed")
    list_per_page = 20
    inlines = [CountryInlineAdmin]

    def queryset(self,request):
        return Country.objects.all()

admin.site.register(Country, CountryAdmin)

class StateInlineAdmin(admin.StackedInline):
    model = StateTranslation
    extra = 1

class StateAdmin(admin.ModelAdmin):
    model = State
    list_display = (title, slug, "code", "country")
    list_filter = ("country",)
    inlines = [StateInlineAdmin]

admin.site.register(State, StateAdmin)

