import os, sys, site

sys.path.append('/home/tafadzwa/public_html/')
sys.path.append('/home/tafadzwa/public_html/lottostar/')


sys.path.append('/opt/.virtualenvs/lottostar/lib/python2.7/site-packages/')
sys.executable = '/opt/.virtualenvs/lottostar/bin/python'

site.addsitedir('/opt/.virtualenvs/lottostar/lib/python2.7/site-packages')
# We defer to a DJANGO_SETTINGS_MODULE already in the environment. This breaks
# if running multiple sites in the same mod_wsgi process. To fix this, use
# mod_wsgi daemon mode with each site in its own daemon process, or use
# os.environ["DJANGO_SETTINGS_MODULE"] = "lottoflutter.settings"
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

# Apply WSGI middleware here.
# from helloworld.wsgi import HelloWorldApplication
# application = HelloWorldApplication(application)
