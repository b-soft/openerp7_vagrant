var datetime_element, time_element;
$(function() {
    $("#id_schedule").change(function(){ alter_rep(); });
    $("#id_schedule").click(function(){ alter_rep(); });
    $("#id_schedule").keyup(function(){ alter_rep(); });
    datetime_element = $('#id_date');
    datetime_element.datetime({minDate: '+0 days'});
    datetime_element.datetime("disable");

    time_element = $('#id_time');
    time_element.datetime({withDate: false, format: 'hh:ii'});
    time_element.datetime("disable");

    $("#field_block").css("display", "none");
    $("#id_dow").css("display", "none");
    $("#id_day").css("display", "none");
    $("#id_date").css("display", "none");
    $("#id_time").css("display", "none");
    alter_rep();
});

function alter_rep(){
    if($("#id_schedule").val() == ""){
        $("#field_block").css("display", "none");
        datetime_element.datetime("disable");
        time_element.datetime("disable");
    }

    if($("#id_schedule").val() == "once"){
        $("#id_param_header").html("Once");
        $("#id_param_label").html("Date &amp; Time:");
        $("#id_dow").css("display", "none");
        $("#id_day").css("display", "none");
        $("#id_date").css("display", "block");
        $("#id_time").css("display", "none");
        $("#field_block").css("display", "block");
        datetime_element.datetime("enable");
        time_element.datetime("disable");
    }

    if($("#id_schedule").val() == "weekly"){
        $("#id_param_header").html("Weekly");
        $("#id_param_label").html("Day of the Week &amp Time:");
        $("#id_dow").css({"display": "inline-block", "float":"left"});
        $("#id_day").css("display", "none");
        $("#id_date").css("display", "none");
        $("#id_time").css({"display": "inline-block", "float":"left"});
        $("#field_block").css("display", "block");
        datetime_element.datetime("disable");
        time_element.datetime("enable");
    }

    if($("#id_schedule").val() == "monthly"){
        $("#id_param_header").html("Monthly");
        $("#id_param_label").html("Day of the Month &amp Time:");
        $("#id_dow").css("display", "none");
        $("#id_day").css({"display": "inline-block", "float":"left"});
        $("#id_time").css({"display": "inline-block", "float":"left"});
        $("#id_date").css("display", "none");
        $("#field_block").css("display", "block");
        datetime_element.datetime("disable");
        time_element.datetime("enable");
    }

    if($("#id_schedule").val() == "yearly"){
        $("#id_param_header").html("Yearly");
        $("#id_param_label").html("Day &amp Time:");
        $("#id_dow").css("display", "none");
        $("#id_day").css("display", "none");
        $("#id_date").css("display", "block");
        $("#id_time").css("display", "none");
        $("#field_block").css("display", "block");
        datetime_element.datetime("enable");
        time_element.datetime("disable");
    }
}
