(function (e) {
    e(function () {
        e(".toggle-nav").on("click", function (t) {
            var n = e(t.currentTarget);
            n.hasClass("open") ? (n.removeClass("open"), e("#menuLanguage").slideUp(400)) : (n.addClass("open"), e("#menuLanguage").slideDown(400))
        })
    })
})(jQuery);

