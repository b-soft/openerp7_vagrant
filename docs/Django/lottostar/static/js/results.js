$(function(){
    var change_date = function(){
        $("#resultloader").css({"display": "block"});
        $("#game_draw_results").css({"display": "none"});
        $.ajax({
            type: 'GET',
            url: "/game/draw-result/",
            data: {
                event_id: $("#game_event").val(),
                product_slug: product_slug
            },
            success: function(response){
                $('#game_draw_results').html(response);
                $("#resultloader").css({"display": "none"});
                $("#game_draw_results").css({"display": "block"});
            }
        });
    };
    var game_event_el = $("#game_event");
    game_event_el.change(change_date);
    game_event_el.keyup(change_date);
    game_event_el.click(change_date);

    $("#resultloader").css({"display": "none"});

});


