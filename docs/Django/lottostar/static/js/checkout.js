function removeEntry(slug,id,game_id){
    if (confirm($("#confirm_message").html())) {
        var params = {
            slug:slug,
            id:id,
            'csrfmiddlewaretoken': $("#csrf_token_id").html()
        };

        $.ajax({
            type: "POST",
            url: '/game/remove/basket-entry/',
            data: params,
            dataType: "text",
            success: function(response) {
                if($(".game_basket_entry").length <= 1){
                    window.location.reload();
                }else{
                    $("#game_basket_entry_"+id).remove();
                    if($(".game_identifier_"+game_id).length == 0) $("#game_basket_entry_vouchers_"+game_id).remove();
                    $("#final_cost").html(response);
                }

                if($(".game_basket_vouchers").length < 1) $("#basket_vouchers").hide();
            }
        });
    }
   return false;
}

var tu, tc;

$(function(){

    $("#btnCheckout").click(function(){
        $("#btnCheckout").css("display", "none");
    });

    var play_credit_el = $("#id_buy_play_credits");
    var update_play_credit = function(){
        try{
            clearTimeout(tu);
        }catch(e){}

        tu = setTimeout(function(){
            var params = {
                'csrfmiddlewaretoken': $("#csrf_token_id").html(),
                "buy_play_credits": play_credit_el.val()
            };
            $.ajax({
                type: "POST",
                url: '/checkout/set/pay-credits/',
                data: params,
                success: function(response) {
                    $("#final_cost").html(response);
                }
            });
        }, 1000);
    }
    play_credit_el.change(update_play_credit);
    play_credit_el.keyup(update_play_credit);
    play_credit_el.keydown(update_play_credit);
});

function integersonly(e){
    var unicode=e.charCode? e.charCode : e.keyCode;
    //if the key isn't the backspace key, left, up right, fullstop, down or enter  (which we should allow)
    if (unicode!=8  && unicode!=13 && unicode!=37 && unicode!=38 && unicode!=39 && unicode!=40){
        if (unicode<48||unicode>57){ //if not a number
            return false //disable key press
        }
    }
}

function numbersonly(e){
    var unicode=e.charCode? e.charCode : e.keyCode;
    //if the key isn't the backspace key, left, up right, fullstop, down or enter  (which we should allow)
    if (unicode!=8  && unicode!=13 && unicode!=37 && unicode!=38 && unicode!=39 && unicode!=40 && unicode!=46){
        if (unicode<48||unicode>57){ //if not a number
            return false //disable key press
        }
    }
}
