/*
jquery.percentageloader.js 
 
Copyright (c) 2012, Better2Web
All rights reserved.

This jQuery plugin is licensed under the Simplified BSD License. Please
see the file license.txt that was included with the plugin bundle.

*/

/*global jQuery */

(function ($) {
    /* Strict mode for this plugin */
    "use strict";
    /*jslint browser: true */

    /* Our spiral gradient data */

    var imgdata = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAH8AAACACAIAAACtGyGIAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAADvFJREFUeNrcXVuS67gNJXhVqVTWMAvIVrLG7Ct/2cN8TaUyfY20W5bEBx4HJNXdN56uO37Isn0OcACCJER//+e/U3dj5u4p9SHvD/QDPo5g8Xn5E8X3ym8s3tcdUH4s/nH9V9Z+HSdG8FEhTSknSiM3Gj0YuX/3jYP3QyfEj+B39KVfTkQ+3HT+n4QDqHlEs9TSHBYcApNdv5e9WTV8+Zis/bYFBCzxmyhzGoIL7HcYelXGswEKRAAsODKINAXxqPCwqQe8UILsCJptcH0Cxsz/DsG5QYJihh+EvkMfJEC1X9j8vyreerEXMHwGWfTzRgF9hAAyeaBh06e7KOGJeNAaPuRnCPR6xhklwNAfxPw/zQ9MLbIMP6I5IPRqxjlHwM2jh/toAN+zCHor44RAIUx/CM0fST0jBXHieBrqGj7CIoe8LYdSDicCg/oDugkBd/ofadzBDRrXHHtU5SVduY11FCRgQH8Iw/o+tQlnokOaIw7E6vifd1N18o0RAizzp6kosZ4PtV5Was489AXu+ydmebQ5SYAr1rS4rjBbe+Dgu+agl3OeZQQA9wWUbxUfNuKiU2227k9AL+Q8UwQIiQ9NCT2N+IRVdg+GAXaGWFPQdzkPzRGA6A8AqOUWw9UuJAflmOaMQM9Gla2wWSswggS4xRz6FM0BE9MU0xwc+jPGxuo8OAF2AGj1B9GT2+o8vuFrmsNG2AWyqYE6D0iAG4HHUabI8QyBjrJiRdoF0Bc5jwgudQQQTAA4fFU8Y23e2aLJjuE7Z2NzMp0toW8OgyoNVhwmNACo4ZduLr1xLOexDd+B3jiJWGkAawBWHA4RYEwvemUfxCHYLeCzzgkvgN6Isf2TWROcWBhACLCsnhyHWyg7DQhw8gNCDxZ5oKjrhIEQAQamhLE1Lzgm1mJ4CEPP0EKHV9RVHVp3gjABhv7YaxpogeVbiSabmjMGPWDy5yfmCw7cCaIEICBSukN8/FWObvlzEvok83HUOFMRVM3Uc4AAS38087+5vg8ZPst5TAB6PdG8wnITdS8OENukIhEiua7gBACcNu3fEgjtX29EZsq9Um/gIr1hzLdq3NWoazkBrkIAAWpORXDNDimlsWn4ejS+NIfn1IaVRFgb66pOEFIhcobBiPnPSD8nRg3fiLQ8qza9yUNj3ScHRL4ThAjQ9Of+BZ2t4euaMwh9/3nMtgVkN8uUhagXehokAJX46ApyMBgMQM9yGECkxhvrRoWoI8pYTiLmoCRUgGhefLitCLOlOQD0V4x1TV6XGmmsi422ZCEyVAiJASRVHAyTH6gwy4YvlR9M6H21EaWGraw/WyNbhQNfhRACKv1xzH9yZpHFx03BOQQ9AwHcxB3V/f553wnKMEAJqk434VcnQxMirgybDSeQNMeEnpO9g641eQD3gO6jHJCyYpm6VKcJANogl5Qoja8kZHXIW8t9915t1yOvwf2F/h4n5WUEyMrO5DiBEIdVAirzhybLEBFirg2/h16e+XI3jbI7u6vizvv5j6hLKcBByAn0ifU+AltcDuxZFPFinRtN6MdMnuWFLbx/ZhV169KNEPTGODDCAEkROKn+4S/9lDZBtOAmKdKyKfRrcGexKKTMbYmugHHgb1wxCCBvAjGUcWqG70KfFOjjuDfG3uX7xu80ONCTztYJahVCCShfCokPV3YqwApCz+2pqkKFWOGRcFfjsDPaKjkgTI56DiQVakfOpA0C1D1fvXfwlTlKxsuO4LQxtq50+rgLxu7gLo22XBpADlIxsOqdgDqFEVWe9LQKqjOwGGxF6C+kWPIebSAdN/bGSzZVWFmV3ScYTbmc6rcUD+l1+HEspfMO8fO//vnnnf3V93vkCbr9O1vNUaDvVQvuP+KuczZe3aD4xhYNIAci0C8C0oH4+xPMJwHt2w1Tqv8EreBjVMW60Et+4+DOYcRr9BHjIouG1hUUDionOI8pbbwjoAL9pMHascwNK6VhXgIuxeEI7swJWCIHZAcbZOkuDb0r2BwwVyKzv5G4JSBdLmPjbkXgDnrR5OUqW+rWB86A3h2zlRGtAs4+qU6DwEEqNH3ngEh2goYALcvkhKpQ0qG3F/OAIhNfJl2eaNNKiSgTIg1nmiK6wmHUlRCJBCR+6U9x9uMoamxcnk5hE3p34eYY6PDOGVX3LSaiNGgclEJ05pc9AYfsnHxYorNnfUe6LUBvSI2LewR0RydV3e9O1DIBpEON6FeuUKiNKEQXylQbfp2SqsXkM+SW0LeVTmU+PQo6jjjjGadORjPyqRJBxmiQOHgJ0ZntfIy89ocvw9//+4gHT7aY+1Lly/ATl9C3qQ7DuAMBdmwPMJDvx8lQmehi7+UKRbJ/nu38SU36//IDMtdH8lHCL6HHy2oJWEqubziK3raRwaRChs+ESIPEQUXA++uZ+MFn9eJ5PB3i0+jMfv9RC72UZfptqFzEp5uMbWDRPEpGs9lI0O6UyiFu8wuvZ47h2E7A6xF1yVhBwJMnXXlSdB/EJNw8ozwUp0SadC1rZlXEK2h4Vdn48puTg9IDysodH7MW+62B/pIao67QT2kNYz2iPAO3qKM0VVG+iskXE90uxlPiz3Rzr4hf2RHVg6zHE/o21wRXi8SLBEtu25VdzN9AVhqguWaii+evZOcDUHrQsyj+EvcDv3fQ97/3X/Eokn1pWJvgVkcLUe7Py6XuRxcQxNiy5Yv0XPYYAO/J6H7nHd9ilPAhOu+4/3wRcBl+khbw8E1AD1rvNmjlEbbkr+Z3+aQz19zz/ZeSnPv33v3gz/TE/e1An1msbg6hvEYObkH/Rqq6cXY5n/NKqB7E+Xnv53/S47+Px5+Pp9U/JKn5dECH8v1vc3OpOgXqx19//P6v39OvfMvf7htR+0f7LR9/PyhvOf8lb3/bfvvHb29/vD3N/+1xSv8uQaUKJU7OdOuX2f7nAOq/7jVDoWsfx4uG7SNbe3sf2D7eHz4NqZ6vlycj+08c/dpfhz4NvAPuONVv+j0JoCfKT1f4QVeu/PPQ+nzUIY4iaLuqhevMU/kaZxnqVkCASkP4CwxtOScx3Untolo6/s3UjLaeUTd/JKOHWL2SpXOG0uBemwyhyG/kEbC2+JUgUhpq9egcJq7UrJezldCXRr0PBZ5FiHxMwhylaTGVunJZr0AigEwq0rG93qwozy2aSHZW3x3TLHArRP/K9w/leVl6MZQty3aXG3E9jmvWaTH2hfVqir2sR0R1c9aJ0ZQM+YhLxi7LfXGnXSz9kefvUy5lYa60+opjLup9rHwpDuLg1bhEbrZlIZ4QwfIbqDa7Ki6gD9G3rfKF+7FopQoA58PaFbr5f8lMedQozTnw7RbrNsw86W0Cut0s5WauS/31fVvntOWp7KUTyBwkiYaUBGlaQkb99m0x3Cn5OYaWVqZurXmjPElf7dsgyAUBdJTnWOFAocF3iGkytjWiT5GkXtttYUNvbIcvatTn5IFMQHlwmRexkoNSnTIx1j0dJmObz2QCbaVA3JO/W7HJJi8Qu+UqJwHn8qFS+otnOw4MGkomogGZZqpsbjuLEO4e9MLWLcP9z1r0qT9H1O1fLRlyOFCejDmERMa2BvE0tL2035qCbGhxf1sBbktA6lQomRx4rqA6BPZtt1tAH8A9YZu5+o9gAYsrzDYEnC+XYwCDA9sVpJ9/rYVhCup+gvvkwNsKke3UKvRJrXfKJs9KDUYg4OUGzdqWKtdxOcBoMBxiuwn0hLSeIqs9uZz4Y9l0lWVSFRYqXeoJSAAHQRoMh9igX0axV/1mPmZneHULtV6srkZVYjR+VpulwFAkqYITiBwYrgDTsJ8y+8ZOgVfV9iVY8wYx0o7vWXR7wBgtGWZ67yCvfpxyW2LskL0nvYOb2TspNhxheSBWBQBpZCA7QYL9YMgVpP48FHOFkeZ5ZEJPkeChDWrw3l8caUCVzHZ38E7dAn0XdAP3gcaRYz3zhm+RvnezrR4BwSlp2MLTfsmrOsX7RULRNVTUJSAaS4nmuci3VSFpYHEO34REgIHvzF6XDMveF/VKdQwf7o9qdb40zD8t6Hfq+IHVJQPOc2K4z0C/+CavCx8kYICDZHSEnMQ9xS+VMNGmH+3Pw51HhBrrT1zQIMTBBo1rQhemmbxExSfcysBw5qB9DEhFGCjHya64G/GgzkGzXic27f0+6M3r44xhveRaNpa9p7gfaFEXwn3uwiwzZeoxtXeQMscNkAoNcZAX4J5iV4VyNccwe3VmUUt7kIzIvWhidX62RT/EQUZx99QfvCKaqzm3hIH5q1ZyDWMa2gbTX/kDxd3sTjsIPVK/jFJi9xmx34oR4KgQVnI4uqEO1dSqIsEw9J9j+LD5jxCQRveCva49MWDymtCHwmz8sipr0h8fJo8A8KJ+gBPkEdzjl6BOwGXAb0/8GTJMBi9kA6qQ+VKOZXhDl19PbuFeM3wKe4a73xwxf2F3XfCC6aAQ5ajJr4eeYsQPZvqu+S8kAHaCHDX5m6CnTyw1oA0f4wREnSA7Jm8ANKn1Y+PeVYNe8K1BAgIqFMp5BqD3Af20Kyz2JTDM/LXQESBgJOex1QaDHom03/HGXgqU/FJP3X9P/ag8qzYg9O5HgOuuQFn3Ls2U8E4lWoMBv4GY/xF5Sm1w6H8Vw9fEZogAV4XUq9rfC/2XKH7I/O8gIAEZp9xkf5XgLMl2sL21qzKfRQSwn3Gi+/lpAsqvyPHDuX+IMIgAQYWyozYR6EOaM2XpS9J8HnlGbfGDlZobFcoxoQ9B70Aa2HgU9ZJQP0cOqlKMADMM5IDQG1AShTTnu+RCQ+afBppciVdo2ee2AojQBPS/Vo45RkCkxnnMbU1AP5bJfFmiqaaeY9nRLAF5EvrFhr9WmmJpzID5x880U2Eeh57GQ+gXSA/P6U+6ocK8eGBF+IELrqt+30BskAC/wjwA/Yzh0+cbOVoScHJWHurIz3ZHyNXQf9+MiOeeHCUgL7NEuktDvqbwMOI9YQJyFMqY3NNXuwjfYumK+YcJy2ug/78w/Hnzt8bAjKC/EPo1xWZ4KM5DOA7rzAoC8i2CY4D4TYKwqTOTtOEE5Fno7zT8b50R2ckrRkCeAotWGP7d6T+PHIyaP099pf8JMABnLdEnfEw5CgAAAABJRU5ErkJggg==",
        gradient = new Image();
    gradient.src = imgdata;

    /** Percentage loader
     * @param	params	Specify options in {}. May be on of width, height, progress or value.
     *
     * @example $("#myloader-container).percentageLoader({
		    width : 256,  // width in pixels
		    height : 256, // height in pixels
		    progress: 0,  // initialise progress bar position, within the range [0..1]
		    value: '0kb'  // initialise text label to this value
		});
     */
    $.fn.percentageLoader = function (params) {
        var settings, canvas, percentageText, valueText, items, i, item, selectors, s, ctx, progress,
            value, cX, cY, lingrad, innerGrad, tubeGrad, innerRadius, innerBarRadius, outerBarRadius,
            radius, startAngle, endAngle, counterClockwise, completeAngle, setProgress, setValue,
            applyAngle, drawLoader, clipValue, outerDiv;

        /* Specify default settings */
        settings = {
            width: 256,
            height: 256,
            progress: 0,
            value: '0kb',
            controllable: false
        };

        /* Override default settings with provided params, if any */
        if (params !== undefined) {
            $.extend(settings, params);
        } else {
            params = settings;
        }

        outerDiv = document.createElement('div');
        outerDiv.style.width = settings.width + 'px';
        outerDiv.style.height = settings.height + 'px';
        outerDiv.style.position = 'relative';

        $(this).append(outerDiv);

        /* Create our canvas object */
        canvas = document.createElement('canvas');
        canvas.setAttribute('width', settings.width);
        canvas.setAttribute('height', settings.height);
        outerDiv.appendChild(canvas);

        /* Create div elements we'll use for text. Drawing text is
         * possible with canvas but it is tricky working with custom
         * fonts as it is hard to guarantee when they become available
         * with differences between browsers. DOM is a safer bet here */
        percentageText = document.createElement('div');
        percentageText.style.width = (settings.width.toString() - 2) + 'px';
        percentageText.style.textAlign = 'center';
        percentageText.style.height = '50px';
        percentageText.style.left = 0;
        percentageText.style.position = 'absolute';

        valueText = document.createElement('div');
        valueText.style.width = (settings.width - 2).toString() + 'px';
        valueText.style.textAlign = 'center';
        valueText.style.height = '0px';
        valueText.style.overflow = 'hidden';
        valueText.style.left = 0;
        valueText.style.position = 'absolute';

        /* Force text items to not allow selection */
        items = [valueText, percentageText];
        for (i  = 0; i < items.length; i += 1) {
            item = items[i];
            selectors = [
                '-webkit-user-select',
                '-khtml-user-select',
                '-moz-user-select',
                '-o-user-select',
                'user-select'];

            for (s = 0; s < selectors.length; s += 1) {
                $(item).css(selectors[s], 'none');
            }
        }

        /* Add the new dom elements to the containing div */
        outerDiv.appendChild(percentageText);
        outerDiv.appendChild(valueText);

        /* Get a reference to the context of our canvas object */
        ctx = canvas.getContext("2d");


        /* Set various initial values */

        /* Centre point */
        cX = (canvas.width / 2) - 1;
        cY = (canvas.height / 2) - 1;

        /* Create our linear gradient for the outer ring */
        lingrad = ctx.createLinearGradient(cX, 0, cX, canvas.height);
        lingrad.addColorStop(0, '#d1d3d5');
        lingrad.addColorStop(1, '#eff0f1');

        /* Create inner gradient for the outer ring */
        innerGrad = ctx.createLinearGradient(cX, cX * 0.133333, cX, canvas.height - cX * 0.133333);
        innerGrad.addColorStop(0, '#fcfdfe');
        innerGrad.addColorStop(1, '#e8e9eb');

        /* Tube gradient (background, not the spiral gradient) */
        tubeGrad = ctx.createLinearGradient(cX, 0, cX, canvas.height);
        tubeGrad.addColorStop(0, '#f6f7f8');
        tubeGrad.addColorStop(1, '#f6f7f8');

        /* The inner circle is 2/3rds the size of the outer one */
        innerRadius = cX * 0.6666;
        /* Outer radius is the same as the width / 2, same as the centre x
        * (but we leave a little room so the borders aren't truncated) */
        radius = cX - 2;

        /* Calculate the radii of the inner tube */
        innerBarRadius = innerRadius + (cX * 0.06);
        outerBarRadius = radius - (cX * 0.06);

        /* Bottom left angle */
        startAngle = 2.5307963267949;
        /* Bottom right angle */
        endAngle = 0.9707963267949 + (Math.PI * 2.0);

        /* Nicer to pass counterClockwise / clockwise into canvas functions
        * than true / false */
        counterClockwise = false;

        /* Borders should be 1px */
        ctx.lineWidth = 1;

        /**
         * Little helper method for transforming points on a given
         * angle and distance for code clarity
         */
        applyAngle = function (point, angle, distance) {
            return {
                x : point.x + (Math.cos(angle) * distance),
                y : point.y + (Math.sin(angle) * distance)
            };
        };


        /**
         * render the widget in its entirety.
         */
        drawLoader = function () {
            /* Clear canvas entirely */
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            /*** IMAGERY ***/

            /* draw outer circle */
            ctx.fillStyle = lingrad;
            ctx.beginPath();
            ctx.strokeStyle = '#bfc0c3';
            ctx.arc(cX, cY, radius, 0, Math.PI * 2, counterClockwise);
            ctx.fill();
            ctx.stroke();

            /* draw inner circle */
            ctx.fillStyle = innerGrad;
            ctx.beginPath();
            ctx.arc(cX, cY, innerRadius, 0, Math.PI * 2, counterClockwise);
            ctx.fill();
            ctx.strokeStyle = '#bfc0c3';
            ctx.stroke();

            ctx.beginPath();

            /**
             * Helper function - adds a path (without calls to beginPath or closePath)
             * to the context which describes the inner tube. We use this for drawing
             * the background of the inner tube (which is always at 100%) and the
             * progress meter itself, which may vary from 0-100% */
            function makeInnerTubePath(startAngle, endAngle) {
                var centrePoint, startPoint, controlAngle, capLength, c1, c2, point1, point2;
                centrePoint = {
                    x : cX,
                    y : cY
                };

                startPoint = applyAngle(centrePoint, startAngle, innerBarRadius);

                ctx.moveTo(startPoint.x, startPoint.y);

                point1 = applyAngle(centrePoint, endAngle, innerBarRadius);
                point2 = applyAngle(centrePoint, endAngle, outerBarRadius);

                controlAngle = endAngle + (3.142 / 2.0);
                /* Cap length - a fifth of the canvas size minus 4 pixels */
                capLength = (cX * 0.20) - 4;

                c1 = applyAngle(point1, controlAngle, capLength);
                c2 = applyAngle(point2, controlAngle, capLength);

                ctx.arc(cX, cY, innerBarRadius, startAngle, endAngle, false);
                ctx.bezierCurveTo(c1.x, c1.y, c2.x, c2.y, point2.x, point2.y);
                ctx.arc(cX, cY, outerBarRadius, endAngle, startAngle, true);

                point1 = applyAngle(centrePoint, startAngle, innerBarRadius);
                point2 = applyAngle(centrePoint, startAngle, outerBarRadius);

                controlAngle = startAngle - (3.142 / 2.0);

                c1 = applyAngle(point2, controlAngle, capLength);
                c2 = applyAngle(point1, controlAngle, capLength);

                ctx.bezierCurveTo(c1.x, c1.y, c2.x, c2.y, point1.x, point1.y);
            }

            /* Background tube */
            ctx.beginPath();
            ctx.strokeStyle = '#bfc0c3';
            makeInnerTubePath(startAngle, endAngle);

            ctx.fillStyle = tubeGrad;
            ctx.fill();
            ctx.stroke();

            /* Calculate angles for the the progress metre */
            completeAngle = startAngle + (progress * (endAngle - startAngle));

            ctx.beginPath();
            makeInnerTubePath(startAngle, completeAngle);

            /* We're going to apply a clip so save the current state */
            ctx.save();
            /* Clip so we can apply the image gradient */
            ctx.clip();

            /* Draw the spiral gradient over the clipped area */
            ctx.drawImage(gradient, 0, 0, canvas.width, canvas.height);

            /* Undo the clip */
            ctx.restore();

            /* Draw the outline of the path */
            ctx.beginPath();
            makeInnerTubePath(startAngle, completeAngle);
            ctx.stroke();

            /*** TEXT ***/
            (function () {
                var fontSize, string, smallSize, heightRemaining;
                /* Calculate the size of the font based on the canvas size */
                fontSize = cX / 2;

                percentageText.style.top = ((settings.height / 2) - (fontSize / 2)-9).toString() + 'px';
                percentageText.style.color = '#004b76';
                percentageText.style.font = fontSize.toString() + 'px PT Sans';
                percentageText.style.fontWeight = 'Bold';
                percentageText.style.textShadow = '0 2px 0px #FFFFFF';

                /* Calculate the text for the given percentage */
                string = (progress * 100.0).toFixed(0) + '%';

                percentageText.innerHTML = string;

                /* Calculate font and placement of small 'value' text */
                smallSize = cX / 5.5;
                valueText.style.color = '#80a9c8';
                valueText.style.font = smallSize.toString() + 'px PT Sans';
                valueText.style.height = smallSize.toString() + 'px';
                valueText.style.textShadow = 'None';

                /* Ugly vertical align calculations - fit into bottom ring.
                 * The bottom ring occupes 1/6 of the diameter of the circle */
                heightRemaining = (settings.height * 0.16666666) - smallSize;
                valueText.style.top = ((settings.height * 0.8333333) + (heightRemaining / 4)).toString() + 'px';
            }());
        };

        /**
        * Check the progress value and ensure it is within the correct bounds [0..1]
        */
        clipValue = function () {
            if (progress < 0) {
                progress = 0;
            }

            if (progress > 1.0) {
                progress = 1.0;
            }
        };

        /* Sets the current progress level of the loader
         *
         * @param value the progress value, from 0 to 1. Values outside this range
         * will be clipped
         */
        setProgress = function (value) {
            /* Clip values to the range [0..1] */
            progress = value;
            clipValue();
            drawLoader();
        };

        this.setProgress = setProgress;

        setValue = function (val) {
            value = val;
            valueText.innerHTML = value;
        };

        this.setValue = setValue;
        this.setValue(settings.value);

        progress = settings.progress;
        clipValue();

        /* Do an initial draw */
        drawLoader();

        /* In controllable mode, add event handlers */
        if (params.controllable === true) {
            (function () {
                var mouseDown, getDistance, adjustProgressWithXY;
                getDistance = function (x, y) {
                    return Math.sqrt(Math.pow(x - cX, 2) + Math.pow(y - cY, 2));
                };

                mouseDown = false;

                adjustProgressWithXY = function (x, y) {
                    /* within the bar, calculate angle of touch point */
                    var pX, pY, angle, startTouchAngle, range, posValue;
                    pX = x - cX;
                    pY = y - cY;

                    angle = Math.atan2(pY, pX);
                    if (angle > Math.PI / 2.0) {
                        angle -= (Math.PI * 2.0);
                    }

                    startTouchAngle = startAngle - (Math.PI * 2.0);
                    range = endAngle - startAngle;
                    posValue = (angle - startTouchAngle) / range;
                    setProgress(posValue);

                    if (params.onProgressUpdate) {
                        /* use the progress value as this will have been clipped
                         * to the correct range [0..1] after the call to setProgress
                         */
                        params.onProgressUpdate(progress);
                    }
                };

                $(outerDiv).mousedown(function (e) {
                    var offset, x, y, distance;
                    offset = $(this).offset();
                    x = e.pageX - offset.left;
                    y = e.pageY - offset.top;

                    distance = getDistance(x, y);

                    if (distance > innerRadius && distance < radius) {
                        mouseDown = true;
                        adjustProgressWithXY(x, y);
                    }
                }).mouseup(function () {
                    mouseDown = false;
                }).mousemove(function (e) {
                    var offset, x, y;
                    if (mouseDown) {
                        offset = $(outerDiv).offset();
                        x = e.pageX - offset.left;
                        y = e.pageY - offset.top;
                        adjustProgressWithXY(x, y);
                    }
                }).mouseleave(function () {
                    mouseDown = false;
                });
            }());
        }
        return this;
    };
}(jQuery));
