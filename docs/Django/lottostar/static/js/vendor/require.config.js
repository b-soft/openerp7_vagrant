var jam = {
    "packages": [
        {
            "name": "backbone.marionette",
            "location": "static/js/vendor/backbone.marionette",
            "main": "lib/core/amd/backbone.marionette.js"
        },
        {
            "name": "jquery",
            "location": "static/js/vendor/jquery",
            "main": "dist/jquery.js"
        },
        {
            "name": "backbone",
            "location": "static/js/vendor/backbone",
            "main": "backbone.js"
        },
        {
            "name": "underscore",
            "location": "static/js/vendor/underscore",
            "main": "underscore.js"
        },
        {
            "name": "backbone.babysitter",
            "location": "static/js/vendor/backbone.babysitter",
            "main": "lib/amd/backbone.babysitter.js"
        },
        {
            "name": "backbone.wreqr",
            "location": "static/js/vendor/backbone.wreqr",
            "main": "lib/amd/backbone.wreqr.js"
        }
    ],
    "version": "0.2.17",
    "shim": {
        "backbone": {
            "deps": [
                "underscore",
                "jquery"
            ],
            "exports": "Backbone"
        },
        "underscore": {
            "exports": "_"
        }
    }
};

if (typeof require !== "undefined" && require.config) {
    require.config({
    "packages": [
        {
            "name": "backbone.marionette",
            "location": "static/js/vendor/backbone.marionette",
            "main": "lib/core/amd/backbone.marionette.js"
        },
        {
            "name": "jquery",
            "location": "static/js/vendor/jquery",
            "main": "dist/jquery.js"
        },
        {
            "name": "backbone",
            "location": "static/js/vendor/backbone",
            "main": "backbone.js"
        },
        {
            "name": "underscore",
            "location": "static/js/vendor/underscore",
            "main": "underscore.js"
        },
        {
            "name": "backbone.babysitter",
            "location": "static/js/vendor/backbone.babysitter",
            "main": "lib/amd/backbone.babysitter.js"
        },
        {
            "name": "backbone.wreqr",
            "location": "static/js/vendor/backbone.wreqr",
            "main": "lib/amd/backbone.wreqr.js"
        }
    ],
    "shim": {
        "backbone": {
            "deps": [
                "underscore",
                "jquery"
            ],
            "exports": "Backbone"
        },
        "underscore": {
            "exports": "_"
        }
    }
});
}
else {
    var require = {
    "packages": [
        {
            "name": "backbone.marionette",
            "location": "static/js/vendor/backbone.marionette",
            "main": "lib/core/amd/backbone.marionette.js"
        },
        {
            "name": "jquery",
            "location": "static/js/vendor/jquery",
            "main": "dist/jquery.js"
        },
        {
            "name": "backbone",
            "location": "static/js/vendor/backbone",
            "main": "backbone.js"
        },
        {
            "name": "underscore",
            "location": "static/js/vendor/underscore",
            "main": "underscore.js"
        },
        {
            "name": "backbone.babysitter",
            "location": "static/js/vendor/backbone.babysitter",
            "main": "lib/amd/backbone.babysitter.js"
        },
        {
            "name": "backbone.wreqr",
            "location": "static/js/vendor/backbone.wreqr",
            "main": "lib/amd/backbone.wreqr.js"
        }
    ],
    "shim": {
        "backbone": {
            "deps": [
                "underscore",
                "jquery"
            ],
            "exports": "Backbone"
        },
        "underscore": {
            "exports": "_"
        }
    }
};
}

if (typeof exports !== "undefined" && typeof module !== "undefined") {
    module.exports = jam;
}