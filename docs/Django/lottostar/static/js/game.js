var widgets = {}, t, costTimeout, winningTimeout, et, msgerror, error_tickets = [], prog_an;
var Game;

var duration_el, day_sel_el, shedule_el;


$.fn.hasAttr = function(name) {
    return this.attr(name) !== undefined;
};

function time_get_count(new_date){
    function pad(n){ return n<10 ? '0'+n : n }

    var nt = new Date(new_date);
    var dn = new Date($("#server_timestamp").val());

    var seconds = (nt.getTime() - dn.getTime()) / 1000;
    // if time is already past
    if(seconds < 0){
        return 0;
    }else{
        return Math.floor(seconds);
    }
}

jQuery(document).ready(function($) {
    duration_el = $("#id_draws"), day_sel_el = $("#day-selector"), shedule_el = $("#schedule_start_date");

    $('#game-block').css({"display": "block" });
    $("#deckloader").css({"display": "none"});

    $("#id_prev").css({"display": "none"});
    $("#id_next").css({"display": "none"});

    $("#btn_tour_all").click(function(){
        $(document).foundation('joyride', 'start');
    });

//    $(document).foundation('joyride', 'start');

    widgets = {
        random_number_list: [],
        boost_value: boost_value,
        boost_max_range: boost_max_range[0],
        boost_min_range: boost_min_range[0],
        continuous_deck: false,

        update_game: function(data){
            if(data.status == "limit"){
                openMessage(locale_exceeded_entry_limit, "error");
                return false;
            }else{
                $('#game_jackpot').html("&euro; " + data.jackpot);
                $('#id_total_cost').val(parseFloat(data.total_cost));
                $("#game_cost").html("&euro; " + data.total_cost_format);
                $("#page_no").html(data.page);

                // calculate_winning_percentage
                widgets.guage.update(parseInt(data.percentage));

                var html, has_entry;
                for(var $i=1;$i<=entries;$i++){
                    has_entry = false;
                    for(var $d = 0; $d <= data.entries.length - 1; $d++){
                        if(data.entries[$d].ticket == $i){
                            has_entry = data.entries[$d];
                        }
                    }
                    if(has_entry){
                        if(has_entry.number_of_tickets == 0){
                            html = "Incomplete";
                        }else{
                            html = has_entry.number_of_tickets;
                            if(has_entry.number_of_tickets == 1){
                                html += " entry";
                            }else{
                                html += " entries";
                            }
                        }
                    }else{
                        html = "--";
                    }
                    $("#ticket_system_" + $i).html(html);
                }

                widgets.active_next_button();
                widgets.active_prev_button();
                widgets.validate_submit();
                return true;
            }
        },

        boost:{
            init:function(){
                $(".nitro").noUiSlider({range: [0, widgets.boost_max_range], start: widgets.boost_value, step: widgets.boost_min_range, handles: 1,
                    slide: function(){
                        widgets.boost_value = $(this).val();
                        var params = {
                            "csrfmiddlewaretoken": crosssrf,
                            "product": product_id,
                            "boost": widgets.boost_value
                        };

                        if(edit_id){ params["edit_id"] = edit_id }

                        $.ajax({
                            type: "POST",
                            url:"/game/ticket/boost/",
                            data: params,
                            dataType: "json",
                            success: function(data){
                                widgets.update_game(data);
                            },
                            error: function(code){ console.log(code);}
                        })
                    }
                });
            }
        },
        guage:{
            topLoader: null,
            percentage: game_percentage,
            init:function(){

                this.topLoader = $("#topLoader").percentageLoader({
                    width: 256, height: 256,
                    controllable: false,
                    progress: (widgets.guage.percentage / 100),
                    onProgressUpdate: function (val) {
                        widgets.guage.topLoader.setValue(Math.floor(Math.round(val * 100)));
                    }
                });

                var ticket_hover = function(){
                    for(var $i=1;$i<=entries;$i++){
                        $("#ticket_" + $i).css({"z-index": 1, "position": "relative"});
                    }
                };

                var ticket_el;
                for(var $i=1;$i<=entries;$i++){
                    ticket_el = $("#ticket_" + $i);
                    ticket_el.mouseenter(function(){
                        ticket_hover();
                        $(this).css({"z-index": 100});
                    });
                    ticket_el.mouseleave(function(){ ticket_hover(); });
                }

                var scoller_width = 0;
                $(".row_deck").each(function(){
                    scoller_width += ($(this).width() + 141);
                });
                $("#scroller").css("width", scoller_width + "px");
            },
            update:function(newpos){
                if(newpos >= 100){
                    newpos = 99;
                }
                var going_up = false;
                if(widgets.guage.percentage <= newpos){
                    going_up = true;
                }

                /* start */
                var animateFunc = function() {
                    try{ clearTimeout(prog_an); }catch(e){};
                    if(going_up){
                        if(widgets.guage.percentage <= newpos-1){
                            widgets.guage.percentage += 1;
                            widgets.guage.topLoader.setProgress(widgets.guage.percentage / 100);
                            prog_an = setTimeout(animateFunc, 25);
                        }else{
                            widgets.guage.percentage = newpos;
                            widgets.guage.topLoader.setProgress(widgets.guage.percentage / 100);
                        }
                    }else{
                        if(widgets.guage.percentage >= newpos-1){
                            widgets.guage.percentage -= 1;
                            widgets.guage.topLoader.setProgress(widgets.guage.percentage / 100);
                            prog_an = setTimeout(animateFunc, 25);
                        }else{
                            widgets.guage.percentage = newpos;
                            widgets.guage.topLoader.setProgress(widgets.guage.percentage / 100);
                        }
                    }
                };
                animateFunc();
            }
        },
        is_unique_rand: function(value){
            var return_val = true;
            for(var $k=0; $k <= widgets.random_number_list.length-1; $k++){
                if(value == widgets.random_number_list[$k]){
                    return_val = false;
                }
            }
            return return_val;
        },
        get_rand: function(max_no){ return Math.floor(Math.random()*max_no) + 1; },
        get_random_numbers: function(max_no, amount, exclude_number){
            var random_number = widgets.get_rand(max_no);
            if(widgets.is_unique_rand(random_number) && random_number != exclude_number){
                widgets.random_number_list.push(random_number);
            }else{
                widgets.get_random_numbers(max_no, amount, exclude_number);
            }
            if(widgets.random_number_list.length < amount){
                widgets.get_random_numbers(max_no, amount, exclude_number);
            }
        },
        get_numbers: function(max_no, amount, exclude_number){
            widgets.random_number_list = [];
            widgets.get_random_numbers(max_no, amount, exclude_number);
            return widgets.random_number_list;
        },

        select_numbers: function(ticket, numbers){
            var ticket_name = ticket.split("_");
            var ticket_el, el, c, item, numbers_list;
            var number_ballset = String(numbers).split("_");
            c = 0;
            for (var ball_index in ballset) {
                item = ballset[ball_index];

                ticket_el = $("#ticket_" + ticket_name[2] + " .playblock_ballset_" + item.id + " a");

                // reset the ticket
                ticket_el.each(function(){  if($(this).hasClass("on")){ $(this).removeClass("on"); }  });

                numbers_list = String(number_ballset[c]).split("-");

                ticket_el.each(function(){
                    for(var $t=0; $t <= numbers_list.length - 1; $t++){
                        el = $(this);
                        if(parseInt(el.html()) == numbers_list[$t]){
                            el.addClass("on");
                        }
                    }
                });

                c++;
            }

            var addon_name = String(ticket).split("_");
            widgets.update_exclusive_addon("#id_bonus_" + addon_name[2] + "_" +  addon.id);

            widgets.validate_submit();
        },

        send_ticket_numbers: function(ticket, numbers){

            var ticket_name = ticket.split("_");
            var params = {
                "csrfmiddlewaretoken": crosssrf,
                "product": ticket_name[1],
                "ticket": ticket_name[2],
                "numbers": numbers
            };

            if(edit_id){ params["edit_id"] = edit_id }

            $.ajax({
                type: "POST",
                url:"/game/ticket/quickpick/",
                data: params,
                dataType: "json",
                success: function(data){
                    if(widgets.update_game(data)){
                        widgets.select_numbers(ticket,numbers);
                    }
                },
                error: function(code){ console.log(code);}
            });
        },

        quickpick: function(ticket){
            var count_main, main_min, el, cc, c, ticket_el, item;
            var ticket_name = ticket.split("_");
            var number_sel = "", exclude_number = 0;

            c = 0;
            for (var ball_index in ballset) {
                item = ballset[ball_index];
                ticket_el = $("#ticket_" + ticket_name[2] + " .playblock_ballset_" + item.id + " a");

                // check selection count greater that item.main - set minimum random amount to count of selection
                // else minimum is set to item.main
                count_main = 0;
                ticket_el.each(function(){ if($(this).hasClass("on")){ count_main++; } });

                main_min = item.main;

                if(count_main >= main_min){ main_min = count_main; }

                if(exclude_selection){
                    exclude_number = $("#id_bonus_" + ticket_name[2] + "_" + addon.id).val();
                }

                var numbers_list = widgets.get_numbers(item.max, main_min, exclude_number);

                if(c != 0){ number_sel += "_"; }
                number_sel += "" + String(numbers_list);
                c++;
            }
            // cleanup
            number_sel = number_sel.replace(/ /g, "").replace(/,/g,"-");
            return number_sel;
        },

        quickpick_all: function(){
            var go_next = true, start_loop, end_loop, number_sel;

            if(page == 1){
                start_loop = 1; end_loop = 4;
            }else{
                start_loop = (((page-1) * 4) + 1);
                end_loop = ((page-1) * 4) + 4;
            }

            var ticket_complete, number_sel, ticket_no;
            var incomplete_tickets = [];
            for(var $i=start_loop;$i<=end_loop;$i++){
                ticket_complete = true;

                for (var ball in ballset) {
                    var item = ballset[ball];
                    if($("#ticket_" + $i + " .playblock_ballset_" + item.id + " .on").length < item.main){
                        ticket_complete = false;
                    }
                }

                if(ticket_complete == false){
                    incomplete_tickets.push($i);
                }
            }

            var params = {
                "csrfmiddlewaretoken": crosssrf,
                "product": product_id
            };

            if(incomplete_tickets.length){
                for(var $i = 0; $i <= incomplete_tickets.length - 1; $i++){
                    ticket_no = "ticket_" + product_id + "_" + incomplete_tickets[$i];
                    number_sel = widgets.quickpick(ticket_no);
                    params["number" + incomplete_tickets[$i]] = number_sel;
                    widgets.select_numbers(ticket_no, number_sel);
                }
            }else{
                for(var $i=start_loop;$i<=end_loop;$i++){
                    ticket_no = "ticket_" + product_id + "_" + $i;
                    number_sel = widgets.quickpick(ticket_no);
                    params["number" + $i] = number_sel;
                    widgets.select_numbers(ticket_no, number_sel);
                }
            }
            if(edit_id){ params["edit_id"] = edit_id }

            $.ajax({
                type: "POST",
                url:"/game/ticket/quickpick-all/",
                data: params,
                dataType: "json",
                success: function(data){
                    if(widgets.update_game(data)){
                        widgets.validate_submit();
                    }
                },
                error: function(code){ console.log(code);}
            });
        },

        refresh_ticket: function(ticket, refresh_addon){
            var ticket_name = ticket.split("_");
            for (var ball in ballset) {
                var item = ballset[ball];
                $("#ticket_" + ticket_name[2] + " .playblock_ballset_" + item.id + " a.on").each(function(){
                    $(this).removeClass("on");
                });
            }

            if(addon && refresh_addon){
                $("#id_bonus_" + ticket_name[2] + "_" + addon.id + ' option').each(function(item){
                    $(this).removeAttr('selected');
                });
                $("#id_bonus_" + ticket_name[2] + "_" + addon.id + " option:first-child").attr('selected', 'selected');
                $("#id_bonus_" + ticket_name[2] + "_" + addon.id).parent().find(".dropdown .current").text('--');
                $("#label_addon_" + ticket_name[2] + "_" + addon.id).removeClass("active");
                widgets.update_exclusive_addon("#id_bonus_" + ticket_name[2] + "_" + addon.id);
            }
        },

        max_all: function(){
            var params = {
                "csrfmiddlewaretoken": crosssrf,
                "product": product_id,
                "boost": boost_max_range[0]
            };

            $(".nitro").val(Number(boost_max_range[0]));

            if(edit_id){ params["edit_id"] = edit_id }

            $.ajax({
                type: "POST",
                url:"/game/ticket/boost/",
                data: params,
                dataType: "json",
                success: function(data){
                    widgets.addon_all();

                },
                error: function(code){ console.log(code);}
            })
        },

        random_no_confict: function(ticket_list, number){
            if(ticket_list.indexOf(number) >= 0){
                return widgets.random_no_confict(ticket_list, widgets.get_rand(addon.max));
            }else{
                return number;
            }
        },

        addon_all: function(){

            var start_loop, end_loop, random_number, number_val = "";

            if(page == 1){
                start_loop = 1; end_loop = 4;
            }else{
                start_loop = (((page-1) * 4) + 1);
                end_loop = ((page-1) * 4) + 4;
            }

            var incomplete_bonus = [];

            var addon_added = 0;
            var reset_all = false;

            for(var $i=start_loop;$i<=end_loop;$i++){
                for (var ball in ballset) {
                    var item = ballset[ball];
                    if(addon){
                        if(Number($("#id_bonus_" + $i + "_" + addon.id).val()) != 0){
                            addon_added += 1;
                        }
                    }
                }
            }

            if(addon_added >= 4){
                reset_all = true;
            }

            var params = {
                "csrfmiddlewaretoken": crosssrf,
                "product": product_id
            };
            if(edit_id){ params["edit_id"] = edit_id }

            var bonus_el, num_val, num_el, ticket_list = [];

            for(var $i=start_loop;$i<=end_loop;$i++){
                for (var ball in ballset) {
                    var item = ballset[ball];
                    if(addon){
                        bonus_el = $("#id_bonus_" + $i + "_" + addon.id);
                        if(Number(bonus_el.val()) == 0 || reset_all){
                            // todo: if exclude selection is on, make sure the
                            // random number is not selected in the ballsets

                            random_number = widgets.get_rand(addon.max);
                            if(exclude_selection){
                                ticket_list = [];
                                $("#ticket_" + $i + " .playblock_ballset_" + item.id + " .on").each(function(){
                                    ticket_list.push(parseInt($(this).text()))
                                });
                                random_number = widgets.random_no_confict(ticket_list, random_number);
                            }

                            params["number" + $i] = random_number;
                            $("#id_bonus_" + $i + "_" + addon.id).val(random_number);
                            $("#id_bonus_" + $i + "_" + addon.id).parent().find(".dropdown .current").text(random_number);
                            $("#label_addon_" + $i + "_" + addon.id).addClass("active");

                            if(exclude_selection){
                                // get first ballset_id
                                var ballset_id = 0, $c = 0;
                                for (var ball_index in ballset) { if($c == 0){ ballset_id = ballset[ball_index].id; } $c++; }

                                $("#ticket_" + $i + " .playblock_ballset_" + ballset_id).find("a").each(function(){
                                    if($(this).hasClass("off")){
                                        $(this).removeClass("off");
                                    }

                                    if(!$(this).hasClass("on")){
                                        num_val = $(this).text();
                                        num_el = $(this);
                                        bonus_el.parent().find(".dropdown ul li").each(function(){
                                            if(String($(this).text()) == "--" || parseInt($(this).text()) == parseInt(num_val)){
                                                $(this).removeClass("hide");
                                            }
                                        });

                                        if(String($(this).text()) == "--" || parseInt(bonus_el.parent().find(".dropdown .current").text()) == parseInt(num_val)){
                                            $(this).addClass("off");
                                        }
                                    }
                                });
                             }
                        }
                    }
                }
            }

            if(addon){
                $.ajax({
                        type: "POST",
                        url:"/game/ticket/addon-all/",
                        data: params,
                        dataType: "json",
                        success: function(data){
                            widgets.update_game(data);
                        },
                        error: function(code){ console.log(code);}
                    }
                );
            }

            var ticket_complete = false;
            var incomplete_tickets = [];
            for(var $i=start_loop;$i<=end_loop;$i++){
                ticket_complete = true;

                for (var ball in ballset) {
                    var item = ballset[ball];
                    if($("#ticket_" + $i + " .playblock_ballset_" + item.id + " .on").length < item.main){
                        ticket_complete = false;
                    }
                }
                if(ticket_complete == false){
                    incomplete_tickets.push($i);
                }
            }
            if(incomplete_tickets.length){
                widgets.quickpick_all();
            }

        },

        clear: function(ticket){
            var ticket_name = ticket.split("_");

            var params = {
                "csrfmiddlewaretoken": crosssrf,
                "product": ticket_name[1],
                "ticket": ticket_name[2]
            };
            if(edit_id){ params["edit_id"] = edit_id }

            $.ajax({
                    type: "POST",
                    url:"/game/ticket/clear/",
                    data: params,
                    dataType: "json",
                    success: function(data){
                        widgets.update_game(data);
                    },
                    error: function(code){ console.log(code);}
                }
            );
            widgets.refresh_ticket(ticket, true);
        },

        check_page_deck: function(){
            var go_next = true, start_loop, end_loop, last_success;

            var set_page = page - 1;

            if(set_page == 0){
                start_loop = 1;
                end_loop = 4;
            }else{
                start_loop = ((set_page-1) * 4) + 1;
                end_loop = ((set_page-1) * 4) + 4;
            }

            for(var $i=start_loop;$i<=end_loop;$i++){
                for (var ball in ballset) {
                    var item = ballset[ball];
                    if($("#ticket_" + $i + " .playblock_ballset_" + item.id + " .on").length < ballset[ball].main){
                        go_next = false;
                    }
                }
            }

            if(go_next == true){
                if(page == 1){
                    $("#scroller").animate({"margin-left":"0px"}, {duration: 1000, complete: function(){
                        widgets.active_next_button();
                        widgets.active_prev_button();
                    }});
                }else{
                    var margin_goto = 0;
                    for(var $p = 1; $p< page; $p++){
                        margin_goto += Number($("#row_deck_" + $p).width() + 208);
                    }
                    $("#scroller").css({"margin-left": "-" + margin_goto + "px"});
                    widgets.active_next_button();
                    widgets.active_prev_button();
                }
            }else{
                page = 1;
            }
        },

/* -------------------------------- VALIDATE  -------------------------------- */

        validate_submit: function(){
            widgets.active_next_button();
            widgets.active_prev_button();

            var is_valid = true, ticket_is_entered = false, has_addon = false, addon_val = 0;
            var entered_tickets = 0, ticket_has_no_errors = true; error_tickets = [];
            var has_addon_selected = false;


            for(var $k=0; $k < entries; $k++){
                $("#ticket_" + $k).removeClass("error");
            }

            for(var $i=1;$i<=entries;$i++){
                addon_val = parseInt($("#id_bonus_" + $i + "_" + addon.id).val());
                if(addon_val != 0){
                    has_addon_selected = true;
                }
            }
            for(var $i=1;$i<=entries;$i++){
                has_addon = false;
                ticket_is_entered = false;
                ticket_has_no_errors = true;

                for (var ball in ballset) {
                    var item = ballset[ball];
                    if($("#ticket_" + $i + " .playblock_ballset_" + item.id + " .on").length != 0){
                        ticket_is_entered = true;
                    }
                }
                if(ticket_is_entered == true){
                    entered_tickets += 1;
                    for (var ball in ballset) {
                        var item = ballset[ball];
                        if($("#ticket_" + $i + " .playblock_ballset_" + item.id + " .on").length < ballset[ball].main){
                            is_valid = false;
                            ticket_has_no_errors = false;
                        }
                    }
                }
                if(addon){
                    addon_val = parseInt($("#id_bonus_" + $i + "_" + addon.id).val());
                    if(addon_val != 0){
                        has_addon = true;
                    }
                    if(has_addon && !ticket_is_entered){
                        is_valid = false;
                        ticket_has_no_errors = false;
                    }

                    if(has_addon_selected){
                        if(addon_val == 0 && ticket_is_entered){
                            is_valid = false;
                            ticket_has_no_errors = false;
                        }
                    }
                }

                if(!ticket_has_no_errors){
                    error_tickets.push($i);
                }
            }

            if(!entered_tickets){
                is_valid = false;
            }

            if(!is_valid){
                $("#btnConfirm").addClass("disabled");
            }else{
                $("#btnConfirm").removeClass("disabled");
            }
            return is_valid;
        },

/* -------------------------------- ACTIVATE PAGE -------------------------------- */

        active_continue_button: function(){
            if(widgets.check_next()){
                $("#id_next").fadeIn();
            }else{
                $("#id_next").fadeOut();
            }
        },

/* -------------------------------- SET PAGE -------------------------------- */

        set_page: function(){
            var params = {
                "csrfmiddlewaretoken": crosssrf,
                "product": product_id,
                "page": page
            };
            if(edit_id){ params["edit_id"] = edit_id }

            $.ajax({
                type: "POST",
                url:"/game/set/page/",
                data: params,
                dataType:"json",
                success: function(data){
                    widgets.update_game(data);
                },
                error: function(code){ console.log(code);}
            });
        },

/* -------------------------------- NEXT -------------------------------- */

        check_next: function(){
            var go_next = true, start_loop, end_loop;

            if(page == 1){
                start_loop = 1; end_loop = 4;
            }else{
                start_loop = (((page-1) * 4) + 1);
                end_loop = ((page-1) * 4) + 4;
            }

            for(var $i=start_loop;$i<=end_loop;$i++){
                for (var ball in ballset) {
                    var item = ballset[ball];
                    if($("#ticket_" + $i + " .playblock_ballset_" + item.id + " .on").length < ballset[ball].main){
                        go_next = false;
                    }
                }
            }

            if(widgets.continuous_deck == false){
                if(page >= $(".row_deck").length){ go_next = false }
            }

            var check_next = (page + 1);
            start_loop = (((check_next-1) * 4) + 1);
            end_loop = entries;

            for(var $i=start_loop;$i<=end_loop;$i++){
                for (var ball in ballset) {
                    var item = ballset[ball];
                    if($("#ticket_" + $i + " .playblock_ballset_" + item.id + " .on").length != 0){
                        go_next = true;
                    }
                }
            }

            return go_next;
        },

        active_next_button: function(){
            if(widgets.check_next()){
                $("#id_next").fadeIn();
            }else{
                $("#id_next").fadeOut();
            }
        },

        go_next: function(){
            if(widgets.check_next()){
                page += 1;
                if(page >= $(".row_deck").length+1){ page = 1; }

                if(page == 1){
                    $("#scroller").animate({"margin-left":"0px"}, {duration: 1000, complete: function(){
                        widgets.active_next_button();
                        widgets.active_prev_button();
                    }});
                }else{
                    var margin_goto = 0;
                    for(var $p = 1; $p< page; $p++){
                        margin_goto += Number($("#row_deck_" + $p).width() + 106);
                    }
                    $("#scroller").animate({"margin-left": "-" + margin_goto + "px"}, {duration: 1000, complete: function(){
                        widgets.active_next_button();
                        widgets.active_prev_button();
                    }});
                }
                widgets.set_page();
            }else{
                openMessage(locale_complete_tickets, "error");
            }
        },

/* -------------------------------- PREVIOUS -------------------------------- */

        check_prev: function(){
            var go_prev = true, start_loop, end_loop;

            var set_page = page - 1;

            if(widgets.continuous_deck){
                if(set_page <= 0){
                    set_page = $(".row_deck").length;
                }
            }
            if(set_page == 0){
                start_loop = (set_page-1) * 4;
                end_loop = ((set_page-1) * 4) + 4;
            }else{
                start_loop = (set_page-1) * 4;
                end_loop = ((set_page-1) * 4) + 4;
            }

            for(var $i=(start_loop+1);$i<=end_loop;$i++){
                for (var ball in ballset) {
                    var item = ballset[ball];
                    if($("#ticket_" + $i + " .playblock_ballset_" + item.id + " .on").length < ballset[ball].main){
                        go_prev = false;
                    }
                }
            }

            if(set_page != 0){
                go_prev = true;
            }

            return go_prev;
        },

        active_prev_button: function(){
            if(widgets.check_prev()){
                $("#id_prev").fadeIn();
            }else{
                $("#id_prev").fadeOut();
            }
        },

        go_prev: function(){
            if(widgets.check_prev()){
                page -= 1;
                if(page <= 0){ page = $(".row_deck").length; }

                if(page == 1){
                    $("#scroller").animate({"margin-left":"0px"}, {duration: 1000, complete: function(){
                        widgets.active_next_button();
                        widgets.active_prev_button();
                    }});
                }else{
                    var margin_goto = 0;
                    for(var $p = 1; $p< page; $p++){
                        margin_goto += Number($("#row_deck_" + $p).width() + 106);
                    }
                    $("#scroller").animate({"margin-left": "-" + margin_goto + "px"}, {duration: 1000, complete: function(){
                        widgets.active_next_button();
                        widgets.active_prev_button();
                    }});
                }
                widgets.set_page();
            }else{
                openMessage(locale_complete_tickets, "error");
            }
        },

/* -------------------------------- PLAYBLOCK -------------------------------- */

        playblock:{
            init:function(){

                widgets.check_page_deck();

                var content_height = 310 + $(".basic:first-child").height(),  nav_top;
                $('.game-content').css({"height": content_height + 100 + "px" });
                var control_bar_height = $('#play-control-bar').position().top;
                $('#play-control-bar').css({"position":"absolute", "top":  content_height - 40 + "px"});

                nav_top = ($(".basic:first-child").height() / 2) - ($('#id_next').height() / 2) - 3;
                $('#id_next').css({"top":nav_top + "px"});
                $('#id_prev').css({"top":nav_top + "px"});

                $('.playblock .basic .singleticket a').click(function(evt){
                    var ball_name = this.id.split("-");
                    var ball = $(this);
                    var is_ticked = false;
                    var limit = 0;
                    if(ball.hasClass("on")){
                        is_ticked = true;
                    }

                    if(exclude_selection && addon){
                        if(ball.hasClass("off")){
                            return false;
                        }
                    }

                    var selection_limit = $('#selection_limit_'+ball_name[3]).val();
                    if(selection_limit && selection_limit != "None" && selection_limit != "undefined"){
                        limit = $('#selection_limit_'+ball_name[3]).val();
                    }

                    var params = {
                        "csrfmiddlewaretoken": crosssrf,
                        "product": ball_name[1],
                        "ticket": ball_name[2],
                        "ballset": ball_name[3],
                        "number": ball_name[4],
                        "is_ticked": is_ticked,
                        "limit": limit
                    };
                    if(edit_id){ params["edit_id"] = edit_id }

                    $.ajax({
                        type: "POST",
                        url:"/game/ticket/toggle/",
                        dataType: "json",
                        data: params,
                        success: function(data){
                            if(limit && data.status == "true" && $("#ticket_" + ball_name[2] + " .playblock_ballset_" + ball_name[3] + " .on").length > limit){
                              ball.removeClass('on');
                              openMessage(locale_exceeded_wheeling, "error");
                            }else if(data.status == "true"){
                              ball.addClass('on');
                            }else{
                              ball.removeClass('on');
                            }
                            widgets.update_game(data);
                        },
                        error: function(code){ console.log(code); }
                    });

                    if(ball.hasClass('on')){
                        ball.removeClass('on');
                    }else{
                        ball.addClass('on');
                    }

//                    if exclude_selected, remove from addon
                    widgets.update_exclusive_addon("#id_bonus_" + ball_name[2] + "_" +  addon.id);

                    widgets.active_next_button();
                    widgets.active_prev_button();
                });
            }
        },

        update_exclusive_addon: function(ticket){
             var ticket_name = "";
            if(ticket.id){
                ticket_name = String(ticket.id).split("_");
            }else{
                if($(ticket).hasClass("selector")){
                    ticket_name = String($(ticket).parent().parent().find("select").attr("id")).split("_");
                }else{
                    ticket_name = String(ticket).split("_");
                }
            }
            var $c = 0;

            var bonus_el = $("#id_bonus_" + ticket_name[2] + "_" + addon.id);

            if(exclude_selection){
                var num_val, num_el, ballset_id = 0;
                if(bonus_el){
                    for (var ball_index in ballset) {
                        if($c == 0){
                            ballset_id = ballset[ball_index].id;
                        }
                        $c++;
                    }
                    bonus_el.parent().find(".dropdown ul li").addClass("hide");
                    $("#ticket_" + ticket_name[2] + " .playblock_ballset_" + ballset_id).find("a").each(function(){
                        if($(this).hasClass("off")){
                            $(this).removeClass("off");
                        }

                        if(!$(this).hasClass("on")){
                            num_val = $(this).text();
                            num_el = $(this);

                            bonus_el.parent().find(".dropdown ul li").each(function(){
                                if(num_val == "--" || parseInt($(this).text()) == parseInt(num_val)){
                                    $(this).removeClass("hide");
                                }
                            });

                            if(String($(this).text()) == "--" || parseInt(bonus_el.parent().find(".dropdown .current").text()) == parseInt(num_val)){
                                $(this).addClass("off");
                            }
                        }
                    });

                    var ticket_selection = [], ticket_name;
                    $("#ticket_" + ticket_name[2] + " .playblock_ballset_" + ballset_id).find("a").each(function(){
                        ticket_name = String(this.id).split("-");
                        if($(this).hasClass("on")){
                            ticket_selection.push(parseInt(ticket_name[4]));
                        }
                    });
                    if (ticket_selection.indexOf( parseInt(bonus_el.parent().find(".dropdown .current").text()) ) >= 0) {
                       bonus_el.parent().find(".dropdown .current").text('--');
                       $("#label_addon_" + ticket_name[2] + "_" + addon.id).removeClass("active");
                       bonus_el.val(0);
                    }
                }
            }
        },

        setup_addons: function(){
            var addon_el, label_el;
            var session_addon = function(ticket){

                var ticket_name = String(ticket.id).split("_");

                var params = {
                    "csrfmiddlewaretoken": crosssrf,
                    "product": product_id,
                    "addon": ticket_name[3],
                    "number": $(ticket).val(),
                    "ticket": ticket_name[2]
                };
                if(edit_id){ params["edit_id"] = edit_id }

                $.ajax({
                    type: "POST",
                    url:"/game/ticket/addon/",
                    data: params,
                    dataType: "json",
                    success: function(data){
                        widgets.update_game(data);
                    },
                    error: function(code){ console.log(code); }
                });
                label_el = $("#label_addon_" + ticket_name[2] + "_" + ticket_name[3]);

                if(Number($(ticket).val()) == 0){
                    label_el.removeClass("active");
                }else{
                    label_el.addClass("active");
                }

                widgets.update_exclusive_addon(ticket);
            };

            for(var $i=1;$i<=entries;$i++){
                addon_el = $("#id_bonus_" + $i + "_" +  addon.id);

                addon_el.parent().find("a").click(function(){ widgets.update_exclusive_addon(this); });

                addon_el.change(function(){ session_addon(this); });
                addon_el.click(function(){ session_addon(this); });
                addon_el.keyup(function(){ session_addon(this); });
            }

            for(var $i=1;$i<=entries;$i++){
                widgets.update_exclusive_addon("#id_bonus_" + $i + "_" +  addon.id);
            }
        },

        setup_buttons: function(){
            var ticket_name;
            for(var $i=1;$i<=entries;$i++){
                $("#ticket_" + $i + " .button_quickpick").each(function(){
                    $(this).css("cursor", "pointer");
                    $(this).click(function(){
                        widgets.send_ticket_numbers(this.id, widgets.quickpick(this.id));
                    })
                });

                $("#ticket_" + $i + " .button_clear").each(function(){
                    $(this).css("cursor", "pointer");
                    $(this).click(function(){
                        widgets.clear(this.id);
                    });
                    widgets.active_next_button();
                    widgets.active_prev_button();
                });
            }
        }
    };

    widgets.playblock.init(); widgets.guage.init();
    widgets.boost.init();
    widgets.setup_buttons();
    widgets.validate_submit();
    if(addon){ widgets.setup_addons(); }

    for(var $i=1;$i<=entries;$i++){
        $("#ticket_" + $i).css({"z-index": 100});
    }

    widgets.active_next_button();
    widgets.active_prev_button();

    duration_el.keyup(set_day_selector);
    shedule_el.click(set_day_selector);
    shedule_el.change(set_day_selector);
    shedule_el.keyup(set_day_selector);

    function pad(num, size) {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    }

    function secondsToString(seconds){
        var numdays = Math.floor(seconds / 86400);
        var numhours = Math.floor((seconds % 86400) / 3600);
        var numminutes = Math.floor(((seconds % 86400) % 3600) / 60);
        return pad(numdays,2) + ":" + pad(numhours,2) + ":" + pad(numminutes,2);
    }
    var run_time_interval, run_time_seconds = 60;

    var run_the_time = function(){
        var game_time_el = $('#game_time');

        if(remaining_time == "00:00:00"){
            try{ clearInterval(run_time_interval); }catch(e){}

            var params = {
                "csrfmiddlewaretoken": crosssrf,
                "product": product_id
            };
            if(edit_id){ params["edit_id"] = edit_id }

            $.ajax({
                type: "POST",
                url:"/game/get/time/",
                data: params,
                success: function(response){
                    remaining_time = response;
                    if(response != "00:00:00"){
                        run_time_interval = setInterval(run_the_time, (run_time_seconds * 1000));
                    }
                },
                error: function(code){ console.log(code);}
            });
        }else{
            var time_val = String(remaining_time).split(":");

            var new_time_val = [], total_seconds = 0;
            for(var $i=0;$i<time_val.length;$i++){
                new_time_val.push(parseInt(time_val[$i]));

                if($i == 0){ // days
                    total_seconds = time_val[$i] * 86400;
                }
                if($i == 1){ // hours
                    total_seconds = total_seconds + (time_val[$i] * 3600);
                }
                if($i == 2){ // minutes
                    total_seconds = total_seconds + (time_val[$i] * 60);
                }
            }

            var seconds_in_string = secondsToString(total_seconds - 1);
            remaining_time = seconds_in_string;
        }
        var game_time = String(remaining_time).split("");

        game_time_el.find('span:eq(0)').html(game_time[0]);
        game_time_el.find('span:eq(1)').html(game_time[1]);

        game_time_el.find('span:eq(3)').html(game_time[3]);
        game_time_el.find('span:eq(4)').html(game_time[4]);

        game_time_el.find('span:eq(6)').html(game_time[6]);
        game_time_el.find('span:eq(7)').html(game_time[7]);

    }

    run_time_interval = setInterval(run_the_time, (run_time_seconds * 1000));

    $("#id_next").click(function(){ $("#id_prev").fadeOut(); $("#id_next").fadeOut({complete: function(){ widgets.go_next(); }}) });
    $("#id_prev").click(function(){ $("#id_next").fadeOut(); $("#id_prev").fadeOut({complete: function(){ widgets.go_prev(); }}) });

    $("#btn_max_all").click(function(){widgets.max_all();});
    $("#btn_quickpick_all").click(function(){widgets.quickpick_all();});
    $("#btn_addon_all").click(function(){widgets.addon_all();});
});

function set_day_selector(){
    var duration_el = $("#id_draws");

    try{ clearTimeout(t); }catch(e){}
    t = setTimeout(function(){

        var duration_val = "";
        if(parseInt(duration_el.val())){
            duration_val = parseInt(duration_el.val());
        }
        if(duration_val < 1){
            openMessage(locale_duration_error, "error");
            duration_val = 1;
            duration_el.val(duration_val);
        }

        if(duration_val > 52){
            openMessage(locale_duration_error, "error");
            duration_val = 52;
            duration_el.val(duration_val);
        }
        update_day_session();
    }, 800);
}

function update_day_session(){
    var dow = "", k = 0;
    var duration_el = $("#id_draws");
    $(".date-selector .dow").each(function(item){
        if($(this).hasClass("checked")){
            if(k != 0){ dow += "-"; }
            dow = dow + String(this.id).replace("id_", "");
            k++;
        }
    });

    var duration_val = "";
    if(parseInt(duration_el.val())){
        duration_val = parseInt(duration_el.val());
    }
    if(duration_val < 1){
        duration_val = 1;
        duration_el.val(duration_val);
    }

    if(duration_val > 52){
        duration_val = 52;
        duration_el.val(duration_val);
    }

    var params = {
        "csrfmiddlewaretoken": crosssrf,
        "product": product_id,
        "start": $("#schedule_start_date").val(),
        "days": dow,
        "duration": duration_val
    };
    if(edit_id){ params["edit_id"] = edit_id }

    $.ajax({
        type: "POST",
        url:"/game/set/draw/",
        data: params,
        dataType: "json",
        success: function(data){
            widgets.update_game(data)
        },
        error: function(code){ console.log(code);}
    });
}

function validateGame(){
    if(!widgets.validate_submit()){
        var ticket_msg = " ";
        for(var $k=0; $k<error_tickets.length; $k++){
            $("#ticket_" + error_tickets[$k]).addClass("error");
            if($k && $k < error_tickets.length){
                ticket_msg += ", "
            }
            ticket_msg += "Ticket " + error_tickets[$k];
        }
        openMessage(locale_validate_fail + ticket_msg, "error");
        return false;
    }
    if($("#btnConfirm").hasClass("disabled")){
        return false;
    }
    return true;
}

function openMessage(msg, status){
    try{ $("#message").destroy(); }catch(e){}
    $("body").append('<div id="message"> <div class="alert alert-' + status + '"> <div class="msgicon"></div><span> ' + msg + ' </span> </div>');
    $(".alert").delay(3000).fadeOut(1000);
}

function toggle_day(day){
    var day_el = $("#id_" + day);
    if(!day_el.hasClass("disabled")){
        if(day_el.hasClass("checked")){
            if($(".date-selector .checked").length >= 2){
                $(day_el).removeClass("checked");
            }else{
                openMessage(locale_day_error, "error");
            }
        }else{
            $(day_el).addClass("checked");
        }
    }
    update_day_session();
}

function integersonly(e){
    var unicode=e.charCode? e.charCode : e.keyCode;
    //if the key isn't the backspace key, left, up right, fullstop, down or enter  (which we should allow)
    if (unicode!=8  && unicode!=13 && unicode!=37 && unicode!=38 && unicode!=39 && unicode!=40){
        if (unicode<48||unicode>57){ //if not a number
            return false //disable key press
        }
    }
}

//Scroll to Target

$(".scroll").click(function (event) {
    event.preventDefault();
    //calculate destination place
    var dest = 0;
    if ($(this.hash).offset().top > $(document).height() - $(window).height()) {
        dest = $(document).height() - $(window).height();
    } else {
        dest = $(this.hash).offset().top;
    }
    //go to destination
    $('html,body').animate({scrollTo: dest}, 1000, 'swing');
});


