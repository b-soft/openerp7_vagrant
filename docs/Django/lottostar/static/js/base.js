var topmenu = true;

$(function (e) {
    e(function () {
        e(".toggle-nav").on("click", function (t) {
            var n = e(t.currentTarget);
            n.hasClass("open") ? (n.removeClass("open"), e("#menuLanguage").slideUp(400)) : (n.addClass("open"), e("#menuLanguage").slideDown(400))
        })
    });
    jQuery(document).foundation();
});

// dropdown styles
$(document).ready(function () {
    $(".styled-select").fancyfields();
});


jQuery(document).ready(function($){

    $(".toggle-topbar.menu-icon").click(function(){
        if(topmenu == false){
            $(".top-bar").css("height", "30px");
            topmenu = true;
        }else{
            $(".top-bar").css("height", "auto");
            topmenu = false;
        }
    });

    //Promotional window
    $(document)
      .foundation()
      .foundation('reveal', {closeOnBackgroundClick: false});
    $("#myModal").foundation('reveal','open');


    //controls submission of initial offer; user can accept or reject an offer after which the modal must be closed.
    var submit_offer_modal = function(e, url, form){
        e.preventDefault();
        $.ajax({
            url: url,
            type: 'post',
            cache: false,
            data: form.serialize(),
            success: $('#myModal').foundation('reveal', 'close')
        })
        // if user accepts an offer on the checkout page reload page to display the selected offer.
        if(window.location.pathname == "/checkout/"){
            window.location.reload();
        }
    };

    $(document).on('submit', '.initial-offer-form', function(e){
        var url = $(this).attr('action');
        submit_offer_modal(e, url, $(this));
    });

    $(document).on('click', '.reject-initial-offer', function(e){
        var url = $(this).attr('href');
        submit_offer_modal(e, url, $('.initial-offer-form'));
    });


    try{ clearInterval(msgtimer) }catch(e){};
    var msgtimer = setTimeout(function(){
        $("#formmessage").fadeOut(3000);
        $("#message").fadeOut(3000);
    }, 3000);

});



