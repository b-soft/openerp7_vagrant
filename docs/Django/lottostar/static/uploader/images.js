var folder_name = "part";
var c = 0;
var items = [];
var myAjax = [];


$(document).ready(function() {
    $('#id_file').uploadify({
        'uploader'          : '/media/filebrowser/uploadify/uploadify.swf',
        'script'            : '/admin/get_folder/upload_file/',
        'scriptData'        : {'session_key': session_key},
        'checkScript'       : '/admin/get_folder/check_file/',
        'cancelImg'         : '/media/filebrowser/uploadify/cancel.png',
        'auto'              : false,
        'folder'            : folder_name,
        'multi'             : true,
        'fileDesc'          : '*.jpg;*.jpeg;*.gif;*.png;*.tif;*.tiff;',
        'fileExt'           : '*.jpg;*.jpeg;*.gif;*.png;*.tif;*.tiff;',
        'sizeLimit'         : 10485760,
        'scriptAccess'      : 'sameDomain',
        'queueSizeLimit'    : 500,
        'simUploadLimit'    : 10,
        'width'             : 110,
        'height'            : 30,
        'hideButton'        : false,
        'buttonText'        : "SELECT",
        'rollover'          : false,
        "queueID"           : "upload_images",
        'wmode'             : 'transparent',
		onSelect            : function() {
            if(c == 0){
                $("#upload_images").html("");
            }
            c = c + 1;
		},
		onCancel            : function() {
            c = c - 1;
            if(c < 0){
                c = 0;
            }
            if(c == 0){
                $("#upload_images").html("please select files to upload...");
                $("#uploadmsg").css({"display":"none"});
                $("#buttons").css({"display":"block"});
            }
		},
		onAllComplete            : function() {
            $("#upload_images").html("please select files to upload...");
            $("#uploadmsg").css({"display":"none"});
            $("#buttons").css({"display":"block"});
            $("#buttons").css({"display":"block"});
            $("#uploadbox").css({"top": "0px"});
            get_folder(folder_name);            
            if(c > 0){
                c = 0;
            }

        }
    });
    $('input:submit').click(function(){
        if(c > 0){
            $('#id_file').uploadifyUpload();
            $("#buttons").css({"display":"none"});
            $("#uploadmsg").css({"display":"block"});
            $("#uploadbox").css({"top": "60px"});
        }else{
            alert("Please select your Files first.");
        }
        return false;
    });

    $('#button_csv').click(function() {
        window.open('/admin/image-uploader/csv/?foldername=' + folder_name,'csv');
    });
    $('#button_cleanup').click(function() {
        $('#button_cleanup').css({"display":"none"});
        $.ajax({
            type: "GET",
            url:  "/admin/image-uploader/cleanup/",
            data: "foldername=" + folder_name,
            success: function(request){
                $('#button_cleanup').css({"display":"block"});
                get_folder(folder_name);
                $('#id_file').uploadifySettings("folder", folder_name, true);
            }
        });//close ajax    
    });
    
    $('#button_part').click(function() {
        folder_name = "part";
        if(c > 0){
            var rc = confirm("Are you sure you want to clear your current selected list of images?");
            if(rc){
                if(c > 0){
                    $('#id_file').uploadifyClearQueue();
                    c = 0;
                    $("#upload_images").html("please select files to upload...");
                }
                stopAjax();
                get_folder(folder_name);
                $('#id_file').uploadifySettings("folder", folder_name, true);
            }
        }else{
            stopAjax();
            get_folder(folder_name);            
            $('#id_file').uploadifySettings("folder", folder_name, true);
        }
    });    

    $('#button_vehicle').click(function() {
        folder_name = "vehicle";
        if(c > 0){
            var rc = confirm("Are you sure you want to clear your current selected list of images?");
            if(rc){
                if(c > 0){
                    $('#id_file').uploadifyClearQueue();
                    c = 0;
                    $("#upload_images").html("please select files to upload...");
                }
                stopAjax();
                get_folder(folder_name);
                $('#id_file').uploadifySettings("folder", folder_name, true);
            }
        }else{
            stopAjax();
            get_folder(folder_name);
            $('#id_file').uploadifySettings("folder", folder_name, true);
        }
    });    
    $('#button_manufacturer').click(function() {
        folder_name = "manufacturer";
        if(c > 0){
            var rc = confirm("Are you sure you want to clear your current selected list of images?");
            if(rc){
                if(c > 0){
                    $('#id_file').uploadifyClearQueue();
                    c = 0;
                    $("#upload_images").html("please select files to upload...");
                }
                stopAjax();
                get_folder(folder_name);
                $('#id_file').uploadifySettings("folder", folder_name, true);
            }
        }else{
            stopAjax();
            get_folder(folder_name);
            $('#id_file').uploadifySettings("folder", folder_name, true);
        }
    });    
    get_folder(folder_name);
});

function get_folder(foldername){
    $("#uploadmsg").css({"display":"none"});
    $("#buttons").css({"display":"block"});
    $("#button_vehicle").removeClass("button_selected");
    $("#button_manufacturer").removeClass("button_selected");
    $("#button_part").removeClass("button_selected");
    $("#button_" + foldername).addClass("button_selected");
    $("#uploaded_images").addClass("ajax-loading2");
    $("#uploaded_images").html("loading...");
    var k = 0;
    $.getJSON('/admin/get_folder/?foldername=' + foldername, function(data) {
        items = [];
        $("#uploaded_images").css({"text-align":"center"}).html("loading...");
        if(data.length != 0){
            $.each(data, function(data, item) {
                if(k <= 50){
                    items.push('<tr id="filename_' + item.mvdb_code + '"><td style="width: 100px"><a href="/media/upload/' + foldername + '/' + item.filename + '" target="_blank"><img width="30px" src="/media/upload/' + foldername + '/' + item.filename + '"/></a></td><td style="text-align:left;width: 200px">' + item.filename + '</td><td style="text-align:left;width:250px" id="filename_result_' + item.mvdb_code + '">' + item.text + '</td><td width="100px">' + item.mvdb_code + '</td><td><div class="cancelbutton" onclick="deleteFile(\''+ item.filename + '\', \'' + item.mvdb_code + '\')"></div></td></tr>');

                    $.ajax({
                        type: "GET",
                        url:  "/admin/get_folder/" + foldername + "/",
                        data: "filename=" + item.filename,
                        success: function(request){
                            $("#filename_result_" + item.mvdb_code).html(request);
                        }
                    });//close ajax    
                }
                k = k + 1;
            });             
        }else{
            items.push("<tr><td colsnan='5' style='text-align:center'>There are no files in this directory.</td></td>")
        }

        $("#uploaded_images").removeClass("ajax-loading2");        
        $('#uploaded_images').html('<div><table width="100%">' + items.join('') + '</table></div>');
    });    
}

function deleteFile(filename, mvdb_id){
    var con = confirm("Are you sure you want to delete this file?");
    if(con == true){
        $.ajax({
            type: "GET",
            url:  "/admin/image-uploader/remove/",
            data: "foldername=" + folder_name + "&filename=" + filename,
            success: function(request){
                $("#filename_" + mvdb_id).remove();
            }
        });//close ajax        
    }
}

$.xhrPool = [];
$.xhrPool.abortAll = function() {
    $(this).each(function(idx, jqXHR) {
        jqXHR.abort();
    });
};
$.ajaxSetup({
    beforeSend: function(jqXHR) {
        $.xhrPool.push(jqXHR);
    }
});



function stopAjax(){    
    $.xhrPool.abortAll();
}
