
$(document).ready(function() {
    showProgress();
});

function showProgress(){
    $.ajax({
        type: "GET",
        url:  "/admin/upload/feed/",
        success: function(request){
            $('#results_go').html(request);
            if(request.indexOf("COMPLETE!") != -1 && request.indexOf("UPLOAD INTERRUPTED!") != -1){
            }else{
                setTimeout(showProgress, 100);
            }
        }
    });
}