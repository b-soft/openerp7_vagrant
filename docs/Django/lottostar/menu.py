
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from admin_tools.menu import items, Menu

class CustomMenu(Menu):
    """
    Custom Menu for livebidsadmin site.
    """
    def __init__(self, **kwargs):
        Menu.__init__(self, **kwargs)
        self.children += [
            items.MenuItem(_('Dashboard'), reverse('admin:index')),
#            items.MenuItem('Users',
#                children=[
#                    items.MenuItem(_('Customers'), reverse("admin:auth_customer_changelist")),
#                    items.MenuItem(_('Administrators'), reverse("admin:auth_administrator_changelist")),
#                    items.MenuItem(_('User Permissions'), reverse("admin:auth_userpermission_changelist")),
#                ]
#            ),

            items.MenuItem(_('Customers'), reverse("admin:auth_customer_changelist")),
            items.MenuItem(_('Transaction'),
                children=[
                    items.MenuItem(_('Transaction list'), reverse("admin:play_transaction_changelist")),
                ]
            ),
            items.MenuItem(_('Games'), reverse("admin:game_game_changelist")),

            #items.MenuItem(_('Promotions'), reverse("admin:promotions_offer_changelist")),
            items.MenuItem(_('Sites'),
                children=[
                    items.MenuItem(_('Home / Site Settings'), reverse("admin:pages_homeconfig_changelist")),
                    items.MenuItem(_('Sites'), reverse("admin:sites_site_changelist")),
                    items.MenuItem(_('Countries'), reverse("admin:countries_country_changelist")),
#                    items.MenuItem(_('Banned IPs'), reverse("admin:auth_bannedip_changelist")),
                    items.MenuItem(_('Site Redirects'), reverse("admin:system_siteredirect_changelist")),
                    items.MenuItem(_('No index pages'), reverse("admin:system_noindex_changelist")),
                ]
            ),
            items.MenuItem(_('Content'),
                children=[
                    items.MenuItem(_('Pages'), reverse("admin:pages_page_changelist")),
                    items.MenuItem(_('FAQ'), reverse("admin:faq_faq_changelist")),
                    items.MenuItem(_('Email Templates'), reverse("admin:pages_emailcontent_changelist")),
                    items.MenuItem(_('Translations'), reverse("admin:trans_text_changelist")),
                    items.MenuItem(_('Reversion'), reverse("admin:system_lottoflutterrevision_changelist")),
                    items.MenuItem(_('SEO Meta'), reverse("admin:system_seometadata_changelist")),
                ]
            ),
            items.MenuItem(_('File Manager'), reverse('fb_browse')),
            items.MenuItem(_('Promotions'),
                children = [
                    items.MenuItem(_('Offers'), reverse("admin:promotions_offer_changelist")),
                    items.MenuItem(_('Customer offers'), reverse("admin:promotions_offercustomer_changelist")),
                ]
            ),
            items.MenuItem(_('Mobile'), reverse("admin:mobile_publisher_changelist")),

            items.MenuItem(_('Survey'),
                children=[
                    items.MenuItem(_('Survey'), reverse("admin:surveys_survey_changelist")),
                    items.MenuItem(_('Question'), reverse("admin:surveys_surveyquestion_changelist")),
                    # items.MenuItem(_('Answer'), reverse("admin:surveys_surveyanswer_changelist")),
                    # items.MenuItem(_('Customer'), reverse("admin:surveys_surveycustomer_changelist")),
                ]
            ),

            items.MenuItem(_('Reporting'),
                children=[
                    items.MenuItem(_('Game Report'), reverse("admin_game_overview")),
                ]
            ),
            items.MenuItem(_('Payments'), url=""),
            items.MenuItem(_('User Activity'), url=""),

        ]


    def init_with_context(self, context):
        pass