from django.conf.urls import patterns, include, url
from django.conf import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^',                       include('trans.urls')),
    #url(r'^',                       include('surveys.urls')),

    url(r'^admin/',                 include(admin.site.urls)),
    url(r'^admin/doc/',             include('django.contrib.admindocs.urls')),
    url(r'^admin/',                 include(admin.site.urls)),

    url(r'^admin_tools/',           include('admin_tools.urls')),
    url(r'^admin/filebrowser/',     include('filebrowser.urls')),
    url(r'^media/tinymce/',         include('tinymce.urls')),

    url(r'^faq/',                   include('faq.urls')),
    url(r'^blog/',                  include('blog.urls')),
    url(r'^',                       include('game.urls')),
    url(r'^',                       include('play.urls')),
    url(r'^',                       include('account.urls')),
    url(r'^',                       include('pages.urls')),
    url(r'^',                       include('system.urls')),
    url(r'^promotions/',            include('promotions.urls')),
    url(r'^',                       include('mobile.urls')),
    url(r'^test/',                  include('devtest.urls')),
    url(r'^',                       include('surveys.urls')),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^static/(.*)$',            'django.views.static.serve', {'document_root': settings.PROJECT_DIR + '/static/' }),
        (r'^sites/(.*)$',             'django.views.static.serve', {'document_root': settings.PROJECT_DIR + '/sites/' }),
        (r'^admin_tools/(.*)$',       'django.views.static.serve', {'document_root': settings.PROJECT_DIR + '/static/admin_tools/'}),
        (r'^robots.txt$',             'django.views.static.serve', {'path' : "/static/robots.txt", 'document_root': settings.PROJECT_ROOT, 'show_indexes': False }),
        (r'^favicon.ico/$',           'django.views.static.serve', {'path' : "/static/favicon.ico", 'document_root': settings.PROJECT_ROOT, 'show_indexes': False }),
        (r'^media/(.*)$',             'django.views.static.serve', {'document_root': settings.PROJECT_DIR + '/media/' }),
    )
