DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'lottostar',
        'USER': 'lottostar',
        'PASSWORD': 'lottostar',
        'HOST': 'localhost',
        'PORT': '',
        }
}

"""DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'webapp.db',
        'USER': '',
        'PASSWORD': '',
        'HOST': 'localhost',
        'PORT': '',
        }
}"""

EMAIL_TEST_MODE = False
THUMBNAIL_DEBUG = False
DEBUG = False #True
#ALLOWED_HOSTS = True
