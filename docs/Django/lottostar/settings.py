# -*- coding: utf-8 -*-

import os.path, sys
from django.utils.translation import ugettext_lazy as _

sys.path.append(os.path.join(os.path.dirname(__file__), 'vendor'))
sys.path.append(os.path.join(os.path.dirname(__file__), 'apps'))

SITES_DIR = os.path.join(os.path.dirname(__file__), 'sites')
sys.path.append(SITES_DIR)

# ==============================================================================

DEBUG = True
MIDDLEWARE_DEBUG = True
TEMPLATE_DEBUG = DEBUG
THUMBNAIL_DEBUG = DEBUG

#===============================================================================
# MINIMUM_PLAY_CREDITS (Currency: EURO)
#===============================================================================
MINIMUM_PLAY_CREDITS = 10.00

#===============================================================================
# LIMIT UPDATE COOLING PERIOD (HOURS)
#===============================================================================
COOLING_PERIOD = 24 #24 hours cooling period

# ==============================================================================
# FILE HANDLERS
# ==============================================================================
PROJECT_FOLDER = "lottoflutter"

ROOT_URLCONF = 'urls'

LOGIN_URL = "/login/"
LOGIN_REDIRECT_URL = "/accounts/"

PROJECT_ROOT = os.path.dirname(__file__)
PROJECT_DIR = PROJECT_ROOT

MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'static', 'img')
MEDIA_URL = '/static/img/'

STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')
STATIC_URL = "/static/"

GEOIP_URL  = os.path.join(PROJECT_ROOT, 'apps/account/GeoIPCity.dat')

ADMIN_TOOLS_MEDIA_URL = '/static/'
ADMIN_MEDIA_PREFIX = '/static/admin/'

FILEBROWSER_URL_FILEBROWSER_MEDIA = "/static/filebrowser/"
FILEBROWSER_PATH_FILEBROWSER_MEDIA = os.path.join(STATIC_ROOT, '/filebrowser/')

FILEBROWSER_URL_TINYMCE = "/static/js/tinymce/"
FILEBROWSER_PATH_TINYMCE = STATIC_URL + "js/tinymce/"

FILEBROWSER_MEDIA_ROOT = MEDIA_ROOT
FILEBROWSER_MEDIA_URL = MEDIA_URL
FILEBROWSER_DIRECTORY = 'upload/'

STATICFILES_DIRS = ()
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)

SECRET_KEY = 'z-hjp_!w@y^8cf5^2fr0ez-19)!c5w1g$^uz8%54xb=&53a30@'

INTERNAL_IPS = ('127.0.0.1', '41.164.141.98', )

TIME_ZONE = 'Africa/Johannesburg'
LANGUAGE_CODE = 'en'
SITE_ID = 1

USE_I18N = True
USE_L10N = True
FORMAT_MODULE_PATH = "formats"

# ==============================================================================
# ENABLE SSL FOR SENSITIVE PAGES
# ==============================================================================

SSL_ON = True
SSL_ALWAYS = False
HTTPS_PATHS=[]
SSL_PORT= None

# ==============================================================================
# TWITTER
# ==============================================================================

TWITTER_COUNT = 10
TWITTER_TIMEOUT = 3600
TWITTER_CONSUMER_KEY = 'W3yGGIzgRgvi4OKJ4pqgIQ'
TWITTER_CONSUMER_SECRET = 'Dj7E9Ktm6x0nCQ3yZxlNCNVZrqKSmdLO3MqnAK5rE'
TWITTER_ACCESS_TOKEN_KEY = '1652514415-Y93jaW6ivsjUBzo1TjC7jJdEM1zkMho4NjU146C'
TWITTER_ACCESS_TOKEN_SECRET = 'CsXcE9s6t7OoZ3crW15J5fXWWXcolOhRG1BVB8sT1E'

# ==============================================================================
# DATABASE
# ==============================================================================

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'webapp.db',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
        }
}

# ==============================================================================
# Languages
# ==============================================================================

LANGUAGES = (
    ('en', _('English')),
    ('de', _('German')),
    ('es', _('Spanish')),
    ('sv', _('Swedish')),
    ('pl', _('Polish')),
    ('pt', _('Portuguese')),
    ('ru', _('Russian')),
    ('zh', _('Chinese')),
    ('cs', _('Czech')),
    ('fr', _('French')),
    ('ja', _('Japanese')),
    ('ro', _('Romanian')),
)

# ==============================================================================
# MAIL
# ==============================================================================

DEFAULT_FROM_EMAIL = 'server@lottostar.com'
SERVER_EMAIL = 'server@lottostar.com'

ADMINS = (
    ('Andrew von Hoesslin', 'nganpet2007@gmail.com'),
)
""",
    ('Darren Kerr', 'darren@symfony.co.za'),
    ('Paul von Hoesslin', 'paul@symfony.co.za'),
)"""
MANAGERS = ADMINS

WEBCONTACT_RECIPIENTS = ['lazare.jojo@gmail.com',] #['support@symfony.co.za',]

TEST_EMAIL_DIR = os.path.join(os.path.dirname(__file__), 'tmp', 'emails')

EMAIL_TEST_MODE = True

EMAIL_HOST = 'localhost'
EMAIL_HOST_PASSWORD = ''
EMAIL_HOST_USER = ''
EMAIL_PORT = 25
EMAIL_USE_TLS = False
EMAIL_SYSTEM_SENDER = "lottostar <noreply@lottostar.com>"

MAIL_WRITE_TO_HDD = False
# ==============================================================================
# AUTHENTICATION
# ==============================================================================

AUTH_PROFILE_MODULE = 'account.UserProfile'

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'account.backends.EmailAuthBackend',
)

# ==============================================================================
# MIDDLEWARE
# ==============================================================================

MIDDLEWARE_CLASSES = (
    'ssl_redirect.middleware.SSLRedirectMiddleware',
    'dynamicsites.middleware.DynamicSitesMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.transaction.TransactionMiddleware',
    'reversion.middleware.RevisionMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'pagination.middleware.PaginationMiddleware',
    'account.middleware.IPRequestMiddleware',
    'system.middleware.SystemRedirectMiddleware',
    'system.middleware.SystemNoindexMiddleware',
    'system.middleware.SystemSEOMiddleware',
    'account.middleware.LottoFlutterLoginMiddleware',
)

# ==============================================================================
# APPS
# ==============================================================================

DJANGO_ADMIN_APPS = (
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
)

DJANGO_CONTRIB_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.humanize',
    'django.contrib.messages',
    'django.contrib.webdesign',
    'django.contrib.markup',
)

PROJECT_APPS = (
    'pages',
    'account',
    'blog',
    'faq',
    'dynamicsites',
    'game',
    'play',
    'trans',
    'system',
    'promotions',
    'mobile',
    'devtest',
    'surveys',
)

THIRD_PARTY_APPS = (
    'twitter',
    'countries',
    'translatable',
    'registration',
    'pagination',
    'debug_toolbar',
    'filebrowser',
    'south',
    'sorl.thumbnail',
    'json_field',
    'django_reset',
    'reversion',
    "compressor",
)

INSTALLED_APPS = DJANGO_ADMIN_APPS + DJANGO_CONTRIB_APPS + PROJECT_APPS + THIRD_PARTY_APPS

# ==============================================================================
# CACHES
# ==============================================================================

#COMPRESS_ENABLED = True
COMPRESS_ENABLED = False

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

CACHE_TIME = 86400

# ==============================================================================
# TEMPLATES
# ==============================================================================

TEMPLATE_CONTEXT_PROCESSORS = (
    'dynamicsites.context_processors.current_site',
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'system.contexts.seometatags',
    'pages.contexts.global_settings',
    'account.contexts.account_settings',
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MESSAGE_STORAGE = 'django.contrib.messages.storage.cookie.CookieStorage'

TEMPLATE_DIRS = (os.path.join(PROJECT_DIR, 'templates'),)

for app in PROJECT_APPS:
    if os.path.isdir(os.path.join(PROJECT_DIR, 'apps', app, 'templates')):
        TEMPLATE_DIRS += (os.path.join(PROJECT_DIR, 'apps', app, 'templates'),)

    if os.path.isdir(os.path.join(PROJECT_DIR, 'apps', app, 'templates/partials')):
        TEMPLATE_DIRS += (os.path.join(PROJECT_DIR, 'apps', app, 'templates/partials'),)

    if os.path.isdir(os.path.join(PROJECT_DIR, 'apps', app, 'templates/text')):
        TEMPLATE_DIRS += (os.path.join(PROJECT_DIR, 'apps', app, 'templates/text'),)

# ==============================================================================
# MISC SETUP CONFIGS
# ==============================================================================

ADMIN_TOOLS_MENU = 'menu.CustomMenu'
ADMIN_TOOLS_INDEX_DASHBOARD = 'dashboard.CustomIndexDashboard'
ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'dashboard.CustomAppIndexDashboard'

#should not be on main settings file
DEBUG_TOOLBAR_CONFIG = {
   'INTERCEPT_REDIRECTS': False,
   'HIDE_DJANGO_SQL': True,
   'TAG': 'div',
   }

TINYMCE_JS_URL = '/media/tinymce/jscripts/tiny_mce/tiny_mce.js'

# Add to your settings file
MAX_UPLOAD_SIZE = "429916160"
# 2.5MB - 2621440
# 5MB - 5242880
# 10MB - 10485760
# 20MB - 20971520
# 50MB - 5242880
# 100MB 104857600
# 250MB - 214958080
# 500MB - 429916160

# ==============================================================================
# tickets
# ==============================================================================

TICKETS= (
    ('general', _('General')),
)

# ==============================================================================
# LOGGING
# ==============================================================================

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
            },
        }
}

# ==============================================================================
# OVERWRITE
# ==============================================================================
try:
    from settings_site import *
except ImportError:
    print "Couldn't find settings_site.py, no site settings loaded."

try:
    from settings_local import *
except ImportError:
    print "Couldn't find settings_site.py, no site settings loaded."

# ~~END~~=======================================================================
