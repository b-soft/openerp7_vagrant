from django.contrib.admin import ModelAdmin


class RemoveDeleteAdmin(ModelAdmin):
    def has_delete_permission(self, request, obj=None):
        return False
