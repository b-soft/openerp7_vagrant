Important Sysadmin Stuff 

Afrihost Dedicated Server's Backups

Hiren:"

VMs run backups daily, host pulls from Vms once a day, offsite pulls from host once a day.

/usr/local/bin/backupdbs.py in VMs contains which dbs to backup keeps 7 days of backups

crontab -l -u backups, on host for daily sync schedule, same on office machine."


vboxadm@192.168.1.141 -> (Pulls Backups:(rsync+ssh)) ->Afrihost Dedicated -> (Pulls Backups:(rsync+ssh)) -> Virtual Machines on Afrihost 

#0 1 * * * rsync --delete -r -e 'ssh -p 3322' backups@localhost:/home/backups/dbs/ /home/backups/dbs/7/
#0 0 * * * rsync --delete -r -e 'ssh -p 10012' backups@localhost:/home/backups/dbs/ /home/backups/dbs/6/
#0 1 * * * rsync --delete -r -e 'ssh -p 2222' backups@localhost:/home/backups/dbs/ /home/backups/dbs/5/
#0 1 * * * rsync --delete -r -e 'ssh -p 4422' backups@localhost:/home/backups/dbs/ /home/backups/dbs/sbu-grow/
#0 1 * * * rsync --delete -r -e 'ssh -p 3332' backups@localhost:/home/backups/dbs/  /home/backups/dbs/7-testing/

0 1 * * * rsync -z -r -e 'ssh -p 3322' backups@localhost:/home/backups/dbs/ /home/backups/dbs/7/
0 0 * * * rsync -z -r -e 'ssh -p 10012' backups@localhost:/home/backups/dbs/ /home/backups/dbs/6/
0 1 * * * rsync -z -r -e 'ssh -p 2222' backups@localhost:/home/backups/dbs/ /home/backups/dbs/5/
0 1 * * * rsync -z -r -e 'ssh -p 4422' backups@localhost:/home/backups/dbs/ /home/backups/dbs/sbu-grow/
0 1 * * * rsync -z -r -e 'ssh -p 3332' backups@localhost:/home/backups/dbs/  /home/backups/dbs/7-testing/

#--delete was removed because it deleted everything that was different on the destination. Thus never had old backups
# -z (Compresses while transferring to reduce bandwith usage.)
# -r (Recursive, needs this to work.)



    Every VM that needs to be backed up will run the backupdbs.py script using cron before the backed dbs are moved to the host and then offsite with Rsync. 

To edit the cron entry for backupdbs.py (In the vm)

$ sudo vim /etc/cron.d/dbbackups 

Here is a sample dbbackups entry from cron, from openerp7 server:

MAILTO=timo@datasmith.co.za
#MAILTO=erpwebza@gmail.com

0 23 * * * root /usr/local/bin/backupdbs.py

The backups are pulled from the dedicated server at afrihost by a machine in datasmith. The machine can be accessed internally:

$ ssh vboxadm@192.168.1.141

It runs a script that pulls the db backups from the Afrihost dedicated server. This is the script:

#!/bin/bash

while [ 1 ] 
do
    #rsync --delete --append --partial --progress -vz -r -e 'ssh' backups@openerp.co.za:/home/backups/dbs/ /home/vboxadm/dbs/
    rsync -z -r -e 'ssh' backups@openerp.co.za:/home/backups/dbs/ /home/vboxadm/dbs/ 
    if [ "$?" = "0" ] ; then
        echo "rsync completed normally $(date) " >> /var/log/backups
        exit
    else
        echo "rsync failure. Retrying in a minute... $(date) " >> /var/log/backups
    
        sleep 60
    fi  
done

The new version of the script deletes the source file after rsync. This script pulls from afrihost dedicated server to vboxadm@192.168.1.141 Thus we won't have problems with hardrive space. Still need to implement this between VMs and dedicated server.

#!/bin/bash

while [ 1 ] 
do
    #rsync --delete --append --partial --progress -vz -r -e 'ssh' backups@openerp.co.za:/home/backups/dbs/ /home/vboxadm/dbs/
    #rsync -z -r -e 'ssh' backups@openerp.co.za:/home/backups/dbs/ /home/vboxadm/dbs/ 
    rsync --remove-source-files -z -r -e 'ssh' backups@openerp.co.za:/home/backups/dbs/ /home/vboxadm/dbs/  
    if [ "$?" = "0" ] ; then
        echo "rsync completed normally $(date) " >> /var/log/backups
        exit
    else
        echo "rsync failure. Retrying in a minute... $(date) " >> /var/log/backups
    
        sleep 60
    fi  
done




Add New VM To Be Backed Up Procedure

On the New VM You Created you should create a user called "backups".

Create a directory called dbs in the Virtual Machine.

$ mkdir /home/backups/dbs

In the Virtual machine create a script called /usr/local/bin/backupdbs.py

$ sudo vim /usr/local/bin/backupdbs.py

The script should contain the following code and edit it according to your need. Change the database names to those that you have on your system.

#!/usr/bin/env python

import os
import sys
from datetime import datetime
import re
import glob

dbs = [
    'internal',
    'adoptaschool',
    'balancelldev3',
    'balancell',

]
pgdump = '/usr/bin/pg_dump'
backupdir = '/home/backups/dbs/'
cust_mod_path = '/opt/custom_modules'

keepdays = 7    

def get_files(dir):
    if not dir.endswith('/'):
        dir += '/' 
    files = glob.glob(dir + '*dump*')
    return files

def run_rotation(files):
    datestr = datetime.strftime(datetime.now(),"%Y%m%d")
    for f in files:
        m = re.search('([0-9]{8})-[0-9]{4}',f)
        if m:
            filestr =  m.groups()[0]
            if int(filestr) < (int(datestr) - keepdays) and filestr[-1] != '1':
                os.remove(f)
        else:
            sys.stderr.write("ignoring file not in expected format: " + f)

def main():
    datestr = datetime.strftime(datetime.now(),"%Y%m%d-%H%M")
    print "Running db backups.."
    for db in dbs:
        backfile = backupdir + db + '.dump.' + datestr
        os.system('su postgres -c "' + pgdump + ' -Fc ' + db + ' >' + backfile + '"')
        print "Backed up db {} to file {}".format(db,backfile)
    files = get_files(backupdir)
    run_rotation(files)
# This was:
#  addons_cmd = 'tar -czf ' + backupdir + 'custmods.dump.' + datestr + ' ' + cust_mod_path 
    addons_cmd = 'tar -chzf ' + backupdir + 'custmods.dump.' + datestr + ' ' + cust_mod_path
# We added the -h option for tar because we couldnt create a tar.gz of files that were symlinked to others. This fixes this issue.
# You will have to rename the backup made by this part of the script to .tar.gz for it to properly decompress.
    os.system(addons_cmd)
    print "Rsync crons will sync this down to the office"


if __name__ == '__main__':
    main()




Remember to make the script executable:

$ sudo chmod +x /usr/local/bin/backupdbs.py




The Afrihost has a list of VM's it pulls backed up postgresql databases from every day. To view the list of vm's you will have to run:

$ sudo crontab -u backups -l
0 1 * * * rsync --delete -r -e 'ssh -p 3322' backups@localhost:/home/backups/dbs/ /home/backups/dbs/7/
0 0 * * * rsync --delete -r -e 'ssh -p 10012' backups@localhost:/home/backups/dbs/ /home/backups/dbs/6/
0 1 * * * rsync --delete -r -e 'ssh -p 2222' backups@localhost:/home/backups/dbs/ /home/backups/dbs/5/
0 1 * * * rsync --delete -r -e 'ssh -p 4422' backups@localhost:/home/backups/dbs/ /home/backups/dbs/sbu-grow/


To view the SSH port for a vm you can

$ alias



Run the command that you want to put into the crontab manually first:

$ sudo -i -u backups


$ rsync --delete -r -e 'ssh -p 3332' backups@localhost:/home/backups/dbs/  /home/backups/dbs/7-testing/


If it works fine with no problems then you can add it to crontab. Now put this into crontab, this is the entry for testing:


0 1 * * * rsync --delete -r -e 'ssh -p 3332' backups@localhost:/home/backups/dbs/  /home/backups/dbs/7-testing/

You will have to add the vm to this list if you want it backed up to the host/dedicated server. The backups can only be pulled to the inhouse backup at Datasmith if it is also pulled to the Host/Dedicated Server at Afrihost. To add an entry to the crontab on host to pull the backups from the VM to the host you will have to be logged in as user backups on the host.

$ sudo -i -u backups

Now to edit the crontab:

$ crontab -e


This part happens in the VM. This is to run the python script backupdbs.py

$ sudo vim /etc/cron.d/dbbackups 


Here is a sample dbbackups entry from cron, from openerp7 server:

MAILTO=timo@datasmith.co.za
#MAILTO=erpwebza@gmail.com

0 23 * * * root /usr/local/bin/backupdbs.py

If you see errrors like this:

root@ubuntuOP7-testing:/home/lox# /usr/local/bin/backupdbs.py
Running db backups..
bash: /home/backups/dbs/sbu-docs.dump.20140610-1035: Permission denied
Backed up db sbu-docs to file /home/backups/dbs/sbu-docs.dump.20140610-1035
bash: /home/backups/dbs/sbu-growdev.dump.20140610-1035: Permission denied
Backed up db sbu-growdev to file /home/backups/dbs/sbu-growdev.dump.20140610-1035
bash: /home/backups/dbs/sbu-smmedev.dump.20140610-1035: Permission denied
Backed up db sbu-smmedev to file /home/backups/dbs/sbu-smmedev.dump.20140610-1035
bash: /home/backups/dbs/internal.dump.20140610-1035: Permission denied
Backed up db internal to file /home/backups/dbs/internal.dump.20140610-1035
bash: /home/backups/dbs/adoptaschool.dump.20140610-1035: Permission denied
Backed up db adoptaschool to file /home/backups/dbs/adoptaschool.dump.20140610-1035
bash: /home/backups/dbs/adoptaschooldev.dump.20140610-1035: Permission denied
Backed up db adoptaschooldev to file /home/backups/dbs/adoptaschooldev.dump.20140610-1035
bash: /home/backups/dbs/balancelldev.dump.20140610-1035: Permission denied
Backed up db balancelldev to file /home/backups/dbs/balancelldev.dump.20140610-1035


Then make sure that postgres can write to the directory /home/backups/dbs:

$ sudo chown backups:postgres /home/backups/dbs



Offsite Backup Mechanism

Offsite Backups Run on the Machine in the office with the IP: 192.168.1.141. Login to the user vboxadm to find out more on this machine.

The offsite machine pulls the backups from the afrihost dedicated server everyday with:


# This is in the crontab
0 2 * * * sh /usr/local/bin/rsync.sh

To view the crontab login as user vboxadm and run

$ crontab -e

rsync.sh contains:

!/bin/bash

while [ 1 ]
do
    rsync --delete --append --partial --progress -vz -r -e 'ssh' backups@openerp.co.za:/home/backups/dbs/ /home/vboxadm/dbs/
    if [ "$?" = "0" ] ; then
        echo "rsync completed normally $(date) " >> /var/log/backups
        exit
    else
        echo "rsync failure. Retrying in a minute... $(date) " >> /var/log/backups


         sleep 43200
       # sleep 60 >> this is a issue when the power goes off or any interruption occurs

    fi
done



Install New VM on Testing Machine Internal

Install New VM on Testing Machine Internal:

$ tar -xvf GitLab.tar.xz 
$ cd GitLab/
$ vboxmanage registervm ~/VirtualBox\ VMs/GitLab/GitLab.vbox
$ VBoxManage modifyvm "GitLab" --nic1 nat
$ VBoxManage modifyvm "GitLab" --cableconnected1 on

Forward port for SSH and for OpenERP:

$ VBoxManage modifyvm "GitLab" --natpf1 ssh,tcp,,5505,,22
$ VBoxManage modifyvm "GitLab" --natpf1 ssh,tcp,,4069,,8069
$ vboxmanage startvm "GitLab" --type headless



Copy a VM from Production to Testing Server

First rsync it:

$ ssh vboxadm@192.168.1.141

$  rsync -arz -e 'ssh' erpwevpy@openerp.co.za:"/home/erpwevpy/VirtualBox\ VMs/openerp7\ Testing/" "/home/vboxadm/backup/openerp7 Testing/"

Then follow this guide:

    Install New Vm on Testing Machine Internal 

$ tar -xvf GitLab.tar.xz 
$ cd GitLab/
$ vboxmanage registervm ~/VirtualBox\ VMs/GitLab/GitLab.vbox
$ VBoxManage modifyvm "GitLab" --nic1 nat
$ VBoxManage modifyvm "GitLab" --cableconnected1 on

Forward port for SSH and for OpenERP:

$ VBoxManage modifyvm "GitLab" --natpf1 ssh,tcp,,5505,,22
$ VBoxManage modifyvm "GitLab" --natpf1 ssh,tcp,,4069,,8069
$ vboxmanage startvm "GitLab" --type headless




VirtualBox Stuff 
Delete NAT Rule VirtualBox


$ vboxmanage controlvm "vm_name" natpf1 delete "rulename"

$ vboxmanage controlvm "OpenErp5" natpf1 delete http


Shut Down VM

This is how you shutdown a VM:

$ vboxmanage controlvm "vm_name" poweroff

$ vboxmanage controlvm "ubuntu14" poweroff

List Hardrives Virtualbox

List hardrives and hardrives in Use by Virtualbox:

$ vboxmanage list hdds

Restore Snapshot

$ VBoxManage snapshot <vm_name> restore snap1-before-upgrade

$ VBoxManage snapshot vm04-zca8 restore snap1-before-upgrade

Install VboxTool

From Document left by Hiren on servers.

"vboxtool should handle clean VM shutdown and startup of VMs upon host shutdown and boot up.

Configured in /etc/vboxtool/, esp config file machines.conf (config file defines VMs to manage, and ports to forward from host to guest etc)

vboxtool --help |less, for detailed info on using and configuring

(was brought in to handle clean shutdown of VMs when host rebooted, and start of VMs when host comes up)"

Install VboxTool:


Download from here: 
http://sourceforge.net/projects/vboxtool/

$ unzip vboxtool-0.4.zip

$ sudo cp ~/script/vboxtool /usr/local/bin/

$ sudo chmod +x /usr/local/bin/vboxtool

$ sudo cp script/vboxtoolinit /etc/init.d

$ sudo chmod +x /etc/init.d/vboxtoolinit

$ sudo update-rc.d vboxtoolinit defaults 99 10

$ sudo mkdir /etc/vboxtool

$ vbox_user='vbox'

Read more here: 
http://www.admin-magazine.com/Articles/Server-Virtualization-with-VirtualBox
http://ubuntuforums.org/showthread.php?t=933099

VirtualBox Start tag expected not found

Solution:

    Start Tag Virtualbox Not Found 

"Check the files of the VM in the VirtualBox VMs folder.

If the .vbox file has size zero, then copy the .vbox-prev file on it.

Restart VirtualBox, it should be repaired."

$ du -hs openerp.vbox

0	openerp7.vbox

$ rm openerp7.vbox

$  cp openerp7.vbox-prev openerp7.vbox

$ vboxmanage registervm /home/vboxadm/backup/openerp7/openerp7.vbox

VM inaccessible error

Solution is:

    Fix VM Inaccessible 

$ vboxmanage unregistervm b5e7c99e-0271-4e64-a257-808143be014d

$ vboxmanage unregistervm <UUID>

You can get the UUID by running:

$ vboxmanage list vms

Next step is to register the VM again with the same name and start it. 


Delete Virtualbox Vm Completely

Delete Vm VirtualBox:

    Delete VM VirtualBox Correct:
http://askubuntu.com/questions/316177/how-to-completely-remove-an-os-from-virtualbox
    Vboxmanage unregistervm:
http://www.virtualbox.org/manual/ch08.html#vboxmanage-registervm 

$ VBoxManage unregistervm --delete "<Name of Machine>"

"The unregistervm command unregisters a virtual machine. If --delete is also specified, the following files will automatically be deleted as well:

all hard disk image files, including differencing files, which are used by the machine and not shared with other machines;

saved state files that the machine created, if any (one if the machine was in "saved" state and one for each online snapshot);

the machine XML file and its backups;

the machine log files, if any;

the machine directory, if it is empty after having deleted all the above. " 





Step One backup .vbox xml files
[edit] Backup old .vbox-prev

In the event of .vbox xml corruption a .vbox-prev file is created with old settings. Backup this file with 'cp' and either make this the .vbox file or

vboxmanage unregestire "vm"; vboxmanage registervm vm.vbox-prev;     ("vm" is your vm name)

[edit] Check showvminfo against .vbox

Also check that vbox file corresponds to vboxmanage showvminfo "vm"
[edit] Copy .vbox from dev server and start vm

Worst case scenario check .vbox files on dev server or similar server and recover settings with that .vbox
[edit] Step Two VM boots, but CPU cap 100% and no response through ssh or web port

Could be that VM's Grub has lost its settings and is waiting for the keyboard command "enter". To check if this is the case use the screenshotpng to take a screenshot and 'scp' from local to remote server to check file.

 vboxmanage controlvm "vm" screenshotpng /Screenshots/screenshot1_vm.png    ("vm" is your vm name)

If this is the case (by checking the png image as in example [[1]]) then this is easily bootable with the command

VBoxManage controlvm "vm" keyboardputscancode 1c 9c    ("vm" is your vm name)

The 1c 9c is the enter key in Hex code

Future fix for recurring issue is to set the GRUB default to Ubuntu, with Linux 3.2xxxxxxxxxxx and to update GRUB 





Virtual box won't start kernel issues

IF the following type of error occurs after trying to reinstall broken virtualbox

Setting up virtualbox (4.1.12-dfsg-2ubuntu0.1) ...
 * Stopping VirtualBox kernel modules                                    [ OK ] 
 * Starting VirtualBox kernel modules
 * No suitable module for running kernel found                           [fail]

[edit] Step 1 Check your kernel version

dpkg --list | grep linux-image 
or
uname -r


sudo apt-get purge virtualbox
sudo apt-get install linux-headers-server
sudo shutdown -r 1

When host is back up then re-install virtualbox

sudo apt-get install virtualbox --reinstall




DB:


select id, sale_order_id, sale_line_id from project_task where sale_line_id is not null;


select                                                                                                                                
a.id, 
a.sale_order_id, 
a.sale_line_id ,
b.order_id,
concat ('update project_task set sale_order_id = where id = ', cast( a.id as varchar )  
from 
project_task  a,
sale_order_line b
where 
a.sale_line_id is not null
and a.sale_line_id = b.id;
);


update project_task set sale_order_id = (select order_id from sale_order_line where id = sale_line_id) where sale_line_id is not null;

#################################################
Vagrant_Openerp: https://github.com/arthru/vagrant_openerp

vagrant_openerp
Vagrant and Fabric files for creating OpenERP 7 VMs

vagrant_openerp is a tool for installing automatically OpenERP 7 on a Vagrant VM. You can also use the recipe to install it on a physical server.

It is made for debian based system but it has only been tested with Ubuntu 12.04 Precise Pangolin.
Prerequisites

You must install vagrant 1.0.x (may work with 1.1) and fabtools (only tested with 0.13.0). For example, if you don't want to use your system package manager :

pip install fabtools
gem install vagrant

Note: if you are using Debian 7.4 (Wheezy) vagrant can be installed through apt/aptitude, however fabtools is not included in its repositories and pip fails to install it, so I would recommend using easy_install (which is part of python-setuptools):

aptitude install vagrant python-setuptools
easy_install fabtools

You should also add a box to vagrant :

vagrant box add precise32 http://files.vagrantup.com/precise32.box

Quickstart

Clone vagrant_openerp from github :

git clone https://github.com/arthru/vagrant_openerp.git

Go to the new directory and set up the vagrant VMs :

cd vagrant_openerp
vagrant up

Launch fab in order to provision the vm for OpenERP :

fab vagrant openerp

And here it is ! Simply browse to http://localhost:8069/ to log in your freshly installed OpenERP instance (with admin login and admin password).


##################openerp7 DataError: encoding UTF8 does not match locale en_US######################

I had the same problem. The problem is connected with wrong encoding in PostgreSQL and template1 database. The solution is to fix encoding in Linux to UTF-8 and then:

sudo su postgres

psql

update pg_database set datistemplate=false where datname='template1';
drop database Template1;
create database template1 with owner=postgres encoding='UTF-8'

lc_collate=len_US.utf8' lc_ctype='en_US.utf8' template template0;

update pg_database set datistemplate=true where datname='template1';
################################################################################


Aero Report for 8.0:https://github.com/jamotion/aeroo


##########IIA#################
                            <group class="oe_subtotal_footer oe_right" colspan="2" name="sale_total">
                                <field name="amount_untaxed" widget='monetary' options="{'currency_field': 'currency_id'}"/>
                                <field name="amount_tax" widget='monetary' options="{'currency_field': 'currency_id'}"/>
                                <div class="oe_subtotal_footer_separator oe_inline">
                                    <label for="amount_total" />
                                    <button name="button_dummy"
                                        states="draft,sent" string="(update)" type="object" class="oe_edit_only oe_link"/>
                                </div>
                                <field name="amount_total" nolabel="1" class="oe_subtotal_footer_separator" widget='monetary' options="{'currency_field': 'currency_id'}$
                            </group>
                            <div class="oe_clear"/>
                            <field name="note" class="oe_inline" placeholder="Terms and conditions..."/>
                        </page>
#################################################################################################################

###############################INSITE & Short cut##################################################################

                        <div class="oe_right oe_button_box" name="button_box">
                            <!-- Put here related buttons -->
                        </div>


                <xpath expr="//div[@name='button_box']" position="inside">
                    <button name="%(act_hr_employee_holiday_request)d" string="Leaves" type="action" groups="base.group_hr_user"/>
                </xpath>

        <!-- Shortcuts -->
        <record id="act_hr_employee_holiday_request" model="ir.actions.act_window">
            <field name="name">Leaves</field>
            <field name="type">ir.actions.act_window</field>
            <field name="res_model">hr.holidays</field>
            <field name="src_model">hr.employee</field>
            <field name="view_type">form</field>
            <field name="view_mode">tree,form</field>
            <field name="context">{'search_default_employee_id': [active_id], 'default_employee_id': active_id}</field>
            <field name="domain">[('type','=','remove')]</field>
            <field name="view_id" eval="view_holiday"/>
        </record>

###############################################################################################################################

############################################SMART BUTTON##########################################
Creating Smart Buttons in Odoo v8
New Video Update

In this video we use much of the new Odoo 8 features for defining the functional fields and triggering them... but we use the old style Odoo 7 code for actual processing of the recordset. View our new video on Odoo 8 API  Recordsets and learn how to update the payment buttons to use the new Odoo 8 API for processing.

Steps to creating Smart Buttons in Odoo 8

When creating smart buttons in Odoo 8 you must tie together several aspects of Odoo development. Below are the basic steps to creating a smart button with example code segments located below.
1. Add required fields to the model so you can store your calculations
    voucher_count = fields.Integer(compute='_voucher_count', string='# of Vouchers')
    account_voucher_ids = fields.One2many('account.voucher', 'partner_id', 'Voucher History')

2. Create a custom view template and use the interhit id that matches the external id of the form
    <record id="view_partners_form_crm2" model="ir.ui.view">
            <field name="name">view.res.partner.form.crm.inherited2</field>
            <field name="model">res.partner</field>
            <field name="priority" eval="200"/>
            <field name="inherit_id" ref="base.view_partner_form"/>
3. Use xpath in the template to tell the Odoo framework where to place your button
<xpath expr="//div[@name='buttons']" position="inside">
4. Add your Odoo smart button code to the view
<button class="oe_inline oe_stat_button" type="action" name="%(pay_view_voucher_tree)d"
               attrs="{'invisible': [('customer', '=', False)]}"
               icon="fa-usd">
        <field string="Payments" name="voucher_count" widget="statinfo"/>
   </button>
5. Create a python function to calcuate the functional field (voucher_count)
    @api.one
    @api.depends('account_voucher_ids','account_voucher_ids.state')
    def _voucher_count(self):
        vouchers = self.env['account.voucher'].search([('partner_id', '=', self.id)])
        self.voucher_count = len(vouchers)  
6. Create a view for the action to display your results (payments in this case) when the user clicks the button
          <record model="ir.actions.act_window" id="pay_view_voucher_tree">
            <field name="name">Payments</field>
            <field name="res_model">account.voucher</field>
            <field name="view_type">form</field>
            <field name="view_mode">tree,form,graph</field>
            <field name="context">{'search_default_partner_id': active_id}</field>
            <field name="arch" type="xml">
                <tree string="Payments">
                    <field name="date"/>
                    <field name="number"/>
                    <field name="reference"/>
                    <field name="partner_id"/>
                    <field name="type" invisible="context.get('visible', True)"/>
                    <field name="amount" sum="Total Amount"/>
                    <field name="state"/>
                </tree>
            </field>
        </record>
############################################################################################################
Odoo8:	http://odedrabhavesh.blogspot.in/2014/12/how-to-install-wkhtmltopdf-in-odoo.html

#http://programminglabpoint.blogspot.com/2012/11/install-tryton-on-ubuntu-12.html (For Tryton)

WIKI:

Install Aeroo Odoo Ubuntu LTS 14 04

On Ubuntu LTS there is an issue with LibreOffice and python-uno package, the python-uno package on Ubuntu LTS 14.04 is the python 3 package.

You will have to follow these instructions to get the correct versions installed of LibreOffice and python-uno on Ubuntu LTS 14.04:

    Rebuild python-uno and Libre Office Ubuntu LTS 14.04 (https://gist.github.com/hbrunn/6f4a007a6ff7f75c0f8b)

Use a combination of these two instructions to get aeroo installed:

    Install Aeroo for Odoo lukbranch Github (https://github.com/lukebranch/aeroo)
    Install Aeroo Jamotion Github (https://github.com/jamotion/aeroo)

It is very important that you install this base_field_serialized module too on Odoo:

Clone it into your custom modules folder and login on Odoo and install it as a normal module before installing report_aeroo and report_aeroo_ooo

$ git clone https://github.com/jamotion/base_field_serialized

You need to install Aerolib also before installing the aero odoo modules:

$ cd /tmp
$ git clone https://github.com/jamotion/aeroolib.git
$ cd aeroolib
$ python setup.py install

You will need to start libreoffice in order for Aero OpenOffice module to connect to Libreoffice. You need to start LibreOffice from the commandline with this command:

$ sudo soffice --nologo --nofirststartwizard --headless --norestore --invisible "-accept=socket,host=localhost,port=8100,tcpNoDelay=1;urp;" &

You need to test the connection to LibreOffice. In Odo go to Settings-> Aeroo Reports-> Configure OpenOffice

To keep everything working fine you want LibreOffice to start automatically at boot. Create a script and add the above command to the script.

$ sudo vim /usr/local/bin/start_soffice.sh

Now in root's crontab add the script and tell it to start on reboot:

$ sudo crontab -e

The crontab entry should look like this:


# 0 5 * * 1 tar -zcf /var/backups/home.tgz /home/
# 
# For more information see the manual pages of crontab(5) and cron(8)
# 
# m h  dom mon dow   command
@reboot /usr/local/bin/start_soffice.sh


You also want to make sure that the custom .deb packages that you had to recompile and install never update during apt-get update or apt-get upgrade. Mark them to be held back with the following commands:

$ echo libreoffice-common hold | sudo dpkg --set-selections
$ echo libreoffice-core hold | sudo dpkg --set-selections
$ echo python-uno hold | sudo dpkg --set-selections

or

$ sudo apt-mark hold python-uno
$ sudo apt-mark hold libreoffice-common
$ sudo apt-mark hold libreoffice-core

# Proxy Pass not working
$ sudo su

a2enmod proxy
a2enmod proxy_http
a2enmod proxy_connect
systemctl restart apache2
systemctl reload apache2

#Djidda: (ssh -p 1607 192.168.1.138 -l iia)'sftp://192.168.0.138:1607/opt' => Yaounde TODO

#Database issue: error 'database databasename is being accessed by other users'.
select pg_terminate_backend(pid) from pg_stat_activity where datname='YourDatabase';	# pid or procpid
DROP DATABASE "YourDatabase";



#POSTFIX: http://flurdy.com/docs/postfix/   (sudo apt-get install postfix postfix-mysql)





#Good for Openerp!!!! (https://pypi.python.org/pypi/openerp-client-lib/1.0.3) post from: http://stackoverflow.com/questions/11493578/how-to-write-a-python-script-that-uses-the-openerp-orm-to-directly-upload-to-pos ; also see odoo doc @: https://www.odoo.com/documentation/8.0/api_integration.html ; https://doc.odoo.com/6.0/developer/6_22_XML-RPC_web_services/ ; ERPpeek: http://wirtel.be/posts/en/2014/06/13/using_erppeek_to_discuss_with_openerp/


OpenERP Client Library Guide

First install the library:

sudo easy_install openerp-client-lib

Now copy-paste the following script describing a simple interaction with an OpenERP server:

import openerplib

connection = openerplib.get_connection(hostname="localhost", database="my_db", \
    login="my_user", password="xxx")
user_model = connection.get_model("res.users")
ids = user_model.search([("login", "=", "admin")])
user_info = user_model.read(ids[0], ["name"])
print user_info["name"]
# will print "Administrator"

In the previous script, the get_connection() method creates a Connection object that represents a communication channel with authentification to an OpenERP server. By default, get_connection() uses XML-RPC, but you can specify it to use NetRPC. You can also change the port. Example with a NetRPC communication on port 6080:

connection = openerplib.get_connection(hostname="localhost", protocol="netrpc", port=6080, ...) # (connection = openerplib.get_connection(hostname="41.223.33.6", protocol="xmlrpc", port=7069, database=dbname, \
    login=username, password=pwd))

The get_model() method on the Connection object creates a Model object. That object represents a remote model on the OpenERP server (for OpenERP addon programmers, those are also called osv). Model objects are dynamic proxies, which means you can remotely call methods in a natural way. In the previous script we demonstrate the usage of the search() and read() methods. That scenario is equivalent to the following interaction when you are coding an OpenERP addon and this code is executed on the server:

user_osv = self.pool.get('res.users')
ids = user_osv.search(cr, uid, [("login", "=", "admin")])
user_info = user_osv.read(cr, uid, ids[0], ["name"])

Also note that coding using Model objects offer some syntaxic sugar compared to vanilla addon coding:

    You don’t have to forward the “cr” and “uid” to all methods.

    The read() method automatically sort rows according the order of the ids you gave it to.

    The Model objects also provides the search_read() method that combines a search and a read, example:

    user_info = user_model.search_read([('login', '=', 'admin')], ["name"])[0]

Here are also some considerations about coding using the OpenERP Client Library:

    Since you are using remote procedure calls, every call is executed inside its own transaction. So it can be dangerous, for example, to code multiple interdependant row creations. You should consider coding a method inside an OpenERP addon for such cases. That way it will be executed on the OpenERP server and so it will be transactional.
    The browse() method can not be used. That method returns a dynamic proxy that lazy loads the rows’ data from the database. That behavior is not implemented in the OpenERP Client Library but it could be in future versions.

flask/bin/pip install gunicorn
source /home/gutembert/Documents/microblog/flask/bin/activate
cd /home/gutembert/Documents/microblog
######run.py###### Ref: https://realpython.com/blog/python/kickstarting-flask-on-ubuntu-setup-and-deployment/
#!flask/bin/python
from app import app
if __name__ == '__main__':
app.run(host='0.0.0.0')	#app.run(debug=True)
############
gunicorn run:app -b localhost:5000

sudo pkill gunicorn

#(host='0.0.0.0', port=5000)	#app.run(debug=True)



#My psql tricks: sudo su postgres
# psql 
# create database <db_name>
# c^ D
# psql -U postgres -d <db_name> -f <backup.sql> or pg_restore -U <username> -d <dbname> -1 -f <filename>.dump
# psql
# alter database sbu_2015_05_07 owner to odoo;

#NB: Backup&Restore

#   sudo su postgres

#   Backup:  $ pg_dump -U {user-name} {source_db} -f {dumpfilename.sql}

#   Restore: $ psql -U {user-name} -d {desintation_db}-f {dumpfilename.sql}



# Postgres Backup and Restore Command Examples: http://www.thegeekstuff.com/2009/01/how-to-backup-and-restore-postgres-database-using-pg_dump-and-psql/ or http://stackoverflow.com/questions/2732474/restore-a-postgres-backup-file-using-the-command-line
#Automated Backup and Restore PostGres: 

	All about PostGres Backup and Restore "A backup that is not tested , is not a backup."

#	    PostGreSQL Corruption: https://wiki.postgresql.org/wiki/Corruption
#	    The Worst Day of Your Life: Presentation on PostGreSQL DB Corruption: http://thebuild.com/presentations/worst-day-fosdem-2014.pdf
#	    Tips on PostGreSQL+OpenERP Performance: http://www.slideshare.net/openobject/tips-on-how-to-improve-the-performance-of-your-custom-modules-for-high-volumes-deployment-olivier-dony-open-erp#btnNext 

	Todo:

	    Need to create backup strategy.
	    Need to automate restoring backups on a clean host. Need to login as each company and check if everything is fine.
	    Need to write some code to restore backup of db and run some queries to check that db has been restored without problems. 


	Idea for backup script

# 	Dump all databases.


# 	Rsync to another machine.

#	Drop all databases.

#	Create databases for each datbase name in a given list.

#	Restore database for each given database.

# Docker: http://danielmartins.ninja/posts/a-week-of-docker.html

###
# crontabs' stuff:
#	To list all the users on crontabs: for user in $(cut -f1 -d: /etc/passwd); do echo $user; crontab -u $user -l; done (http://stackoverflow.com/questions/134906/how-do-i-list-all-cron-jobs-for-all-users)
###


#Start
The Postgresql backup for Hetzner Server Schedule

pg_backup_rotate 1:30am every day with backups kept for 7days and 2 weeks

rsync from hetzner to internal 3am
Scripts

pg_backup_rotate.sh (script to backup and delete old backups) located (pg_backup.config  pg_backup_rotated.sh  pg_backup.sh) at: /home/username/backup_scripts then the dbs are stored in /home/username/database_backup/postgresql

pg_backup.config (settings)

#Source:https://wiki.postgresql.org/wiki/Automated_Backup_on_Linux
Settings Hetzner
User

Unix - postgres Postgres - postgres
Host

Host - Localhost
Backup Location

/home/erpwevpy/database_backup/postgresql/ ( will be the date of when the backup ran)
Crontab
On Hetzner

30 1 * * * /home/cg/postgres_backup_scripts/pg_backup_rotated.sh > /dev/null
Settings Local Devs
Crontab

0 3 * * * rsync -avz -e ssh erpwevpy@odoo.zone:/home/erpwevpy/database_backup/postgresql/ /home/vboxadm/backup_hetzner/
SSH key

Added Hetzner public key to known_hosts
#end

# # sudo apt-get install apache2 libapache2-mod-fcgid apache2-mpm-worker php5 php5-cgi (http://www.binarytides.com/setup-apache-php-mod-fcgid-ubuntu/)


#####
#Odoo_Web: https://www.hbee.eu/en-us/blog/archive/category/odoo/
#Bitbucket: https://bitbucket.org/hbee/odoo_blog/overview
#####

####
# Multiple dbs on their subdomain listenig to one port:
#   trick No1: make sure the db_name=subdomain (db_name=host.split('.')[0] see: https://bugs.launchpad.net/openobject-server/+bug/1195341) 
#   trick No2: install the 'dbfilter_from_header' module from Odoo (https://apps.openerp.com/apps/modules/7.0/dbfilter_from_header/)
#   In nginx, use proxy_set_header X-OpenERP-dbfilter [your filter];
#   In apache2, use Header add X-ODOO_DBFILTER "company1", RequestHeader add X-ODOO_DBFILTER "company1" see: https://github.com/odoo/odoo/issues/1401 NB: first install headers mod (sudo a2enmod headers)
#   And finaly, all are solved by seting db_host = False, db_name = False, db_port = False, dbfilter = %d, list_db = True and * xmlrpc = True, xmlrpc_interface = 127.0.0.1, xmlrpc_port = 8069, xmlrpcs = True, xmlrpcs_interface =, xmlrpcs_port = 8071 * from blocking acces from host:port; in the /etc/openerp/openerp-server.conf then ensure to create a db with name host subdomaine (db_name=subdomain (db_name=host.split('.')[0]) see: https://elmerdpadilla.wordpress.com/2014/08/30/domain-based-db-filter/
#   Known bug fixed for this regards: https://bugs.launchpad.net/openobject-server/+bug/940439
####

###
# Run Odoo from port 80 instead of 8069: http://pulse7.net/openerp/run-odoo-port-80-instead-8069/
###

###
# Tips for keeping the run.py (./run.py) running for ever! do: create an executable file flash_web_serv.sh in /etc/init.d/ containing: #!/bin/bash sudo python /var/www/firsttrophy/run.py & as sudo su then update-rc.d flash_web_serv.sh defaults, then reboot the server!; optional (do the sudo crontab -e and add these lines: @reboot /etc/init.d/flash_web_serv.sh; 49 17 * * * ./init.d/flash_web_serv.sh)
###

### dbs Migration!!!
#https://doc.therp.nl/openupgrade/intro.html
# sudo apt-get install python-bzrlib
# sudo python /home/temo/odoo_migration/migrate.py --config=/etc/openerp/openerp-server.conf --database=firsttrophy_2015_05_24 --run-migrations=8.0	#or --run-migrations=7.0,8.0
# psycopg2.OperationalError: FATAL:  Peer authentication failed for user "openerp"
# External access to a Postgres DB at the OpenERP VM (db remote access) = https://community.bitnami.com/t/external-access-to-a-postgres-db-at-the-openerp-vm/21520
# sol: sudo nano /etc/postgresql/9.3/main/pg_hba.conf, Placing this line:
#local   all             openerp                                 trust
#Just before:
# "local" is for Unix domain socket connections only
#local   all             all                                     peer
# sudo su postgres, service postgresql restart, C^D, sudo su, then python /home/temo/odoo_migration/migrate.py --config=/etc/openerp/openerp-server.conf --database=firsttrophy_2015_05_24 --run-migrations=7.0,8.0 or $ ./migrate.py --config=/etc/openerp/openerp-server.conf --database=ctmil_v7_0 --run-migrations=8.0 -B ../.. -A l10n_ar_migrate.py
###
