Create Qweb Report in Odoo



Steps to create Qweb report in Odoo.

QWeb is the template engine used by the OpenERP Web Client. It is an XML-based templating language. Qweb reports are view based reports that offer the same flexibility as standards OpenERP views, (inherit, groups, xpath, translation, etc).

First of all you need to understand the template engine which is used in qweb report. You can refer it from : https://doc.odoo.com/trunk/web/qweb/ . Template engine provides looping structure, conditional structure and many more features which you can refer it from the above link.

There are three basic things you need to perform to create qweb report.

    Create parser class to access dynamic data in report.
    Create template based view
    Register qweb report menu

Create parser class and wrapped it in template to use it in report.

Parser Class:

To create parser class you need to import report_sxw.rml_parse.
The methods you want to access in report must be updated in localcontext in parser class.

Create another class which which inherits features of osv.AbstractModel and it creates bond between parser class and template engine.
This new class have few parameters and each of the parameters have fixed pattern so must follow it.

_name = ‘report.<module_name>.<report_name>’
_inherit = ‘report.abstract_report’
_template = ‘<module_name>.<report_name>’
_wrapped_report_class = <parser_class_name>

QWeb Report View:

The rationale behind using QWeb instead of a more popular template syntax is that its extension mechanism is very similar to the openerp view inheritance mechanism. Like openerp views a QWeb template is an xml tree and therefore xpath or dom manipulations are easy to performs on it.

Report Menu:
Used to create menu for the report.

Let’s create new Qweb report from scratch

Create parser class first

import time
from openerp.osv import osv
from openerp.report import report_sxw

class sale_quotation_report(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(sale_quotation_report, self).__init__(cr, uid, name, context=context)
            self.localcontext.update({
                    ‘time’: time,
                    ‘get_total’: self._get_total,
            })

    def _get_total(self, lines, field):
        total = 0.0
        for line in lines :
            total += line.product_uom_qty or 0.0
        return total

class report_saleorderqweb(osv.AbstractModel):
    _name = ‘module_name.report_sale_order_qweb’
    _inherit = ‘report.abstract_report’
    _template = ‘module_name.report_sale_order_qweb’
    _wrapped_report_class = sale_quotation_report

Create Report View :
In this example we are printing only few details of sale order – product name with it’s qty and sum of qty at the end of report.

<?xml version=”1.0″ encoding=”utf-8″?>
<openerp>
<data>
<template id=”report_sale_order_qweb”>
    <t t-call=”report.html_container”>
        <t t-foreach=”docs” t-as=”o”>
            <div class=”page”>
                <div class=”oe_structure”/>
                    <div class=”row”>
                        <table style=”font-name: ‘Helvetica’;width:100%;”>
                            <tr colspan=”4″ style=”font-size: 24px;font-weight: bold;”>
                                <td align=”center”>
                                      <strong> <p t-field=”o.warehouse_id.partner_id.name”/> </strong>
                                </td>
                            </tr> 
                            <tr colspan=”4″ style=”font-size: 12px;”>
                                 <td align=”center”>
                                       QUOTE NO :
                                        <strong>
                                            <span t-field=”o.name”/>
                                        </strong>

                                       QUOTE DATE :
                                       <strong>
                                            <span t-field=”o.date_order” t-field-options=”{‘format’: ‘d/MM/y’}”/>
                                       </strong>

                                       SALES PERSON :
                                            <span t-field=”o.user_id.partner_id.name”/>
                                </td>
                           </tr>
                      </table>
                 </div>
                 <br/>
                 <table class=”table-condensed” style=”font-size: 12px;”>
                     <thead>
                         <tr style=”border-bottom: 1px solid black;”>
                              <th>
                                   <strong>Name</strong>
                              </th>
                              <th>
                                   <strong>Qty</strong>
                              </th>
                              </tr>
                     </thead>
                     <tbody>
                        <tr t-foreach=”o.order_line” t-as=”line” style=”border-bottom: 1px solid black;”>
                              <td>
                                   <span t-field=”line.product_id.name”/>
                              </td>
                              <td>
                                    <span t-field=”line.product_uom_qty”/>
                               </td>
                         </tr>
                          <tr> 
                               <td/>
                               <td>
                                     <!– Print total of product uom qty –>
                                     <strong>
                                           <span t-esc=”get_total(o.order_line)”/>
                                     </strong>
                                </td> 
                         </tr>
                    </tbody>
              </table>
            
          </div>
        </t>
    </t>
</template>
</data>
</openerp>

 

Add Menu for the Report

it will add menu “Quotation / Order” to the Sale Order inside the print option.

<report
           string=”Quotation / Order”
           id=”sale.report_sale_order”
           model=”sale.order”
           report_type=”qweb-pdf”
           name=”module_name.report_sale_order_qweb”
           file=”module_name.report_sale_order_qweb”
/>

