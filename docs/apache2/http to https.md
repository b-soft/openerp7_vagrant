#OpenERP 7 with SSL on Ubuntu 12.04 (http://acespritechblog.com/2013/05/29/openerp-7-with-ssl-on-ubuntu-12-04/)


Lets assume that apache and required packages are installed on system.

There are some commands needs to execute to run OpenERP on SSL.

#    sudo apt-get install apache2
#    sudo a2enmod ssl proxy_http headers rewrite

Now we have created certificate and key so we have to execute these commands one by one.

#    sudo mkdir /etc/ssl/apache2
#    From Home directory, do:

    openssl genrsa -des3 -out oeserver.pkey 1024
    openssl rsa -in oeserver.pkey -out oeserver.key
    openssl req -new -key oeserver.key -out oeserver.csr

At last we signed certificate here.

    openssl x509 -req -days 365 -in oeserver.csr -signkey oeserver.key -out oeserver.crt
#    Then cp/mv these files to /etc/ssl/apache2

We also created apache configuration file.

    sudo vim /etc/apache2/sites-available/openerp

    <virtualhost *:80>
        ServerName me.mysite.com
        Redirect / https://me.mysite.com/

    </virtualhost>


    <VirtualHost *:443>

    ServerName me.mysite.com
    ServerAdmin webmaster@localhost

    SSLEngine on
    SSLCertificateFile /etc/ssl/apache2/oeserver.crt
    SSLCertificateKeyFile /etc/ssl/apache2/oeserver.key

    ProxyRequests Off

    <Proxy *>
    Order deny,allow
    Allow from all
    </Proxy>
    ProxyVia On
    ProxyPass / http://127.0.0.1:8069/
    <location />
    ProxyPassReverse /
    </location>

    RequestHeader set “X-Forwarded-Proto” “https”

    SetEnv proxy-nokeepalive 1
    </VirtualHost>

Now enable above configuration execute below command

    sudo a2ensite openerp


