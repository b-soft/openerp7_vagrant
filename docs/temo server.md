https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-12-04

###########################################################
1-Initial Server Setup with Ubuntu 12.04 or latest

ks3ujire 41.185.27.177

ssh -p 1981 temo@41.185.27.177;T3m0#1;http://41.185.27.177:8069;temo;temo in digit
passwd: T3m0#1 
http://41.185.27.177:8069;temo;temo in digit

######################
ssh root@123.45.67.890

sudo passwd (To change the password)

sudo adduser temo (to create a new user)

visudo

	Find the section called user privilege specification. It will look like this:

	# User privilege specification
	root    ALL=(ALL:ALL) ALL

	Under there, add the following line, granting all the permissions to your new user:

	temo    ALL=(ALL:ALL) ALL

	Type ‘cntrl x’ to exit the file.

nano /etc/ssh/sshd_config

	Find the following sections and change the information where applicable:

	Port 1981
	Protocol 2
	PermitRootLogin no

	We’ll take these one by one.

	Port: Although port 22 is the default, you can change this to any number between 1025 and 65536. In this example, I am using port 25000. Make sure you make a note of the new port number. You will need it to log in in the future. This change will make it more difficult for unauthorized people to log in.

	PermitRootLogin: change this from yes to no to stop future root login. You will now only be logging on as the new user.

	Add these lines to the bottom of the document, replacing *demo* in the AllowUsers line with your username. (AllowUsers will limit login to only the users on that line. To avoid this, skip this line):

	UseDNS no
	AllowUsers temo

	Save and Exit

Add Public key into the server Authorise keys:
if .ssh directory exists, copy and paste your public key in .ssh/authorized_keys
else, mkdir -p ~/.ssh/ (then the file authorized_keys will be created where the public key can be stored)

reload ssh
ssh -p 1981 temo@41.185.27.177

	Your prompt should now say:

	[temo@yourname ~]$



2-How To Install Linux, Apache, MySQL, PHP (LAMP) stack on Ubuntu

sudo apt-get update
sudo apt-get install apache2
	That’s it. To check if Apache is installed, direct your browser to your server’s IP address (eg. http://12.34.56.789). 
	The page should display the words “It works!" like this.

	a-Install PHP
	Enable PHP support for apache2 webserver

	If you want to enable php5 or php4 support to your apache webserver use the following commands to install require packages

	For PHP5

	    sudo aptitiude install php5 libapache2-mod-php5 or sudo apt-get install php5 libapache2-mod-php5

	For PHP4

	    sudo aptitiude install php4 libapache2-mod-php4

	You also make sure the php5 and php4 modules are enabled using the following commands

	sudo a2enmod php5

	sudo a2enmod php4

	After installing php support you need to restart apache webserver using the following command

	sudo apache2ctl restart

	Test your PHP Support foe apache webserver

	To check the status of your PHP installation

	sudo nano /var/www/testphp.php

	and insert the following line

	    <?php phpinfo(); ?>

	Save and exit the file

	Now open web browser at http://yourserveripaddress/testphp.php and check.


	b-Enable CGI and perl support for apache2 server

	You need to install the following package

	    sudo aptitude install libapache2-mod-perl2

	Configure a cgi-bin directory

	You need to create a cgi-bin directory using the following command

	sudo mkdir /home/www/cgi-bin

	Configuring Apache to allow CGI program execution is pretty easy. Create a directory to be used for CGI programs and add the following to the site configuration file (again between the <VirtualHost> tags).

	    ScriptAlias /cgi-bin/ /home/www/cgi-bin/

	    <Directory /home/www/cgi-bin/>
	    Options ExecCGI
	    AddHandler cgi-script cgi pl (add also py so that files that end with the *.py extensions should be considered CGI programs and executed.)
	    </Directory>

	The first line creates an alias that points to the directory in which CGI scripts are stored. The final line tells Apache that only files that end with the *.cgi and *.pl extensions should be considered CGI programs and executed.

	Test your Perl Program

	cd /home/www/cgi-bin

	sudo nano perltest.pl

	Copy and paste the following section save and exit the file.

#!/usr/bin/perl -w
print "Content-type: text/html\r\n\r\n";
print "Hello there!<br />\nJust testing .<br />\n";

for ($i=0; $i<10; $i++)
{
  print $i."<br />";     
}

	make sure you change permissions on it
	sudo chmod a+x perltest.pl NB: All files in cgi-bin must be executable (chmod +x file_name)

go in sudo nano  /etc/apache2/sites-available/default-ssl.conf and comment out:
        ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
        <Directory "/usr/lib/cgi-bin">
                AllowOverride None
                Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
                Order allow,deny
                Allow from all
        </Directory>


3- Install virtualbox and vagrant
	a- sudo apt-get install virtualbox
	b- aptitude install vagrant python-setuptools
		easy_install fabtools (pip install fabtools)
		You should also add a box to vagrant :
		vagrant box add precise32 http://files.vagrantup.com/precise32.box not w!



NEXT: STARTING UP VMs (https://leemendelowitz.github.io/blog/ubuntu-server-virtualbox.html or http://www.perkin.org.uk/posts/create-virtualbox-vm-from-the-command-line.html):
cd ~/VirtualBox\ VMs/

# Change these variables as needed
VM_NAME="UbuntuServer"
UBUNTU_ISO_PATH=~/Downloads/ubuntu-14.04.1-server-amd64.iso
VM_HD_PATH="UbuntuServer.vdi" # The path to VM hard disk (to be created).
SHARED_PATH=~ # Share home directory with the VM


vboxmanage createvm --name $VM_NAME --ostype Ubuntu_64 --register
vboxmanage createhd --filename $VM_NAME.vdi --size 32768
vboxmanage storagectl $VM_NAME --name "SATA Controller" --add sata --controller IntelAHCI
vboxmanage storageattach $VM_NAME --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium $VM_HD_PATH
vboxmanage storagectl $VM_NAME --name "IDE Controller" --add ide
vboxmanage storageattach $VM_NAME --storagectl "IDE Controller" --port 0 --device 0 --type dvddrive --medium $UBUNTU_ISO_PATH
vboxmanage modifyvm $VM_NAME --ioapic on
vboxmanage modifyvm $VM_NAME --memory 1024 --vram 128
vboxmanage modifyvm $VM_NAME --nic1 nat
vboxmanage modifyvm $VM_NAME --natpf1 "guestssh,tcp,,2222,,22"
vboxmanage modifyvm $VM_NAME --natdnshostresolver1 on
vboxmanage sharedfolder add $VM_NAME --name shared --hostpath $SHARED_PATH --automount

INSTALL Odoo (see OpenERPDebianInstall7.doc or 8)
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys DEF2A2198183CBB5




