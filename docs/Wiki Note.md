							
-----------------------------------------------------------Internal stuff------------------------------------------------------------------------------------- 

Create a new vm on prod
-----------------------

create a vm as the correct user (not root)

   VBoxManage createvm --name 'charterconnect'

register the vm

  VBoxManage registervm <path to vbox file from previous command>

create a disk for the vm

  VBoxManage createhd --filename <path to file> --size 8000 --format VDI 

switch on the vrde display

  VBoxManage modifyvm <vmname> --vrde on
  VBoxManage modifyvm <vmname> --vrdeport 10000

add a disk to the vm

  VBoxManage storagectl <vmname> --add scsi --name disk1 --bootable on

add a cdrom to the vm

   VBoxManage storagectl <vmname> --add ide --name cdrom --bootable on

add storage to files on host

  VBoxManage storageattach <vmname> --storagectl cdrom --port 0 --device 0  --type  dvddrive --medium <path to iso>
  VBoxManage storageattach <vmname> --storagectl disk1 --port 0 --device 0  --type  hdd --medium <path to vdi file>

add network to the vm

  VBoxManage modifyvm <vmname>  --nic1 nat
  VBoxManage modifyvm <vmname> --cableconnected1 on

port forward to vm (host 3323 to guest 22)

  VBoxManage modifyvm <vmname> --natpf1 ssh,tcp,,3323,,22

NOTE: modifyvm doesn't work on running VMs, so use controlvm instead:

  VBoxManage controlvm <vmname> natpf1 ssh,tcp,,3323,,22

start vm

   VBoxManage startvm <vmname> --type headless

connect to vm vrde port on host with rdp, install guest

remove cdrom from guest

  VBoxManage storageattach <vmname> --storagectl cdrom --port 0 --device 0  --type  dvddrive --medium none

----------------------------------------------------------------------------------------------------------------------


OpenERP hour topics
-------------------

    using erppeek howto
    security and access control
    debugging and diagnosing howto
    xpaths in xmls
    module inheritance
    reports and rml and libreoffice reports etc 

------------------------------------------------------------------


Prod vm's and ports
-------------------

Host ports natted to guest services:

    openerp 5 

 gtk client 8169
 http 9999
 munin 6001
 psql 10011
 ssh 2222
 vnc 5411

    openerp 6 

 unknown 8070
 munin 6000
 openerp 8069
 psql 9834
 ssh 10012

    openerp 7 

 munin 6002
 openerp 3389
 psql 9837
 ssh 3322
 http 1246 (sbu-reporting.openerp.co.za) Asp App That Uses Apache+ Mod_Mono)

    openerp 7 flask 

 flask 5000
 munin 6003
 nginx 5001
 ssh 3321

    openerp 7 testing 

 munin 6005
 openerp 4445
 ssh 3332

    openerp6 headless clone dev 

 ssh 11122
 openerp 11169
 unknown 10070
 munin 6004

    Shanduka Black Umbrellas Grow Project (sbu-grow) 

 ssh 4422
 openerp 4469

-----------------------------------------------------------------------------------------------

Allow remote access to psql server
Create a psql user that will access database remotely
-----------------------------------------------------

Switch user to the psql postgres user

   $ sudo su - postgres

open psql

   $psql

create a new user

   #  CREATE USER lox WITH PASSWORD 'lox';

give user specific privileges

   # GRANT ALL PRIVILEGES ON DATABASE specific_database to lox; 
   # exit


edit the pg_hba.conf file to allow connections (you can add more security, please do)

   $vim /etc/postgresql/9.1/main/pg_hba.conf

append the following line to the end of pg_hba.conf file

   host    all             openerp        0.0.0.0/0          md5

exit and save the file then edit the /etc/postgresql/9.1/main/postgresql.conf file

   $vim /etc/postgresql/9.1/main/postgresql.conf

uncomment the following line

   #listen_addresses = 'localhost'          # what IP address(es) to listen on;

change it to

   listen_addresses = '*'          # what IP address(es) to listen on;

logout as postgres and restart the postgresql service 

------------------------------------------------------------------------------------------------------

Using psql to access the postgres db
------------------------------------

    ssh to relevant vm 

    switch user to postgres 

 su - postgres

    drop into psql shell 

 psql

    list all db's 

 \l

    connect to a db 

 \c <db>

    list tables 

 \dt
 \dtS+

    describe a table 

 \d table

    query a table 

 select * from <table>

------------------------------------

Openerp dns with sadomain
-------------------------

http://www.sadomain.co.za/

(Go to the linux panel)

Username: xamissa

Password: R0semary 

----------------------------------

Client issue report check list
List of what to get from clients on report of hosted issues
------------------------------------------------------------

    dns lookup to www.openerp.co.za
    speedtest.net result, to see what bandwidth they currently have
    ping output to www.openerp.co.za
    traceroute to www.openerp.co.za
    telnet to the port of the instance they are using
    complete browser error message or screenshot of error
    timestamp of when the issue was seen
    username of user having the issue
    browser version and maybe what plugins are installed on the browser 

---------------------------------------------------------------------------

Start up flask
---------------

tmux double screen.

 $tmux 
 ^b"

enter virtualenv in both

 $. ~/venv/bin/activate

cd to latest directory

 $cd ~/latest

enter the following command in screen one

 gunicorn charterquest:app --log-level=DEBUG --timeout 180

enter the following command in screen two

 celery -A tasks worker --loglevel=info

------------------------------------------------------------------

Cron
----

A list of Cron jobs currently running in production.
openerp6

Grabs general ledger data from the bu database on openerp5 and pulls it into the sbu database on openerp6. The data is then used in the donations report from the donations module.

 # m h  dom mon dow   command
 @daily /opt/openerp/server/openerp/addons/donations/xml-rpc/get_general_ledger_balances.py > /dev/null 2>&1

-------------------------------------------------------------------------------------------------------------------

Monit
-----

Monit production configuration notes.
openerp5

Monit has been configured to monitor the openerp-server process since it appears to die periodically. Below is the monitrc that was used (available at /etc/monitrc and /etc/monit/monitrc):

   set mailserver 10.0.2.2 # primary only on the VBox host
   set mail-format { from: monit@erpwebsolutions.openerp.co.za }
   set alert erpwebza@gmail.com
   # openerp-server    
   check process openerp-server with pidfile /var/run/openerp-server.pid
       start program = "/etc/init.d/openerp-server start"
       stop program = "/etc/init.d/openerp-server stop"
       if failed port 8070 then restart
       if 3 restarts within 5 cycles then timeout

    Both /etc/monitrc and /etc/monit/monitrc need to be kept in sync because of a bug in this particular version of Monit.
    Notifications will be sent to erpwebza@gmail.com.
    If the server has to be restarted 3 times within 5 consecutive cycles of 180 seconds it will give up trying to restart it. 

-----------------------------------------------------------------------------------------------------------------------------------

							
-----------------------------------------------------------Useful config links-------------------------------------------------------------------------------------
                                         ------------------OpenERP v7------------------------------

Multi-company setup: http://www.zbeanztech.com/blog/multi-company-configuration-openerp-7?page=0%2C0%2C0%2C6

-------

Install notes
-------------
Contents

 Using the Package Manager
 Install

   $ sudo vi /etc/apt/sources.list.d/openerp.list

Add the following line to the file, save and exit.

   deb http://nightly.openerp.com/7.0/nightly/deb/ ./

Then update and install openerp from the new source

   $ sudo apt-get update
   $ sudo apt-get install openerp

 Upgrade

   $ sudo apt-get update
   $ sudo apt-get install openerp

 Additional Addons Directory

   $ sudo mkdir /opt/custom_modules
   $ sudo vi /etc/openerp/openerp-server.conf

Add the following to the file, save and exit

   addons_path = /usr/lib/pymodules/python2.7/openerp/addons,/opt/custom_modules

 From Source

original article

How to install OpenERP 7.0 on Ubuntu 12.04 LTS

Introduction

Welcome to the latest of our very popular OpenERP installation "How To".

The new release of OpenERP 7.0 is a major upgrade and a new Long Term Support release; the 7.0 Release Notes extend to over 90 pages! The most noticeable change is a complete re-write of the User Interface that features a much more modern look and feel.

OpenERP 7.0 is not only better looking and easier to use, it also brings many improvements to the existing feature-set and adds a number of brand new features which extend the scope of the business needs covered by OpenERP. Integration of social network capabilities, integration with Google Docs and LinkedIn, new Contract Management, new Event Management, new Point of Sale, new Address Book, new Fleet Management, are only some of the many enhancements in OpenERP 7.0.

The How To

Following that introduction, I bet you can't wait to get your hands dirty

Just one thing before we start: You can simply download a ".deb" package of OpenERP and install that on Ubuntu. Unfortunately that approach doesn't provide us (Libertus Solutions) with enough fine-grained control over where things get installed, and it restricts our flexibility to modify & customise, hence I prefer to do it a slightly more manual way (this install process below should only take about 10-15 minutes once the host machine has been built).

So without further ado here we go:

Step 1. Build your server

I install just the bare minimum from the install routine (you may want to install the openssh-server during the install procedure or install subsequently depending on your needs).

After the server has restarted for the first time I install the openssh-server package (so we can connect to it remotely) and denyhosts to add a degree of brute-force attack protection. There are other protection applications available: I'm not saying this one is the best, but it's one that works and is easy to configure and manage. If you don't already, it's also worth looking at setting up key-based ssh access, rather than relying on passwords. This can also help to limit the potential of brute-force attacks. [NB: This isn't a How To on securing your server...]

   sudo apt-get install openssh-server denyhosts

Now make sure your server has all the latest versions & patches by doing an update:

   sudo apt-get update
   sudo apt-get dist-upgrade

Although not always essential it's probably a good idea to reboot your server now and make sure it all comes back up and you can login via ssh.

Now we're ready to start the OpenERP install.

Step 2. Create the OpenERP user that will own and run the application

   sudo adduser --system --home=/opt/openerp --group openerp

This is a "system" user. It is there to own and run the application, it isn't supposed to be a person type user with a login etc. In Ubuntu, a system user gets a UID below 1000, has no shell (it's actually /bin/false) and has logins disabled. Note that I've specified a "home" of /opt/openerp, this is where the OpenERP server code will reside and is created automatically by the command above. The location of the server code is your choice of course, but be aware that some of the instructions and configuration files below may need to be altered if you decide to install to a different location.

[Note: If you want to run multiple versions of OpenERP on the same server, the way I do it is to create multiple users with the correct version number as part of the name, e.g. openerp70, openerp61 etc. If you also use this when creating the Postgres users too, you can have full separation of systems on the same server. I also use similarly named home directories, e.g. /opt/openerp70, /opt/openerp61 and config and start-up/shutdown files. You will also need to configure different ports for each instance or else only the first will start.]

A question I have been asked a few times is how to run the OpenERP server as the openerp system user from the command line if it has no shell. This can be done quite easily:

   sudo su - openerp -s /bin/bash

This will su your current terminal login to the openerp user (the "-" between su and openerp is correct) and use the shell /bin/bash. When this command is run you will be in openerp's home directory: /opt/openerp.

When you have done what you need you can leave the openerp user's shell by typing exit.

Step 3. Install and configure the database server, PostgreSQL

   sudo apt-get install postgresql

Then configure the OpenERP user on postgres:

First change to the postgres user so we have the necessary privileges to configure the database.

   sudo su - postgres

Now create a new database user. This is so OpenERP has access rights to connect to PostgreSQL and to create and drop databases. Remember what your choice of password is here; you will need it later on:

   createuser --createdb --username postgres --no-createrole --no-superuser --pwprompt openerp
   Enter password for new role: ********
   Enter it again: ********

Finally exit from the postgres user account:

   exit

Step 4. Install the necessary Python libraries for the server

   sudo apt-get install python-dateutil python-docutils python-feedparser python-gdata \
   python-jinja2 python-ldap python-libxslt1 python-lxml python-mako python-mock python-openid \
   python-psycopg2 python-psutil python-pybabel python-pychart python-pydot python-pyparsing \
   python-reportlab python-simplejson python-tz python-unittest2 python-vatnumber python-vobject \
   python-webdav python-werkzeug python-xlwt python-yaml python-zsi

With that done, all the dependencies for installing OpenERP 7.0 are now satisfied (note that there are some new packages required since 6.1).

Step 5. Install the OpenERP server

I tend to use wget for this sort of thing and I download the files to my home directory.

Make sure you get the latest version of the application: at the time of writing this it's 7.0. I got the download links from their download pages (note there are also deb, rpm and exe builds in this area too). There isn't a static 7.0 release tarball as such anymore, but there is a nightly build of the 7.0 source tree which should be just as good and will contain patches as and when things get fixed. The link below is to the source tarball for the 7.0 branch.

Note: As an alternative method of getting the code onto your server, Jerome added a very useful comment showing how to get it straight from launchpad. Thanks!

   wget http://nightly.openerp.com/7.0/nightly/src/openerp-7.0-latest.tar.gz

Now install the code where we need it: cd to the /opt/openerp/ directory and extract the tarball there.

   cd /opt/openerp
   sudo tar xvf ~/openerp-7.0-latest.tar.gz

Next we need to change the ownership of all the the files to the OpenERP user and group we created earlier.

   sudo chown -R openerp: *

And finally, the way I have done this is to copy the server directory to something with a simpler name so that the configuration files and boot scripts don't need constant editing (I called it, rather unimaginatively, server). I started out using a symlink solution, but I found that when it comes to upgrading, it seems to make more sense to me to just keep a copy of the files in place and then overwrite them with the new code. This way you keep any custom or user-installed modules and reports etc. all in the right place.

   sudo cp -a openerp-7.0 server

As an example, should OpenERP 7.0.1 come out soon, I can extract the tarballs into /opt/openerp/ as above. I can do any testing I need, then repeat the copy command so that the modified files will overwrite as needed and any custom modules, report templates and such will be retained. Once satisfied the upgrade is stable, the older 7.0 directories can be removed if wanted.

That's the OpenERP server software installed. The last steps to a working system is to set up the configuration file and associated boot script so OpenERP starts and stops automatically when the server itself stops and starts.

Step 6. Configuring the OpenERP application

The default configuration file for the server (in /opt/openerp/server/install/) is actually very minimal and will, with only one small change work fine so we'll simply copy that file to where we need it and change its ownership and permissions:

   sudo cp /opt/openerp/server/install/openerp-server.conf /etc/
   sudo chown openerp: /etc/openerp-server.conf
   sudo chmod 640 /etc/openerp-server.conf

The above commands make the file owned and writeable only by the openerp user and group and only readable by openerp and root.

To allow the OpenERP server to run initially, you should only need to change one line in this file. Toward to the top of the file change the line db_password = False to the same password you used back in step 3. Use your favourite text editor here. I tend to use nano, e.g.

   sudo nano /etc/openerp-server.conf

One other line we might as well add to the configuration file now, is to tell OpenERP where to write its log file. To complement my suggested location below add the following line to the openerp-server.conf file:

   logfile = /var/log/openerp/openerp-server.log

Once the configuration file is edited and saved, you can start the server just to check if it actually runs.

   sudo su - openerp -s /bin/bash
   /opt/openerp/server/openerp-server

If you end up with a few lines eventually saying OpenERP is running and waiting for connections then you are all set.

On my system I noticed the following warning:

2012-12-19 11:53:51,613 6586 WARNING ? openerp.addons.google_docs.google_docs: Please install latest gdata-python-client from http://code.google.com/p/gdata-python-client/downloads/list

The Ubuntu 12.04 packaged version of the python gdata client library is not quite recent enough, so to install a more up-to-date version I did the following (exit from the openerp user's shell if you are still in it first):

   sudo apt-get install python-pip
   sudo pip install gdata --upgrade

Going back and repeating the commands to start the server resulted in no further warnings

   sudo su - openerp -s /bin/bash
   /opt/openerp/server/openerp-server

If there are errors, you'll need to go back and find out where the problem is.

Otherwise simply enter CTL+C to stop the server and then exit to leave the openerp user account and go back to your own shell.

Step 7. Installing the boot script

For the final step we need to install a script which will be used to start-up and shut down the server automatically and also run the application as the correct user. There is a script you can use in /opt/openerp/server/install/openerp-server.init but this will need a few small modifications to work with the system installed the way I have described above. Here's a link to the one I've already modified for 7.0.

Similar to the configuration file, you need to either copy it or paste the contents of this script to a file in /etc/init.d/ and call it openerp-server. Once it is in the right place you will need to make it executable and owned by root:

   sudo chmod 755 /etc/init.d/openerp-server
   sudo chown root: /etc/init.d/openerp-server

In the configuration file there's an entry for the server's log file. We need to create that directory first so that the server has somewhere to log to and also we must make it writeable by the openerp user:

   sudo mkdir /var/log/openerp
   sudo chown openerp:root /var/log/openerp

Step 8. Testing the server

To start the OpenERP server type:

   sudo /etc/init.d/openerp-server start

You should now be able to view the logfile and see that the server has started.

   less /var/log/openerp/openerp-server.log

If there are any problems starting the server you need to go back and check. There's really no point ploughing on if the server doesn't start


OpenERP 7 Database Management Screen

If the log file looks OK, now point your web browser at the domain or IP address of your OpenERP server (or localhost if you are on the same machine) and use port 8069. The url will look something like this:

http://IP_or_domain.com:8069

What you should see is a screen like this one (it is the Database Management Screen because you have no OpenERP databases yet):

What I do recommend you do at this point is to change the super admin password to something nice and strong (Click the "Password" menu). By default this password is just "admin" and knowing that, a user can create, backup, restore and drop databases! This password is stored in plain text in the /etc/openerp-server.conf file; hence why we restricted access to just openerp and root. When you change and save the new password the /etc/openerp-server.conf file will be re-written and will have a lot more options in it.

Now it's time to make sure the server stops properly too:

   sudo /etc/init.d/openerp-server stop

Check the logfile again to make sure it has stopped and/or look at your serverâ€™s process list.

Step 9. Automating OpenERP startup and shutdown

If everything above seems to be working OK, the final step is make the script start and stop automatically with the Ubuntu Server. To do this type:

   sudo update-rc.d openerp-server defaults

You can now try rebooting you server if you like. OpenERP should be running by the time you log back in.

If you type ps aux | grep openerp you should see a line similar to this:

openerp 1491 0.1 10.6 207132 53596 ? Sl 22:23 0:02 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf

Which shows that the server is running. And of course you can check the logfile or visit the server from your web browser too.


OpenERP 70 Main Setup Screen

That's it! Next I would suggest you create a new database filling in the fields as desired. Once the database is initialised, you will be directed straight to the new main configuration screen which gives you a fell for the new User Interface in OpenERP 7 and shows you how easy it is to set up a basic system. 

------------------------------------------------------------------------------------------------------------------------------------------------------------------------

A product lifecycle management setup/module: http://www.omniasolutions.eu/OmniaSolutions/index.php?option=com_content&view=article&id=30&Itemid=25&lang=en

----------------------------------------------------------------------------------------------------------------------------------------------------------

How to install Aeroo Reports: https://accounts.openerp.com/forum/Help-1/question/How-to-install-Aeroo-Reports-2780

--------------------------------------------------------------------------------------------------------------------

                                         ------------------Users see only tasks assigned to them - openerp7------------------------------

     Log in as admin
    Go to settings
    Go to security
    Go to record rules
    Open the rule "Sales order - personal orders"
    Duplicate rule
    Change name from personal orders to personal tasks
    Change object to "Task"
    Leave groups empty to apply to all users OR add group to apply to certain users
    Save record rule
    Done, given users should see only tasks assigned to them 

------------------------------------------------------------------------------------------

                                         ------------------Multi-companies child company sees only its leads and not other child leads-------------------

Create a new record rule:

Object: Lead/Opportunity

Domain:

 ['|','|',('user_id','=',False),('user_id.company_id','=',False),('user_id.company_id','child_of',[user.company_id.id])]

----------------------------------------------------------------------------------------------------------------------------

                                         ------------------Crm calendar users only see own company meetings - openerp7----------------------


7 by default shows everyone everywhere's meetings in the crm calendar for multi-company,

To restrict meeting views to only a users company and any child companies, put in this resource record rule:

Name: meeting multi company

Object: Meeting

Domain filter: [('user_id.company_id','child_of',[user.company_id.id])]

no groups - make global (or group if so desired)

--------------------------------------------------------------------------------------------------------------------------------------------

                                         ------------------Mail loop warning - openerp7----------------------

If a client decides to create a new mailbox, and have his original inbox copy every message to the new one, and configure openerp to pull from the new one, there is a mail loop issue with the default configuration. Openerp will pull email from the new mailbox, but by default, with user settings, it will create a feed and email the user about the mail just pulled. It will send a mail to his original inbox as a feed. This feed gets copied client side to the openerp mailbox again, which gets pulled on the next fetch by openerp, and a mail loop goes on.

If the client has this mailbox copying setup, users in openerp need to be configured to send no feeds, to prevent this mail loop. To do so configure each user with this setup as follows:

    settings - users
    edit user
    change the field "Receive Feeds by Email" to "never" 

The mail loop also causes Admin's inbox to flood, since the feeds are sent by admin, mail pulled back in gets related to admin, and admin gets a notification in his inbox for the mail, and so admin's inbox gets flooded very quickly. This causes the admin login to be painfully slow, especially if there are a lot of mails and they have large attachments.

To clear admin's notifications to clean his inbox:

    log into psql as postgresql, and connect to the relevant db.
    find admin's partner_id from the res_user table
    clear the notification messages for that partner_id with: update mail_notification set read=true where partner_id = 3; (assuming admin's partner_id is 3 in this case) 

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

                                         ------------------Openerp7 multi-company access denied warnings----------------------

If your users get access denied warnings especially when searching, ensure that they've setup contacts in multi-company as follows:

For every company contact that is assigned to a multi-company branch, all user contacts that are assigned to that company contact, must be assigned to the same multi-company branch.

Eg,

    edit a contact which is a company
    click on "sales and purchases", note the company branch that the contact is assigned to in multi-company setups
    click on contacts to view persons under the given company
    for each person under the company, open them as a contact, click "sales and purchases", check the assigned multi-company branch for the user, it should be the same as for the company itself seen in step 2. 
 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------

                                         ------------------Admin user and outgoing mail server - openerp7----------------------

If a client has one outgoing mail server pointing to their smtp server, an admin@ account needs to exist on the mailserver, openerp will send alerts, emails and other things using the admin account, it sends these as admin@ (what ever the alias is fixed to), but some smtp servers will not accept mail from an account that does not exist, they error out with "sender verification failed".

The admin account needs to be able to send email out from its alias, to see backed up email, go to settings - email - emails. In the search box remove the filters, and look for queued messages that could not be sent out.

The only way I know to find out why the messages failed to deliver, is from the logs, openerp-server.log on the server running openerp. 

----------------------------------------------------------------------------------------------------------------------------------------------------

                                         ------------------Openerp7 gotchas----------------------

    If uploading pdf files to document management system stack traces, install this ubuntu package: poppler-utils
    The CRM module should be installed for email aliases to work correctly
    Configure an outgoing mail server and remember to set the company email address for the reset password functionality to work
    When creating custom modules do not use "pool" as a column name, it's a keyword. 

-------------------------------------------------------------------------------------------------

                                         ------------------Openerp7 - clear users inbox, mark msgs are read----------------------

To clear admin's notifications to clean his inbox:


log into psql as postgresql, and connect to the relevant db.

find the users partner_id from the res_user table (get his record using his login name)

clear the notification messages for that partner_id with: update mail_notification set read=true where partner_id = 3; (assuming users partner_id is 3 in this case) 

----------------------------------------------------------------------------------------------------------------------------------------------------------------------

                                         ------------------Manual db restore not picked up by openerp 7?----------------------

If you restored manually via pg_restore, and you find the openerp frontend is not finding the new db, try this:

 select usesysid from pg_user where usename='openerp';

Assuming openerp is connecting to the db as user openerp. This gets you the users sysid from the query.

Then do this:

 update pg_database set datdba=<sysid> where datname='<dbname>';

This sets the dba for the db to sysid, openerp should then start finding the new db. 

-----------------------------------------------------------------------------------------------

openerp regular db backup scripts: https://accounts.openerp.com/forum/Help-1/question/How-to-setup-a-regular-PostgreSQL-database-backup-4728

----------------------------------------------------------------------------------------------------------------------------------------------

RML guides: http://www.reportlab.com/software/documentation/

-----------------------------------------------------------------

                                         ------------------Unit price from supplier pricelist----------------------

To get the unit price from the supplier price list you need to:

    In Settings->Procurements: Manage pricelist per supplier need to be checked 

    You need a rule configured per product in the pricelist of the supplier. 

    On the product you need to add the supplier under the Procurements tab. 

--------------------------------------------------------------------------------------


---------------------------------------------------------------ERPweb dev notes ---------------------------------------------------------------------------------------

                                         ------------------Logging----------------------

    import logging lib 

import logging

    getlogger 

log = logging.getLogger(__name__)


    log stuff 

log.info("sale.line.order logging >>> " + str(i.product_id.id)) 

--------------------------------------------------------------------------

                                         ------------------Bitbucket and git----------------------
Bitbucket

We have an ERPWeb Bitbucket acccount. New team members need to be added to the ERPWeb team in order to access all the private repositories.
 Git
 Getting started

Set some configuration variables

   $ git config --global user.email "yourbitbucket@email.com"

   $ git config --global user.name "Your Name"

You can also set the editor to use for editing commit messages when you don't pass the -m flag when using git commit:

   $ git config --global core.editor vim

The --global flag saves the configuration values to ~/.gitconfig instead of saving it locally in .git/config in your repository.
 New repository

Initialise the repository

   $ git init

Add a Bitbucket remote repository so that we can push to it (this needs to be created on Bitbucket first under the ERPWeb profile)

   $ git remote add origin https://username@bitbucket.org/erpweb/repo.git

Now add .pyc files to the .gitignore file to tell git to ignore those types of files

   $ echo "*.pyc" >> .gitignore

Before committing any changes we need to add it to the staging area

   $ git add .gitignore

Commit your changes

   $ git commit -m "Initial commit"

Push the changes to the master branch on the remote called origin which in this case is Bitbucket

   $ git push origin master

 Getting help

   $ git help

You can get help on a specific command using the following

   $ git help config

 Deploy an OpenERP module to production from Bitbucket

From the addons directory run the following command to clone the repository

   $ git clone https://erpweb@bitbucket.org/erpweb/repo.git

To update to the latest changes that have been pushed to Bitbucket

   $ git pull

------------------------------------------------------------------------------------------

                                         ------------------Create Tasks from sales Orders----------------------

The product needs to be setup with the following settings.

- Product Type : Service
- Procurement Method : Make to Order
- Supply Method : Manufacture

The project is then selected in the product 

--------------------------------------------------------------------


----------------------------------------------------------------------ERPweb links --------------------------------------------------------------------------------------
POS Thermal Printer: https://accounts.openerp.com/forum/Help-1/question/How-to-configure-thermal-printer-to-work-with-POS-webclient-ver7-2564

-------------------------------------------------------------------------------------------------------------------------------------------------

##############################
Important Sysadmin Stuff 

Afrihost Dedicated Server's Backups

Hiren:"

VMs run backups daily, host pulls from Vms once a day, offsite pulls from host once a day.

/usr/local/bin/backupdbs.py in VMs contains which dbs to backup keeps 7 days of backups

crontab -l -u backups, on host for daily sync schedule, same on office machine."


vboxadm@192.168.1.141 -> (Pulls Backups:(rsync+ssh)) ->Afrihost Dedicated -> (Pulls Backups:(rsync+ssh)) -> Virtual Machines on Afrihost 

#0 1 * * * rsync --delete -r -e 'ssh -p 3322' backups@localhost:/home/backups/dbs/ /home/backups/dbs/7/
#0 0 * * * rsync --delete -r -e 'ssh -p 10012' backups@localhost:/home/backups/dbs/ /home/backups/dbs/6/
#0 1 * * * rsync --delete -r -e 'ssh -p 2222' backups@localhost:/home/backups/dbs/ /home/backups/dbs/5/
#0 1 * * * rsync --delete -r -e 'ssh -p 4422' backups@localhost:/home/backups/dbs/ /home/backups/dbs/sbu-grow/
#0 1 * * * rsync --delete -r -e 'ssh -p 3332' backups@localhost:/home/backups/dbs/  /home/backups/dbs/7-testing/

0 1 * * * rsync -z -r -e 'ssh -p 3322' backups@localhost:/home/backups/dbs/ /home/backups/dbs/7/
0 0 * * * rsync -z -r -e 'ssh -p 10012' backups@localhost:/home/backups/dbs/ /home/backups/dbs/6/
0 1 * * * rsync -z -r -e 'ssh -p 2222' backups@localhost:/home/backups/dbs/ /home/backups/dbs/5/
0 1 * * * rsync -z -r -e 'ssh -p 4422' backups@localhost:/home/backups/dbs/ /home/backups/dbs/sbu-grow/
0 1 * * * rsync -z -r -e 'ssh -p 3332' backups@localhost:/home/backups/dbs/  /home/backups/dbs/7-testing/

#--delete was removed because it deleted everything that was different on the destination. Thus never had old backups
# -z (Compresses while transferring to reduce bandwith usage.)
# -r (Recursive, needs this to work.)



    Every VM that needs to be backed up will run the backupdbs.py script using cron before the backed dbs are moved to the host and then offsite with Rsync. 

To edit the cron entry for backupdbs.py (In the vm)

$ sudo vim /etc/cron.d/dbbackups 

Here is a sample dbbackups entry from cron, from openerp7 server:

MAILTO=timo@datasmith.co.za
#MAILTO=erpwebza@gmail.com

0 23 * * * root /usr/local/bin/backupdbs.py

The backups are pulled from the dedicated server at afrihost by a machine in datasmith. The machine can be accessed internally:

$ ssh vboxadm@192.168.1.141

It runs a script that pulls the db backups from the Afrihost dedicated server. This is the script:

#!/bin/bash

while [ 1 ] 
do
    #rsync --delete --append --partial --progress -vz -r -e 'ssh' backups@openerp.co.za:/home/backups/dbs/ /home/vboxadm/dbs/
    rsync -z -r -e 'ssh' backups@openerp.co.za:/home/backups/dbs/ /home/vboxadm/dbs/ 
    if [ "$?" = "0" ] ; then
        echo "rsync completed normally $(date) " >> /var/log/backups
        exit
    else
        echo "rsync failure. Retrying in a minute... $(date) " >> /var/log/backups
    
        sleep 60
    fi  
done

The new version of the script deletes the source file after rsync. This script pulls from afrihost dedicated server to vboxadm@192.168.1.141 Thus we won't have problems with hardrive space. Still need to implement this between VMs and dedicated server.

#!/bin/bash

while [ 1 ] 
do
    #rsync --delete --append --partial --progress -vz -r -e 'ssh' backups@openerp.co.za:/home/backups/dbs/ /home/vboxadm/dbs/
    #rsync -z -r -e 'ssh' backups@openerp.co.za:/home/backups/dbs/ /home/vboxadm/dbs/ 
    rsync --remove-source-files -z -r -e 'ssh' backups@openerp.co.za:/home/backups/dbs/ /home/vboxadm/dbs/  
    if [ "$?" = "0" ] ; then
        echo "rsync completed normally $(date) " >> /var/log/backups
        exit
    else
        echo "rsync failure. Retrying in a minute... $(date) " >> /var/log/backups
    
        sleep 60
    fi  
done




Add New VM To Be Backed Up Procedure

On the New VM You Created you should create a user called "backups".

Create a dorectory called dbs in the Virtual Machine.

$ mkdir /home/backups/dbs

In the Virtual machine create a script called /usr/local/bin/backupdbs.py

$ sudo vim /usr/local/bin/backupdbs.py

The script should contain the following code and edit it according to your need. Change the database names to those that you have on your system.

#!/usr/bin/env python

import os
import sys
from datetime import datetime
import re
import glob

dbs = [
    'internal',
    'adoptaschool',
    'balancelldev3',
    'balancell',

]
pgdump = '/usr/bin/pg_dump'
backupdir = '/home/backups/dbs/'
cust_mod_path = '/opt/custom_modules'

keepdays = 7    

def get_files(dir):
    if not dir.endswith('/'):
        dir += '/' 
    files = glob.glob(dir + '*dump*')
    return files

def run_rotation(files):
    datestr = datetime.strftime(datetime.now(),"%Y%m%d")
    for f in files:
        m = re.search('([0-9]{8})-[0-9]{4}',f)
        if m:
            filestr =  m.groups()[0]
            if int(filestr) < (int(datestr) - keepdays) and filestr[-1] != '1':
                os.remove(f)
        else:
            sys.stderr.write("ignoring file not in expected format: " + f)

def main():
    datestr = datetime.strftime(datetime.now(),"%Y%m%d-%H%M")
    print "Running db backups.."
    for db in dbs:
        backfile = backupdir + db + '.dump.' + datestr
        os.system('su postgres -c "' + pgdump + ' -Fc ' + db + ' >' + backfile + '"')
        print "Backed up db {} to file {}".format(db,backfile)
    files = get_files(backupdir)
    run_rotation(files)
# This was:
#  addons_cmd = 'tar -czf ' + backupdir + 'custmods.dump.' + datestr + ' ' + cust_mod_path 
    addons_cmd = 'tar -chzf ' + backupdir + 'custmods.dump.' + datestr + ' ' + cust_mod_path
# We added the -h option for tar because we couldnt create a tar.gz of files that were symlinked to others. This fixes this issue.
# You will have to rename the backup made by this part of the script to .tar.gz for it to properly decompress.
    os.system(addons_cmd)
    print "Rsync crons will sync this down to the office"


if __name__ == '__main__':
    main()




Remember to make the script executable:

$ sudo chmod +x /usr/local/bin/backupdbs.py




The Afrihost has a list of VM's it pulls backed up postgresql databases from every day. To view the list of vm's you will have to run:

$ sudo crontab -u backups -l
0 1 * * * rsync --delete -r -e 'ssh -p 3322' backups@localhost:/home/backups/dbs/ /home/backups/dbs/7/
0 0 * * * rsync --delete -r -e 'ssh -p 10012' backups@localhost:/home/backups/dbs/ /home/backups/dbs/6/
0 1 * * * rsync --delete -r -e 'ssh -p 2222' backups@localhost:/home/backups/dbs/ /home/backups/dbs/5/
0 1 * * * rsync --delete -r -e 'ssh -p 4422' backups@localhost:/home/backups/dbs/ /home/backups/dbs/sbu-grow/


To view the SSH port for a vm you can

$ alias



Run the command that you want to put into the crontab manually first:

$ sudo -i -u backups


$ rsync --delete -r -e 'ssh -p 3332' backups@localhost:/home/backups/dbs/  /home/backups/dbs/7-testing/


If it works fine with no problems then you can add it to crontab. Now put this into crontab, this is the entry for testing:


0 1 * * * rsync --delete -r -e 'ssh -p 3332' backups@localhost:/home/backups/dbs/  /home/backups/dbs/7-testing/

You will have to add the vm to this list if you want it backed up to the host/dedicated server. The backups can only be pulled to the inhouse backup at Datasmith if it is also pulled to the Host/Dedicated Server at Afrihost. To add an entry to the crontab on host to pull the backups from the VM to the host you will have to be logged in as user backups on the host.

$ sudo -i -u backups

Now to edit the crontab:

$ crontab -e


This part happens in the VM. This is to run the python script backupdbs.py

$ sudo vim /etc/cron.d/dbbackups 


Here is a sample dbbackups entry from cron, from openerp7 server:

MAILTO=timo@datasmith.co.za
#MAILTO=erpwebza@gmail.com

0 23 * * * root /usr/local/bin/backupdbs.py

If you see errrors like this:

root@ubuntuOP7-testing:/home/lox# /usr/local/bin/backupdbs.py
Running db backups..
bash: /home/backups/dbs/sbu-docs.dump.20140610-1035: Permission denied
Backed up db sbu-docs to file /home/backups/dbs/sbu-docs.dump.20140610-1035
bash: /home/backups/dbs/sbu-growdev.dump.20140610-1035: Permission denied
Backed up db sbu-growdev to file /home/backups/dbs/sbu-growdev.dump.20140610-1035
bash: /home/backups/dbs/sbu-smmedev.dump.20140610-1035: Permission denied
Backed up db sbu-smmedev to file /home/backups/dbs/sbu-smmedev.dump.20140610-1035
bash: /home/backups/dbs/internal.dump.20140610-1035: Permission denied
Backed up db internal to file /home/backups/dbs/internal.dump.20140610-1035
bash: /home/backups/dbs/adoptaschool.dump.20140610-1035: Permission denied
Backed up db adoptaschool to file /home/backups/dbs/adoptaschool.dump.20140610-1035
bash: /home/backups/dbs/adoptaschooldev.dump.20140610-1035: Permission denied
Backed up db adoptaschooldev to file /home/backups/dbs/adoptaschooldev.dump.20140610-1035
bash: /home/backups/dbs/balancelldev.dump.20140610-1035: Permission denied
Backed up db balancelldev to file /home/backups/dbs/balancelldev.dump.20140610-1035


Then make sure that postgres can write to the directory /home/backups/dbs:

$ sudo chown backups:postgres /home/backups/dbs



Offsite Backup Mechanism

Offsite Backups Run on the Machine in the office with the IP: 192.168.1.141. Login to the user vboxadm to find out more on this machine.

The offsite machine pulls the backups from the afrihost dedicated server everyday with:


# This is in the crontab
0 2 * * * sh /usr/local/bin/rsync.sh

To view the crontab login as user vboxadm and run

$ crontab -e

rsync.sh contains:

!/bin/bash

while [ 1 ]
do
    rsync --delete --append --partial --progress -vz -r -e 'ssh' backups@openerp.co.za:/home/backups/dbs/ /home/vboxadm/dbs/
    if [ "$?" = "0" ] ; then
        echo "rsync completed normally $(date) " >> /var/log/backups
        exit
    else
        echo "rsync failure. Retrying in a minute... $(date) " >> /var/log/backups


         sleep 43200
       # sleep 60 >> this is a issue when the power goes off or any interruption occurs

    fi
done



Install New VM on Testing Machine Internal

Install New VM on Testing Machine Internal:

$ tar -xvf GitLab.tar.xz 
$ cd GitLab/
$ vboxmanage registervm ~/VirtualBox\ VMs/GitLab/GitLab.vbox
$ VBoxManage modifyvm "GitLab" --nic1 nat
$ VBoxManage modifyvm "GitLab" --cableconnected1 on

Forward port for SSH and for OpenERP:

$ VBoxManage modifyvm "GitLab" --natpf1 ssh,tcp,,5505,,22
$ VBoxManage modifyvm "GitLab" --natpf1 ssh,tcp,,4069,,8069
$ vboxmanage startvm "GitLab" --type headless



Copy a VM from Production to Testing Server

First rsync it:

$ ssh vboxadm@192.168.1.141

$  rsync -arz -e 'ssh' erpwevpy@openerp.co.za:"/home/erpwevpy/VirtualBox\ VMs/openerp7\ Testing/" "/home/vboxadm/backup/openerp7 Testing/"

Then follow this guide:

    Install New Vm on Testing Machine Internal 

$ tar -xvf GitLab.tar.xz 
$ cd GitLab/
$ vboxmanage registervm ~/VirtualBox\ VMs/GitLab/GitLab.vbox
$ VBoxManage modifyvm "GitLab" --nic1 nat
$ VBoxManage modifyvm "GitLab" --cableconnected1 on

Forward port for SSH and for OpenERP:

$ VBoxManage modifyvm "GitLab" --natpf1 ssh,tcp,,5505,,22
$ VBoxManage modifyvm "GitLab" --natpf1 ssh,tcp,,4069,,8069
$ vboxmanage startvm "GitLab" --type headless




VirtualBox Stuff 
Delete NAT Rule VirtualBox


$ vboxmanage controlvm "vm_name" natpf1 delete "rulename"

$ vboxmanage controlvm "OpenErp5" natpf1 delete http


Shut Down VM

This is how you shutdown a VM:

$ vboxmanage controlvm "vm_name" poweroff

$ vboxmanage controlvm "ubuntu14" poweroff

List Hardrives Virtualbox

List hardrives and hardrives in Use by Virtualbox:

$ vboxmanage list hdds

Restore Snapshot

$ VBoxManage snapshot <vm_name> restore snap1-before-upgrade

$ VBoxManage snapshot vm04-zca8 restore snap1-before-upgrade

Install VboxTool

From Document left by Hiren on servers.

"vboxtool should handle clean VM shutdown and startup of VMs upon host shutdown and boot up.

Configured in /etc/vboxtool/, esp config file machines.conf (config file defines VMs to manage, and ports to forward from host to guest etc)

vboxtool --help |less, for detailed info on using and configuring

(was brought in to handle clean shutdown of VMs when host rebooted, and start of VMs when host comes up)"

Install VboxTool:


Download from here: 
http://sourceforge.net/projects/vboxtool/

$ unzip vboxtool-0.4.zip

$ sudo cp ~/script/vboxtool /usr/local/bin/

$ sudo chmod +x /usr/local/bin/vboxtool

$ sudo cp script/vboxtoolinit /etc/init.d

$ sudo chmod +x /etc/init.d/vboxtoolinit

$ sudo update-rc.d vboxtoolinit defaults 99 10

$ sudo mkdir /etc/vboxtool

$ vbox_user='vbox'

Read more here: 
http://www.admin-magazine.com/Articles/Server-Virtualization-with-VirtualBox
http://ubuntuforums.org/showthread.php?t=933099

VirtualBox Start tag expected not found

Solution:

    Start Tag Virtualbox Not Found 

"Check the files of the VM in the VirtualBox VMs folder.

If the .vbox file has size zero, then copy the .vbox-prev file on it.

Restart VirtualBox, it should be repaired."

$ du -hs openerp.vbox

0	openerp7.vbox

$ rm openerp7.vbox

$  cp openerp7.vbox-prev openerp7.vbox

$ vboxmanage registervm /home/vboxadm/backup/openerp7/openerp7.vbox

VM inaccessible error

Solution is:

    Fix VM Inaccessible 

$ vboxmanage unregistervm b5e7c99e-0271-4e64-a257-808143be014d

$ vboxmanage unregistervm <UUID>

You can get the UUID by running:

$ vboxmanage list vms

Next step is to register the VM again with the same name and start it. 


Delete Virtualbox Vm Completely

Delete Vm VirtualBox:

    Delete VM VirtualBox Correct:
http://askubuntu.com/questions/316177/how-to-completely-remove-an-os-from-virtualbox
    Vboxmanage unregistervm:
http://www.virtualbox.org/manual/ch08.html#vboxmanage-registervm 

$ VBoxManage unregistervm --delete "<Name of Machine>"

"The unregistervm command unregisters a virtual machine. If --delete is also specified, the following files will automatically be deleted as well:

all hard disk image files, including differencing files, which are used by the machine and not shared with other machines;

saved state files that the machine created, if any (one if the machine was in "saved" state and one for each online snapshot);

the machine XML file and its backups;

the machine log files, if any;

the machine directory, if it is empty after having deleted all the above. " 





Step One backup .vbox xml files
[edit] Backup old .vbox-prev

In the event of .vbox xml corruption a .vbox-prev file is created with old settings. Backup this file with 'cp' and either make this the .vbox file or

vboxmanage unregestire "vm"; vboxmanage registervm vm.vbox-prev;     ("vm" is your vm name)

[edit] Check showvminfo against .vbox

Also check that vbox file corresponds to vboxmanage showvminfo "vm"
[edit] Copy .vbox from dev server and start vm

Worst case scenario check .vbox files on dev server or similar server and recover settings with that .vbox
[edit] Step Two VM boots, but CPU cap 100% and no response through ssh or web port

Could be that VM's Grub has lost its settings and is waiting for the keyboard command "enter". To check if this is the case use the screenshotpng to take a screenshot and 'scp' from local to remote server to check file.

 vboxmanage controlvm "vm" screenshotpng /Screenshots/screenshot1_vm.png    ("vm" is your vm name)

If this is the case (by checking the png image as in example [[1]]) then this is easily bootable with the command

VBoxManage controlvm "vm" keyboardputscancode 1c 9c    ("vm" is your vm name)

The 1c 9c is the enter key in Hex code

Future fix for recurring issue is to set the GRUB default to Ubuntu, with Linux 3.2xxxxxxxxxxx and to update GRUB 





Virtual box won't start kernel issues

IF the following type of error occurs after trying to reinstall broken virtualbox

Setting up virtualbox (4.1.12-dfsg-2ubuntu0.1) ...
 * Stopping VirtualBox kernel modules                                    [ OK ] 
 * Starting VirtualBox kernel modules
 * No suitable module for running kernel found                           [fail]

[edit] Step 1 Check your kernel version

dpkg --list | grep linux-image 
or
uname -r


sudo apt-get purge virtualbox
sudo apt-get install linux-headers-server
sudo shutdown -r 1

When host is back up then re-install virtualbox

sudo apt-get install virtualbox --reinstall




DB:


select id, sale_order_id, sale_line_id from project_task where sale_line_id is not null;


select                                                                                                                                
a.id, 
a.sale_order_id, 
a.sale_line_id ,
b.order_id,
concat ('update project_task set sale_order_id = where id = ', cast( a.id as varchar )  
from 
project_task  a,
sale_order_line b
where 
a.sale_line_id is not null
and a.sale_line_id = b.id;
);


update project_task set sale_order_id = (select order_id from sale_order_line where id = sale_line_id) where sale_line_id is not null;

#################################################
Vagrant_Openerp: https://github.com/arthru/vagrant_openerp

vagrant_openerp
Vagrant and Fabric files for creating OpenERP 7 VMs

vagrant_openerp is a tool for installing automatically OpenERP 7 on a Vagrant VM. You can also use the recipe to install it on a physical server.

It is made for debian based system but it has only been tested with Ubuntu 12.04 Precise Pangolin.
Prerequisites

You must install vagrant 1.0.x (may work with 1.1) and fabtools (only tested with 0.13.0). For example, if you don't want to use your system package manager :

pip install fabtools
gem install vagrant

Note: if you are using Debian 7.4 (Wheezy) vagrant can be installed through apt/aptitude, however fabtools is not included in its repositories and pip fails to install it, so I would recommend using easy_install (which is part of python-setuptools):

aptitude install vagrant python-setuptools
easy_install fabtools

You should also add a box to vagrant :

vagrant box add precise32 http://files.vagrantup.com/precise32.box

Quickstart

Clone vagrant_openerp from github :

git clone https://github.com/arthru/vagrant_openerp.git

Go to the new directory and set up the vagrant VMs :

cd vagrant_openerp
vagrant up

Launch fab in order to provision the vm for OpenERP :

fab vagrant openerp

And here it is ! Simply browse to http://localhost:8069/ to log in your freshly installed OpenERP instance (with admin login and admin password).


##################openerp7 DataError: encoding UTF8 does not match locale en_US######################

I had the same problem. The problem is connected with wrong encoding in PostgreSQL and template1 database. The solution is to fix encoding in Linux to UTF-8 and then:

sudo su postgres

psql

update pg_database set datistemplate=false where datname='template1';
drop database Template1;
create database template1 with owner=postgres encoding='UTF-8'

lc_collate=len_US.utf8' lc_ctype='en_US.utf8' template template0;

update pg_database set datistemplate=true where datname='template1';
################################################################################


Aero Report for 8.0:https://github.com/jamotion/aeroo


##########IIA#################
                            <group class="oe_subtotal_footer oe_right" colspan="2" name="sale_total">
                                <field name="amount_untaxed" widget='monetary' options="{'currency_field': 'currency_id'}"/>
                                <field name="amount_tax" widget='monetary' options="{'currency_field': 'currency_id'}"/>
                                <div class="oe_subtotal_footer_separator oe_inline">
                                    <label for="amount_total" />
                                    <button name="button_dummy"
                                        states="draft,sent" string="(update)" type="object" class="oe_edit_only oe_link"/>
                                </div>
                                <field name="amount_total" nolabel="1" class="oe_subtotal_footer_separator" widget='monetary' options="{'currency_field': 'currency_id'}$
                            </group>
                            <div class="oe_clear"/>
                            <field name="note" class="oe_inline" placeholder="Terms and conditions..."/>
                        </page>
#################################################################################################################

###############################INSITE & Short cut##################################################################

                        <div class="oe_right oe_button_box" name="button_box">
                            <!-- Put here related buttons -->
                        </div>


                <xpath expr="//div[@name='button_box']" position="inside">
                    <button name="%(act_hr_employee_holiday_request)d" string="Leaves" type="action" groups="base.group_hr_user"/>
                </xpath>

        <!-- Shortcuts -->
        <record id="act_hr_employee_holiday_request" model="ir.actions.act_window">
            <field name="name">Leaves</field>
            <field name="type">ir.actions.act_window</field>
            <field name="res_model">hr.holidays</field>
            <field name="src_model">hr.employee</field>
            <field name="view_type">form</field>
            <field name="view_mode">tree,form</field>
            <field name="context">{'search_default_employee_id': [active_id], 'default_employee_id': active_id}</field>
            <field name="domain">[('type','=','remove')]</field>
            <field name="view_id" eval="view_holiday"/>
        </record>

###############################################################################################################################


Odoo8:	http://odedrabhavesh.blogspot.in/2014/12/how-to-install-wkhtmltopdf-in-odoo.html


