Creating a wizard from a button: 
	<button name="%(action_view_wizard)d" string="Create Wizard" type="action" states="manual" class="oe_highlight" groups="base.group_user"/>

            where action_view_wizard is the id of the ir.actions.act_window linked to the wizard view form id

Workflow: https://doc.odoo.com/trunk/server/workflows/
	  https://www.odoo.com/forum/help-1/question/need-a-way-to-move-around-workflow-states-back-and-forth-in-sales-order-21509
	  http://forum.openerp.com/forum/topic32312.html
	  https://doc.odoo.com/v6.1/developer/07_workflows.html/
	  placeholder: https://doc.odoo.com/trunk/server/form-view-guidelines/




firsttrophy=# SELECT id from product_product where ean13='5901234123457';

  id  
------
 553
 1299
 1297
 1298
 1300
(5 rows) access products with specific id's.


firsttrophy=# DELETE FROM product_product where id in (553, 1299, 1297, 1298, 1300);
ERROR:  null value in column "product_id" violates not-null constraint
CONTEXT:  SQL statement "UPDATE ONLY "public"."stock_move" SET "product_id" = NULL WHERE $1 OPERATOR(pg_catalog.=) "product_id""  CAN NOT delete these products because they are linked in other tables/models (e.g: "stock_move"). Therefore first delete them from the respective models! as shown below:


firsttrophy=# DELETE FROM procurement_order where product_id in (553, 1299, 1297, 1298, 1300);
DELETE 5 deleted from procurement_order with the specific id's. 

firsttrophy=# DELETE FROM stock_move where product_id in (553, 1299, 1297, 1298, 1300);
DELETE 5 deleted from stock_move with the specific id's.


firsttrophy=# DELETE FROM product_product where id in (553, 1299, 1297, 1298, 1300);
DELETE 5 deleted from product_product with the specific id's after all the linked models/tables are deleted!

NBBB: After creating a menu view, if menu inherated from parent, do:

db:
alter table ir_ui_menu drop parent_left;
alter table ir_ui_menu drop parent_right;

openerp:
add this to /etc/init.d/openerp:
--update=all

restart site

:BBBN

if db complaining about a column not existing, do:
alter table "table_name" add column "column_name" "column_type"; e.g: alter table res_partner add column is_customer bool;

remote accessing a server: ssh -p PORT IP -l USER (from a command line) or ssh://IP:PORT/PATH (from ubuntu folder "file->connect to server")
secure copying from a remote server: scp -p PORT FILE/FOLDER_TO_COPY DESTINATION (USER@IP:/PATH)

