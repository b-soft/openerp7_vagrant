#Start as Linux daemon

Unless you are using mod_wsgi, you should setup the web2py server so that it can be started/stopped/restarted as any other Linux daemon, and so it can start automatically at the computer boot stage.

The process to set this up is specific to various Linux/Unix distributions.

In the web2py folder, there are two scripts which can be used for this purpose:

scripts/web2py.ubuntu.sh
scripts/web2py.fedora.sh

On Ubuntu, or other Debian-based Linux distribution, edit "web2py.ubuntu.sh" and replace the "/usr/lib/web2py" path with the path of your web2py installation, then type the following shell commands to move the file into the proper folder, register it as a startup service, and start it: (NB: also add port and host arguments in: DAEMON_ARGS="web2py.py --password=<recycle> --pid_filename=$PIDFILE --host='0.0.0.0' --port=8001") or web2py.py -a 'admin_password' -c server.crt -k server.key -i 0.0.0.0 -p 8000

sudo cp scripts/web2py.ubuntu.sh /etc/init.d/web2py
sudo update-rc.d web2py defaults
sudo /etc/init.d/web2py start

On Fedora, or any other distributions based on Fedora, edit "web2py.fedora.sh" and replace the "/usr/lib/web2py" path with the path of your web2py installation, then type the following shell commands to move the file into the proper folder, register it as a startup service and start it:

sudo cp scripts/web2py.fedora.sh /etc/rc.d/init.d/web2pyd
sudo chkconfig --add web2pyd
sudo service web2py start


#IDEA: make flask app to be downloaded from the internet, set it up to run as daemon using a bash file. Do the same using sudo apt-get install flask. 					In Odoo, write a web app that will allow a visitor to sign up with their email, name and company name then automatically create its cloud web instance as mycompany.odoo.com, database as mycompany with admin replaced by their name (this will require: write a script for creating a subdomain on apache, on the ISP, writing a script to create a db with owner odoo)

#For GIC: GIC server with odoo on port 80. Create user group with access to website, email, and meeting/discussion blog. Once the user login, he/she might decide to sign up with his/her "email, name" (already using) and company name then automatically create its cloud web instance as mycompany.gic.com, database as mycompany with admin replaced by his her name (this will require: write a script for creating a subdomain on apache, on the ISP, writing a script to create a db with owner odoo)   

# create a subdomain remotely:
>>> import xmlrpclib
>>> server = xmlrpclib.ServerProxy('https://api.webfaction.com/')
>>> session_id, account = server.login('widgetsco', 'widgetsrock')
>>> server.create_domain(session_id, 'widgetcompany.example', 'www', 'design')
{'domain': 'widgetcompany.example',
 'id': 47255,
 'subdomains': ['www', 'design']}

#

This answer may help someone to solve the same problem,

1) Select your (or default) Security Group in Ec2 Section of cloud Server.

2) Select "Inbound" Tab and create new Rule for "All TCP" and give access to your required port.

as per my knowledge, second step will inform cloud server to open selected port for Inbound access from end users.
#

###
#Deamon startup on Linux
replace

python web2py.py -a 'admin_password' -c server.crt -k server.key -i 0.0.0.0 -p 8000 with:
python web2py.py -a '<recycle>' -c server.crt -k server.key -i 0.0.0.0 -p 8001 as explained here: http://web2py.com/books/default/chapter/29/13#Start-as-Linux-daemon

useful:

nohup python web2py.py -a '<recycle>' -i 127.0.0.1 -p 8000 &

from http://web2py.com/books/default/chapter/29/13/deployment-recipes

http://web2py.com/books/default/chapter/29/13#Start-as-Linux-daemon from http://web2py.com/books/default/chapter/29/13

#Create a daemon user: sudo adduser --system --home=/opt/flask --group flask (http://www.theopensourcerer.com/2014/09/how-to-install-openerp-odoo-8-on-ubuntu-server-14-04-lts/)
###

# See web2py/script/ for more HOWTO Sys Admin!!
