from wtforms import Form, StringField, TextAreaField, validators, FloatField, SelectField


class ProductForm(Form):
	price = FloatField('Price')
	email = StringField('Email', [validators.Required()])
	product_ids = StringField('', [validators.Required()])


class CountryForm(Form):
	country_id = SelectField(u'Regional Country', choices=[('258', 'EU-Countries'), ('167', 'Norway'), ('256', 'USA/CANADA/MEXICO/RUSSIA')])
