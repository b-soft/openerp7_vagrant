# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

from openerp.http import Controller, route, request
from openerp.addons.website.models.website import slug
from werkzeug.utils import redirect
#from wtforms import Form, StringField, TextAreaField, validators
from .forms import ProductForm, CountryForm
import xmlrpclib

import logging
_logger = logging.getLogger(__name__)


username = 'admin' #the user
pwd      = 'mGnG54'      #the password of the user
dbname   = 'firsttrophy'    #the database
IP       =  "openerp.co.za" #hostname
port     =  7069    #port
url      = 'http://firsttrophy.openerp.co.za' #'http://41.223.33.6:7069' # The url to connect to

#xmlrpclib
common     = xmlrpclib.ServerProxy('{}/xmlrpc/common'.format(url))
uid        = common.login(dbname, username, pwd)
models     = xmlrpclib.ServerProxy('{}/xmlrpc/object'.format(url))


class Calculator(Controller):

    def product_selected(self, p_id, country_id):
        product_id = models.execute(dbname, uid, pwd, 'product.product', 'read', p_id, ['id'])
        product_id_info = product_id["id"]

        product_name = models.execute(dbname, uid, pwd, 'product.product', 'read', p_id, ['name'])
        product_name_info = product_name["name"]

        product_all_inc_ids = models.execute(dbname, uid, pwd, 'product.product', 'read', p_id, ['all_inclusive_ids'])
        product_all_inc_info = product_all_inc_ids["all_inclusive_ids"]
        
        for all_inc_id in product_all_inc_info:
            product_all_inclusive_country = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['country_id'])
            product_all_inclusive_country_info = product_all_inclusive_country["country_id"]

            if product_all_inclusive_country_info[0] == country_id:
                product_all_inclusive_name = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['name'])
                product_all_inclusive_name_info = product_all_inclusive_name["name"]

                product_all_inclusive_taxidermy_work = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['taxidermy_work'])
                product_all_inclusive_taxidermy_work_info = product_all_inclusive_taxidermy_work["taxidermy_work"]

                product_all_inclusive_export_papers = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['export_papers'])
                product_all_inclusive_export_papers_info = product_all_inclusive_export_papers["export_papers"]

                product_all_inclusive_eu_shipping = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['eu_shipping'])
                product_all_inclusive_eu_shipping_info = product_all_inclusive_eu_shipping["eu_shipping"]

                product_all_inclusive_packing_crating = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['packing_crating'])
                product_all_inclusive_packing_crating_info = product_all_inclusive_packing_crating["packing_crating"]

                product_all_inclusive_insurance = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['insurance'])
                product_all_inclusive_insurance_info = product_all_inclusive_insurance["insurance"]
                    
                product_all_inclusive_vat = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['vat'])
                product_all_inclusive_vat_info = product_all_inclusive_vat["vat"]        

                product_all_inclusive_total = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['tot'])
                product_all_inclusive_total_info = product_all_inclusive_total["tot"]

                pipe = "| "
         
                print "____________________________________________________________________________________________________________________________________________________________"
                print pipe, product_name_info, pipe, product_all_inclusive_name_info, pipe, product_all_inclusive_taxidermy_work_info, pipe, product_all_inclusive_export_papers_info, pipe, product_all_inclusive_eu_shipping_info, pipe, product_all_inclusive_packing_crating_info, pipe, product_all_inclusive_insurance_info, pipe, product_all_inclusive_vat_info, pipe, product_all_inclusive_total_info, pipe,     

        return product_id_info, product_name_info, product_all_inclusive_name_info, product_all_inclusive_taxidermy_work_info, product_all_inclusive_export_papers_info, product_all_inclusive_eu_shipping_info, product_all_inclusive_packing_crating_info, product_all_inclusive_insurance_info, product_all_inclusive_vat_info, product_all_inclusive_total_info, product_all_inclusive_country_info

    def create_partner(self, name, email, lang):
        partner = {
           'name': "{0}".format(name),
           'email': "{0}".format(email),
           'sales_tax_ids': [(6, 0, [7])],
           #'customer':True,
           'lang': "{0}".format(lang),
        }

        partner_id = models.execute(dbname, uid, pwd, 'res.partner', 'create', partner)
        return partner_id

    def create_quotation(self, partner_id, pricelist_id, partner_invoice_id, partner_shipping_id):
        order = {
           'partner_id': partner_id,
           'pricelist_id': pricelist_id,
           'partner_invoice_id': partner_invoice_id,
           'partner_shipping_id': partner_invoice_id,
           #'sales_tax_ids': [(6, 0, [7])],
           #'eu_vat': True,
        }

        order_id = models.execute(dbname, uid, pwd, 'sale.order', 'create', order)
        return order_id

    def create_order_line(self, order_id, product_id, name, direction, taxidermy_work, export_papers, eu_shipping, packing_crating, insurance):
        order_line1 = {                
            'order_id': order_id,
            'product_id': product_id,
            'name':"{0}{1}{2}".format((name or ''), (". " or ''), (direction or '')),
            'product_uom_qty': 1,
            'price_unit': taxidermy_work,  
            'direction': "{0}".format(direction or ''),  
            'tax_id': [(6, 0, [7])],                
        }

        order_line1_id = models.execute(dbname, uid, pwd, 'sale.order.line', 'create', order_line1)

        order_line2 = {
            'order_id': order_id,
            'name':"{0}".format('DIP & PACK EXPORT PAPERS'),
            'product_uom_qty': 1,
            'price_unit': export_papers,
        }

        order_line2_id = models.execute(dbname, uid, pwd, 'sale.order.line', 'create', order_line2)

        order_line3 = {
            'order_id': order_id,
            'name':"{0}".format('DIRECT SHIPPING TO PRIVATE ADDRESS IN ALL EU-COUNTRIES'),
            'product_uom_qty': 1,
            'price_unit': eu_shipping,
        } 

        order_line3_id = models.execute(dbname, uid, pwd, 'sale.order.line', 'create', order_line3)

        order_line4 = {
            'order_id': order_id,
            'name':"{0}".format('PACKING & CRATING'),
            'product_uom_qty': 1,
            'price_unit': packing_crating,
        } 

        order_line4_id = models.execute(dbname, uid, pwd, 'sale.order.line', 'create', order_line4)

        order_line5 = {
            'order_id': order_id,
            'name':"{0}".format('INSURANCE'),
            'product_uom_qty': 1,
            'price_unit': insurance,
        }

        order_line5_id = models.execute(dbname, uid, pwd, 'sale.order.line', 'create', order_line5)

        return order_line1_id, order_line2_id, order_line3_id, order_line4_id, order_line5_id

    #Printing and emailing
    def print_email(self, order_id, email):
        import time
        import base64
        printmodels = xmlrpclib.ServerProxy('{}/xmlrpc/report'.format(url))
        model = 'sale.order'
        id_report = printmodels.report(dbname, uid, pwd, model, [order_id], {'model': model, 'id': order_id, 'report_type':'pdf'})
        time.sleep(5)
        state = False
        attempt = 0
        while not state:
            report = printmodels.report_get(dbname, uid, pwd, id_report)
            state = report['state']
            if not state:
                time.sleep(1)
                attempt += 1
            if attempt>200:
                print 'Printing aborted, too long delay !'

        string_pdf = base64.decodestring(report['result'])
        file_pdf = open('/opt/custom_modules/web_app1/Quotation%s.pdf'%int(order_id),'w')
        file_pdf.write(string_pdf)
        file_pdf.close()    

        print 'Done printing!'

        # welcome email sent to portal users
        # (note that calling '_' has no effect except exporting those strings for translation)
        WELCOME_EMAIL_SUBJECT = ("Your OpenERP Quotation From %(company)s")
        WELCOME_EMAIL_BODY = ("""Dear %(name)s,

        Please view your <a href='http://openerp.co.za/firsttrophy/Quotation%(order_id)s.pdf'>online Quotation</a> you requested when you last visited 
        our web site.
        We gladly welcome you as our potential %(portal)s.


        --
        FCT - First Class Trophy
        http://firstclasstrophy.com/
        """)


        data = {
            'company': 'First Class Trophy',
            'portal': 'Customer',
            'name': "Visitor",
            'order_id': int(order_id)
                }

        mail_values = {
            'email_from': 'office@firstclasstrophy.com',
            'email_to': email,
            'subject': (WELCOME_EMAIL_SUBJECT) % data,
            'body_html': '<pre>%s</pre>' % ((WELCOME_EMAIL_BODY) % data),
            'state': 'outgoing',
            'type': 'email',
                }
        mail_id = models.execute(dbname, uid, pwd,
                'mail.mail', 'create', mail_values)

        print mail_id
        attachments = []
        attachments.append(("Quotation.pdf", string_pdf))
        #print attachments
        attachment_ids = []
        for data in attachments:
            attachment_data = {
                'name': data[0],
                'datas_fname': data[0],
                'datas': data[1],
                'res_model': 'mail.message',    #'mail.mail',
                'res_id': mail_id,
                    }   

        #attach_id = models.execute(dbname, uid, pwd, 'ir.attachment', 'create', attachment_data)
        #attachment_ids.append(attach_id)
        #print attach_id
        if attachments:
            #models.execute(dbname, uid, pwd, 'mail.mail', 'write', mail_id, {'attachment_ids': [(6, 0, attachment_ids)]})
            models.execute(dbname, uid, pwd, 'mail.mail', 'send', [mail_id])
        #mail_model = connection.get_model("mail.mail")
        #mail_model.send([mail_id])
        print "Done 2!"  

    @route('/country/', auth='public', website=True)
    def show_country(self, **form_data):

        form = CountryForm(request.httprequest.form)

        if request.httprequest.method == 'POST' and form.validate():
            print form.country_id.data
            country_id = int(form.country_id.data)
            #show_entries(country_id)
            return redirect("/products/%d" % country_id)
            #return redirect('/')

        return request.render('web_app1.show_country_id', {'form': form})

    @route('/products/<country_id>', auth='public', website=True)
    def show_products(self, country_id, **form_data):

        form = ProductForm(request.httprequest.form)
        #Products
        ids = models.execute(dbname, uid, pwd,
            'product.product', 'search',
            (['is_all_inclusive', '=', True], ['is_displayed_on_website', '=', True]))  #[('is_all_inclusive', '=', True)])  #[('product_tmpl_id.name', '=', 'Antilope Leg DIP & SHIP, All Inclusive')])

        products_info = models.execute(dbname, uid, pwd,
                'product.product', 'read', ids, ['id', 'name', 'all_inclusive_ids'])
        #print products_info
        #spicies   = ('Baboon', 'Barbersheep', 'Blesbock, Common', 'Blesbock, White', 'Bontebock', 'Buffalo', 'Bushbock', 'Bushpig', 'Civet cat', 'Cheetah', 'Crocodile < 2,5 meter', 'Crocodile 2,5 -3 meter', 'Crocodile 3-4 meter', 'Crocodile 4 -5 meter', 'Damara Dik-dik', 'Duiker, Blue', 'Duiker, Grey', 'Giraffe', 'Eland', 'Elephant', 'Fallow Deer', 'Gemsbock/Oryx', 'Grysbock', 'Red Hartebeest', 'Hippo','Hyena', 'Impala', 'Jackall', 'Klipspringer', 'Kudu', 'Red Lechwe', 'Leopard', 'Lion Male', 'Lioness', 'Caracal/Lynx','Nyala', 'Oribi', 'Ostrich', 'Porcupine', 'Reedbuck, Common', 'Reedbuck, Mountain', 'Rhebock, vaal', 'Rhino', 'Roan', 'Sable', 'Springbock, common', 'Springbock, golden', 'Springbock, white', 'Steenbock', 'Tsessebe', 'Warthog', 'Waterbuck', 'Wildebeest, Black' 'Wildebeest, Blue', "Zebra, Hartmann's", 'Zebra, Burchell')
        spicies = ['Baboon', 'Barbersheep', 'Blesbuck Common', 'Blesbuck White', 'Bontebuck', 'Buffalo', 'Bushbuck', 'Bushpig', 'Caracal/lynx', 'Cheetah', 'Civet Cat', 'Crocodile 2,5 -3 Meter', 'Crocodile 3-4 Meter', 'Crocodile 4 -5 Meter', 'Crocodile < 2,5 Meter', 'Damara Dik-dik', 'Duiker Blue', 'Duiker Grey', 'Eland', 'Elephant', 'Fallow Deer', 'Gemsbuck/oryx', 'Giraffe', 'Grysbuck', 'Hippo', 'Hyena Brown', 'Hyena Spotted', 'Impala', 'Jackal (sjakal)', 'Klipspringer', 'Kudu', 'Leopard', 'Lioness', 'Lion Male', 'Nyala', 'Oribi', 'Ostrich', 'Porcupine', 'Red Hartebeest', 'Red Lechwe', 'Reedbuck Common', 'Reedbuck Mountain', 'Rhebuck Vaal', 'Rhino', 'Roan', 'Sable','Springbuck Black', 'Springbuck Common', 'Springbuck Golden', 'Springbuck White', 'Steenbuck', 'Tsessebe', 'Warthog', 'Waterbuck', 'Wildebeest Black', 'Wildebeest Blue', "Zebra Burchell's", "Zebra Hartmann's"]
        #taxidermy = ('Shoulder Mounts', 'Pedestal Mounts', 'Full Mounts', 'EU Mounts', 'Tanning of Full Skin', 'Tanning Full Skin with Felt', 'Tanning of Back Skins', 'Tanning Back Skin with Felt', 'Rug mounts', 'Taxidermi full Skin', 'Taxidermi back skin')
        taxidermy = ('Shoulder Mounts', 'Full Mounts', 'EU Mounts', 'Tanning of Full Skin', 'Tanning Full Skin with Felt', 'Tanning of Back Skins', 'Tanning Back Skin with Felt', 'Rug mounts')
        all_inc_ids = []
        entriessm   = []
        entriesped  = []
        entriesfm   = []
        entriesem = []
        entriestfs   = []
        entriestfswf   = []
        entriestbs = []
        entriestbswf = []
        entriesrm = []
        print len(taxidermy)
        print len(spicies)
        if request.httprequest.method == 'POST' and form.validate():
            print form.product_ids.data
            print form.price.data
            print form.email.data
            ids = eval(form.product_ids.data)
            #ids = list(ids)
            _logger.info("--------- RESULT {0}--------".format(ids))

            email = form.email.data
            if int(country_id) == 167:
                lang = 'da_DK'
            else:
                lang = 'en_US'
            partner_id = self.create_partner(email, email, lang)
            order_id = self.create_quotation(partner_id, 4, partner_id, partner_id)
            for p_id in ids:
                select = self.product_selected(p_id, int(country_id))
                order_id = float(order_id)
                product_id = int(select[0])
                name = str(select[1])
                taxidermy_work = float(select[3])
                export_papers = float(select[4])
                eu_shipping = float(select[5])
                packing_crating = float(select[6])
                insurance = float(select[7])
                order_line_ids = self.create_order_line(order_id, product_id, name, None, taxidermy_work, export_papers, eu_shipping, packing_crating, insurance)
            print order_line_ids
            #print product_vals
            #create_order_line(order_id, product_vals[0], product_vals[1], direction, product_vals[3], product_vals[4], product_vals[5], product_vals[6], product_vals[7])
            # 01234567, product_all_inclusive_vat_info, product_all_inclusive_total_info, product_all_inclusive_country_info
            self.print_email(order_id, email)
            return redirect('/thanks/%s' % email)

        for product_info in products_info:
            for all_in in product_info['all_inclusive_ids']:
                all_inc_ids.append(all_in)
        #print all_inc_ids
        all_inc = models.execute(dbname, uid, pwd,
                'product.all.inclusive', 'read', all_inc_ids, [])
        #print all_inc    
        for all_inc_id in all_inc:
            #print all_inc_id
                #d = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
            if 'Shoulder Mount' in all_inc_id['all_inc_product_id'][1]: #product_info['name']: #if all_inc_id['country_id'][0] == 258:
                    #print entriessm#product_info['name']
                    #dsm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
                 entriessm.append(dict(idsm=all_inc_id['all_inc_product_id'][0], namesm=all_inc_id['all_inc_product_id'][1], pricesm=all_inc_id['tot'], countrysm=all_inc_id['country_id']))
            if 'Pedestal' in all_inc_id['all_inc_product_id'][1]: #if all_inc_id['country_id'][0] == 258:
                    #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
                entriesped.append(dict(idped=all_inc_id['all_inc_product_id'][0], nameped=all_inc_id['all_inc_product_id'][1], priceped=all_inc_id['tot'], countryped=all_inc_id['country_id']))
            if 'Full Mount' in all_inc_id['all_inc_product_id'][1]: #product_info['name']:
                    #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
                entriesfm.append(dict(idfm=all_inc_id['all_inc_product_id'][0], namefm=all_inc_id['all_inc_product_id'][1], pricefm=all_inc_id['tot'], countryfm=all_inc_id['country_id']))
            if 'European Mount' in all_inc_id['all_inc_product_id'][1]: #product_info['name']:
                    #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
                entriesem.append(dict(idem=all_inc_id['all_inc_product_id'][0], nameem=all_inc_id['all_inc_product_id'][1], priceem=all_inc_id['tot'], countryem=all_inc_id['country_id']))
            if 'Tanning of Full Skin' in all_inc_id['all_inc_product_id'][1]: #product_info['name']:
                    #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
                entriestfs.append(dict(idtfs=all_inc_id['all_inc_product_id'][0], nametfs=all_inc_id['all_inc_product_id'][1], pricetfs=all_inc_id['tot'], countrytfs=all_inc_id['country_id']))    
            if 'Tanning Full Skin with Felt' in all_inc_id['all_inc_product_id'][1]: #product_info['name']:
                    #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
                entriestfswf.append(dict(idtfswf=all_inc_id['all_inc_product_id'][0], nametfswf=all_inc_id['all_inc_product_id'][1], pricetfswf=all_inc_id['tot'], countrytfswf=all_inc_id['country_id']))    
            if 'Tanning of Back Skin' in all_inc_id['all_inc_product_id'][1]: #product_info['name']:
                    #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
                entriestbs.append(dict(idtbs=all_inc_id['all_inc_product_id'][0], nametbs=all_inc_id['all_inc_product_id'][1], pricetbs=all_inc_id['tot'], countrytbs=all_inc_id['country_id']))    
            if 'Tanning Back Skin with Felt' in all_inc_id['all_inc_product_id'][1]: #product_info['name']:
                    #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
                entriestbswf.append(dict(idtbswf=all_inc_id['all_inc_product_id'][0], nametbswf=all_inc_id['all_inc_product_id'][1], pricetbswf=all_inc_id['tot'], countrytbswf=all_inc_id['country_id']))    
            if 'Rugmount' in all_inc_id['all_inc_product_id'][1]: #product_info['name']:
                    #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
                entriesrm.append(dict(idrm=all_inc_id['all_inc_product_id'][0], namerm=all_inc_id['all_inc_product_id'][1], pricerm=all_inc_id['tot'], countryrm=all_inc_id['country_id']))    
                
                    #entriesped.append(all_inc_id['tot'])      
                #print all_inc_id['id'], all_inc_id['name'], all_inc_id['taxidermy_work'], all_inc_id['export_papers'], all_inc_id['eu_shipping'], all_inc_id['packing_crating'], all_inc_id['insurance'], all_inc_id['vat'], all_inc_id['tot'], all_inc_id['country_id'] 
        entriessm = sorted(entriessm, key=lambda k: (k['namesm']))
        taxidermy = sorted(taxidermy)
        entriesped = sorted(entriesped, key=lambda k: (k['nameped']))
        entriesfm = sorted(entriesfm, key=lambda k: (k['namefm']))
        entriesem = sorted(entriesem, key=lambda k: (k['nameem']))
        entriestfs = sorted(entriestfs, key=lambda k: (k['nametfs']))
        entriestfswf = sorted(entriestfswf, key=lambda k: (k['nametfswf']))
        entriestbs = sorted(entriestbs, key=lambda k: (k['nametbs']))
        entriestbswf = sorted(entriestbswf, key=lambda k: (k['nametbswf']))
        entriesrm = sorted(entriesrm, key=lambda k: (k['namerm']))
        entries = (spicies, taxidermy, entriessm, entriesped, entriesfm, entriesem, entriestfs, entriestfswf, entriestbs, entriestbswf, entriesrm)
        return request.render('web_app1.show_products_id', {'form': form})#{'taxidermy': taxidermy}, {'entriessm': entriessm}, {'entriesped': entriesped}, {'entriesfm': entriesfm}, {'entriesem': entriesem}, {'entriestfs': entriestfs}, {'entriestfswf': entriestfswf}, {'entriestbs': entriestbs}, {'entriestbswf': entriestbswf}, {'entriesrm': entriesrm}, {'spicies': spicies}, {'taxidermy': taxidermy})

    @route('/thanks/<email>', auth='public', website=True)
    def show_thanks(self, email, **form_data):

        if request.httprequest.method == 'POST':
            return redirect("/country")

        return request.render('web_app1.show_thanks_id', {'email': email})