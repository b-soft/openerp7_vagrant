{
	# The human-readable name of your module, displayed in the interface
	'name': "Web App",
	# Describes the software version
	'version': '1.0',
	# The developer/auther
	'author': "Gutembert Nganpet Nzeugaing <nganpet2007@gmail.com>",
	# Describes which category does the software fall into
	'category': 'remote access',
	# A more extensive description
	'description': """
						This module helps to use and understand xmlrpc
	""",
	# Which modules must be installed for this one to work
	'depends': ['base', 'web', 'report', 'website', 'website_sale', 'website_crm', 'website_hr_recruitment', 'website_forum_doc', 'auth_signup', 'board'],
	# data files which are always installed
    'data': [
        'templates/country.xml',
        'templates/products.xml', 
        'templates/thanks.xml',
        #'templates/thankshtml.xml',
	],

    'js' : [
        "static/src/js/calculstor.js",
    ],

    'css' : [
        "static/src/css/calculstor.css",
    ],

    'qweb' : [
        "static/src/xml/*.xml",
    ],

}
