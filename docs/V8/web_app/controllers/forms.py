from wtforms import Form, StringField, TextAreaField, validators, FloatField, SelectField


class ProductForm(Form):
    price = FloatField('Price')
    name = StringField('Name', validators=[validators.Required()])
    surname = StringField('Surname', validators=[validators.Required()])
    email = StringField('Email', [validators.Required()])
    product_ids = StringField('', [validators.Required()])
    importvat = FloatField('importVAT')

class CountryForm(Form):
    country_id = SelectField(u'Regional Country', choices=[('258', 'EU-Countries'), ('167', 'Norway'), ('256', 'USA/CANADA/MEXICO/RUSSIA')])





	