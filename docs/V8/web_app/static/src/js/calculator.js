function OnDropDownChange(id, price, dropDown, spicy, country_id, vat) 
{
  var selectedValue = dropDown.options[dropDown.selectedIndex].value;
  var selectedSpecy = document.getElementById("txtSelectedVAT").value;  
    
  form = document.forms['login'];

  if (price == 0.0)
  {
    alert("N/A or POR. After exiting this window, please press the Reset button below then recompute your cost!") ; 
    //form.elements['remember_me'].value = '';
  }

  if (price != 0.0)
  {
    if (country_id != '258')
    {
      var y = form.elements['price'].value;
      var z = +price - +vat;
      var tp = +selectedValue*z;
      var x = Math.round((+y + tp) * 100) / 100;
      form.elements['price'].value = x;
      form.elements['product_ids'].value += '(' + selectedValue + ',' + id + ')' + ',';  //id;
    }
    if (country_id == '258')
    {
      var y = form.elements['price'].value;
      var z = (+price - +vat) + (+price - +vat)*0.25;
      var tp = +selectedValue*z;
      var x = Math.round((+y + tp) * 100) / 100;
      form.elements['price'].value = x;
      form.elements['product_ids'].value += '(' + selectedValue + ',' + id + ')' + ',';  //id;
    }

    if ((spicy == '26') || (spicy == '27') || (spicy == '29'))
    {
      var tspecy = +selectedValue*2;
      var xspecy = +selectedSpecy + +tspecy;
      document.getElementById("txtSelectedVAT").value = xspecy; //'(' + selectedValue + ',' + id + ')' + ', ' + spicy; //imp_vat += 2 
      form.elements['importvat'].value = xspecy;
    }
    if ((spicy == '12') || (spicy == '13') || (spicy == '14') || (spicy == '15'))  //if 'Crocodile' in name:
    {
      var tspecy = +selectedValue*8;
      var xspecy = +selectedSpecy + +tspecy;
      document.getElementById("txtSelectedVAT").value = xspecy; //imp_vat += 8 
      form.elements['importvat'].value = xspecy;
    }
    if ((spicy == '57') || (spicy == '58'))  //if 'Zebra' in name:
    {
      var tspecy = +selectedValue*20;
      var xspecy = +selectedSpecy + +tspecy;
      document.getElementById("txtSelectedVAT").value = xspecy;  //imp_vat += 20 
      form.elements['importvat'].value = xspecy;
    }
    if ((spicy == '10') || (spicy == '32') || (spicy == '33') || (spicy == '34'))
    {  //if 'Cheetah' in name or 'Leopard' in name or 'Lion' in name:
      var tspecy = +selectedValue*40;
      var xspecy = +selectedSpecy + +tspecy;
      document.getElementById("txtSelectedVAT").value = xspecy; //imp_vat += 40
      form.elements['importvat'].value = xspecy;
    }
    if ((spicy == '1') || (spicy == '2') || (spicy == '3') || (spicy == '4') || (spicy == '5') || (spicy == '6') || (spicy == '7') || (spicy == '8') || (spicy == '9') || (spicy == '11') || (spicy == '16') || (spicy == '17') || (spicy == '18') || (spicy == '19') || (spicy == '21') || (spicy == '22') || (spicy == '23') || (spicy == '24') || (spicy == '28') || (spicy == '30') || (spicy == '31') || (spicy == '35') || (spicy == '36') || (spicy == '37') || (spicy == '38') || (spicy == '39') || (spicy == '40') || (spicy == '41') || (spicy == '42') || (spicy == '43') || (spicy == '45') || (spicy == '46') || (spicy == '47') || (spicy == '48') || (spicy == '49') || (spicy == '50') || (spicy == '51') || (spicy == '52') || (spicy == '53') || (spicy == '54') || (spicy == '55') || (spicy == '56'))
    {  //if 'Bushpig' in name or 'Warthog' in name or 'Porcupine' in name or 'Baboon' in name or 'Ostrich' in name or 'Caracal' in name:
      var tspecy = +selectedValue*4;
      var xspecy = +selectedSpecy + +tspecy;
      document.getElementById("txtSelectedVAT").value = xspecy;  //imp_vat += 4
      form.elements['importvat'].value = xspecy;
    }
  }
}

function myFunction() 
{
  document.getElementById("myForm").reset();
}

function myFinalCost()
{
  form = document.forms['login'];     
  document.getElementById("txtSelectedFC").value = Math.round((115 + +form.elements['price'].value + +form.elements['importvat'].value) * 100) / 100;
}