from flask import render_template, flash, redirect, url_for
from app import app
from .forms import LoginForm, CountryForm
import xmlrpclib
#import openerplib

username = 'admin' #the user
pwd      = 'mGnG54'      #the password of the user
dbname   = 'firsttrophy'    #the database
IP       =  "openerp.co.za" #hostname
port     =  7069    #port
url      = 'http://firsttrophy.openerp.co.za' #'http://41.223.33.6:7069' # The url to connect to

#xmlrpclib
common     = xmlrpclib.ServerProxy('{}/xmlrpc/common'.format(url))
uid        = common.login(dbname, username, pwd)
models     = xmlrpclib.ServerProxy('{}/xmlrpc/object'.format(url))

#openerplib
#connection = openerplib.get_connection(hostname=IP, protocol="xmlrpc", port=port, database=dbname, \
    #login=username, password=pwd)

def product_selected(p_id, country_id):
    #product_model = connection.get_model("product.product")
    #product_all_inclusive_model = connection.get_model("product.all.inclusive")

    #product_id = product_model.read(p_id, ["id"])
    product_id = models.execute(dbname, uid, pwd, 'product.product', 'read', p_id, ['id'])
    product_id_info = product_id["id"]

    #product_name = product_model.read(p_id, ["name"])
    product_name = models.execute(dbname, uid, pwd, 'product.product', 'read', p_id, ['name'])
    product_name_info = product_name["name"]

    #product_all_inc_ids = product_model.read(p_id, ["all_inclusive_ids"])
    product_all_inc_ids = models.execute(dbname, uid, pwd, 'product.product', 'read', p_id, ['all_inclusive_ids'])
    product_all_inc_info = product_all_inc_ids["all_inclusive_ids"]
    
    for all_inc_id in product_all_inc_info:
        #product_all_inclusive_country = product_all_inclusive_model.read(all_inc_id, ["country_id"])
        product_all_inclusive_country = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['country_id'])
        product_all_inclusive_country_info = product_all_inclusive_country["country_id"]
            #258 EU
            #167 NW
            #256 USA
        if product_all_inclusive_country_info[0] == country_id:
            #product_all_inclusive_name = product_all_inclusive_model.read(all_inc_id, ["name"])
            product_all_inclusive_name = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['name'])
            product_all_inclusive_name_info = product_all_inclusive_name["name"]

            #product_all_inclusive_taxidermy_work = product_all_inclusive_model.read(all_inc_id, ["taxidermy_work"])
            product_all_inclusive_taxidermy_work = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['taxidermy_work'])
            product_all_inclusive_taxidermy_work_info = product_all_inclusive_taxidermy_work["taxidermy_work"]

            #product_all_inclusive_export_papers = product_all_inclusive_model.read(all_inc_id, ["export_papers"])
            product_all_inclusive_export_papers = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['export_papers'])
            product_all_inclusive_export_papers_info = product_all_inclusive_export_papers["export_papers"]

            #product_all_inclusive_eu_shipping = product_all_inclusive_model.read(all_inc_id, ["eu_shipping"])
            product_all_inclusive_eu_shipping = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['eu_shipping'])
            product_all_inclusive_eu_shipping_info = product_all_inclusive_eu_shipping["eu_shipping"]

            #product_all_inclusive_packing_crating = product_all_inclusive_model.read(all_inc_id, ["packing_crating"])
            product_all_inclusive_packing_crating = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['packing_crating'])
            product_all_inclusive_packing_crating_info = product_all_inclusive_packing_crating["packing_crating"]

            #product_all_inclusive_insurance = product_all_inclusive_model.read(all_inc_id, ["insurance"])
            product_all_inclusive_insurance = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['insurance'])
            product_all_inclusive_insurance_info = product_all_inclusive_insurance["insurance"]
                
            #product_all_inclusive_vat = product_all_inclusive_model.read(all_inc_id, ["vat"])
            product_all_inclusive_vat = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['vat'])
            product_all_inclusive_vat_info = product_all_inclusive_vat["vat"]        

            #product_all_inclusive_total = product_all_inclusive_model.read(all_inc_id, ["tot"])
            product_all_inclusive_total = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['tot'])
            product_all_inclusive_total_info = product_all_inclusive_total["tot"]

            pipe = "| "
     
            #print "____________________________________________________________________________________________________________________________________________________________"
            #print pipe, product_name_info, pipe, product_all_inclusive_name_info, pipe, product_all_inclusive_taxidermy_work_info, pipe, product_all_inclusive_export_papers_info, pipe, product_all_inclusive_eu_shipping_info, pipe, product_all_inclusive_packing_crating_info, pipe, product_all_inclusive_insurance_info, pipe, product_all_inclusive_vat_info, pipe, product_all_inclusive_total_info, pipe,     

    return product_id_info, product_name_info, product_all_inclusive_name_info, product_all_inclusive_taxidermy_work_info, product_all_inclusive_export_papers_info, product_all_inclusive_eu_shipping_info, product_all_inclusive_packing_crating_info, product_all_inclusive_insurance_info, product_all_inclusive_vat_info, product_all_inclusive_total_info, product_all_inclusive_country_info     #, product_model.name_get(p_id)[0]
       
def create_partner(name, surname, email, lang):
    partner = {
       'name': "{0} {1}".format(name, surname),
       'email': "{0}".format(email),
       'sales_tax_ids': [(6, 0, [7])],
       #'customer':True,
       'lang': "{0}".format(lang),
    }
    #partner_model = connection.get_model("res.partner")
    #partner_id = partner_model.create(partner)
    partner_id = models.execute(dbname, uid, pwd, 'res.partner', 'create', partner)
    return partner_id

def create_quotation(partner_id, pricelist_id, partner_invoice_id, partner_shipping_id):
    order = {
       'partner_id': partner_id,
       'pricelist_id': pricelist_id,
       'partner_invoice_id': partner_invoice_id,
       'partner_shipping_id': partner_invoice_id,
       #'sales_tax_ids': [(6, 0, [7])],
       #'eu_vat': True,
    }
    #order_model = connection.get_model("sale.order")
    #order_id = order_model.create(order)
    order_id = models.execute(dbname, uid, pwd, 'sale.order', 'create', order)
    return order_id

def create_order_line(order_id, product_id, name, direction, taxidermy_work, export_papers, eu_shipping, packing_crating, insurance):
    order_line1 = {                
        'order_id': order_id,
        'product_id': product_id,
        'name':"{0}{1}{2}".format((name or ''), (". " or ''), (direction or '')),
        'product_uom_qty': 1,
        'price_unit': taxidermy_work,  
        'direction': "{0}".format(direction or ''),  
        'tax_id': [(6, 0, [7])],                
    }
    #order_line_model = connection.get_model("sale.order.line")
    #order_line1_id = order_line_model.create(order_line1)
    order_line1_id = models.execute(dbname, uid, pwd, 'sale.order.line', 'create', order_line1)

    order_line2 = {
        'order_id': order_id,
        'name':"{0}".format('DIP & PACK EXPORT PAPERS'),
        'product_uom_qty': 1,
        'price_unit': export_papers,
    }
    #order_line2_id = order_line_model.create(order_line2)
    order_line2_id = models.execute(dbname, uid, pwd, 'sale.order.line', 'create', order_line2)

    order_line3 = {
        'order_id': order_id,
        'name':"{0}".format('DIRECT SHIPPING TO PRIVATE ADDRESS IN ALL EU-COUNTRIES'),
        'product_uom_qty': 1,
        'price_unit': eu_shipping,
    } 
    #order_line3_id = order_line_model.create(order_line3)
    order_line3_id = models.execute(dbname, uid, pwd, 'sale.order.line', 'create', order_line3)

    order_line4 = {
        'order_id': order_id,
        'name':"{0}".format('PACKING & CRATING'),
        'product_uom_qty': 1,
        'price_unit': packing_crating,
    } 
    #order_line4_id = order_line_model.create(order_line4)
    order_line4_id = models.execute(dbname, uid, pwd, 'sale.order.line', 'create', order_line4)

    order_line5 = {
        'order_id': order_id,
        'name':"{0}".format('INSURANCE'),
        'product_uom_qty': 1,
        'price_unit': insurance,
    }
    #order_line5_id = order_line_model.create(order_line5) 
    order_line5_id = models.execute(dbname, uid, pwd, 'sale.order.line', 'create', order_line5)

    return order_line1_id, order_line2_id, order_line3_id, order_line4_id, order_line5_id

#Printing and emailing
def print_email(order_id, email, country_id, name, surname):
    import time
    import base64
    printmodels = xmlrpclib.ServerProxy('{}/xmlrpc/report'.format(url))
    model = 'sale.order'
    id_report = printmodels.report(dbname, uid, pwd, model, [order_id], {'model': model, 'id': order_id, 'report_type':'pdf'})
    time.sleep(5)
    state = False
    attempt = 0
    while not state:
        report = printmodels.report_get(dbname, uid, pwd, id_report)
        state = report['state']
        if not state:
            time.sleep(1)
            attempt += 1
        if attempt>200:
            print 'Printing aborted, too long delay !'

    string_pdf = base64.decodestring(report['result'])
    file_pdf = open('/var/www/firsttrophy/Quotation%s.pdf'%int(order_id),'w')   #open('Quotation%s.pdf'%int(order_id),'w')
    file_pdf.write(string_pdf)
    file_pdf.close()    

    #print 'Done printing!'

    # welcome email sent to portal users
    # (note that calling '_' has no effect except exporting those strings for translation)
    WELCOME_EMAIL_SUBJECT_EU = ("Your OpenERP Quotation From %(company)s")
    WELCOME_EMAIL_BODY_EU = ("""<b>Dear %(name)s %(surname)s,</b>

    <b>Here is a detailed and specified "break-down" cost calculation for your hunting trophies in Namibia or South Africa.</b>
    
    <b>This price includes ALL COST like taxidermy work, transport from Africa, insurance, veterinary certificates, 
    custom duties and delivery to your doorstep in your home country. There will 
    be no hidden costs or unexpected additional invoices coming to you after your hunting trip.</b>
    
    <b>Please view your <a href='http://openerp.co.za/firsttrophy/Quotation%(order_id)s.pdf'>online Quotation</a> you requested when you last visited our web site.<b>
    <b>We gladly welcome you as our potential %(portal)s.</b>


<b>--
FCT - First Class Trophy
http://firstclasstrophy.com/</b>
    """)

    WELCOME_EMAIL_SUBJECT_NON_EU = ("Your OpenERP Quotation From %(company)s")
    WELCOME_EMAIL_BODY_NON_EU = ("""<b>Dear %(name)s %(surname)s,</b>

    <b>Here is a detailed and specified "break-down" cost calculation for your hunting trophies in Namibia or South Africa.</b>
    
    <b>This price includes ALL COST like taxidermy work, transport from Africa, insurance, 
    veterinary certificates, custom duties and delivery the incoming broker / agent in your country. 
    There will be no hidden costs or unexpected additional invoices coming to you after 
    your hunting trip from First Class Trophy.
    Eventual cost like import duties, import VAT, transport inside your country, etc. 
    must be paid according to the regulations in your country. 
    Normally the broker/agent in your country can advise the hunter about these costs.</b>

    <b>Please view your <a href='http://openerp.co.za/firsttrophy/Quotation%(order_id)s.pdf'>online Quotation</a> you requested when you last visited our web site.</b>
    <b>We gladly welcome you as our potential %(portal)s.</b>


<b>--
FCT - First Class Trophy
http://firstclasstrophy.com/</b>
    """)


    data = {
        'company': 'First Class Trophy',
        'portal': 'Customer',
        'name': name,
        'surname': surname,
        'order_id': int(order_id)
            }

    mail_values = {
        'email_from': 'office@firstclasstrophy.com',
        'email_to': email,
        'subject': (WELCOME_EMAIL_SUBJECT_EU) % data,
        'body_html': '<pre>%s</pre>' % ((WELCOME_EMAIL_BODY_EU) % data),
        'state': 'outgoing',
        'type': 'email',
            }
    if country_id == 258:
        mail_id = models.execute(dbname, uid, pwd,
            'mail.mail', 'create', mail_values)
    else:
        mail_values.update({
            'subject': (WELCOME_EMAIL_SUBJECT_NON_EU) % data,
            'body_html': '<pre>%s</pre>' % ((WELCOME_EMAIL_BODY_NON_EU) % data),
            })
        mail_id = models.execute(dbname, uid, pwd,
            'mail.mail', 'create', mail_values)

    #print mail_id
    attachments = []
    attachments.append(("Quotation.pdf", string_pdf))
    #print attachments
    attachment_ids = []
    for data in attachments:
        attachment_data = {
            'name': data[0],
            'datas_fname': data[0],
            'datas': data[1],
            'res_model': 'mail.message',    #'mail.mail',
            'res_id': mail_id,
                }   

    #attach_id = models.execute(dbname, uid, pwd, 'ir.attachment', 'create', attachment_data)
    #attachment_ids.append(attach_id)
    #print attach_id
    if attachments:
        #models.execute(dbname, uid, pwd, 'mail.mail', 'write', mail_id, {'attachment_ids': [(6, 0, attachment_ids)]})
        models.execute(dbname, uid, pwd, 'mail.mail', 'send', [mail_id])
    #mail_model = connection.get_model("mail.mail")
    #mail_model.send([mail_id])
    #print "Done 2!"

@app.route('/<country_id>', methods=['GET', 'POST'])
def show_entries(country_id):

    form = LoginForm()
    #Products
    ids = models.execute(dbname, uid, pwd,
        'product.product', 'search',
        (['is_all_inclusive', '=', True], ['is_displayed_on_website', '=', True]))  #[('is_all_inclusive', '=', True)])  #[('product_tmpl_id.name', '=', 'Antilope Leg DIP & SHIP, All Inclusive')])

    products_info = models.execute(dbname, uid, pwd,
            'product.product', 'read', ids, ['id', 'name', 'all_inclusive_ids'])
    #print products_info
    #spicies   = ('Baboon', 'Barbersheep', 'Blesbock, Common', 'Blesbock, White', 'Bontebock', 'Buffalo', 'Bushbock', 'Bushpig', 'Civet cat', 'Cheetah', 'Crocodile < 2,5 meter', 'Crocodile 2,5 -3 meter', 'Crocodile 3-4 meter', 'Crocodile 4 -5 meter', 'Damara Dik-dik', 'Duiker, Blue', 'Duiker, Grey', 'Giraffe', 'Eland', 'Elephant', 'Fallow Deer', 'Gemsbock/Oryx', 'Grysbock', 'Red Hartebeest', 'Hippo','Hyena', 'Impala', 'Jackall', 'Klipspringer', 'Kudu', 'Red Lechwe', 'Leopard', 'Lion Male', 'Lioness', 'Caracal/Lynx','Nyala', 'Oribi', 'Ostrich', 'Porcupine', 'Reedbuck, Common', 'Reedbuck, Mountain', 'Rhebock, vaal', 'Rhino', 'Roan', 'Sable', 'Springbock, common', 'Springbock, golden', 'Springbock, white', 'Steenbock', 'Tsessebe', 'Warthog', 'Waterbuck', 'Wildebeest, Black' 'Wildebeest, Blue', "Zebra, Hartmann's", 'Zebra, Burchell')
    spicies = ['Baboon', 'Barbersheep', 'Blesbuck Common', 'Blesbuck White', 'Bontebuck', 'Buffalo', 'Bushbuck', 'Bushpig', 'Caracal/lynx', 'Cheetah', 'Civet Cat', 'Crocodile 2,5 -3 Meter', 'Crocodile 3-4 Meter', 'Crocodile 4 -5 Meter', 'Crocodile < 2,5 Meter', 'Damara Dik-dik', 'Duiker Blue', 'Duiker Grey', 'Eland', 'Elephant', 'Fallow Deer', 'Gemsbuck/oryx', 'Giraffe', 'Grysbuck', 'Hippo', 'Hyena Brown', 'Hyena Spotted', 'Impala', 'Jackal (sjakal)', 'Klipspringer', 'Kudu', 'Leopard', 'Lioness', 'Lion Male', 'Nyala', 'Oribi', 'Ostrich', 'Porcupine', 'Red Hartebeest', 'Red Lechwe', 'Reedbuck Common', 'Reedbuck Mountain', 'Rhebuck Vaal', 'Rhino', 'Roan', 'Sable','Springbuck Black', 'Springbuck Common', 'Springbuck Golden', 'Springbuck White', 'Steenbuck', 'Tsessebe', 'Warthog', 'Waterbuck', 'Wildebeest Black', 'Wildebeest Blue', "Zebra Burchell's", "Zebra Hartmann's"]
    #taxidermy = ('Shoulder Mounts', 'Pedestal Mounts', 'Full Mounts', 'EU Mounts', 'Tanning of Full Skin', 'Tanning Full Skin with Felt', 'Tanning of Back Skins', 'Tanning Back Skin with Felt', 'Rug mounts', 'Taxidermi full Skin', 'Taxidermi back skin')
    taxidermy = ('Shoulder mount', 'Pedestal mount incl. pedestal','Full mount', 'European mount (skull on shield)', 'Tanning of full skin', 'Tanning full skin with felt', 'Tanning of back skin', 'Tanning back skin with felt', 'Rug mount')
    all_inc_ids = []
    entriessm   = []
    entriesped  = []
    entriesfm   = []
    entriesem = []
    entriestfs   = []
    entriestfswf   = []
    entriestbs = []
    entriestbswf = []
    entriesrm = []
    #print len(taxidermy)
    #print len(spicies)
    if form.validate_on_submit():
        #print form.remember_me.data
        #print form.openid.data
        #print form.email.data
        #print form.name.data
        #print form.surname.data
        ids = eval(form.remember_me.data)
        #print ids
        email = form.email.data
        fname = form.name.data
        surname = form.surname.data
        country_id = int(country_id)
        if country_id == 167:
            lang = 'da_DK'
        else:
            lang = 'en_US'
        partner_id = create_partner(fname, surname, email, lang)
        order_id = create_quotation(partner_id, 4, partner_id, partner_id)
        for p_id in ids:
            qty = int(p_id[0])
            prod_id = int(p_id[1])
            while qty > 0:
                select = product_selected(prod_id, int(country_id))
                order_id = float(order_id)
                product_id = int(select[0])
                name = str(select[1])
                taxidermy_work = float(select[3])
                export_papers = float(select[4])
                eu_shipping = float(select[5])
                packing_crating = float(select[6])
                insurance = float(select[7])
                order_line_ids = create_order_line(order_id, product_id, name, None, taxidermy_work, export_papers, eu_shipping, packing_crating, insurance)
                qty -= 1
        #print order_line_ids
        #print product_vals
        #create_order_line(order_id, product_vals[0], product_vals[1], direction, product_vals[3], product_vals[4], product_vals[5], product_vals[6], product_vals[7])
        # 01234567, product_all_inclusive_vat_info, product_all_inclusive_total_info, product_all_inclusive_country_info
        print_email(order_id, email, country_id, fname, surname)
        return redirect(url_for('show_thanks', name=fname, email=email))

    for product_info in products_info:
        for all_in in product_info['all_inclusive_ids']:
            all_inc_ids.append(all_in)
    #print all_inc_ids
    all_inc = models.execute(dbname, uid, pwd,
            'product.all.inclusive', 'read', all_inc_ids, [])
    #print all_inc    
    for all_inc_id in all_inc:
        #print all_inc_id
            #d = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
        if 'Shoulder Mount' in all_inc_id['all_inc_product_id'][1]: #product_info['name']: #if all_inc_id['country_id'][0] == 258:
                #print entriessm#product_info['name']
                #dsm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
             entriessm.append(dict(idsm=all_inc_id['all_inc_product_id'][0], namesm=all_inc_id['all_inc_product_id'][1], pricesm=all_inc_id['tot'], countrysm=all_inc_id['country_id']))
        if 'Pedestal' in all_inc_id['all_inc_product_id'][1]: #if all_inc_id['country_id'][0] == 258:
                #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
            entriesped.append(dict(idped=all_inc_id['all_inc_product_id'][0], nameped=all_inc_id['all_inc_product_id'][1], priceped=all_inc_id['tot'], countryped=all_inc_id['country_id']))
        if 'Full Mount' in all_inc_id['all_inc_product_id'][1]: #product_info['name']:
                #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
            entriesfm.append(dict(idfm=all_inc_id['all_inc_product_id'][0], namefm=all_inc_id['all_inc_product_id'][1], pricefm=all_inc_id['tot'], countryfm=all_inc_id['country_id']))
        if 'European Mount' in all_inc_id['all_inc_product_id'][1]: #product_info['name']:
                #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
            entriesem.append(dict(idem=all_inc_id['all_inc_product_id'][0], nameem=all_inc_id['all_inc_product_id'][1], priceem=all_inc_id['tot'], countryem=all_inc_id['country_id']))
        if 'Tanning of Full Skin' in all_inc_id['all_inc_product_id'][1]: #product_info['name']:
                #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
            entriestfs.append(dict(idtfs=all_inc_id['all_inc_product_id'][0], nametfs=all_inc_id['all_inc_product_id'][1], pricetfs=all_inc_id['tot'], countrytfs=all_inc_id['country_id']))    
        if 'Tanning Full Skin with Felt' in all_inc_id['all_inc_product_id'][1]: #product_info['name']:
                #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
            entriestfswf.append(dict(idtfswf=all_inc_id['all_inc_product_id'][0], nametfswf=all_inc_id['all_inc_product_id'][1], pricetfswf=all_inc_id['tot'], countrytfswf=all_inc_id['country_id']))    
        if 'Tanning of Back Skin' in all_inc_id['all_inc_product_id'][1]: #product_info['name']:
                #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
            entriestbs.append(dict(idtbs=all_inc_id['all_inc_product_id'][0], nametbs=all_inc_id['all_inc_product_id'][1], pricetbs=all_inc_id['tot'], countrytbs=all_inc_id['country_id']))    
        if 'Tanning Back Skin with Felt' in all_inc_id['all_inc_product_id'][1]: #product_info['name']:
                #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
            entriestbswf.append(dict(idtbswf=all_inc_id['all_inc_product_id'][0], nametbswf=all_inc_id['all_inc_product_id'][1], pricetbswf=all_inc_id['tot'], countrytbswf=all_inc_id['country_id']))    
        if 'Rugmount' in all_inc_id['all_inc_product_id'][1]: #product_info['name']:
                #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
            entriesrm.append(dict(idrm=all_inc_id['all_inc_product_id'][0], namerm=all_inc_id['all_inc_product_id'][1], pricerm=all_inc_id['tot'], countryrm=all_inc_id['country_id']))    
            
                #entriesped.append(all_inc_id['tot'])      
            #print all_inc_id['id'], all_inc_id['name'], all_inc_id['taxidermy_work'], all_inc_id['export_papers'], all_inc_id['eu_shipping'], all_inc_id['packing_crating'], all_inc_id['insurance'], all_inc_id['vat'], all_inc_id['tot'], all_inc_id['country_id'] 
    entriessm = sorted(entriessm, key=lambda k: (k['namesm']))
    spicies = sorted(spicies)
    #print spicies
    entriesped = sorted(entriesped, key=lambda k: (k['nameped']))
    entriesfm = sorted(entriesfm, key=lambda k: (k['namefm']))
    entriesem = sorted(entriesem, key=lambda k: (k['nameem']))
    entriestfs = sorted(entriestfs, key=lambda k: (k['nametfs']))
    entriestfswf = sorted(entriestfswf, key=lambda k: (k['nametfswf']))
    entriestbs = sorted(entriestbs, key=lambda k: (k['nametbs']))
    entriestbswf = sorted(entriestbswf, key=lambda k: (k['nametbswf']))
    entriesrm = sorted(entriesrm, key=lambda k: (k['namerm']))
    return render_template('show_entries.html', country_id=country_id, form=form, title='Calculator', entriessm=entriessm, entriesped=entriesped, entriesfm=entriesfm, entriesem=entriesem, entriestfs=entriestfs, entriestfswf=entriestfswf, entriestbs=entriestbs, entriestbswf=entriestbswf, entriesrm=entriesrm, spicies=spicies, taxidermy=taxidermy)#, country_id=country_id)

@app.route('/', methods=['GET', 'POST'])
def show_welcome():

    form = CountryForm()

    if form.validate_on_submit():
        #print form.country_id.data
        country_id = int(form.country_id.data)
        #show_entries(country_id)
        return redirect(url_for('show_entries', country_id=country_id))
        #return redirect('/')

    return render_template('show_welcome.html', form=form)


@app.route('/countries')
def show_countries():

    #Products
    ids = models.execute(dbname, uid, pwd,
        'product.product', 'search',
        (['is_all_inclusive', '=', True], ['is_displayed_on_website', '=', True]))  #[('is_all_inclusive', '=', True)])

    products_info = models.execute(dbname, uid, pwd,
            'product.product', 'read', ids, ['id', 'name', 'all_inclusive_ids'])

    spicies = ['Baboon', 'Barbersheep', 'Blesbuck Common', 'Blesbuck White', 'Bontebuck', 'Buffalo', 'Bushbuck', 'Bushpig', 'Caracal/lynx', 'Cheetah', 'Civet Cat', 'Crocodile 2,5 -3 Meter', 'Crocodile < 2,5 Meter', 'Crocodile 3-4 Meter', 'Crocodile 4 -5 Meter', 'Damara Dik-dik', 'Duiker Blue', 'Duiker Grey', 'Eland', 'Elephant', 'Fallow Deer', 'Gemsbuck/oryx', 'Giraffe', 'Grysbuck', 'Hippo', 'Hyena Brown', 'Hyena Spotted', 'Impala', 'Jackal (sjakal)', 'Klipspringer', 'Kudu', 'Leopard', 'Lioness', 'Lion Male', 'Nyala', 'Oribi', 'Ostrich', 'Porcupine', 'Red Hartebeest', 'Red Lechwe', 'Reedbuck Common', 'Reedbuck Mountain', 'Rhebuck Vaal', 'Rhino', 'Roan', 'Sable', 'Springbuck Black', 'Springbuck Common', 'Springbuck Golden', 'Springbuck White', 'Steenbuck', 'Tsessebe', 'Warthog', 'Waterbuck', 'Wildebeest Black', 'Wildebeest Blue', "Zebra Burchell's", "Zebra Hartmann's"]
    taxidermy = ('Shoulder Mounts', 'Pedestal Mounts', 'Full Mounts', 'EU Mounts', 'Tanning Full Skin', 'Tanning of Full Skin with Felt', 'Tanning of Back Skins', 'Tanning of Back Skin with Felt', 'Rug mounts', 'Taxidermi full Skin', 'Taxidermi back skin')
    
    countries   = []
    entries   = []

    for product_info in products_info:
        all_inc_ids = product_info['all_inclusive_ids']
        all_inc = models.execute(dbname, uid, pwd,
            'product.all.inclusive', 'read', all_inc_ids, [])
        
        for all_inc_id in all_inc:
            d = dict(country=all_inc_id['country_id'])
            countries.append(d)  
            if 'Rugmount' in product_info['name']:
                #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
                entries.append(dict(name=product_info['name'], price=all_inc_id['tot'], country=all_inc_id['country_id']))    

    #print countries
    return render_template('show_countries.html', spicies=spicies, taxidermy=taxidermy, entries=entries)

@app.route('/thanks/<name> at <email>')
def show_thanks(name, email):
    return render_template('show_thanks.html', title='Calculator', name=name, email=email)

if __name__ == '__main__':
    app.run()
