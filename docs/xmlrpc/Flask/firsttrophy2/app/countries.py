from flask import render_template
from app import app
import xmlrpclib

username = 'admin' #the user
pwd      = 'mGnG54'      #the password of the user
dbname   = 'firsttrophy'    #the database
url      = 'http://firsttrophy.openerp.co.za' # The url to connect to

#xmlrpclib
common     = xmlrpclib.ServerProxy('{}/xmlrpc/common'.format(url))
uid        = common.login(dbname, username, pwd)
models     = xmlrpclib.ServerProxy('{}/xmlrpc/object'.format(url))


@app.route('/countries')
def show_countries():

    #Products
    ids = models.execute(dbname, uid, pwd,
        'product.product', 'search',
        [('is_all_inclusive', '=', True)])

    products_info = models.execute(dbname, uid, pwd,
            'product.product', 'read', ids, ['id', 'name', 'all_inclusive_ids'])

    countries   = []

    for product_info in products_info:
        all_inc_ids = product_info['all_inclusive_ids']
        all_inc = models.execute(dbname, uid, pwd,
            'product.all.inclusive', 'read', all_inc_ids, [])
        
        for all_inc_id in all_inc:
            d = dict(country=all_inc_id['country_id'])
            countries.append(d)      

    print countries
    return render_template('show_countries.html', countries=countries)