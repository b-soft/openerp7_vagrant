import openerplib

username = 'admin' #the user
pwd = 'mGnG54'      #the password of the user
dbname = 'firsttrophy'    #the database
IP =  "41.223.33.6"	#hostname
port =	7069	#port

connection = openerplib.get_connection(hostname=IP, protocol="xmlrpc", port=port, database=dbname, \
    login=username, password=pwd)

user_model = connection.get_model("res.users")
ids = user_model.search([("login", "=", "admin")])
user_info = user_model.read(ids[0], ["id"])
print user_info["id"]
# will print "Administrator"

product_model = connection.get_model("product.product")
product_all_inclusive_model = connection.get_model("product.all.inclusive")
p_ids = product_model.search([("is_all_inclusive", "=", True)])

print "Product Name     Country  Taxidermy work  Dip & pack  Export papers  Direct shipping  Packing & crating  Isurance  Vat  Total(All inclusive)"

for p_id in p_ids:

    product_id = product_model.read(p_id, ["id"])
    product_id_info = product_id["id"]

    product_name = product_model.read(p_id, ["name"])
    product_name_info = product_name["name"]
    
    if p_id == 1028:

        product_all_inc_ids = product_model.read(p_id, ["all_inclusive_ids"])
        product_all_inc_info = product_all_inc_ids["all_inclusive_ids"]

        for all_inc_id in product_all_inc_info:

            product_all_inclusive_country = product_all_inclusive_model.read(all_inc_id, ["country_id"])
            product_all_inclusive_country_info = product_all_inclusive_country["country_id"]
            #258 EU
            #167 NW
            #256 USA
            if product_all_inclusive_country_info[0] == 256:

                product_all_inclusive_name = product_all_inclusive_model.read(all_inc_id, ["name"])
                product_all_inclusive_name_info = product_all_inclusive_name["name"]

                product_all_inclusive_taxidermy_work = product_all_inclusive_model.read(all_inc_id, ["taxidermy_work"])
                product_all_inclusive_taxidermy_work_info = product_all_inclusive_taxidermy_work["taxidermy_work"]

                product_all_inclusive_export_papers = product_all_inclusive_model.read(all_inc_id, ["export_papers"])
                product_all_inclusive_export_papers_info = product_all_inclusive_export_papers["export_papers"]

                product_all_inclusive_eu_shipping = product_all_inclusive_model.read(all_inc_id, ["eu_shipping"])
                product_all_inclusive_eu_shipping_info = product_all_inclusive_eu_shipping["eu_shipping"]

                product_all_inclusive_packing_crating = product_all_inclusive_model.read(all_inc_id, ["packing_crating"])
                product_all_inclusive_packing_crating_info = product_all_inclusive_packing_crating["packing_crating"]

                product_all_inclusive_insurance = product_all_inclusive_model.read(all_inc_id, ["insurance"])
                product_all_inclusive_insurance_info = product_all_inclusive_insurance["insurance"]
                                
                product_all_inclusive_vat = product_all_inclusive_model.read(all_inc_id, ["vat"])
                product_all_inclusive_vat_info = product_all_inclusive_vat["vat"]        

                product_all_inclusive_total = product_all_inclusive_model.read(all_inc_id, ["tot"])
                product_all_inclusive_total_info = product_all_inclusive_total["tot"]

                pipe = "| "
     
                print "____________________________________________________________________________________________________________________________________________________________"
                #print pipe, product_id_info, pipe, product_name_info, pipe, product_all_inclusive_name_info, pipe, product_all_inclusive_taxidermy_work_info, pipe, product_all_inclusive_export_papers_info, pipe, product_all_inclusive_eu_shipping_info, pipe, product_all_inclusive_packing_crating_info, pipe, product_all_inclusive_insurance_info, pipe, product_all_inclusive_vat_info, pipe, product_all_inclusive_total_info, pipe, product_all_inclusive_country_info, pipe, product_model.name_get(p_id)[0], pipe
                print pipe, product_name_info, pipe, product_all_inclusive_name_info, pipe, product_all_inclusive_taxidermy_work_info, pipe, product_all_inclusive_export_papers_info, pipe, product_all_inclusive_eu_shipping_info, pipe, product_all_inclusive_packing_crating_info, pipe, product_all_inclusive_insurance_info, pipe, product_all_inclusive_vat_info, pipe, product_all_inclusive_total_info, pipe,     

########DEFINE########################
       
def create_partner(name, email, lang):
    partner = {
       'name': "{0}".format(name),
       'email': "{0}".format(email),
       'customer':True,
       'lang': "{0}".format(lang),
    }
    partner_model = connection.get_model("res.partner")
    partner_id = partner_model.create(partner)
    return partner_id

def create_quotation(partner_id, pricelist_id, partner_invoice_id, partner_shipping_id):
    order = {
       'partner_id': partner_id,
       'pricelist_id': pricelist_id,
       'partner_invoice_id': partner_invoice_id,
       'partner_shipping_id': partner_invoice_id
    }
    order_model = connection.get_model("sale.order")
    order_id = order_model.create(order)
    return order_id

def create_order_line(order_id, product_id, name, direction, taxidermy_work, export_papers, eu_shipping, packing_crating, insurance):
    order_line1 = {                
        'order_id': order_id,
        'product_id': product_id,
        'name':"{0}{1}{2}".format((name or ''), (". " or ''), (direction or '')),
        'product_uom_qty': 1,
        'price_unit': taxidermy_work,  
        'direction': "{0}".format(direction or ''),                  
    }
    order_line_model = connection.get_model("sale.order.line")
    order_line1_id = order_line_model.create(order_line1)

    order_line2 = {
        'order_id': order_id,
        'name':"{0}".format('DIP & PACK EXPORT PAPERS'),
        'product_uom_qty': 1,
        'price_unit': export_papers,
    }
    order_line2_id = order_line_model.create(order_line2)

    order_line3 = {
        'order_id': order_id,
        'name':"{0}".format('DIRECT SHIPPING TO PRIVATE ADDRESS IN ALL EU-COUNTRIES'),
        'product_uom_qty': 1,
        'price_unit': eu_shipping,
    } 
    order_line3_id = order_line_model.create(order_line3)

    order_line4 = {
        'order_id': order_id,
        'name':"{0}".format('PACKING & CRATING'),
        'product_uom_qty': 1,
        'price_unit': packing_crating,
    } 
    order_line4_id = order_line_model.create(order_line4)

    order_line5 = {
        'order_id': order_id,
        'name':"{0}".format('INSURANCE'),
        'product_uom_qty': 1,
        'price_unit': insurance,
    }
    order_line5_id = order_line_model.create(order_line5) 

    return order_line1_id, order_line2_id, order_line3_id, order_line4_id, order_line5_id

#def send_quotation():

#######CALLS#####################

name = str('customer_name')
email = str('cwustomerjemakil@me.ng')
lang = str('da_DK') #customer_language
partner_id = create_partner(name, email, lang)
print "partner_id: '%s'"%partner_id

partner_id = int(partner_id)
pricelist_id = 1    #int(customer_pricelist)
partner_invoice_id = int(partner_id)
partner_shipping_id = (partner_id)
order_id = create_quotation(partner_id, pricelist_id, partner_invoice_id, partner_shipping_id)
print "order_id: '%s'"%order_id 

order_id = float(order_id)
product_id = int(product_id_info)
name = str(product_name_info)
taxidermy_work = float(product_all_inclusive_taxidermy_work_info)
export_papers = float(product_all_inclusive_export_papers_info)
eu_shipping = float(product_all_inclusive_eu_shipping_info)
packing_crating = float(product_all_inclusive_packing_crating_info)
insurance = float(product_all_inclusive_insurance_info)
order_line_ids = create_order_line(order_id, product_id, name, None, taxidermy_work, export_papers, eu_shipping, packing_crating, insurance)
print order_line_ids#"order_line_ids: '%s'"%order_line_ids