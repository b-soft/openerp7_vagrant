#from __future__ import print_function   #For print function in ERPpeek
import erppeek                          #For ERPpeek
import openerplib
import xmlrpclib

import logging
_logger = logging.getLogger(__name__)

username = 'admin' #the user
pwd      = 'mGnG54'      #the password of the user
dbname   = 'firsttrophy'    #the database
IP       =  "41.223.33.6" #hostname
port     =  7069    #port
url      = 'http://firsttrophy.openerp.co.za'#'http://41.223.33.6:7069' # The url to connect to

#openerplib
connection = openerplib.get_connection(hostname=IP, protocol="xmlrpc", port=port, database=dbname, \
    login=username, password=pwd)

#xmlrpclib
common     = xmlrpclib.ServerProxy('{}/xmlrpc/common'.format(url))
uid        = common.login(dbname, username, pwd)
models     = xmlrpclib.ServerProxy('{}/xmlrpc/object'.format(url))

#erppeek
client     = erppeek.Client(url, dbname, username, pwd)
proxy      = client.model('res.users')

'''L = []
ids = proxy.search([('is_all_inclusive', '=', True)])
# No need to use the model.search method, the model.browse method accepts a domain
products = proxy.browse(ids)

for product in products:
    print('[{product.id}, "{product.name}"]'.format(product=product))
    d = '[{product.id}, "{product.name}"]'.format(product=product)
    #'{"Id" : "%s", "Name" : "%s"}'%(a, b)
    L.append(d)
file_list = open('product.php','w')
file_list.write(L)
file_list.close()

#Products
ids = models.execute(dbname, uid, pwd,
    'product.product', 'search',
    [('is_all_inclusive', '=', True)])#(['is_all_inclusive', '=', True], ['is_displayed_on_website', '=', True]))  #[('product_tmpl_id.name', '=', 'Antilope Leg DIP & SHIP, All Inclusive')])

A = models.execute(dbname, uid, pwd,
        'product.product', 'read', ids, ['id', 'name', 'all_inclusive_ids'])
#print A

i=0
for a in A:
    #print a['all_inclusive_ids']
    i+=1
    all_inc_ids = a['all_inclusive_ids']
    all_inc = models.execute(dbname, uid, pwd,
        'product.all.inclusive', 'read', all_inc_ids, [])
    print "----------------------------------------------------------"
    print i
    print a['id'], a['name']
    for all_inc_id in all_inc:        
        print all_inc_id['id'], all_inc_id['name'], all_inc_id['taxidermy_work'], all_inc_id['export_papers'], all_inc_id['eu_shipping'], all_inc_id['packing_crating'], all_inc_id['insurance'], all_inc_id['vat'], all_inc_id['tot'], all_inc_id['country_id'] 
'''

#Printing
import time
import base64
printmodels = xmlrpclib.ServerProxy('{}/xmlrpc/report'.format(url))
model = 'sale.order'
id_report = printmodels.report(dbname, uid, pwd, model, [1304], {'model': model, 'id': 1304, 'report_type':'pdf'})
time.sleep(5)
state = False
attempt = 0
while not state:
    report = printmodels.report_get(dbname, uid, pwd, id_report)
    state = report['state']
    if not state:
        time.sleep(1)
        attempt += 1
    if attempt>200:
        print 'Printing aborted, too long delay !'

string_pdf = base64.decodestring(report['result'])
file_pdf = open('/home/erpwevpy/Gut/Quotation%s.pdf'%int(1304),'w')
file_pdf.write(string_pdf)
file_pdf.close()     

print 'Done'

# welcome email sent to portal users
# (note that calling '_' has no effect except exporting those strings for translation)
WELCOME_EMAIL_SUBJECT = ("Your OpenERP account at %(company)s")
WELCOME_EMAIL_BODY = ("""Dear %(name)s,

You have been given access to %(portal)s.


--
OpenERP - Open Source Business Applications
https://internal.odoo.zone
""")


data = {
    'company': 'First Class Trophy',
    'portal': 'Customer',
    'name': "Customer's Name",
        }

mail_values = {
    'email_from': 'admin@firsttrophy.openerp.co.za',
    'email_to': 'nganpet2007@gmail.com',
    'subject': (WELCOME_EMAIL_SUBJECT) % data,
    'body_html': '<pre>%s</pre>' % ((WELCOME_EMAIL_BODY) % data),
    'state': 'outgoing',
    'type': 'email',
        }
mail_id = models.execute(dbname, uid, pwd,
        'mail.mail', 'create', mail_values)

print mail_id
attachments = []
attachments.append(("Quotation.pdf", string_pdf))
#print attachments
attachment_ids = []
for data in attachments:
    attachment_data = {
        'name': data[0],
        'datas_fname': data[0],
        'datas': data[1],
        'res_model': 'mail.mail',
        'res_id': mail_id,
            }   

attach_id = models.execute(dbname, uid, pwd, 'ir.attachment', 'create', attachment_data)
attachment_ids.append(attach_id)
if attachment_ids:
    models.execute(dbname, uid, pwd, 'mail.mail', 'write', mail_id, {'attachment_ids': [(6, 0, attachment_ids)]})
    models.execute(dbname, uid, pwd, 'mail.mail', 'send', [mail_id])
#mail_model = connection.get_model("mail.mail")
#mail_model.send([mail_id])
print "Done 2!"


'''# welcome email sent to portal users
# (note that calling '_' has no effect except exporting those strings for translation)
WELCOME_EMAIL_SUBJECT = _("Your OpenERP account at %(company)s")
WELCOME_EMAIL_BODY = _("""Dear %(name)s,

You have been given access to %(portal)s.

Your login account data is:
Database: %(db)s
Username: %(login)s

In order to complete the signin process, click on the following url:
%(url)s

%(welcome_message)s

--
OpenERP - Open Source Business Applications
http://www.openerp.com
""")


        data = {
            'company': this_user.company_id.name,
            'portal': partner.portal_id.name,
            'welcome_message': partner.welcome_message or "",
            'db': cr.dbname,
            'name': user.name,
            'login': user.login,
            'url': user.signup_url,
        }
        mail_mail = self.pool.get('mail.mail')
        mail_values = {
            'email_from': this_user.email,
            'email_to': user.email,
            'subject': _(WELCOME_EMAIL_SUBJECT) % data,
            'body_html': '<pre>%s</pre>' % (_(WELCOME_EMAIL_BODY) % data),
            'state': 'outgoing',
            'type': 'email',
        }
        mail_id = mail_mail.create(cr, uid, mail_values, context=this_context)
        'attachments = []
        'attachments.append(("Quotation.pdf", base64.decodestring(report['result']))#attachments.append(("test.txt", base64.b64decode("Test Attached File")))
        'attachment_ids = []
        'for data in attachments:
            attachment_data = {
                'name': data[0],
                'datas_fname': data[0],
                'datas': data[1],
                'res_model': mail_mail._name,
                'res_id': mail_id,
            }
            'attachment_ids.append(ir_attachment.create(cursor, uid, attachment_data, context=context))
        'if attachment_ids:
        'mail_mail.write(cursor, uid, mail_id, {'attachment_ids': [(6, 0, attachment_ids)]}, context=context)
        'mail_mail_obj.send(cursor, uid, [mail_id], context=context)
        mail_mail.send(cr, uid, [mail_id], context=this_context)
'''