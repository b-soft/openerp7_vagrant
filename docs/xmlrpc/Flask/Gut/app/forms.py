from flask.ext.wtf import Form 
from wtforms import StringField, BooleanField, FloatField, SelectField
from wtforms.validators import DataRequired

class LoginForm(Form):
	openid = FloatField('openid')
	email = StringField('email', validators=[DataRequired()])
	remember_me = StringField('remember_me')


class CountryForm(Form):
	country_id = SelectField(u'Regional Country', choices=[('258', 'EU-Countries'), ('167', 'Norway'), ('256', 'USA/CANADA/MEXICO/RUSSIA')])
