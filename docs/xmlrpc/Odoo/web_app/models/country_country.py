# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

from openerp.models import BaseModel
from openerp import fields


class Country(BaseModel):

    _name = 'country.country'

    country_id = fields.Many2one('res.country', 'Country')