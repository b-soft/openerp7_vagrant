# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

from openerp.models import BaseModel
from openerp import fields


class ProductCountry(BaseModel):

    _name = 'product.country'

    price 		= fields.Float('Price')
    email 		= fields.Char('Email')
    product_ids = fields.Char('')