# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.
{
    'name': 'Odoo Blog',
    'version': '1.0',
    'author': 'HacBee UAB',
    'category': 'Custom',
    'website': 'http://www.hbee.eu',
    'summary': '',
    'description': """
A simple blog application that allows
for creating, editing, deleting and viewing
blog posts.
""",
    'depends': [
        'website',
    ],
    'data': [
        'view/posts.xml',
    ],
    'installable': True,
    'application': False,
}
