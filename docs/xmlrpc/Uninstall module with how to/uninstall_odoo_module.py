#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""Uninstall a module"""

import xmlrpclib
import argparse
import getpass


user = 'admin' #the user
password      = 'admin'      #the password of the user
database   = 'odoo_backend_tut' #'events_confirmation'    #the database
IP       =  "172.16.163.130" #hostname
port     =  8069    #port
url      = 'http://172.16.163.130:8069' #'http://41.223.33.6:7069' # The url to connect to

parser = argparse.ArgumentParser()
# Connection args
parser.add_argument('-d', '--database', help="Use DB as the database name", action='store', metavar='DB', default=database)
parser.add_argument('-u', '--user', help="Use USER as the database user name", action='store', metavar='USER', default=user)
parser.add_argument('-w', '--password', help="Use PASSWORD as the database password.", action='store', metavar='PASSWORD', default=password)
parser.add_argument('-s', '--url', help="Point to the web services hosted at URL", action='store', metavar='URL', default=url)
# Feature args
parser.add_argument('module', help="Uninstall the module MODULE", action='store', metavar='MODULE')

args = vars(parser.parse_args())

# Log in
ws_common = xmlrpclib.ServerProxy(args['url'] + '/xmlrpc/common')
uid = ws_common.login(args['database'], args['user'], args['password'])
print "Logged in to the common web service."
# Get the object proxy
ws_object = xmlrpclib.ServerProxy(args['url'] + '/xmlrpc/object')
print "Connected to the object web service."

# Find the parent location by name
res_ids = ws_object.execute(args['database'], uid, args['password'],'ir.module.module', 'search', [('name', '=', args['module'])])
if len(res_ids) != 1:
    raise Exception("Search failed")

# Uninstall the module
print "Uninstalling '%s'" % args['module']
ws_object.execute(args['database'], uid, args['password'],'ir.module.module', "button_immediate_uninstall", res_ids)
#ws_object.execute(args['database'], uid, args['password'],'ir.module.module', "button_immediate_install", res_ids)	#To install a module

print "All done."

