from flask.ext.wtf import Form 
from wtforms import StringField, BooleanField, FloatField, SelectField
from wtforms.validators import DataRequired

class LoginForm(Form):
	openid = FloatField('openid')
	importvat = FloatField('importVAT')
	email = StringField('email', validators=[DataRequired()])
	remember_me = StringField('remember_me', validators=[DataRequired()])
	name = StringField('Name', validators=[DataRequired()])
	surname = StringField('Surname', validators=[DataRequired()])


class CountryForm(Form):
	country_id = SelectField(u'Regional Country', choices=[('258', 'EU - Country'), ('167', 'Norway'), ('256', 'USA/Canada/Mexico/Russia/China')])
