import xmlrpclib
#import openerplib

username = 'admin' #the user
pwd      = 'mGnG54'      #the password of the user
dbname   = 'firsttrophy'    #the database
IP       =  "openerp.co.za" #hostname
port     =  7069    #port
url      = 'http://41.223.33.6:7069'     #'http://firsttrophy.openerp.co.za' #'http://41.223.33.6:7069' # The url to connect to

#xmlrpclib
common     = xmlrpclib.ServerProxy('{}/xmlrpc/common'.format(url))
uid        = common.login(dbname, username, pwd)
models     = xmlrpclib.ServerProxy('{}/xmlrpc/object'.format(url))

#openerplib
#connection = openerplib.get_connection(hostname=IP, protocol="xmlrpc", port=port, database=dbname, \
    #login=username, password=pwd)

def product_selected(p_id, country_id):
    #product_model = connection.get_model("product.product")
    #product_all_inclusive_model = connection.get_model("product.all.inclusive")

    #product_id = product_model.read(p_id, ["id"])
    product_id = models.execute(dbname, uid, pwd, 'product.product', 'read', p_id, ['id'])
    product_id_info = product_id["id"]

    #product_name = product_model.read(p_id, ["name"])
    product_name = models.execute(dbname, uid, pwd, 'product.product', 'read', p_id, ['name'])
    product_name_info = product_name["name"]

    #product_all_inc_ids = product_model.read(p_id, ["all_inclusive_ids"])
    product_all_inc_ids = models.execute(dbname, uid, pwd, 'product.product', 'read', p_id, ['all_inclusive_ids'])
    product_all_inc_info = product_all_inc_ids["all_inclusive_ids"]
    
    for all_inc_id in product_all_inc_info:
        #product_all_inclusive_country = product_all_inclusive_model.read(all_inc_id, ["country_id"])
        product_all_inclusive_country = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['country_id'])
        product_all_inclusive_country_info = product_all_inclusive_country["country_id"]
            #258 EU
            #167 NW
            #256 USA
        if product_all_inclusive_country_info[0] == country_id:
            #product_all_inclusive_name = product_all_inclusive_model.read(all_inc_id, ["name"])
            product_all_inclusive_name = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['name'])
            product_all_inclusive_name_info = product_all_inclusive_name["name"]

            #product_all_inclusive_taxidermy_work = product_all_inclusive_model.read(all_inc_id, ["taxidermy_work"])
            product_all_inclusive_taxidermy_work = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['taxidermy_work'])
            product_all_inclusive_taxidermy_work_info = product_all_inclusive_taxidermy_work["taxidermy_work"]

            #product_all_inclusive_export_papers = product_all_inclusive_model.read(all_inc_id, ["export_papers"])
            product_all_inclusive_export_papers = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['export_papers'])
            product_all_inclusive_export_papers_info = product_all_inclusive_export_papers["export_papers"]

            #product_all_inclusive_eu_shipping = product_all_inclusive_model.read(all_inc_id, ["eu_shipping"])
            product_all_inclusive_eu_shipping = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['eu_shipping'])
            product_all_inclusive_eu_shipping_info = product_all_inclusive_eu_shipping["eu_shipping"]

            #product_all_inclusive_packing_crating = product_all_inclusive_model.read(all_inc_id, ["packing_crating"])
            product_all_inclusive_packing_crating = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['packing_crating'])
            product_all_inclusive_packing_crating_info = product_all_inclusive_packing_crating["packing_crating"]

            #product_all_inclusive_insurance = product_all_inclusive_model.read(all_inc_id, ["insurance"])
            product_all_inclusive_insurance = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['insurance'])
            product_all_inclusive_insurance_info = product_all_inclusive_insurance["insurance"]
                
            #product_all_inclusive_vat = product_all_inclusive_model.read(all_inc_id, ["vat"])
            product_all_inclusive_vat = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['vat'])
            product_all_inclusive_vat_info = product_all_inclusive_vat["vat"]        

            #product_all_inclusive_total = product_all_inclusive_model.read(all_inc_id, ["tot"])
            product_all_inclusive_total = models.execute(dbname, uid, pwd, 'product.all.inclusive', 'read', all_inc_id, ['tot'])
            product_all_inclusive_total_info = product_all_inclusive_total["tot"]

            pipe = "| "
     
            print "____________________________________________________________________________________________________________________________________________________________"
            print pipe, product_name_info, pipe, product_all_inclusive_name_info, pipe, product_all_inclusive_taxidermy_work_info, pipe, product_all_inclusive_export_papers_info, pipe, product_all_inclusive_eu_shipping_info, pipe, product_all_inclusive_packing_crating_info, pipe, product_all_inclusive_insurance_info, pipe, product_all_inclusive_vat_info, pipe, product_all_inclusive_total_info, pipe,     

    return product_id_info, product_name_info, product_all_inclusive_name_info, product_all_inclusive_taxidermy_work_info, product_all_inclusive_export_papers_info, product_all_inclusive_eu_shipping_info, product_all_inclusive_packing_crating_info, product_all_inclusive_insurance_info, product_all_inclusive_vat_info, product_all_inclusive_total_info, product_all_inclusive_country_info     #, product_model.name_get(p_id)[0]

def create_partner(email, lang):
    partner = {
       'name': email,      #"{0}".format(email),
       'email': email,     #"{0}".format(email),
       #'customer':True,
       'lang': lang,       #"{0}".format(lang),
    }
    #partner_model = connection.get_model("res.partner")
    #partner_id = partner_model.create(partner)
    partner_id = models.execute(dbname, uid, pwd, 'res.partner', 'create', partner)
    return partner_id

#product_selected(1086, 167)

part_id = create_partner(email="ng@cgii.com", lang="da_DK")
print part_id
print 'Done!!!'

'''def create_partner(name, email, lang):
    partner = {
       'name': "{0}".format(name),
       'email': "{0}".format(email),
       'customer':True,
       'lang': "{0}".format(lang),
    }
    #partner_model = connection.get_model("res.partner")
    #partner_id = partner_model.create(partner)
    partner_id = models.execute(dbname, uid, pwd, 'res.partner', 'create', partner)
    return partner_id

def create_quotation(partner_id, pricelist_id, partner_invoice_id, partner_shipping_id):
    order = {
       'partner_id': partner_id,
       'pricelist_id': pricelist_id,
       'partner_invoice_id': partner_invoice_id,
       'partner_shipping_id': partner_invoice_id
    }
    #order_model = connection.get_model("sale.order")
    #order_id = order_model.create(order)
    order_id = models.execute(dbname, uid, pwd, 'sale.order', 'create', order)
    return order_id

def create_order_line(order_id, product_id, name, direction, taxidermy_work, export_papers, eu_shipping, packing_crating, insurance):
    order_line1 = {                
        'order_id': order_id,
        'product_id': product_id,
        'name':"{0}{1}{2}".format((name or ''), (". " or ''), (direction or '')),
        'product_uom_qty': 1,
        'price_unit': taxidermy_work,  
        'direction': "{0}".format(direction or ''),                  
    }
    #order_line_model = connection.get_model("sale.order.line")
    #order_line1_id = order_line_model.create(order_line1)
    order_line1_id = models.execute(dbname, uid, pwd, 'sale.order.line', 'create', order_line1)

    order_line2 = {
        'order_id': order_id,
        'name':"{0}".format('DIP & PACK EXPORT PAPERS'),
        'product_uom_qty': 1,
        'price_unit': export_papers,
    }
    #order_line2_id = order_line_model.create(order_line2)
    order_line2_id = models.execute(dbname, uid, pwd, 'sale.order.line', 'create', order_line2)

    order_line3 = {
        'order_id': order_id,
        'name':"{0}".format('DIRECT SHIPPING TO PRIVATE ADDRESS IN ALL EU-COUNTRIES'),
        'product_uom_qty': 1,
        'price_unit': eu_shipping,
    } 
    #order_line3_id = order_line_model.create(order_line3)
    order_line3_id = models.execute(dbname, uid, pwd, 'sale.order.line', 'create', order_line3)

    order_line4 = {
        'order_id': order_id,
        'name':"{0}".format('PACKING & CRATING'),
        'product_uom_qty': 1,
        'price_unit': packing_crating,
    } 
    #order_line4_id = order_line_model.create(order_line4)
    order_line4_id = models.execute(dbname, uid, pwd, 'sale.order.line', 'create', order_line4)

    order_line5 = {
        'order_id': order_id,
        'name':"{0}".format('INSURANCE'),
        'product_uom_qty': 1,
        'price_unit': insurance,
    }
    #order_line5_id = order_line_model.create(order_line5) 
    order_line5_id = models.execute(dbname, uid, pwd, 'sale.order.line', 'create', order_line5)

    return order_line1_id, order_line2_id, order_line3_id, order_line4_id, order_line5_id

#Printing and emailing
def print_email(order_id, email):
    import time
    import base64
    printmodels = xmlrpclib.ServerProxy('{}/xmlrpc/report'.format(url))
    model = 'sale.order'
    id_report = printmodels.report(dbname, uid, pwd, model, [order_id], {'model': model, 'id': order_id, 'report_type':'pdf'})
    time.sleep(5)
    state = False
    attempt = 0
    while not state:
        report = printmodels.report_get(dbname, uid, pwd, id_report)
        state = report['state']
        if not state:
            time.sleep(1)
            attempt += 1
        if attempt>200:
            print 'Printing aborted, too long delay !'

    string_pdf = base64.decodestring(report['result'])
    file_pdf = open('Quotation%s.pdf'%int(order_id),'w')
    file_pdf.write(string_pdf)
    file_pdf.close()    

    print 'Done printing!'

    # welcome email sent to portal users
    # (note that calling '_' has no effect except exporting those strings for translation)
    WELCOME_EMAIL_SUBJECT = ("Your OpenERP Quotation From %(company)s")
    WELCOME_EMAIL_BODY = ("""Dear %(name)s,

    Please view your <a href='http://openerp.co.za/firsttrophy/Quotation%(order_id)s.pdf'>online Quotation</a> you requested when you last visited 
    our web site.
    We gladly welcome you as our potential %(portal)s.


    --
    FCT - First Class Trophy
    http://firstclasstrophy.com/
    """)


    data = {
        'company': 'First Class Trophy',
        'portal': 'Customer',
        'name': "Visitor",
        'order_id': int(order_id)
            }

    mail_values = {
        'email_from': 'office@firstclasstrophy.com',
        'email_to': email,
        'subject': (WELCOME_EMAIL_SUBJECT) % data,
        'body_html': '<pre>%s</pre>' % ((WELCOME_EMAIL_BODY) % data),
        'state': 'outgoing',
        'type': 'email',
            }
    mail_id = models.execute(dbname, uid, pwd,
            'mail.mail', 'create', mail_values)

    print mail_id
    attachments = []
    attachments.append(("Quotation.pdf", string_pdf))
    #print attachments
    attachment_ids = []
    for data in attachments:
        attachment_data = {
            'name': data[0],
            'datas_fname': data[0],
            'datas': data[1],
            'res_model': 'mail.message',    #'mail.mail',
            'res_id': mail_id,
                }   

    #attach_id = models.execute(dbname, uid, pwd, 'ir.attachment', 'create', attachment_data)
    #attachment_ids.append(attach_id)
    #print attach_id
    if attachments:
        #models.execute(dbname, uid, pwd, 'mail.mail', 'write', mail_id, {'attachment_ids': [(6, 0, attachment_ids)]})
        models.execute(dbname, uid, pwd, 'mail.mail', 'send', [mail_id])
    #mail_model = connection.get_model("mail.mail")
    #mail_model.send([mail_id])
    print "Done 2!"

@app.route('/<country_id>', methods=['GET', 'POST'])
def show_entries(country_id):

    form = LoginForm()
    #Products
    ids = models.execute(dbname, uid, pwd,
        'product.product', 'search',
        (['is_all_inclusive', '=', True], ['is_displayed_on_website', '=', True]))  #[('is_all_inclusive', '=', True)])  #[('product_tmpl_id.name', '=', 'Antilope Leg DIP & SHIP, All Inclusive')])

    products_info = models.execute(dbname, uid, pwd,
            'product.product', 'read', ids, ['id', 'name', 'all_inclusive_ids'])

    #spicies   = ('Baboon', 'Barbersheep', 'Blesbock, Common', 'Blesbock, White', 'Bontebock', 'Buffalo', 'Bushbock', 'Bushpig', 'Civet cat', 'Cheetah', 'Crocodile < 2,5 meter', 'Crocodile 2,5 -3 meter', 'Crocodile 3-4 meter', 'Crocodile 4 -5 meter', 'Damara Dik-dik', 'Duiker, Blue', 'Duiker, Grey', 'Giraffe', 'Eland', 'Elephant', 'Fallow Deer', 'Gemsbock/Oryx', 'Grysbock', 'Red Hartebeest', 'Hippo','Hyena', 'Impala', 'Jackall', 'Klipspringer', 'Kudu', 'Red Lechwe', 'Leopard', 'Lion Male', 'Lioness', 'Caracal/Lynx','Nyala', 'Oribi', 'Ostrich', 'Porcupine', 'Reedbuck, Common', 'Reedbuck, Mountain', 'Rhebock, vaal', 'Rhino', 'Roan', 'Sable', 'Springbock, common', 'Springbock, golden', 'Springbock, white', 'Steenbock', 'Tsessebe', 'Warthog', 'Waterbuck', 'Wildebeest, Black' 'Wildebeest, Blue', "Zebra, Hartmann's", 'Zebra, Burchell')
    spicies = ['Baboon', 'Barbersheep', 'Blesbuck Common', 'Blesbuck White', 'Bontebuck', 'Buffalo', 'Bushbuck', 'Bushpig', 'Caracal/lynx', 'Cheetah', 'Civet Cat', 'Crocodile 2,5 -3 Meter', 'Crocodile < 2,5 Meter', 'Crocodile 3-4 Meter', 'Crocodile 4 -5 Meter', 'Damara Dik-dik', 'Duiker Blue', 'Duiker Grey', 'Eland', 'Elephant', 'Fallow Deer', 'Gemsbuck/oryx', 'Giraffe', 'Grysbuck', 'Hippo', 'Hyena Brown', 'Hyena Spotted', 'Impala', 'Jackal (sjakal)', 'Klipspringer', 'Kudu', 'Leopard', 'Lioness', 'Lion Male', 'Nyala', 'Oribi', 'Ostrich', 'Porcupine', 'Red Hartebeest', 'Red Lechwe', 'Reedbuck Common', 'Reedbuck Mountain', 'Rhebuck Vaal', 'Rhino', 'Roan', 'Sable','Springbuck Black', 'Springbuck Common', 'Springbuck Golden', 'Springbuck White', 'Steenbuck', 'Tsessebe', 'Warthog', 'Waterbuck', 'Wildebeest Black', 'Wildebeest Blue', "Zebra Burchell's", "Zebra Hartmann's"]
    #taxidermy = ('Shoulder Mounts', 'Pedestal Mounts', 'Full Mounts', 'EU Mounts', 'Tanning of Full Skin', 'Tanning Full Skin with Felt', 'Tanning of Back Skins', 'Tanning Back Skin with Felt', 'Rug mounts', 'Taxidermi full Skin', 'Taxidermi back skin')
    taxidermy = ('Shoulder Mounts', 'Full Mounts', 'EU Mounts', 'Tanning of Full Skin', 'Tanning Full Skin with Felt', 'Tanning of Back Skins', 'Tanning Back Skin with Felt', 'Rug mounts')
    entriessm   = []
    entriesped  = []
    entriesfm   = []
    entriesem = []
    entriestfs   = []
    entriestfswf   = []
    entriestbs = []
    entriestbswf = []
    entriesrm = []
    print len(taxidermy)
    print len(spicies)
    if form.validate_on_submit():
        print form.remember_me.data
        print form.openid.data
        print form.email.data
        ids = form.remember_me.data
        email = form.email.data
        #product_selected(p_id, country_id)
        #product_selected(1086, 167)
        print_email(1304, email)
        return redirect('/thanks')

    for product_info in products_info:
        all_inc_ids = product_info['all_inclusive_ids']
        all_inc = models.execute(dbname, uid, pwd,
            'product.all.inclusive', 'read', all_inc_ids, [])
        
        for all_inc_id in all_inc:
            #d = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
            if 'Shoulder Mount' in product_info['name']: #if all_inc_id['country_id'][0] == 258:
                #print entriessm#product_info['name']
                #dsm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
                entriessm.append(dict(idsm=product_info['id'], namesm=product_info['name'], pricesm=all_inc_id['tot'], countrysm=all_inc_id['country_id']))
            if 'Pedestal Mount' in product_info['name']: #if all_inc_id['country_id'][0] == 258:
                #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
                entriesped.append(dict(idped=product_info['id'], nameped=product_info['name'], priceped=all_inc_id['tot'], countryped=all_inc_id['country_id']))
            if 'Full Mount' in product_info['name']:
                #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
                entriesfm.append(dict(idfm=product_info['id'], namefm=product_info['name'], pricefm=all_inc_id['tot'], countryfm=all_inc_id['country_id']))
            if 'European Mount' in product_info['name']:
                #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
                entriesem.append(dict(idem=product_info['id'], nameem=product_info['name'], priceem=all_inc_id['tot'], countryem=all_inc_id['country_id']))
            if 'Tanning of Full Skin' in product_info['name']:
                #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
                entriestfs.append(dict(idtfs=product_info['id'], nametfs=product_info['name'], pricetfs=all_inc_id['tot'], countrytfs=all_inc_id['country_id']))    
            if 'Tanning Full Skin with Felt' in product_info['name']:
                #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
                entriestfswf.append(dict(idtfswf=product_info['id'], nametfswf=product_info['name'], pricetfswf=all_inc_id['tot'], countrytfswf=all_inc_id['country_id']))    
            if 'Tanning of Back Skin' in product_info['name']:
                #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
                entriestbs.append(dict(idtbs=product_info['id'], nametbs=product_info['name'], pricetbs=all_inc_id['tot'], countrytbs=all_inc_id['country_id']))    
            if 'Tanning Back Skin with Felt' in product_info['name']:
                #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
                entriestbswf.append(dict(idtbswf=product_info['id'], nametbswf=product_info['name'], pricetbswf=all_inc_id['tot'], countrytbswf=all_inc_id['country_id']))    
            if 'Rugmount' in product_info['name']:
                #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
                entriesrm.append(dict(idrm=product_info['id'], namerm=product_info['name'], pricerm=all_inc_id['tot'], countryrm=all_inc_id['country_id']))    
            
                #entriesped.append(all_inc_id['tot'])      
            #print all_inc_id['id'], all_inc_id['name'], all_inc_id['taxidermy_work'], all_inc_id['export_papers'], all_inc_id['eu_shipping'], all_inc_id['packing_crating'], all_inc_id['insurance'], all_inc_id['vat'], all_inc_id['tot'], all_inc_id['country_id'] 

    return render_template('show_entries.html', country_id=country_id, form=form, title='Calculator', entriessm=entriessm, entriesped=entriesped, entriesfm=entriesfm, entriesem=entriesem, entriestfs=entriestfs, entriestfswf=entriestfswf, entriestbs=entriestbs, entriestbswf=entriestbswf, entriesrm=entriesrm, spicies=spicies, taxidermy=taxidermy)#, country_id=country_id)

@app.route('/', methods=['GET', 'POST'])
def show_welcome():

    form = CountryForm()

    if form.validate_on_submit():
        print form.country_id.data
        country_id = int(form.country_id.data)
        #show_entries(country_id)
        return redirect(url_for('show_entries', country_id=country_id))
        #return redirect('/')

    return render_template('show_welcome.html', form=form)


@app.route('/countries')
def show_countries():

    #Products
    ids = models.execute(dbname, uid, pwd,
        'product.product', 'search',
        (['is_all_inclusive', '=', True], ['is_displayed_on_website', '=', True]))  #[('is_all_inclusive', '=', True)])

    products_info = models.execute(dbname, uid, pwd,
            'product.product', 'read', ids, ['id', 'name', 'all_inclusive_ids'])

    spicies = ['Baboon', 'Barbersheep', 'Blesbuck Common', 'Blesbuck White', 'Bontebuck', 'Buffalo', 'Bushbuck', 'Bushpig', 'Caracal/lynx', 'Cheetah', 'Civet Cat', 'Crocodile 2,5 -3 Meter', 'Crocodile < 2,5 Meter', 'Crocodile 3-4 Meter', 'Crocodile 4 -5 Meter', 'Damara Dik-dik', 'Duiker Blue', 'Duiker Grey', 'Eland', 'Elephant', 'Fallow Deer', 'Gemsbuck/oryx', 'Giraffe', 'Grysbuck', 'Hippo', 'Hyena Brown', 'Hyena Spotted', 'Impala', 'Jackal (sjakal)', 'Klipspringer', 'Kudu', 'Leopard', 'Lioness', 'Lion Male', 'Nyala', 'Oribi', 'Ostrich', 'Porcupine', 'Red Hartebeest', 'Red Lechwe', 'Reedbuck Common', 'Reedbuck Mountain', 'Rhebuck Vaal', 'Rhino', 'Roan', 'Sable', 'Springbuck Black', 'Springbuck Common', 'Springbuck Golden', 'Springbuck White', 'Steenbuck', 'Tsessebe', 'Warthog', 'Waterbuck', 'Wildebeest Black', 'Wildebeest Blue', "Zebra Burchell's", "Zebra Hartmann's"]
    taxidermy = ('Shoulder Mounts', 'Pedestal Mounts', 'Full Mounts', 'EU Mounts', 'Tanning Full Skin', 'Tanning of Full Skin with Felt', 'Tanning of Back Skins', 'Tanning of Back Skin with Felt', 'Rug mounts', 'Taxidermi full Skin', 'Taxidermi back skin')
    
    countries   = []
    entries   = []

    for product_info in products_info:
        all_inc_ids = product_info['all_inclusive_ids']
        all_inc = models.execute(dbname, uid, pwd,
            'product.all.inclusive', 'read', all_inc_ids, [])
        
        for all_inc_id in all_inc:
            d = dict(country=all_inc_id['country_id'])
            countries.append(d)  
            if 'Rugmount' in product_info['name']:
                #dfm = dict(id=product_info['id'], name=product_info['name'], price=all_inc_id['tot'], inc=all_inc_id['country_id'])
                entries.append(dict(name=product_info['name'], price=all_inc_id['tot'], country=all_inc_id['country_id']))    

    #print countries
    return render_template('show_countries.html', spicies=spicies, taxidermy=taxidermy, entries=entries)

@app.route('/thanks')
def show_thanks():
    return render_template('show_thanks.html', title='Calculator')'''

