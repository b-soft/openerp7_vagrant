Installing OpenERP on debian based OS using debs 

1. First, save your original sources.list file. 
 sudo cp /etc/apt/sources.list /etc/apt/sources.list.orig

2. Enter root mode.
 sudo su

3. Add the openerp/odoo repo.
 echo "deb http://nightly.odoo.com/7.0/nightly/deb/ ./" >> /etc/apt/sources.list.d/openerp.sources.list

4. Make apt aware of the new software repositories by issuing the following command: 
 sudo apt-get update
   If having Key registration issues, do:
	sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys KEY_No
        sudo apt-get updat

5. Install  openerp.
       sudo apt-get install openerp
In certain instances a 'openerp' postgres user is not created and the server returns errors upon install.
In such occasions create a postgres user with the following commands.

6. Switch to the postgres user.
       sudo su postgres

7. Create openerp user for postgres .
       createuser openerp

8. If can not craete a DB, do:
	sudo su postgres
	psql
	ALTER ROLE odoo CREATEROLE CREATEDB
