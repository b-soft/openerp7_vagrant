./createvm.sh vmname sshport odooport

see createvm.sh  and OpenERPDebianInstall7.md/8 bellow or as file:

cd ~/VirtualBox\ VMs/

VM_NAME="$1"
UBUNTU_ISO_PATH=~/Downloads/ubuntu-14.04.1-server-amd64.iso
VM_HD_PATH="$1.vdi" 
SHARED_PATH=~ 


vboxmanage createvm --name $VM_NAME --ostype Ubuntu_64 --register
vboxmanage createhd --filename $VM_NAME.vdi --size 32768
vboxmanage storagectl $VM_NAME --name "SATA Controller" --add sata --controller IntelAHCI
vboxmanage storageattach $VM_NAME --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium $VM_HD_PATH
vboxmanage storagectl $VM_NAME --name "IDE Controller" --add ide
vboxmanage storageattach $VM_NAME --storagectl "IDE Controller" --port 0 --device 0 --type dvddrive --medium $UBUNTU_ISO_PATH
vboxmanage modifyvm $VM_NAME --ioapic on
vboxmanage modifyvm $VM_NAME --memory 1024 --vram 128
vboxmanage modifyvm $VM_NAME --nic1 nat
vboxmanage modifyvm $VM_NAME --natpf1 "ssh,tcp,,$2,,22"
vboxmanage modifyvm $VM_NAME --natpf1 "tcp8069,tcp,,$3,,8069"
vboxmanage modifyvm $VM_NAME --natdnshostresolver1 on
vboxmanage sharedfolder add $VM_NAME --name shared --hostpath $SHARED_PATH --automount


start the vm:
vboxmanage startvm UbuntuServer

install the OpenSSH Server:
sudo apt-get update
sudo apt-get install -y openssh-server

Now you can try logging into your virtual machine over ssh through port sshport, which has been set up to forward to port 22 of your VM:
ssh -p sshport <username>@localhost

Then install openerp/odoo in this virtual machine using:

Installing OpenERP/odoo on debian based OS using debs 

1. First, save your original sources.list file. 
   sudo cp /etc/apt/sources.list /etc/apt/sources.list.orig

2. Enter root mode.
   sudo su

3. Add the openerp repo.
   echo "deb http://nightly.odoo.com/8.0/nightly/deb/ ./"  >> /etc/apt/sources.list.d/openerp.sources.list

4. Make apt aware of the new software repositories by issuing the following command: 
   sudo apt-get update
   If having Key registration issues, do:
	sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys KEY_No
        sudo apt-get update

5. Install  openerp.
   sudo apt-get install openerp
   In certain instances a 'openerp/odoo' postgres user is not created and the server returns errors upon install.
   In such occasions create a postgres user with the following commands.

6. Switch to the postgres user.
   sudo su postgres

7. Create openerp/odoo user for postgres .
   createuser openerp/odoo

8. If can not craete a DB, do:
    sudo su postgres
    psql
    ALTER ROLE openerp/odoo CREATEROLE CREATEDB

Now you can browse your openerp/odoo over internet through port odooport, which has been set up to forward to port 8069 of your VM:
IP:odooport

