Implemented in /etc/apache2/sites-enabled/000-default from the host!

# default domain website

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName www.openerp.co.za

        DocumentRoot /var/www
        <Directory />
                Options FollowSymLinks
                AllowOverride None
        </Directory>
        <Directory /var/www/>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride AuthConfig
                Order allow,deny
                allow from all
        </Directory>

        ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
        <Directory "/usr/lib/cgi-bin">
                AllowOverride None
                Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
                Order allow,deny
                Allow from all
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/error.log

        # Possible values include: debug, info, notice, warn, error, crit,
        # alert, emerg.
        LogLevel warn

        CustomLog ${APACHE_LOG_DIR}/access.log combined

    Alias /doc/ "/usr/share/doc/"
    <Directory "/usr/share/doc/">
        Options Indexes MultiViews FollowSymLinks
        AllowOverride None
        Order deny,allow
        Deny from all
        Allow from 127.0.0.0/255.0.0.0 ::1/128
    </Directory>

</VirtualHost>
###############################
#charterconnect portal stuff

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName charterconnect.openerp.co.za

        ProxyPass / http://localhost:1245/
        ProxyPassReverse / http://localhost:1245/
        ProxyPreserveHost On
</VirtualHost>


<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName charterconnect.co.za

        ProxyPass / http://localhost:1245/
        ProxyPassReverse / http://localhost:1245/
        ProxyPreserveHost On
</VirtualHost>


<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName www.charterconnect.co.za

        ProxyPass / http://localhost:1245/
        ProxyPassReverse / http://localhost:1245/
        ProxyPreserveHost On
</VirtualHost>


###############################
#openerp 7 testing domains


<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName test.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>




<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName internal.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>




<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName fleet.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
	ProxyPreserveHost On
</VirtualHost>


<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName demo001.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>



<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName nsw1dev.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>


<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName akspicedev.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName demo1.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName demo2.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName DPS1dev.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName DPSTEST73dev.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtoualHst>

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName cqdev.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtoualHst>

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName newdb.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>




<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName meditechdev.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName mlgdev.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName zapopdev.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName dps7dev.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>

<VirtualHost *:80>
        ServerAdmin webmaster@localhost

        ServerName manufacturingdev.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName datasmithdev.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName lwhdev.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>

<VirtualHost *:80>

        ServerAdmin webmaster@localhost
        ServerName toufeeq.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName sbudev.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName sbu-smmedev.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>


<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName aasdev.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName firsttrophydev.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName allentest.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName topologicaldev.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName skyfidev.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName csjk9dev.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On

</VirtualHost>

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName sbu-growdev.openerp.co.za

        ProxyPass / http://localhost:4445/
        ProxyPassReverse / http://localhost:4445/
        ProxyPreserveHost On
</VirtualHost>

NB: under this vm (
###############################
#openerp 7 testing domains), There are 30 subdomaines listening to the same port 4445 which has been set up to forward to port 8069 of your VM.
This means that 30 instances (dbs) are running in this VM.
