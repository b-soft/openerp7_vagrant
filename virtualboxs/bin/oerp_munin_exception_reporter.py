#!/usr/bin/python

import time
from collections import deque
import re
import smtplib
import fcntl

curtime = time.mktime(time.gmtime())
reopen_expiry = 60 * 30
counts_expiry = 60 * 4
idlewaittime = 30
logfile = '/var/log/odoo/odoo-server.log'
email_to = ['erpwebza@gmail.com', 'erik@erpweb.co.za', 'christo@erpweb.co.za']
httpcounts = {}
httpsize = {}
httptime = {}
countsfile = "/var/tmp/oerpcounts"

MAILSERVER = 'localhost'
MAILFROM = 'openerp@openerpVB'

def report_exception(comp, text):
    message = "From: {}\r\nTo: {}\r\nSubject: Odoo exception {}\r\n".format(MAILFROM, ', '.join(email_to), comp)
    message += '\n'.join(text)

    server = smtplib.SMTP(MAILSERVER)
    server.sendmail(MAILFROM, email_to, message)
    server.quit() 

def add_count(db):
    if db + '_count' in httpcounts:
        httpcounts[db + '_count'] += 1
    else:
        httpcounts[db + '_count'] = 1

def add_size(db, size):
    if db + '_size_count' in httpsize:
        httpsize[db + '_size_count'] += 1
        httpsize[db + '_size_size'] += int(size)
    else:
        httpsize[db + '_size_count'] = 1
        httpsize[db + '_size_size'] = int(size)

def add_time(db, time):
    if db + '_time_count'  in httptime:
        httptime[db + '_time_count'] += 1
        httptime[db + '_time_time'] += int(time)
    else:
        httptime[db + '_time_count'] = 1
        httptime[db + '_time_time'] = int(time)

def write_counts():
    f = ''
    try:
        f = open(countsfile, "r+")
        fcntl.flock(f, fcntl.LOCK_EX|fcntl.LOCK_NB)
    except:
        if f:
            f.close()
        return

    lines = f.readlines()
    for l in lines:
        l.strip()
        elems = l.split(':')

        if elems[0] in httpcounts:
            httpcounts[elems[0]] += int(elems[1])
        elif elems[0] in httpsize:
            httpsize[elems[0]] += int(elems[1])
        elif elems[0] in httptime:
            httptime[elems[0]] += int(elems[1])

    f.truncate(0)
    f.seek(0, 0)

    for k, v in httpcounts.items():
        f.write("{}:{}\n".format(k,v))
        httpcounts[k] = 0

    for k, v in httpsize.items():
        f.write("{}:{}\n".format(k,v))
        httpsize[k] = 0

    for k, v in httptime.items():
        f.write("{}:{}\n".format(k,v))
        httptime[k] = 0

    fcntl.flock(f, fcntl.LOCK_UN)
    f.close()

def watch_log(log):
    prelines = deque()
    start_regex = re.compile(r'Traceback')
    end_regex = re.compile(r'[\d]{4}-[\d]{2}-[\d]{2} [\d]{2}:[\d]{2}:[\d]{2},[\d]+ [\d]+ [\w]+ ([\w]+)')
    counts_regex = re.compile(r'[\d]{4}-[\d]{2}-[\d]{2} [\d]{2}:[\d]{2}:[\d]{2},[\d]+ [\d]+ [\w]+ ([\w]+).*(POST|GET).*HTTP')
    size_regex = re.compile(r'[\d]{4}-[\d]{2}-[\d]{2} [\d]{2}:[\d]{2}:[\d]{2},[\d]+ [\d]+ [\w]+ ([\w]+).*type:.*size: ([0-9]+)')
    time_regex = re.compile(r'[\d]{4}-[\d]{2}-[\d]{2} [\d]{2}:[\d]{2}:[\d]{2},[\d]+ [\d]+ [\w]+ ([\w]+).*PATH:.*time: ([0-9]+)ms')

    with open(log) as f:
        f.seek(0,2)
        curtime = time.mktime(time.gmtime())
        lastcount = 0

        while 1:
            if lastcount < (time.mktime(time.gmtime()) - counts_expiry):
                write_counts()
                lastcount = time.mktime(time.gmtime())
            l = f.readline()
            if not l:
                if curtime < (time.mktime(time.gmtime()) - reopen_expiry):
                    return
                time.sleep(idlewaittime)
            else:
                if start_regex.match(l):
                    exp_comp = ''
                    exp = []
                    time.sleep(1)

                    exp.append("============================")
                    for e in reversed(prelines):
                        m = end_regex.match(e)
                        if (not exp_comp) and m:
                            exp_comp = m.groups()[0]
                        exp.insert(1,e)
                    prelines.clear() 
                    while l and (not end_regex.match(l)):
                        exp.append(l)
                        l = f.readline()
                    exp.append("============================")
                    report_exception(exp_comp, exp)
                    if l:
                        m = counts_regex.match(l)
                        m1 = size_regex.match(l)
                        m2 = time_regex.match(l)
                        if m:
                            add_count(m.groups()[0])
                        elif m1:
                            add_size(m1.groups()[0], m1.groups()[1])
                        elif m2:
                            add_time(m2.groups()[0], m2.groups()[1])

                        prelines.append(l)
                else:
                    m = counts_regex.match(l)
                    m1 = size_regex.match(l)
                    m2 = time_regex.match(l)
                    if m:
                        add_count(m.groups()[0])
                    elif m1:
                        add_size(m1.groups()[0], m1.groups()[1])
                    elif m2:
                        add_time(m2.groups()[0], m2.groups()[1])

                    if len(prelines) > 10:
                        prelines.popleft()
                        prelines.append(l)
                    else:
                        prelines.append(l)
                curtime = time.mktime(time.gmtime())

def main():
    while 1:
        print "watching: " + logfile
        watch_log(logfile)

if __name__ == '__main__':
    main()
