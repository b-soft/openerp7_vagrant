NB: If these directory /ect/postfix doesn't exist, install postfix using: sudo apt-get install postfix postfix-mysql (http://flurdy.com/docs/postfix/)

Full ref: (See ErpWeb Setup.pdf in setup server/setup)

cd /usr/local/bin/
	
		Then create an executable file "africahrsolutions" containing:
			#!/bin/bash

			cat | /usr/local/bin/openerp_mailgate.py -u 1 -p HSUdESnnwTdTawCNgMcs -d africahrsolutions --host localhost --port 8069
			where HSUdESnnwTdTawCNgMcs is the database admin password and africahrsolutions is the database's name
cd /etc/postfix/

	sudo nano main.cf
		smtpd_relay_restrictions = permit_mynetworks permit_sasl_authenticated defer_unauth_destination
		myhostname = odoo.zone
		alias_maps = hash:/etc/aliases
		alias_database = hash:/etc/aliases
		myorigin = /etc/mailname
		mydestination = odoo.zone, localhost.zone, , localhost
		relayhost =
		mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128
		mailbox_size_limit = 51200000
		recipient_delimiter = +
		inet_interfaces = all
		inet_protocols = all
		home_mailbox = Maildir/
		smtpd_sasl_type = dovecot
		smtpd_sasl_path = private/auth
		smtpd_sasl_local_domain =
		smtpd_sasl_security_options = noanonymous
		broken_sasl_auth_clients = yes
		smtpd_sasl_auth_enable = no
		smtpd_recipient_restrictions = permit_sasl_authenticated,permit_mynetworks,reject_unauth_destination
		smtp_tls_security_level = may
		smtpd_tls_security_level = may
		smtp_tls_note_starttls_offer = yes
		smtpd_tls_loglevel = 1
		smtpd_tls_received_header = yes

		# TLS parameters
		smtpd_tls_cert_file = /etc/ssl/certs/server.crt
		smtpd_tls_key_file = /etc/ssl/private/server.key
		smtpd_use_tls=yes

		#add in virtual mailbox domains this server accepts mail for, to pass to odoo in our case
		virtual_mailbox_domains = internal.odoo.zone,africahrsolutions.odoo.zone

		#map virtual domain to transports postfix will use to deliver emails for domain onto
		transport_maps = hash:/etc/postfix/transport
		################################OR##Fully simplified###############
		# See /usr/share/postfix/main.cf.dist for a commented, more complete version


		# Debian specific:  Specifying a file name will cause the first
		# line of that file to be used as the name.  The Debian default
		# is /etc/mailname.
		#myorigin = /etc/mailname

		smtpd_banner = $myhostname ESMTP $mail_name (Ubuntu)
		biff = no

		# appending .domain is the MUA's job.
		append_dot_mydomain = no

		# Uncomment the next line to generate "delayed mail" warnings
		#delay_warning_time = 4h

		readme_directory = no

		# TLS parameters
		smtpd_tls_cert_file=/etc/ssl/certs/ssl-cert-snakeoil.pem
		smtpd_tls_key_file=/etc/ssl/private/ssl-cert-snakeoil.key
		smtpd_use_tls=yes
		smtpd_tls_session_cache_database = btree:${data_directory}/smtpd_scache
		smtp_tls_session_cache_database = btree:${data_directory}/smtp_scache

		# See /usr/share/doc/postfix/TLS_README.gz in the postfix-doc package for
		# information on enabling SSL in the smtp client.

		myhostname = erpwebsolutions.openerp.co.za
		alias_maps = hash:/etc/aliases
		alias_database = hash:/etc/aliases
		myorigin = /etc/mailname
		mydestination = localhost, openerp.co.za, erpwebsolutions.openerp.co.za, erpwebsolutions
		relayhost =
		mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128
		mailbox_size_limit = 51200000
		recipient_delimiter = +
		inet_interfaces = all
		inet_protocols = all

		virtual_mailbox_domains = skysite.openerp.co.za,mlg.openerp.co.za,
		adoptaschool.openerp.co.za,sbu-grow.openerp.co.za,topological.openerp.co.za,topologicaldev.openerp.co.za,internal.openerp.co.za,skyfi.op$
		transport_maps = hash:/etc/postfix/transport


	sudo nano transport	replace sasl_password with transport"
		# do a: postmap transport, if you change stuff in here

		internal.odoo.zone odoo-internal
		africahrsolutions.odoo.zone africahrsolutions
		#####################OR##Fully simplified#############
		skysite.openerp.co.za openerp-skysite
		mlg.openerp.co.za openerp-mlg
		adoptaschool.openerp.co.za openerp-adoptaschool
		sbu-grow.openerp.co.za openerp-sbu-grow
		topological.openerp.co.za openerp-topological
		topologicaldev.openerp.co.za openerp-topologicaldev
		internal.openerp.co.za openerp-internal
		skyfi.openerp.co.za openerp-skyfi
		allentest.openerp.co.za openerp-allentest
		firsttrophy.openerp.co.za openerp-firsttrophy
		firsttrophydev.openerp.co.za openerp-firsttrophydev


	sudo nano master.cf
		#how postfix will deliver mail for odoo domains using scripts in /usr/local/bin to pipe incoming mail to
		odoo-internal    unix  -       n       n       -       -       pipe
		  user=erpwevpy argv=/usr/local/bin/odoo-internal
		africahrsolutions unix -        n       n       -       -       pipe
		 user=erpwevpy argv=/usr/local/bin/africahrsolutions
		#################OR####Fully Simplified#############################
		africahrsolutions unix - ... = can be any name
		user=erpwevpy argv=/usr/local/bin/africahrsolutions = name of the executable filedefined in /usr/local/bin 
		
	


sudo postmap /etc/postfix/transport
sudo service postfix restart
