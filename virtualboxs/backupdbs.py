#!/usr/bin/env python

import os
import sys
from datetime import datetime
import re
import glob

dbs = [
    'skysite',
    'mlg',
    'endemol',
    'sbu',
    'sbu-smme',
    'charterquest',
    'adoptaschool',
    'topological',
        'BravoInsight', # From here are the new entries added to be backed up. Previously only backed up everything above this comment.
        'Skyfi',
        'firsttrophy',
        'ng',
        'skyfi',
        'skysite-offshore',
        'template0',  # Not sure what these two are but lets back them up anyway.
        'template1'
]
pgdump = '/usr/bin/pg_dump'
backupdir = '/home/backups/dbs/'
cust_mod_path = '/opt/custom_modules'

keepdays = 7

def get_files(dir):
    if not dir.endswith('/'):
        dir += '/'
    files = glob.glob(dir + '*dump*')
    return files

def run_rotation(files):
    datestr = datetime.strftime(datetime.now(),"%Y%m%d")
    for f in files:
        m = re.search('([0-9]{8})-[0-9]{4}',f)
        if m:
            filestr =  m.groups()[0]
            if int(filestr) < (int(datestr) - keepdays) and filestr[-1] != '1':
                os.unlink(f)
        else:
            sys.stderr.write("ignoring file not in expected format: " + f)

def main():
    datestr = datetime.strftime(datetime.now(),"%Y%m%d-%H%M")
    print "Running db backups.."
    for db in dbs:
        backfile = backupdir + db + '.dump.' + datestr
        os.system('su postgres -c "' + pgdump + ' -Fc ' + db + ' >' + backfile + '"')
        print "Backed up db {} to file {}".format(db,backfile)
    files = get_files(backupdir)
    run_rotation(files)
    addons_cmd = 'tar -czf ' + backupdir + 'custmods.dump.' + datestr + ' ' + cust_mod_path
    os.system(addons_cmd)
    print "Rsync crons will sync this down to the office"


if __name__ == '__main__':
    main()



