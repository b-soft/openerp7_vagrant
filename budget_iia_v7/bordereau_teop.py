from datetime import datetime
from dateutil.relativedelta import relativedelta
import time
from operator import itemgetter
from itertools import groupby

from openerp.osv import fields, osv, orm
from openerp.tools.translate import _
from openerp import netsvc
from openerp import tools
from openerp.tools import float_compare, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
import logging
_logger = logging.getLogger(__name__)

#----------------------------------------------------------
# Bordereau
#----------------------------------------------------------
class bordereau_teop(osv.osv):
    _name = "bordereau.teop"
    _inherit = ['mail.thread']
    _description = "Bordereau"

    def _get_total(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for obj in self.browse(cr, uid, ids):
            val = 0
            if obj.bordereau_teop_lines_ids:
                val = sum(line_id.montant for line_id in obj.bordereau_teop_lines_ids)
            _logger.info('-LIQUID=------{0}------'.format(val))
            res[obj.id] = val
        return res

    def _get_amount_to_text_fr(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for obj in self.browse(cr, uid, ids):
            #if obj.amount_total:
            val = obj.total
            val = tools.amount_to_text_fr(val, 'fcfa')
            res[obj.id] = val.upper()
        return res
            #else:
                #raise osv.except_osv(_('Attention!'), _('Montant Total Non Existant!'))

    def bordereau_create(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        bord_line_obj = self.pool.get('bordereau.teop.lines') 
        acc_inv_obj = self.pool.get('account.invoice')
        for obj in self.browse(cr, uid, ids):

            if obj.bordereau_teop_lines_ids: 
                for line in obj.bordereau_teop_lines_ids:
                    bord_line_obj.unlink(cr, uid, line.id, context=context)

            acc_ids = acc_inv_obj.search(cr, uid, [('state', 'in', ('open', 'paid'))]) 
            for acc_obj in acc_inv_obj.browse(cr, uid, acc_ids):
                if acc_obj.invoice_line:
                    val = acc_obj.invoice_line[0]
                vals = {    
                    'name': acc_obj.partner_id.name,
                    'te': acc_obj.number,
                    'op': acc_obj.number,
                    'aff_bud': val.account_analytic_id.name,
                    'x_objet_depense': acc_obj.x_objet_depense,
                    'montant': acc_obj.amount_total,
                    'state': acc_obj.state,
                    'bordereau_teop_id': obj.id,

                }
                bord_line_obj.create(cr, uid, vals, context=context)

        return True

    _columns = {
        'name': fields.char('Bordereau'),
        'total': fields.function(_get_total, type='integer', string='Total'),
        'amount_to_text_fr': fields.function(_get_amount_to_text_fr, type='char', string=''),
        'bordereau_teop_lines_ids': fields.one2many('bordereau.teop.lines', 'bordereau_teop_id', 'Lignes du Bordereau'),
    }

    _defaults = {
        'name': 'Bordereau'
    }
bordereau_teop()
