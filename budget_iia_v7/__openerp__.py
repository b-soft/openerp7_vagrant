{
    "name": "IIA Budget",
    "version": "1.0",
    "depends": ["account", "account_budget", "mail"],
    "author": "Gutembert Nganpet Nzeugaing <nganpet2007@gmail.com>",
    "category": "Custom",
    "description": """ """,
    "update_xml": [
		"view/account_invoice.xml",
        "view/account_analytic_account.xml",
        "view/bordereau_teop.xml",
        "menu/menu.xml",
        "view/crossovered_budget_lines.xml",
	],
    "test": [],
    "installable": True,
    "active": False
}

