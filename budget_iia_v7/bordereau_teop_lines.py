from datetime import datetime
from dateutil.relativedelta import relativedelta
import time
from operator import itemgetter
from itertools import groupby

from openerp.osv import fields, osv, orm
from openerp.tools.translate import _
from openerp import netsvc
from openerp import tools
from openerp.tools import float_compare, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
import logging
_logger = logging.getLogger(__name__)

#----------------------------------------------------------
# Lignes du Bordereau
#----------------------------------------------------------
class bordereau_teop_lines(osv.osv):
    _name = "bordereau.teop.lines"
    _description = "Ligne des Bordereaux"
    _columns = {
        'name': fields.char('Beneficiaire', size=64),
        'te': fields.char('No T. E.'),
        'op': fields.char('No O. P.'),
        'aff_bud': fields.char('Affectation Budgetaire'),
        'x_objet_depense': fields.char('Objet de Depense'),
        'montant': fields.integer('Montant'),
        'state': fields.selection([
            ('draft','Draft'),
            ('proforma','Pro-forma'),
            ('proforma2','Pro-forma'),
            ('open','Ouverte'),
            ('paid','Payer'),
            ('cancel','Cancelled'),
            ],'Status', select=True, readonly=True, track_visibility='onchange'),

        'bordereau_teop_id': fields.many2one('bordereau.teop', 'Bordereau'),
    }
bordereau_teop_lines()
