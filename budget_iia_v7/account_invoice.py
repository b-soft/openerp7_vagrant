from openerp import tools
from openerp.osv import osv, fields
from openerp.tools.translate import _
from openerp.tools import email_re
from openerp import SUPERUSER_ID

import openerp.addons.decimal_precision as dp
import logging

import base64
from PIL import Image
from io import BytesIO
import StringIO
import datetime

_logger = logging.getLogger(__name__)


class account_invoice(osv.osv):
    _inherit = 'account.invoice'

    def _get_dotation(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for obj in self.browse(cr, uid, ids):
            if obj.invoice_line:
                for line in obj.invoice_line:
                    val = line.account_analytic_id.dotation
                    res[obj.id] = val
                    return res
            else:
                raise osv.except_osv(_('Attention!'), _('Budget pas encore existant!'))

    def _get_engagement(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for obj in self.browse(cr, uid, ids):
            if obj.invoice_line:
                for line in obj.invoice_line:
                    val = line.account_analytic_id.engagement #+ int(obj.amount_total)
                    res[obj.id] = val
                    return res
            else:
                raise osv.except_osv(_('Attention!'), _('Budget pas encore existant!'))

    def _get_dispo(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for obj in self.browse(cr, uid, ids):
            if obj.invoice_line:
                for line in obj.invoice_line:
                    val = line.account_analytic_id.dispo
                    res[obj.id] = val
                    return res
            else:
                raise osv.except_osv(_('Attention!'), _('Budget pas encore existant!'))

    def _get_liquid(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for obj in self.browse(cr, uid, ids):
            if obj.invoice_line:
                for line in obj.invoice_line:
                    val = line.account_analytic_id.liquid
                    res[obj.id] = val
                    return res
            else:
                raise osv.except_osv(_('Attention!'), _('Budget pas encore existant!'))

    def _get_amount_to_text_fr(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for obj in self.browse(cr, uid, ids):
            val = obj.amount_total
            val = tools.amount_to_text_fr(val, 'fcfa')
            res[obj.id] = val.upper()
        return res

    def _return_year(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        fmt = "%Y-%m-%d" 
        for obj in self.browse(cr, uid, ids):
            try:
                y = obj.date_invoice  
                y = datetime.datetime.strptime(y, fmt)
                y = y.year
                result[obj.id] = y
            except:
                pass
            return result

    '''def _get_chapter(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for obj in self.browse(cr, uid, ids):
            val = obj.analytic_id.
            val = tools.amount_to_text_fr(val, 'fcfa')
            res[obj.id] = val.upper()
        return resaccount.analytic.account'''

    def create(self, cr, uid, values, context=None):  

        id = super(account_invoice,self).create(cr, uid, values, context)
        invoice = self.browse(cr, uid, id, context)
        vals = {
                'account_analytic_id'   : invoice.analytic_id.id,
                #'general_budget_id'     : invoice.general_budget_id.id,
            }

        invoice_line_obj = self.pool.get( 'account.invoice.line' )
        search_ids = invoice_line_obj.search ( cr, uid, [('invoice_id', '=', invoice.id)] )
        invoice_line_obj.write(cr, uid, search_ids, vals, context=context)

        return id

    def write(self, cr, uid, ids, values, context = None):

        result = super(account_invoice,self).write(cr, uid, ids, values, context)
        
        for line_id in ids:
            line = self.browse( cr, uid, line_id, context )

            vals = {
                'account_analytic_id'   : values.get( 'analytic_id'     , line.analytic_id.id),
                #'general_budget_id'     : values.get( 'general_budget_id'     , line.general_budget_id.id),
            }

            invoice_line_obj = self.pool.get( 'account.invoice.line' )
            search_ids = invoice_line_obj.search ( cr, uid, [('invoice_id', '=', line_id)] )
            invoice_line_obj.write(cr, uid, search_ids, vals, context=context)

        return result

    _columns = {
        'dotation': fields.function(_get_dotation, type='integer', string='Dotation'),
        'engagement': fields.function(_get_engagement, type='integer', string='Engagement'),
        'dispo': fields.function(_get_dispo, type='integer', string='Disponible'),
        'liquid': fields.function(_get_liquid, type='integer', string='Montant liquide'),
        'amount_to_text_fr_tr': fields.function(_get_amount_to_text_fr, type='char', string=''),
        'fisc_year': fields.function(_return_year, type='char', string='Annee Budgetaire1'),
        'fisc_year_id': fields.many2one('account.fiscalyear', "Annee Budgetaire", required=True),
        'fisc_year_name': fields.related('fisc_year_id','name', type='char', readonly=True, size=64, relation='account.fiscalyear', store=True, string='Year'),
        #'general_budget_id': fields.many2one('account.budget.post', "Chapitre"),
        'analytic_id': fields.many2one('account.analytic.account', required=True),
    }
account_invoice()

'''class account_invoice_line(osv.osv):
    _inherit = 'account.invoice.line'

    _columns = {
        'general_budget_id': fields.many2one('account.budget.post', "Chapitre"),
    }
account_invoice_line()'''


class account_analytic_account(osv.osv):
    _inherit = "account.analytic.account"

    def _get_dotation(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for obj in self.browse(cr, uid, ids):
            val = 0
            if obj.crossovered_budget_line:
                val = obj.crossovered_budget_line[0]
                _logger.info('__--3->>--{0}---{1}-----{2}____'.format(val.planned_amount, val.practical_amount, val.dispo))
                val = abs(val.planned_amount)
                res[obj.id] = val
            else:
                raise osv.except_osv(_('Attention!'), _('Budget pas encore existant!'))
        return res

    def _get_engagement(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for obj in self.browse(cr, uid, ids):
            if obj.crossovered_budget_line:
                for eng in obj.crossovered_budget_line:
                    val = eng.prac_amt2 #practical_amount
                    res[obj.id] = val
                    return res
            else:
                raise osv.except_osv(_('Attention!'), _('Budget pas encore existant!'))
        #return res

    def _get_dispo(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for obj in self.browse(cr, uid, ids):
            if obj.crossovered_budget_line:
                val = obj.crossovered_budget_line[0]
                val = val.dispo
                res[obj.id] = val
            else:
                raise osv.except_osv(_('Attention!'), _('Budget pas encore existant!'))
        return res

    def _get_liquid(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for obj in self.browse(cr, uid, ids):
            if obj.crossovered_budget_line:
                for liq in obj.crossovered_budget_line:
                    val = liq.liquid
                #val = obj.crossovered_budget_line[0]
                #val = val.liquid
                    res[obj.id] = val
                    return res
            else:
                raise osv.except_osv(_('Attention!'), _('Budget pas encore existant!'))
        #return res

    _columns = {
        'dotation': fields.function(_get_dotation, type='integer', string='Dotation'),
        'engagement': fields.function(_get_engagement, type='integer', string='Engagement'),
        'dispo': fields.function(_get_dispo, type='integer', string='Disponible'),
        'liquid': fields.function(_get_liquid, type='integer', string='Montant liquide'),
    }
account_analytic_account()


class crossovered_budget_lines(osv.osv):
    _inherit = "crossovered.budget.lines"

    def _get_dispo(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for obj in self.browse(cr, uid, ids):
            val = abs(obj.planned_amount) - obj.prac_amt2 #practical_amount
            res[obj.id] = val
        return res

    def _prac_amt1(self, cr, uid, ids, name, args, context=None):
        res = {}
        result = 0
        if context is None:
            context = {}
        for line in self.browse(cr, uid, ids, context=context):
            acc_ids = [x.id for x in line.general_budget_id.account_ids]
            if not acc_ids:
                raise osv.except_osv(_('Error!'),_("The Budget '%s' has no accounts!") % line.general_budget_id.name)
            date_to = line.date_to
            date_from = line.date_from
            if line.analytic_account_id.id:
                acc_anal_obj = self.pool.get('account.analytic.line')
                acc_anal_ids = acc_anal_obj.search(cr, uid, [('account_id', '=', line.analytic_account_id.id)])
                _logger.info('-ACC_ANAL_IDS--=------{0}------'.format(acc_anal_ids))
                #cr.execute("SELECT (liquid) FROM account_analytic_line WHERE account_id=%s AND (date "
                 #      "between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd')) AND "
                  #     "general_account_id=ANY(%s)", (line.analytic_account_id.id, date_from, date_to,acc_ids,))
                #result = cr.fetchone()[0]
                if acc_anal_ids:
                    liquides = []
                    for liq in acc_anal_obj.browse(cr, uid, acc_anal_ids):
                        _logger.info('-RESULT-LIQUID--=------{0}------'.format(liq.liquid))
                        result = liq.liquid
                        liquides.append(result)
                    res[line.id] = max(liquides)    
                else:
                    res[line.id] = 0 #raise osv.except_osv(_('Error!'),_("The Budget '%s' has no accounts!") % line.general_budget_id.name)
                #if result is None:
                #result = 0
                #_logger.info('-RESULT--LL-=------{0}------'.format(result))
                #res[line.id] = result
        #return res
                #_logger.info('-RESULT--LLIKID-=------{0}------'.format(liquides))
                
        return res

    def _prac_amt2(self, cr, uid, ids, name, args, context=None):
        res = {}
        result = 0
        if context is None:
            context = {}
        for line in self.browse(cr, uid, ids, context=context):
            acc_ids = [x.id for x in line.general_budget_id.account_ids]
            if not acc_ids:
                raise osv.except_osv(_('Error!'),_("The Budget '%s' has no accounts!") % line.general_budget_id.name)
            date_to = line.date_to
            date_from = line.date_from
            if line.analytic_account_id.id:
                acc_anal_obj = self.pool.get('account.analytic.line')
                acc_anal_ids = acc_anal_obj.search(cr, uid, [('account_id', '=', line.analytic_account_id.id)])
                _logger.info('-ACC_ANAL_IDS2--=------{0}------'.format(acc_anal_ids))
                if acc_anal_ids:
                    engagements = []
                    for eng in acc_anal_obj.browse(cr, uid, acc_anal_ids):
                        _logger.info('-RESULT-LIQUID2--=------{0}------'.format(eng.prac_amt2))
                        result = eng.prac_amt2
                        engagements.append(result)
                    res[line.id] = max(engagements)    
                else:
                    res[line.id] = 0                 
        return res


    '''def _get_liquid(self, cr, uid, ids, name, args, context=None):
        res={}
        for line in self.browse(cr, uid, ids, context=context):
            res[line.id] = self._prac_amt1(cr, uid, [line.id], context=context)[line.id]
        return res'''

    _columns = {
        'dispo': fields.function(_get_dispo, type='integer', string='Montant Disponible'),
        'liquid': fields.function(_prac_amt1, type='integer', string='Montant liquide'),
        'prac_amt2': fields.function(_prac_amt2, type='integer', string='Engagement2'),
    }

    _order = 'general_budget_id, analytic_account_id asc' #' general_budget_id desc'
crossovered_budget_lines()


class account_analytic_line(osv.osv):
    _inherit = 'account.analytic.line'
    _description = 'Analytic Line'

    def _get_liquid(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for obj in self.browse(cr, uid, ids):
            val = 0
            acc_inv_obj = self.pool.get('account.invoice')
            acc_inv_ids = acc_inv_obj.search(cr, uid, [('analytic_id', '=', obj.account_id.id)]) #[('analytic_id', '=', )])

            _logger.info('-ACC_IDSng=------{0}------'.format(acc_inv_ids))
            for acc_obj in acc_inv_obj.browse(cr, uid, acc_inv_ids):
                amt = acc_obj.amount_total - acc_obj.residual
                _logger.info('-----AMOUNTng---==--{0}----'.format(amt))
                val += amt
            _logger.info('-LIQUIDng=------{0}------'.format(val))
            res[obj.id] = val
        return res

    def _prac_amt2(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for obj in self.browse(cr, uid, ids):
            val = 0
            acc_inv_obj = self.pool.get('account.invoice')
            acc_inv_ids = acc_inv_obj.search(cr, uid, [('analytic_id', '=', obj.account_id.id)]) #, ('state', '=', 'open')])
            for acc_obj in acc_inv_obj.browse(cr, uid, acc_inv_ids):
                val += acc_obj.amount_total
            res[obj.id] = val
        return res

    _columns = {
        'liquid': fields.function(_get_liquid, type='integer', string='Montant liquide', store=True),
        'prac_amt2': fields.function(_prac_amt2, type='integer', string='Engagement2', store=True),

    }
account_analytic_line()
