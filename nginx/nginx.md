pstream webserver_internal {
    server 127.0.0.1:8069 weight=1 fail_timeout=300s;
}

server {
    listen 80;


    # Strict Transport Security
    add_header Strict-Transport-Security max-age=2592000;

    rewrite        ^ https://$server_name$request_uri? permanent;
    #  http://www.cyberciti.biz/faq/linux-unix-nginx-redirect-all-http-to-https/
    server_name   internal.odoo.zone;
}

server {
    # server port and name
        server_name internal.odoo.zone;
    listen        443;
    ssl on;

...

http://nginx.org/en/linux_packages.html#stable

Installing nginx on debian based OS using debs
 
1. First, save your original sources.list file. 
 sudo cp /etc/apt/sources.list /etc/apt/sources.list.orig

2.  For Debian/Ubuntu, in order to authenticate the nginx repository signature and to eliminate warnings about missing PGP key during installation of the nginx package, 
	it is necessary to add the key used to sign the nginx packages and repository to the apt program keyring. Please download this key (see file "nginx_signing.key") from our web site, 
	and add it to the apt program keyring with the following command:

    sudo apt-key add nginx_signing.key
    or do:
    sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys PGP_key


3. Enter root mode.
 sudo su

4. Add the nginx repo.
 echo "deb http://nginx.org/packages/ubuntu/ trusty nginx" >> /etc/apt/sources.list.d/nginx.sources.list
 echo "deb-src http://nginx.org/packages/ubuntu/ trusty nginx" >> /etc/apt/sources.list.d/nginx.sources.list


5. Make apt aware of the new software repositories by issuing the following command: 
 sudo apt-get update

6. Install  nginx.
       sudo apt-get install nginx


Manual settings after installed:


